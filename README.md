# OvalGrowth

## Name
OvalGrowth Next Generation

## Description
OvalGrowth is a Poultry house management system that allows the user to centrally manage data to understand the situation inside the poultry house and improve its management.
This is the upgrade version of OvalGrowth 1.0. The previous system had been developed and used for more than 10 years.

## Repo structure
This monorepo is the single source of truth that contains all the source code and configuration files needed to check, build, test, deploy and operate this application.

### Top-level directories:
- backend: Houses the Python/Django backend code, including the API and unit tests.
- frontend: Houses the React/TailwindCSS/Javascript frontend code, including the unit tests.
- mobile: For Flutter/Native React mobile app development, including code and tests.
- tests: separate test project for integration/system tests.
- deployment: Stores configuration file for deployment on different platform - On-Prem or On-Cloud.
- infrastructure: Stores your infrastructure provisioning code with Terraform/Ansible.
- docs: Holds project documentation using the chosen markup language.
- scripts: Houses scripts like database migration or data backup.
- ci_cd: Contains the GitLab CI/CD pipeline configuration files for the application.

### Dockerfiles: 
Each service (backend, frontend, mobile app) can have its own Dockerfile for building container images.

### Tests:
- Unit tests using Pytest can reside in the backend/tests directory.
- Integration/system tests using Katalon/Playwright maight be in a separate project and not directly within the monorepo.

### Documentation:
Use a markup language like Markdown for API documentation, architecture diagrams, and other project documentation.


## Installation
The application will be running as rootless Podman containers on an On-Prem server or an On-Cloud VM 

## Usage
To be updated

