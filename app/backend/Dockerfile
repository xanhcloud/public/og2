# Use an official Python runtime as a parent image
FROM python:3.12-alpine3.19

ADD ./requirements.txt /app/requirements.txt

# Install NGINX and other necessary packages
RUN apk update && \
    apk add --no-cache nginx supervisor && \
    mkdir -p /run/nginx && \
    mkdir -p /var/log/supervisor

# Install Python dependencies
RUN set -ex \
    && apk add --no-cache --virtual .build-deps postgresql-dev build-base \
    && python -m venv /env \
    && /env/bin/pip install --upgrade pip \
    && /env/bin/pip install --no-cache-dir -r /app/requirements.txt \
    && runDeps="$(scanelf --needed --nobanner --recursive /env \
        | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
        | sort -u \
        | xargs -r apk info --installed \
        | sort -u)" \
    && apk add --virtual rundeps $runDeps \
    && apk del .build-deps

# Set environment variables for Python
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE ovalgrowth_backend.settings

WORKDIR /app
ADD . /app

# Copy NGINX and Supervisor configuration files
COPY ./config/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./config/nginx/00_ovalgrowth.conf /etc/nginx/conf.d/00_ovalgrowth.conf
COPY ./config/nginx/supervisord.conf /etc/supervisord.conf

# Copy entrypoint script
COPY entrypoint.sh /app/entrypoint.sh
RUN chmod +x /app/entrypoint.sh

# Expose port
EXPOSE 80
EXPOSE 5000

# Run entrypoint script
ENTRYPOINT ["/app/entrypoint.sh"]
