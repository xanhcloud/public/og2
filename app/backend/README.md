# OvalGrowth Backend

## Getting started

```
git clone https://gitlab.com/up5tech/ovalgrowth-backend.git
cd ovalgrowth-backend
pip install -r requirements.txt
python3 -m pip install -U 'channels[daphne]'
pip install channels
pip install channels_redis
pip install django-crontab

# configuration
copy .env.example to .env
add database information to .env
add redis information to .env # REDIS_HOST=redis://username:password@127.0.0.1:6379/0

# run migrate database
python3 manage.py migrate

# create admin user
python3 manage.py createsuperuser --username admin --email admin@example.com

# run server at :5000
python3 manage.py runserver 5000
```

### run collection

```
python3 manage.py collect
```
