# Usage example:
#
#   tab = Tab(1, 'kazu', True)
#
#   print(tab.id)
#   print(tab.name)
#   print(tab.visibility)
#
#   tab.id = 2
#
#   print(tab.id)
#   print(tab.name)
#   print(tab.visibility)
#
class Tab(object):

    def __init__(self, id, name, visibility, batch, prefix):
        self.id = id
        self.name = name
        self.visibility = visibility
        self.batch = batch
        self.prefix = prefix
