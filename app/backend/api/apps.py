from django.apps import AppConfig

# from api.collect import collect_data_loop


class ApiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "api"

    def ready(self):
        import api.signals  # noqa: F401

        # collect_data_loop()
