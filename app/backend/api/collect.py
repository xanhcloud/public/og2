from collections import namedtuple
import configparser
import datetime
import math
import os
import platform
import struct
import sys
import time

from api.models import DbtJson
from api.utils.common import debugPrint, getCurrentPid, log
from api.utils.query_utils import (
    executeRawQuery,
    fetAllRawQuery,
    fetOneRawQuery,
    fetOneRawQueryNoColumns,
    getLastCollectionTime,
)

from channels.layers import get_channel_layer

#
# "Constants"
#
TRACE = 0
DEBUG = 1
INFO = 2
WARN = 3
ERROR = 4
FATAL = 5
ALARM = 6

RESULT_CODE_OK = "0"
RESULT_CODE_NG = "1"

MAX_LOGFILE_SIZE = 50000000
MIN_LOGFILE_SIZE = 10000000

DAILY_TABLE_LINES = 1000
ROOT_PATH = os.path.abspath(os.path.dirname(__file__))

#
# Globals
#
g_loginError = None

#
# Types
#
Color = namedtuple("Color", "red green blue")
Margin = namedtuple("Margin", "left right top bottom")


# Get alarm parameter
iniFilePath = ROOT_PATH + "/ovalGrowth.ini"


if not os.path.isfile(iniFilePath):
    debugPrint("'{0}' not found".format(iniFilePath))
    raise AssertionError("'{0}' not found".format(iniFilePath))
# endif

config = configparser.ConfigParser()
config.read(iniFilePath)

try:
    camera_section = config["camera"]
    alarm_section = config["alarm"]
except KeyError:
    debugPrint("Cannot parse configuration file, no section 'alarm/camera'. ")
    raise
# endtry
g_AlarmEggCount = alarm_section["eggcount"]  # ALRAM_LEVEL: egg count
g_AlarmTempUpper = alarm_section["temp_upper"]  # ALARM_LEVEL: temp upper
g_AlarmTempLower = alarm_section["temp_lower"]  # ALARM_LEVEL: temp lower
g_AlarmFood = alarm_section["food"]  # ALARM_LEVEL: food
g_AlarmWater = alarm_section["water"]  # ALARM_LEVEL: water
g_AlarmFooder = alarm_section["fooder"]  # ALARM_LEVEL: fooder
g_AlarmFooderTimer = alarm_section["fooder_timer"]

g_CameraAddress = camera_section["Ip_address"]

g_ErrLvl = {
    TRACE: "TRACE",
    DEBUG: "DEBUG",
    INFO: "INFO",
    WARN: "WARN",
    ERROR: "ERROR",
    FATAL: "FATAL",
    ALARM: "ALARM",
}
g_DataBaseName = "ovalGrowthDB"  # this is real database name

g_log_path = "./"  # dummy, real in the ini file
g_comm_dir = "./"  # dummy, real in the ini file
g_collection_dir = "./"  # dummy, real in the ini file
g_db_user_id = "dummy_user"  # dummy name, real name in the ini file
g_db_pwd = "dummy_pass"  # dummy pass, real pass in the ini file
g_data_interval = 10 * 60  # dummy (10 min in seconds), real interval in the ini file
g_loggingLvl = DEBUG  # dummy, real in the ini file

g_DebugMode = True  # in final version should be changed to 'False'
g_Region = "en"  # only loading from ini file is implemented

g_FileCheckInterval = 10  # 10 sec (for debug)
g_CurHouseAndLot = "--:--:"  # for log and alarm
g_ResultCode = RESULT_CODE_OK  # lot import result

g_LastLogCheckDate = None

g_record_format1 = "hhhhhh"  # 6 fields, total 6
g_record_format2 = "iiiiiii"  # 7 fields (last is food amount), total 13
#                   ddaaddaaddaaddaaddaaddaaddaaddaaddaa    d - departure, a - arrival
#                   111122223333444455556666777788889999    fooder no
#                   hmhmhmhmhmhmhmhmhmhmhmhmhmhmhmhmhmhm    h - hours, m - minutes
g_record_format3 = (
    "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"  # 18 fields (but 36 values), total 31
)
#                             1         2         3         4         5     55
#                   0         0         0         0         0         0     67
g_record_format4 = (
    "iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii"  # 56 fields, total 87
)
g_record_format5 = "i"  # water amount, one field, total 88
g_FileSize = 340  # will be recalculated

g_isPowerFailure = [False] * 99
g_datPrevList = None
g_timeCmd = "####-##-## --:--"


def rawToCelsius(raw_data):

    dV = 4100 / 970 - (raw_data * (3.3 / 1023)) / (100 / 110)
    dR = (6200 * dV) / (5 - dV)
    dTemp = (-(273.15 * 273.15) * math.log(dR / 6000, 2.718282)) / (
        (273.15 * math.log(dR / 6000, 2.718282)) + 3390.148
    )

    # 2014-01-03
    #    if dTemp < -19:
    #        dTemp = 0
    #        log(ERROR, "Temperature out fo range -19..45: {0:.2f}".format(dTemp))
    #    elif dTemp > 45:
    #        dTemp = 0
    #        log(ERROR, "Temperature out fo range -19..45: {0:.2f}".format(dTemp))
    #    #endif

    return round(dTemp * 10)


def getBatchId(lot_id):
    batch = fetOneRawQuery(
        "SELECT batch_id FROM dbt_batch WHERE lot_id = %s ORDER BY batch_id DESC LIMIT 1"
        % (lot_id)
    )
    if batch:
        batch_id = batch["batch_id"]
    else:
        log(ERROR, "Found 0 records (must be 1) in 'dbt_batch' for given lot")
        batch_id = 0
    # endif

    return batch_id


def dbt_eggInsert(srcList, lot_id, collect_id, created_tdate):
    log(TRACE, "Insert data into dbt_egg")

    affected_count = 0
    eggctr_no = 0
    egg_max = -1
    egg_min = 99999999
    idxList = []
    for egg_count in srcList:
        eggctr_no += 1

        # fetch megg_id for given lot and eggctr_no
        row = fetOneRawQueryNoColumns(
            "SELECT megg_id FROM dbm_egg WHERE lot_id = %s AND eggctr_no = %s"
            % (lot_id, eggctr_no)
        )
        if row:
            megg_id = row[0]
            # created_tdate  = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')

            # INSERT dbt_egg data
            add_egg = "INSERT INTO dbt_egg (collect_id, megg_id, egg_count, created_tdate) VALUES (%s, %s, %s, %s)"
            res = executeRawQuery(
                add_egg % (collect_id, megg_id, egg_count, created_tdate)
            )
            affected_count += res["count"]

            if egg_count:
                log(
                    INFO,
                    "Inserted into dbt_egg[collect_id:{0}, megg_id:{1}] += {2}".format(
                        collect_id, megg_id, egg_count
                    ),
                )
            # endif

            # for alarm
            idx = eggctr_no - 1
            idxList.append(idx)
            if egg_max < egg_count:
                egg_max = egg_count
            # endif
            if egg_min > egg_count:
                egg_min = egg_count
            # endif
        else:
            log(
                ERROR,
                "Found {0} records (must be 1) in 'dbm_egg' for given lot AND 'eggctr_no'={1}".format(
                    0, eggctr_no
                ),
            )
        # endif
    # endfor

    # check alarm
    # calculate total and average without smallest and biggest values
    n_counters_for_alarm = 0
    total_eggs_for_alarm = 0
    total_eggs = 0
    ##    log(INFO,"-------------------------------egg_count1={0}".format(srcList[0]))
    for egg_idx in idxList:
        egg_count = srcList[egg_idx]
        total_eggs += egg_count

        if egg_count == egg_min or egg_count == egg_max:
            continue
        # endif
        total_eggs_for_alarm += egg_count
        n_counters_for_alarm += 1
    # endfor

    # if there are values, except min and max, check alarm contitions
    if n_counters_for_alarm > 0:
        egg_avg = total_eggs_for_alarm / n_counters_for_alarm
        norm_avg = round(egg_avg * 10) / 10
        norm_min = norm_avg - norm_avg * (g_AlarmEggCount / 100)
        norm_min = round(norm_min * 10) / 10
        norm_max = norm_avg + norm_avg * (g_AlarmEggCount / 100)
        norm_max = round(norm_max * 10) / 10

        eggctr_no = 0
        for egg_idx in idxList:
            egg_count = srcList[egg_idx]

            # 2013-09-02: max calculation disabled
            if egg_count < norm_min:  # or norm_max < egg_count
                log(
                    ALARM,
                    "EggCount{0}={1}, Normal={2}..{3}".format(
                        egg_idx + 1, egg_count, norm_min, norm_max
                    ),
                )
            # endif
        # endfor
    # endif

    log(TRACE, "--- inserted {0} rows".format(affected_count))
    return total_eggs


def getScheduledDepartureTime(mfooder_id, now_time_str):
    # 1. select latest apply_date
    row = fetOneRawQueryNoColumns(
        "SELECT apply_date FROM dbm_fooderst WHERE mfooder_id = %s ORDER BY apply_date DESC LIMIT 1"
        % (mfooder_id)
    )
    if row:
        apply_date = row[0]
    else:
        # Cannot happen now (SELECT has LIMIT 1), but, taking into account possible modifications, report error
        log(
            ERROR,
            "Found {0} records (must be 1) in 'dbm_fooderst' for 'mfooder_id'={1}".format(
                0, mfooder_id
            ),
        )
        return None
    # endif

    # 2. select latest depature time but not greater then now
    row = fetOneRawQueryNoColumns(
        "SELECT start_time FROM dbm_fooderst WHERE mfooder_id = %s AND apply_date = %s AND start_time <= %s ORDER BY start_time DESC LIMIT 1"
        % (mfooder_id, apply_date, now_time_str)
    )
    if row:
        # for some reason mysql return time as 'datetime.timedelta'. convert it to datetime.datetime
        start_time = (
            row[0]
            if type(row[0]) == datetime.time
            else (datetime.datetime.min + row[0]).time()
        )
        return start_time
    else:
        # Cannot happen now (SELECT has LIMIT 1), but, taking into account possible modifications, report error
        log(
            ERROR,
            "Found {0} records (must be 1) in 'dbm_fooderst' for 'mfooder_id'={1} and {2} <= apply_date".format(
                0, mfooder_id, apply_date
            ),
        )
    # endif

    return None


def dbt_fooderInsert(srcList, lot_id, collect_id, created_tdate):
    log(TRACE, "Insert data into dbt_fooder")

    affected_count = 0
    fooder_no = 0
    for departure_time, arrival_time in srcList:
        fooder_no += 1

        # fetch mfooder_id for given lot and fooder_no
        row = fetOneRawQueryNoColumns(
            "SELECT mfooder_id FROM dbm_fooder WHERE lot_id = %s AND fooder_no = %s"
            % (lot_id, fooder_no)
        )
        if row:
            mfooder_id = row[0]
            # created_tdate  = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')

            # INSERT dbt_fooder data
            add_fooder = "INSERT INTO dbt_fooder (collect_id, mfooder_id, departure_time, arrival_time, created_tdate) VALUES (%s, %s, %s, %s, %s)"
            data_fooder = (
                collect_id,
                mfooder_id,
                departure_time,
                arrival_time,
                created_tdate,
            )
            res = executeRawQuery(add_fooder, data_fooder)
            affected_count += res["count"]
        else:
            log(
                ERROR,
                "Found {0} records (must be 1) in 'dbm_fooder' for given lot AND 'fooder_no'={1}".format(
                    0, fooder_no
                ),
            )
        # endif
    # endfor

    # check alarm
    fooder_no = 0
    for departure_time, arrival_time in srcList:
        fooder_no += 1

        # dummyDate = datetime.date.today()
        # now_time = datetime.datetime.now().time()
        dummyDate = datetime.datetime.strptime(
            created_tdate, "%Y-%m-%d %H:%M:%S"
        ).date()
        now_time = datetime.datetime.strptime(created_tdate, "%Y-%m-%d %H:%M:%S").time()
        now_time_str = now_time.strftime("%H:%M:%S")
        departure_time_scheduled = getScheduledDepartureTime(mfooder_id, now_time_str)

        d_sched_ts = None
        if departure_time_scheduled:
            d_sched_dt = datetime.datetime.combine(dummyDate, departure_time_scheduled)
            d_sched_ts = time.mktime(d_sched_dt.timetuple())
        # endif

        # 1. check depature delay
        if departure_time:
            # we have valid depature time, check departure delay
            d_dt = datetime.datetime.combine(dummyDate, departure_time)
            d_ts = time.mktime(d_dt.timetuple())

            if d_sched_ts:
                # we still have scheduled departure, check delay
                departureDelayTime = (d_ts - d_sched_ts) / 60
                if g_AlarmFooderTimer < departureDelayTime:
                    log(
                        ALARM,
                        "Fooder={0}, Departure Time={1}, Delay={2} minutes, Max Delay={3} minutes".format(
                            fooder_no,
                            departure_time,
                            round(departureDelayTime),
                            round(g_AlarmFooderTimer),
                        ),
                    )
                # endif
            # endif

            # if we have arrival_time, check round trip time
            if arrival_time:
                # we have arrival_time, check round trip time
                a_dt = datetime.datetime.combine(dummyDate, arrival_time)
                a_ts = time.mktime(a_dt.timetuple())

                # if arrival_time later then departure_time then fooder already arrived
                if departure_time < arrival_time:
                    # fooder arrived, check round trip time
                    roundTripTime = (a_ts - d_ts) / 60
                    if g_AlarmFooder < roundTripTime:
                        log(
                            ALARM,
                            "Fooder={0}, Round Trip Time={1} minutes, Max Time={2} minutes".format(
                                fooder_no, round(roundTripTime), round(g_AlarmFooder)
                            ),
                        )
                    # endif
                else:
                    # arrival_time still from previous measurement, check how long arrival delayed
                    checkArrivalDelay(dummyDate, now_time, d_ts, fooder_no)
                # endif
            else:
                # we don't have arrival_time, check arrival delayed
                checkArrivalDelay(dummyDate, now_time, d_ts, fooder_no)
            # endif
        else:
            # we don't have valid departure time
            if d_sched_ts:
                # but we have scheduled departure, check delay
                now_dt = datetime.datetime.combine(dummyDate, now_time)
                now_ts = time.mktime(now_dt.timetuple())

                departureDelayTime = (now_ts - d_sched_ts) / 60
                if g_AlarmFooderTimer < departureDelayTime:
                    log(
                        ALARM,
                        "Fooder={0}, Departure Time=No, Delay={1} minutes, Max Delay={2} minutes".format(
                            fooder_no,
                            round(departureDelayTime),
                            round(g_AlarmFooderTimer),
                        ),
                    )
                # endif
            # endif
        # endif
    # endfor

    log(TRACE, "--- inserted {0} rows".format(affected_count))


def checkArrivalDelay(dummyDate, now_time, departure_ts, fooder_no):
    global g_AlarmFooder

    arrivalMax_ts = departure_ts + g_AlarmFooder * 60  # latest arrival time, seconds
    # fooder not yet arrived
    # now_time = datetime.datetime.now().time()
    now_dt = datetime.datetime.combine(dummyDate, now_time)
    now_ts = time.mktime(now_dt.timetuple())

    # check, if current time is later then latest arrival time
    if arrivalMax_ts < now_ts:
        waitingTime = (now_ts - departure_ts) / 60
        log(
            ALARM,
            "Fooder={0}, Time From Depature={1} minutes, Max Time={2} minutes".format(
                fooder_no, round(waitingTime), round(g_AlarmFooder)
            ),
        )
    # endif


def dbt_thermoInsert(srcList, lot_id, collect_id, created_tdate):
    log(TRACE, "Insert data into dbt_thermo")

    affected_count = 0
    # first insert temperature1
    # 2013.07.11: added RAW A/D data type
    # mthermo_type = 1 - Celsius temperature (this data need to be inserted into DB)
    # mthermo_type = 2 - RAW data from A/D converter (this data are in collectionXX.dat file)
    mthermo_no = 1
    # 2013-09-02: get mthermo_type from dbm_thermo
    #    mthermo_type = 1
    #    cursor.execute("SELECT mthermo_id FROM dbm_thermo WHERE mthermo_type = %s AND mthermo_no = %s AND lot_id = %s", (mthermo_type, mthermo_no, lot_id,))
    row = fetOneRawQueryNoColumns(
        "SELECT mthermo_id,mthermo_type FROM dbm_thermo WHERE mthermo_no = %s AND lot_id = %s"
        % (mthermo_no, lot_id)
    )
    if row:
        mthermo_id = row[0]
        mthermo_type = row[1]
        # created_tdate  = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')

        # INSERT into dbt_thermo insight temperature
        # 2013-09-02: convert RAW to Celsius only if mthermo_type == 1
        cur_val1 = srcList[0]
        high_val1 = srcList[1]
        low_val1 = srcList[2]
        if mthermo_type == 1:
            cur_val1 = rawToCelsius(srcList[0])
            high_val1 = rawToCelsius(srcList[1])
            low_val1 = rawToCelsius(srcList[2])
        # endif

        add_thermo = "INSERT INTO dbt_thermo (collect_id, mthermo_id, cur_val, high_val, low_val, created_tdate) VALUES (%s, %s, %s, %s, %s, %s)"
        res = executeRawQuery(
            add_thermo
            % (
                collect_id,
                mthermo_id,
                cur_val1,
                high_val1,
                low_val1,
                created_tdate,
            )
        )
        affected_count += res["count"]

        # check alarm
        if cur_val1 < g_AlarmTempLower * 10:
            log(
                ALARM,
                "Temperature={0}, Min={1}".format(cur_val1 / 10, g_AlarmTempLower),
            )
        # endif
        if g_AlarmTempUpper * 10 < cur_val1:
            log(
                ALARM,
                "Temperature={0}, Max={1}".format(cur_val1 / 10, g_AlarmTempUpper),
            )
        # endif
    else:
        log(
            ERROR,
            "Found {0} records (must be 1) in 'dbm_thermo' for given lot AND mthermo_type={1} and 'mthermo_no'={2}".format(
                0, mthermo_type, mthermo_no
            ),
        )
    # endif

    # and then temperature2
    # 2013.07.11: added RAW A/D data type
    # mthermo_type = 1 - Celsius temperature (this data need to bi inserted to DB)
    # mthermo_type = 2 - RAW data from A/D converter (this data is in collectionXX.dat file)
    mthermo_no = 2
    # 2013-09-02: get mthermo_type from dbm_thermo
    #    mthermo_type = 1
    #    cursor.execute("SELECT mthermo_id FROM dbm_thermo WHERE mthermo_type = %s AND mthermo_no = %s AND lot_id = %s", (mthermo_type, mthermo_no, lot_id,))
    row = fetOneRawQueryNoColumns(
        "SELECT mthermo_id,mthermo_type FROM dbm_thermo WHERE mthermo_no = %s AND lot_id = %s"
        % (
            mthermo_no,
            lot_id,
        )
    )
    if row:
        mthermo_id = row[0]
        mthermo_type = row[1]

        # INSERT into dbt_thermo outside
        # 2013-09-02: convert RAW to Celsius only if mthermo_type == 1
        cur_val2 = srcList[3]
        high_val2 = srcList[4]
        low_val2 = srcList[5]
        if mthermo_type == 1:
            cur_val2 = rawToCelsius(srcList[3])
            high_val2 = rawToCelsius(srcList[4])
            low_val2 = rawToCelsius(srcList[5])
        # endif

        add_thermo = "INSERT INTO dbt_thermo (collect_id, mthermo_id, cur_val, high_val, low_val, created_tdate) VALUES (%s, %s, %s, %s, %s, %s)"
        res = executeRawQuery(
            add_thermo
            % (
                collect_id,
                mthermo_id,
                cur_val2,
                high_val2,
                low_val2,
                created_tdate,
            )
        )
        affected_count += res["count"]
    else:
        log(
            ERROR,
            "Found {0} records (must be 1) in 'dbm_thermo' for given lot AND mthermo_type={1} and 'mthermo_no'={2}".format(
                0, mthermo_type, mthermo_no
            ),
        )
    # endif

    log(TRACE, "--- inserted {0} rows".format(affected_count))


def getPrevFoodWaterAmount(mfw_id, now_time_str):
    row = fetOneRawQueryNoColumns(
        """SELECT dbt_fw.food_amount,dbt_fw.water_amount FROM dbt_fw
       JOIN dbt_collection ON dbt_fw.collect_id = dbt_collection.collect_id
       WHERE mfw_id = %s AND dbt_collection.col_tdate > DATE_SUB(%s, INTERVAL 1 DAY)
       ORDER BY tfw_id ASC LIMIT 1"""
        % (mfw_id, now_time_str)
    )
    if row is None:
        food_amount = 0
        water_amount = 0
    else:
        food_amount = row[0]
        water_amount = row[1]
    # endif
    return (food_amount, water_amount)


def dbt_fwInsert(lot_id, collect_id, food_amount, water_amount, created_tdate):
    log(TRACE, "Insert data into dbm_fw")

    affected_count = 0

    row = fetOneRawQueryNoColumns(
        "SELECT mfw_id FROM dbm_fw WHERE lot_id = %s" % (lot_id)
    )
    if row:
        mfw_id = row[0]
        # created_tdate  = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')

        # INSERT dbt_fw data
        add_fw = "INSERT INTO dbt_fw (collect_id, mfw_id, food_amount, water_amount, created_tdate) VALUES (%s, %s, %s, %s, %s)"
        res = executeRawQuery(
            add_fw % (collect_id, mfw_id, food_amount, water_amount, created_tdate)
        )
        affected_count += res["count"]

        if water_amount:
            log(
                INFO,
                "Inserted into dbt_fw[collect_id:{0}, mfw_id:{1}] += {2}".format(
                    collect_id, mfw_id, water_amount
                ),
            )
        # endif
    else:
        log(ERROR, "Found {0} records (must be 1) in 'dbt_fw' for given lot".format(0))
    # endif

    # check alarm
    now_time = datetime.datetime.strptime(created_tdate, "%Y-%m-%d %H:%M:%S").time()
    now_time_str = now_time.strftime("%H:%M:%S")
    prev_food_amount, prev_water_amount = getPrevFoodWaterAmount(mfw_id, now_time_str)
    # 2013-09-02: added prev_water_amount check
    if prev_water_amount:
        water_min = prev_water_amount - prev_water_amount * (g_AlarmWater / 100)
        water_max = prev_water_amount + prev_water_amount * (g_AlarmWater / 100)
        if water_amount < water_min or water_max < water_amount:
            log(
                ALARM,
                "Water Amount={0}, Normal={1}..{2}".format(
                    water_amount, water_min, water_max
                ),
            )
        # endif
    # endif
    # 2013-09-02: added prev_food_amount check
    if prev_food_amount:
        food_min = prev_food_amount - prev_food_amount * (g_AlarmFood / 100)
        food_max = prev_food_amount + prev_food_amount * (g_AlarmFood / 100)
        if food_amount < food_min or food_max < food_amount:
            log(
                ALARM,
                "Food Amount={0}, Normal={1}..{2}".format(
                    food_amount, food_min, food_max
                ),
            )
        # endif
    # endif

    log(TRACE, "--- inserted {0} rows".format(affected_count))


def dbt_summaryInsert(
    batch_id,
    day_periods,
    week_periods,
    collect_id,
    egg_count,
    food_amount,
    water_amount,
    curr_count,
    dead_count,
    thermoList,
    created_tdate,
    firstRecord=False,
):

    debugPrint("Insert new record into dbt_summary")

    affected_count = 0
    # summary_date  = datetime.date.today()
    summary_date = created_tdate
    # created_tdate = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')

    # INSERT dbt_summary data
    # 2013-07-17 added Room and Outer Temperature
    #    add_sm = "INSERT INTO dbt_summary (batch_id, summary_date, collect_id, egg_count, food_amount, water_amount, curr_count, dead_count, created_tdate) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
    #    data_sm = (batch_id, summary_date, collect_id, egg_count, food_amount, water_amount, curr_count, dead_count, created_tdate)
    cur_val1 = rawToCelsius(thermoList[0])
    high_val1 = rawToCelsius(thermoList[1])
    low_val1 = rawToCelsius(thermoList[2])
    cur_val2 = rawToCelsius(thermoList[3])
    high_val2 = rawToCelsius(thermoList[4])
    low_val2 = rawToCelsius(thermoList[5])
    if firstRecord:
        # 2013-07-22 added week_periods
        # 2013-09-23 added day_periods
        debugPrint("***")
        add_sm = """INSERT INTO dbt_summary (
            batch_id, summary_date, day_periods, week_periods, collect_id, egg_count, food_amount, water_amount, curr_count, dead_count, created_tdate,
            room_temp, room_temp_hi, room_temp_lo, outer_temp, outer_temp_hi, outer_temp_lo)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,   %s, %s, %s, %s, %s, %s)"""
        # 2013-07-22 added week_periods
        # 2013-09-23 added day_periods
        data_sm = (
            batch_id,
            summary_date,
            day_periods,
            week_periods,
            collect_id,
            egg_count,
            food_amount,
            water_amount,
            curr_count,
            dead_count,
            created_tdate,
            cur_val1,
            high_val1,
            low_val1,
            cur_val2,
            high_val2,
            low_val2,
        )
    else:
        debugPrint("***")
        # 2013-08-23 copy some values from previous record
        add_sm = """
            INSERT INTO dbt_summary (
              batch_id,    summary_date, day_periods, week_periods, collect_id, egg_count,
              food_amount, water_amount, curr_count,   dead_count, created_tdate,
              room_temp,   room_temp_hi, room_temp_lo, outer_temp, outer_temp_hi, outer_temp_lo,
              hen_weight,        egg_weight,  defection,     coeff,        daylight_hours,
              preset_temp,       humidity,    humidity_upl,  humidity_lol, min_ventilation,
              ventilation_setup, ventilation, food_interval)
            SELECT
              %s, %s, %s, %s, %s, %s,   %s, %s, %s, %s, %s,   %s, %s, %s, %s, %s,   %s,
              hen_weight,        egg_weight,  defection,     coeff,        daylight_hours,
              preset_temp,       humidity,    humidity_upl,  humidity_lol, min_ventilation,
              ventilation_setup, ventilation, food_interval
            FROM dbt_summary
            WHERE batch_id = %s AND summary_date < CURRENT_DATE  ORDER BY summary_id DESC  LIMIT 1
            """
        data_sm = (
            batch_id,
            summary_date,
            day_periods,
            week_periods,
            collect_id,
            egg_count,
            food_amount,
            water_amount,
            curr_count,
            dead_count,
            created_tdate,
            cur_val1,
            high_val1,
            low_val1,
            cur_val2,
            high_val2,
            low_val2,
            batch_id,
        )
    # endif
    try:
        executeRawQuery(add_sm % data_sm)
    except Exception as e:
        ##        log(ERROR,  "cursor.statemente {0} ".format(cursor.statement))
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        log(
            ERROR,
            "Exception: {0}  {1} {2}:{3}".format(
                str(e), exc_type, fname, exc_tb.tb_lineno
            ),
        )
    # endtry
    debugPrint("*** inserted")


def dbt_summaryUpdate(
    batch_id,
    collect_id,
    egg_count,
    food_amount,
    water_amount,
    thermoList,
    created_tdate,
):
    debugPrint("Update dbt_summary")
    created_tdate = datetime.datetime.strptime(
        created_tdate, "%Y-%m-%d %H:%M:%S"
    ).date()

    # get record for given batch
    quiery = "SELECT summary_id,summary_date,curr_count,dead_count FROM dbt_summary WHERE batch_id = {0} ORDER BY summary_id DESC LIMIT 1".format(
        batch_id
    )
    row = fetOneRawQueryNoColumns(quiery)

    if row:
        # 1. found summary record for this batch
        insert_flag = False

        summary_id = row[0]
        summary_date = row[1]
        curr_count = row[2]
        dead_count = (
            row[3] if row[3] else 0
        )  # 'dead_count' was added during development, handle invalid value
        if summary_date:  # check that summary_date is valid
            # 1.1. summary_date for summary record is valid
            dayDiff = created_tdate - summary_date
            if dayDiff.days == 1:
                # 1.1.1. last record in DB is yesterday, need to insert new record
                insert_flag = True
            elif dayDiff.days > 1:
                # 1.1.2. last record in DB is older than yesterday, report error, but insert new record
                log(
                    WARN,
                    "Last 'summary_date' '{0}' in 'dbt_summary' for 'batch_id' = {1} is older than yesterday".format(
                        summary_date, batch_id
                    ),
                )
                insert_flag = True
            elif dayDiff.days < 0:
                # 1.1.3. last record in DB is in the future, report error
                log(
                    ERROR,
                    "Last 'summary_date' '{0}' in 'dbt_summary' for 'batch_id' = {1} is in the future".format(
                        summary_date, batch_id
                    ),
                )
                return
            # endif

            # update current summary record or insert new one
            if insert_flag:
                # 1.1.a. need to insert new record

                # 2013-07-22 added week_periods
                # 2013-09-23 added day_periods
                quiery = "SELECT start_date,initial_count,feeding_date FROM dbt_batch WHERE batch_id = {0} AND start_date <= CURRENT_DATE ORDER BY start_date DESC LIMIT 1".format(
                    batch_id
                )
                row = fetOneRawQueryNoColumns(quiery)

                if row:
                    # 2.1. batch record found, check batch start_date
                    # today = datetime.date.today()
                    start_date = row[0]
                    initial_count = row[1]
                    feeding_date = row[2]
                    dayDiff = created_tdate - start_date
                    # 2013-07-22 added week_periods
                    week_periods = (
                        0
                        if not feeding_date
                        else round((created_tdate - feeding_date).days / 7)
                    )
                    # 2013-09-23 added day_periods
                    day_periods = (
                        0
                        if not feeding_date
                        else round((created_tdate - feeding_date).days)
                    )
                    if dayDiff.days < 0:
                        # 2.1.1. last record in DB is in the future, report error
                        log(
                            ERROR,
                            "'start_date' for 'dbt_batch'.'batch_id' = {0} is in the future: '{1}'".format(
                                batch_id, start_date
                            ),
                        )
                    else:
                        # 2.1.2. batch start date is created_tdate or erlier, insert new summary record
                        # 2013-07-22 added week_periods
                        # 2013-09-23 added day_periods
                        # 2014-03-13 new_curr_count = initial_count - dead_count (before: new_curr_count = curr_count - dead_count)
                        ##                        new_curr_count = curr_count - dead_count
                        new_curr_count = initial_count - dead_count
                        dbt_summaryInsert(
                            batch_id,
                            day_periods,
                            week_periods,
                            collect_id,
                            egg_count,
                            food_amount,
                            water_amount,
                            new_curr_count,
                            dead_count,
                            thermoList,
                            created_tdate,
                            firstRecord=False,
                        )
                    # endif
                else:
                    # 2.2. batch record for this batch_id doesn't exist or it has start_date in the future
                    log(
                        ERROR,
                        "Cannot find record in 'dbt_batch' for batch_id = '{0}' AND start_date <= curdate()".format(
                            batch_id
                        ),
                    )
                # endif
            else:
                # 1.1.b. update created_tdate's record
                # 2013-07-17 added Room and Outer Temperature
                ##                add_sm = "UPDATE dbt_summary SET collect_id = %s, egg_count = %s, food_amount = %s, water_amount = %s WHERE summary_id = %s"
                ##                data_sm = (collect_id, egg_count, food_amount, water_amount, summary_id)
                cur_val1 = rawToCelsius(thermoList[0])
                high_val1 = rawToCelsius(thermoList[1])
                low_val1 = rawToCelsius(thermoList[2])
                cur_val2 = rawToCelsius(thermoList[3])
                high_val2 = rawToCelsius(thermoList[4])
                low_val2 = rawToCelsius(thermoList[5])
                add_sm = """UPDATE dbt_summary SET collect_id = %s, egg_count = %s, food_amount = %s, water_amount = %s,
                            room_temp = %s, room_temp_hi = %s, room_temp_lo = %s, outer_temp = %s, outer_temp_hi = %s, outer_temp_lo = %s
                            WHERE summary_id = %s"""
                data_sm = (
                    collect_id,
                    egg_count,
                    food_amount,
                    water_amount,
                    cur_val1,
                    high_val1,
                    low_val1,
                    cur_val2,
                    high_val2,
                    low_val2,
                    summary_id,
                )
                executeRawQuery(add_sm % data_sm)
            # endif
        else:
            # 1.2. summary_date for summary record is NULL, report error
            log(
                ERROR,
                "'summary_date' for 'dbt_summary'.'batch_id' = {0} is NULL!".format(
                    batch_id
                ),
            )
        # endif
    else:
        # 2. summary for this batch doesn't exist, create first record
        # first get initial data from batch record
        # 2013-07-22 added week_periods
        # 2013-09-23 added day_periods
        quiery = "SELECT start_date,initial_count,feeding_date FROM dbt_batch WHERE batch_id = {0} AND start_date <= curdate() ORDER BY start_date DESC LIMIT 1".format(
            batch_id
        )
        row = fetOneRawQueryNoColumns(quiery)

        if row:
            # 2.1. batch record found, check batch start_date
            # today = datetime.date.today()
            start_date = row[0]
            initial_count = row[1]
            feeding_date = row[2]
            dayDiff = created_tdate - start_date
            # 2013-07-22 added week_periods
            week_periods = (
                0
                if not feeding_date
                else round((created_tdate - feeding_date).days / 7)
            )
            # 2013-09-23 added day_periods
            day_periods = (
                0 if not feeding_date else round((created_tdate - feeding_date).days)
            )
            if dayDiff.days < 0:
                # 2.1.1. last record in DB is in the future, report error
                log(
                    ERROR,
                    "'start_date' for 'dbt_batch'.'batch_id' = {0} is in the future: '{1}'".format(
                        batch_id, start_date
                    ),
                )
            else:
                # 2.1.2. batch start date is created_tdate or erlier, insert new summary record
                # 2013-07-22 added week_periods
                # 2013-09-23 added day_periods
                dbt_summaryInsert(
                    batch_id,
                    day_periods,
                    week_periods,
                    collect_id,
                    egg_count,
                    food_amount,
                    water_amount,
                    initial_count,
                    0,
                    thermoList,
                    created_tdate,
                    firstRecord=True,
                )

            # endif
        else:
            # 2.3. Cannot happen (SELECT has LIMIT 1), but, taking into account possible modifications, report error
            log(
                ERROR,
                "Found {0} records (must be 1) in 'dbt_batch' where 'batch_id'={1}".format(
                    0, batch_id
                ),
            )
        # endif


def getEggList(data):
    list = []
    for key, value in data.items():
        if key.startswith("EC_"):
            # list[key[3:]] = value
            list.append(value)
    return list


def getFooderDeparture(data):
    list = ()
    depTime = {}
    arrTime = {}
    for key, value in data.items():
        if key.startswith("FE_"):
            depTime[key[3:]] = value
        elif key.startswith("FS_"):
            arrTime[key[3:]] = value
    for key, value in depTime.items():
        list += (value, arrTime[key])
    return list


def getTemperature(data):
    list = []
    for key, value in data.items():
        if key.startswith("TEMP_"):
            list.append(value)
    return list


def getWaterList(data):
    list = []
    for key, value in data.items():
        if key.startswith("W_"):
            list.append(value)
    return list


def collect_data_loop():
    global g_data_interval
    global g_CurHouseAndLot
    global g_ResultCode
    global g_record_format1
    global g_record_format2
    global g_record_format3
    global g_record_format4
    global g_record_format5
    global g_FileSize
    global g_datPrevList
    global g_timeCmd

    # init some global variables
    record_size1 = struct.calcsize(g_record_format1)
    record_size2 = struct.calcsize(g_record_format2)
    record_size3 = struct.calcsize(g_record_format3)
    record_size4 = struct.calcsize(g_record_format4)
    record_size5 = struct.calcsize(g_record_format5)
    g_FileSize = (
        record_size1 + record_size2 + record_size3 + record_size4 + record_size5
    )

    # get number of lot
    lotCountArr = fetOneRawQueryNoColumns("SELECT COUNT(*) FROM dbm_lot")
    lotCount = lotCountArr[0]

    # 2013-09-23 init foodInput
    dbm_lots = fetAllRawQuery("SELECT LOT_ID,FOOD_INPUT FROM dbm_lot")
    foodInput = {}
    for lot_id, food_input in dbm_lots:
        foodInput[lot_id] = food_input
    # endfor

    # 2014-01-03 init previous data buffer for every lot
    g_datPrevList = [None] * (99)

    ##############################
    # DEBUG
    if platform.node() == "Mixa-PC":
        lotCount = 1
    # endif
    ##############################

    # if last record in <dbt_collection> older then <g_data_interval> do recovery
    # else set commandIssueTime = dbt_collection.CREATED_TDATE + g_data_interval
    recoveryMode = False
    startup_gap = (
        1  # 1 sec gap to be sure that commandIssueTime-time.time() will be positive
    )
    collectionTime = getLastCollectionTime()
    if collectionTime == None:
        collectionTime = time.time() + startup_gap
        commandIssueTime = collectionTime
    else:
        commandIssueTime = collectionTime + g_data_interval
        if commandIssueTime + startup_gap < time.time():
            recoveryMode = True
        # endif
    # endif

    debugPrint(
        "--- Recovery:{}  commandIssueTime:{}".format(
            recoveryMode,
            time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(commandIssueTime)),
        )
    )

    dataTimeout = 60  # 60 sec

    collection = DbtJson.objects.filter(used=False).first()
    if collection:
        # 1. read data
        data = collection.data
        lot_id = data["LOT_ID"]

        # 2.2 get temperature1 and temperature2
        thermoList = getTemperature(data)
        food_amount = thermoList[6]

        # 2.3 get fooder departure/arrival times
        fooderDepArrTime = getFooderDeparture(data)

        # --------------------------------------------------------------
        # 2.4 get egg counters
        eggList = getEggList(data)
        # 2013-09-23 if foodInput for given lot_id is 1, get food_amount from eggList[54]
        if lot_id in foodInput.keys():
            if foodInput[lot_id] == 1:
                food_amount = eggList[54]

        # 2.5 get water amount
        waterList = getWaterList(data)
        water_amount = waterList[0]

        # --------------------------------------------------------------
        # 3.1 dbt_collection
        # insert new data into dbt_collection
        batch_id = getBatchId(lot_id)
        if batch_id == 0:
            debugPrint("ERROR: getBatchId(lot_id=%s) == 0" % (lot_id))
            return 0

        created_tdate = datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")
        if recoveryMode:
            created_tdate = time.strftime(
                "%Y-%m-%d %H:%M:%S", time.localtime(collectionTime)
            )
        # endif

        affected_count = 0
        r = executeRawQuery(
            "INSERT INTO dbt_collection (batch_id, col_tdate, created_tdate) VALUES (%s, '%s', '%s')"
            % (batch_id, created_tdate, created_tdate)
        )
        collect_id = r["last_id"]
        affected_count += r["count"]

        # --------------------------------------------------------------
        # 3.2 dbt_egg
        # insert new data into dbt_egg
        total_eggs = dbt_eggInsert(eggList, lot_id, collect_id, created_tdate)

        # --------------------------------------------------------------
        # 3.3 dbt_fooder
        # insert new data into dbt_fooder
        dbt_fooderInsert(fooderDepArrTime, lot_id, collect_id, created_tdate)

        # --------------------------------------------------------------
        # 3.4 dbt_thermo
        # insert new data into dbt_thermo
        dbt_thermoInsert(thermoList, lot_id, collect_id, created_tdate)

        # --------------------------------------------------------------
        # 3.5 dbt_fw
        # insert new data into dbt_fw
        # 2013-11-20 Change food_amount of ovalBox
        row = fetOneRawQueryNoColumns(
            "SELECT food_amount FROM dbm_lot WHERE lot_id = %s", (lot_id,)
        )
        equip_type = row[0]
        if equip_type == 0:
            food_amount = food_amount * 10
        # endif

        dbt_fwInsert(lot_id, collect_id, food_amount, water_amount, created_tdate)

        # --------------------------------------------------------------
        # 3.6 dbt_summary
        # insert data in the dbt_summary
        # 2013-07-17 added Room and Outer Temperature
        # dbt_summaryUpdate(cursor, batch_id, collect_id, total_eggs, food_amount, water_amount)
        dbt_summaryUpdate(
            batch_id,
            collect_id,
            total_eggs,
            food_amount,
            water_amount,
            thermoList,
            created_tdate,
        )

        # update used flag
        affected_count += DbtJson.objects.filter(id=collection.id).update(used=True)
        if affected_count > 0:
            trigger_change_data()
        # endif
    # endif
    return 1


async def trigger_change_data():
    debugPrint("trigger_change_data")
    channel_layer = get_channel_layer()
    await channel_layer.group_send(
        "database_activity",
        {
            "type": "database.changed",
            "message": {
                "type": "post_save",
                "data": {
                    "model": "Dashboard",
                    "id": 0,
                },
            },
        },
    )
