from collections import namedtuple
import configparser
import datetime
import glob
import json
import os
import struct
import threading
import time
from api.utils.common import getCurrentPid, log
from api.utils.debugPrint import debugPrint
from api.utils.query_utils import (
    fetAllRawQuery,
    fetOneRawQuery,
    fetOneRawQueryNoColumns,
)

#
# "Constants"
#
TRACE = 0
DEBUG = 1
INFO = 2
WARN = 3
ERROR = 4
FATAL = 5
ALARM = 6

RESULT_CODE_OK = "0"
RESULT_CODE_NG = "1"

MAX_LOGFILE_SIZE = 50000000
MIN_LOGFILE_SIZE = 10000000

DAILY_TABLE_LINES = 1000
ROOT_PATH = os.path.abspath(os.path.dirname(__file__))

#
# Globals
#
g_loginError = None

#
# Types
#
Color = namedtuple("Color", "red green blue")
Margin = namedtuple("Margin", "left right top bottom")


# Get alarm parameter
iniFilePath = ROOT_PATH + "/ovalGrowth.ini"


if not os.path.isfile(iniFilePath):
    debugPrint("'{0}' not found".format(iniFilePath))
    raise AssertionError("'{0}' not found".format(iniFilePath))
# endif

config = configparser.ConfigParser()
config.read(iniFilePath)

try:
    camera_section = config["camera"]
    alarm_section = config["alarm"]
except KeyError:
    debugPrint("Cannot parse configuration file, no section 'alarm/camera'. ")
    raise
# endtry
g_AlarmEggCount = alarm_section["eggcount"]  # ALRAM_LEVEL: egg count
g_AlarmTempUpper = alarm_section["temp_upper"]  # ALARM_LEVEL: temp upper
g_AlarmTempLower = alarm_section["temp_lower"]  # ALARM_LEVEL: temp lower
g_AlarmFood = alarm_section["food"]  # ALARM_LEVEL: food
g_AlarmWater = alarm_section["water"]  # ALARM_LEVEL: water
g_AlarmFooder = alarm_section["fooder"]  # ALARM_LEVEL: fooder
g_AlarmFooderTimer = alarm_section["fooder_timer"]

g_CameraAddress = camera_section["Ip_address"]

g_ErrLvl = {
    TRACE: "TRACE",
    DEBUG: "DEBUG",
    INFO: "INFO",
    WARN: "WARN",
    ERROR: "ERROR",
    FATAL: "FATAL",
    ALARM: "ALARM",
}
g_DataBaseName = "ovalGrowthDB"  # this is real database name

g_log_path = "./"  # dummy, real in the ini file
g_comm_dir = "./"  # dummy, real in the ini file
g_collection_dir = "./"  # dummy, real in the ini file
g_db_user_id = "dummy_user"  # dummy name, real name in the ini file
g_db_pwd = "dummy_pass"  # dummy pass, real pass in the ini file
g_data_interval = 10 * 60  # dummy (10 min in seconds), real interval in the ini file
g_loggingLvl = DEBUG  # dummy, real in the ini file

g_DebugMode = True  # in final version should be changed to 'False'
g_Region = "en"  # only loading from ini file is implemented

g_FileCheckInterval = 10  # 10 sec (for debug)
g_CurHouseAndLot = "--:--:"  # for log and alarm
g_ResultCode = RESULT_CODE_OK  # lot import result

g_LastLogCheckDate = None

g_record_format1 = "hhhhhh"  # 6 fields, total 6
g_record_format2 = "iiiiiii"  # 7 fields (last is food amount), total 13
#                   ddaaddaaddaaddaaddaaddaaddaaddaaddaa    d - departure, a - arrival
#                   111122223333444455556666777788889999    fooder no
#                   hmhmhmhmhmhmhmhmhmhmhmhmhmhmhmhmhmhm    h - hours, m - minutes
g_record_format3 = (
    "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"  # 18 fields (but 36 values), total 31
)
#                             1         2         3         4         5     55
#                   0         0         0         0         0         0     67
g_record_format4 = (
    "iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii"  # 56 fields, total 87
)
g_record_format5 = "i"  # water amount, one field, total 88
g_FileSize = 340  # will be recalculated

g_isPowerFailure = [False] * 99
g_datPrevList = None
g_timeCmd = "####-##-## --:--"
##########################################################


def getLastCollectionTime():
    row = fetOneRawQueryNoColumns(
        "SELECT col_tdate FROM dbt_collection ORDER BY COLLECT_ID DESC LIMIT 1"
    )
    if row == None:
        return None
    # endif
    lastCollectionTime = row[0]

    return time.mktime(lastCollectionTime.timetuple())


def createCommandFile(cmd1):
    global g_comm_dir

    if g_comm_dir[-1] == "/":
        cmdPath = g_comm_dir + "command.info"
    else:
        cmdPath = g_comm_dir + "/command.info"
    # endif

    fp = open(cmdPath, "w")
    fp.write(cmd1 + "\n")
    fp.close()
    log(
        INFO,
        ">>> {0}: 'command.info': '{1}'".format(
            datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), cmd1
        ),
    )


def countDataFiles():
    if g_collection_dir[-1] == "/":
        collectionPath = g_collection_dir + "data??.json"
    else:
        collectionPath = g_collection_dir + "/data??.json"
    # endif

    fileCount = len(glob.glob(collectionPath))

    return fileCount


def getLotName(datFile):
    lot_name = datFile[-7:-5]
    return lot_name


def getLotId(datFile):
    lot_name = getLotName(datFile)
    debugPrint("lot_name:{0}".format(lot_name))

    lot = fetOneRawQuery("SELECT lot_id FROM dbm_lot WHERE lot_name = %s" % (lot_name))
    if lot:
        return lot["lot_id"]
    log(ERROR, "Cannot find 'dbm_lot'.'lot_id' for lot_name = '{0}'".format(lot_name))
    return 0


def deleteIfExists(file):
    try:
        os.remove(file)
    except OSError:
        pass


def PF_saveFile(datFile, data):
    with open(datFile, "w", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


def PF_createAlways(datFile, nowTStamp):
    data = {
        "DATE": nowTStamp.strftime("%Y-%m-%d"),
        "TIME": nowTStamp.strftime("%H:%M:%S"),
    }

    PF_saveFile(datFile, data)

    return data


def PF_createIfNotExist(datFile, nowTStamp):
    global g_FileSize
    if not os.path.exists(datFile):
        return PF_createAlways(datFile, nowTStamp)
    else:
        fp = open(datFile, "rb")
        data = bytearray(fp.read(g_FileSize))
        fp.close()

        return data


def PF_dump(datFile, data):
    log(TRACE, "------------------------------------------------")
    log(TRACE, "Dump {0} dump".format(datFile))


def PF_getDateTime(data):
    date = data["DATE"]
    time = data["TIME"]
    datetime_str = "{0} {1}".format(date, time)

    timeStamp = datetime.datetime.strptime(datetime_str, "%Y-%m-%d %H:%M:%S")

    return timeStamp


def PF_check(data, dataPrev, isNextDay):
    isPF = False
    return isPF


def PF_handle(datFile, nowTStampString, recoveryMode, isNextDay):
    global g_datPrevList
    global g_isPowerFailure

    nowTStamp = datetime.datetime.strptime(nowTStampString, "%Y-%m-%d %H:%M:%S")

    alarmString = ""
    isPF = False

    # process data files
    tmp = getLotName(datFile)
    datFileIdx = int(tmp)
    log(INFO, "**** PF {0}".format(datFile))

    datFileLast = datFile + ".last"
    datFilePast = datFile + ".past"
    datFileFake = datFile + ".fake"

    data = PF_createIfNotExist(datFile, nowTStamp)
    dataLast = PF_createIfNotExist(
        datFileLast, nowTStamp
    )  # previous good data (updated only if not PF)
    dataPast = PF_createIfNotExist(datFilePast, nowTStamp)
    dataFake = PF_createIfNotExist(datFileFake, nowTStamp)
    # ?? dump data-------------
    PF_dump(datFile, data)
    PF_dump(datFileLast, dataLast)
    PF_dump(datFilePast, dataPast)
    PF_dump(datFileFake, dataFake)
    # ------------------------------

    # verify size of the buffer for current data to detect power failure
    # dataPrev = g_datPrevList[datFileIdx]
    # if dataPrev == None or len(dataPrev) != g_FileSize:
    #     dataPrev = bytearray([0] * g_FileSize)
    #     dataPrev[:] = data
    #     g_datPrevList[datFileIdx] = data
    # endif

    # check midnight time shift and clear counters
    # if after midnight we still getting data from previous day
    try:
        dataTStamp = PF_getDateTime(data)
        delta = nowTStamp - dataTStamp
        deltaMax = 1 * 60 * 60  # 1 hour
        if (
            nowTStamp.day != dataTStamp.day
            and 0 < delta.seconds
            and delta.seconds < deltaMax
        ):
            log(INFO, "***** PF Midnight time shift detected, reset data")

            for i in range(112, 340):
                data[i] = 0
            # endfor
        # endif
    except:
        # .dat file without date - PF - here do nothing
        pass
    # endtry

    # check current data to detect power failure
    dataPrev = None
    isPF = PF_check(data, dataPrev, isNextDay)

    # if PF fire alarm message
    if isPF:
        alarmString = "{0}:鶏舎データファイル".format(datFileIdx)
    # endif


def collect_data_loop():
    global g_data_interval
    global g_CurHouseAndLot
    global g_ResultCode
    global g_record_format1
    global g_record_format2
    global g_record_format3
    global g_record_format4
    global g_record_format5
    global g_FileSize
    global g_datPrevList
    global g_timeCmd

    # init some global variables
    record_size1 = struct.calcsize(g_record_format1)
    record_size2 = struct.calcsize(g_record_format2)
    record_size3 = struct.calcsize(g_record_format3)
    record_size4 = struct.calcsize(g_record_format4)
    record_size5 = struct.calcsize(g_record_format5)
    g_FileSize = (
        record_size1 + record_size2 + record_size3 + record_size4 + record_size5
    )

    # get number of lot
    lotCountArr = fetOneRawQueryNoColumns("SELECT COUNT(*) FROM dbm_lot")
    lotCount = lotCountArr[0]

    # init foodInput
    dbm_lots = fetAllRawQuery("SELECT LOT_ID,FOOD_INPUT FROM dbm_lot")
    foodInput = {}
    for lot_id, food_input in dbm_lots:
        foodInput[lot_id] = food_input

    # init previous data buffer for every lot
    g_datPrevList = [None] * (99)

    # if last record in <dbt_collection> older then <g_data_interval> do recovery
    # else set commandIssueTime = dbt_collection.CREATED_TDATE + g_data_interval
    recoveryMode = False
    startup_gap = (
        1  # 1 sec gap to be sure that commandIssueTime-time.time() will be positive
    )
    collectionTime = getLastCollectionTime()
    if collectionTime == None:
        collectionTime = time.time() + startup_gap
        commandIssueTime = collectionTime
    else:
        commandIssueTime = collectionTime + g_data_interval
        if commandIssueTime + startup_gap < time.time():
            recoveryMode = True
        # endif
    # endif

    debugPrint(
        "--- Recovery:{}  commandIssueTime:{}".format(
            recoveryMode,
            time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(commandIssueTime)),
        )
    )
    dataTimeout = 60  # 60 sec

    try:
        while True:
            # check if another instance started
            pid, tid = getCurrentPid()
            log(
                INFO,
                "--- If (pid, tid): {},{} != {} -> terminate".format(
                    pid, tid, threading.get_ident()
                ),
            )

            # -------------------------------------------------------------------------------------------------------
            # wait for Collection command time
            collectionTimeStr = time.strftime(
                "%Y-%m-%d %H:%M", time.localtime(collectionTime)
            )
            commandIssueTimeStr = time.strftime(
                "%Y-%m-%d %H:%M", time.localtime(commandIssueTime)
            )

            waitTime = commandIssueTime - time.time()
            if waitTime > 0:
                if recoveryMode:
                    log(INFO, "*** Recovery is over")
                # endif

                # normal mode
                recoveryMode = False
                time.sleep(waitTime)
                # 2014-03-08: to avoid time syncronization problem between local
                # and remote computers always use Recovery mode
                ##                createCommandFile('Collection')
                g_timeCmd = commandIssueTimeStr
                createCommandFile(
                    "Recovery {} {}".format(collectionTimeStr, commandIssueTimeStr)
                )
            else:
                # recovery mode
                g_timeCmd = commandIssueTimeStr
                createCommandFile(
                    "Recovery {} {}".format(collectionTimeStr, commandIssueTimeStr)
                )
            # endif

            isNextDay = (
                datetime.datetime.fromtimestamp(collectionTime).day
                != datetime.datetime.fromtimestamp(commandIssueTime).day
            )
            log(
                INFO,
                "***** PF Next day check (DB, now): {0}, {1}".format(
                    collectionTimeStr, commandIssueTimeStr
                ),
            )

            # set next Collection command time
            collectionTime = commandIssueTime
            commandIssueTime += g_data_interval

            # store command time
            startTime = time.time()
            data_ready = False

            # -------------------------------------------------------------------------------------------------------
            # wait for result.info loop
            while (time.time() - startTime) < dataTimeout:
                # periodically count files collectionXX.dat until all will be ready
                time.sleep(1)
                fileCount = countDataFiles()
                if fileCount == lotCount:
                    data_ready = True
                    break
                # endif
            # endwhile

            # end of waiting loop. OK or timeout?
            if not data_ready:
                # timeout
                # elapsed_time = (time.time() - startTime) * 1000
                log(
                    ERROR,
                    "--- Waiting dataXX.json timeout! {0:.0f} sec  {1} of {2} file(s) found".format(
                        dataTimeout, fileCount, lotCount
                    ),
                )
            else:
                # OK
                log(
                    INFO,
                    "--- Waiting dataXX.json time: {0:.0f} sec - Data found!".format(
                        time.time() - startTime
                    ),
                )
            # endif

            # check and adjust collection directory format
            collDir = g_collection_dir
            if g_collection_dir[-1] != "/":
                collDir = g_collection_dir + "/"
            # endif

            nowTStamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:00")
            if recoveryMode:
                nowTStamp = time.strftime(
                    "%Y-%m-%d %H:%M:00", time.localtime(collectionTime)
                )
            # endif

            log(
                INFO,
                "--- Processing dataXX.json started, nowTStamp: {0}'".format(nowTStamp),
            )

            # -------------------------------------------------------------------------------------------------------
            # read data from collectionXX.dat file
            # and insert new data into dbt_collection, dbt_egg, dbt_fooder, dbt_thermo and dbt_fw
            # connect to mySQL
            processedFileCount = 0
            for datFile in glob.glob(collDir + "data??.json"):
                if os.path.isfile(datFile):
                    log(
                        INFO,
                        "------ Processing {0} -------------------------------".format(
                            datFile
                        ),
                    )
                else:  ## Show an error ##
                    log(ERROR, "Error: file {0} not found".format(datFile))
                # endif

                # ---------------------------------------------------------------------------------------------------
                # 0. fetch lot id and house name
                lot_id = getLotId(datFile)
                row = fetOneRawQuery(
                    "SELECT dbm_house.house_name,dbm_lot.lot_name FROM dbm_house,dbm_lot WHERE dbm_lot.lot_id = %s and dbm_house.house_id = dbm_lot.lot_id"
                    % (lot_id)
                )

                if row:
                    house_name = row["house_name"]
                    lot_name = row["lot_name"]
                else:
                    log(
                        ERROR,
                        "File '{0}': Cannot find House and Lot for '{1}'. File skipped.".format(
                            datFile, lot_id
                        ),
                    )
                    log(g_loggingLvl, RESULT_CODE_NG, noFormat=True)

                    # remove json file
                    deleteIfExists(datFile)
                    continue
                # endif

                g_CurHouseAndLot = house_name + ":" + lot_name + ":"

                # ---------------------------------------------------------------------------------------------------
                # 1. file size check
                # try:
                #     statinfo = os.stat(datFile)
                # except:
                #     log(
                #         ERROR,
                #         "Error: file {0} not found. Skipped -----".format(datFile),
                #     )
                #     continue
                # # endtry

                # if statinfo.st_size != g_FileSize:
                #     log(
                #         ERROR,
                #         "'{0}': Wrong file size. Expected {1} bytes, found {2} bytes. File skipped.".format(
                #             datFile, g_FileSize, statinfo.st_size
                #         ),
                #     )
                #     log(g_loggingLvl, RESULT_CODE_NG, noFormat=True)
                #     log(INFO, "------ Processing {0} is over -----".format(datFile))

                #     # remove collectionXX.dat
                #     deleteIfExists(datFile)
                #     continue
                # endif

                # ---------------------------------------------------------------------------------------------------
                # 1a. 2014.01.30 power failure check
                alarmString = PF_handle(datFile, nowTStamp, recoveryMode, isNextDay)
                if len(alarmString) > 0:
                    log(ALARM, alarmString)
                # endif

    except Exception as e:
        pass
    finally:
        pass
