import json

from channels.generic.websocket import AsyncJsonWebsocketConsumer


class DataActivityConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        await self.channel_layer.group_add("database_activity", self.channel_name)
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            "database_activity",
            self.channel_name,
        )

    async def receive(self, json_data):
        type = json_data["type"]
        message = json_data["message"]
        await self.send(text_data=json.dumps({"type": type, "message": message}))

    async def database_changed(self, json_data):
        message = json_data["message"]
        await self.send(text_data=json.dumps(message))
