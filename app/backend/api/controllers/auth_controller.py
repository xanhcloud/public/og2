from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.response import Response
from rest_framework.decorators import (
    permission_classes,
    authentication_classes,
    api_view,
)

from api.serializers.users_serializers import UserSerializer


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def Profile(request):
    user = UserSerializer(request.user, context={"request": request})
    return Response(user.data)
