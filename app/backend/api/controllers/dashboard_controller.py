import configparser
import os
from django.contrib.auth.models import User
from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.response import Response
from rest_framework.decorators import (
    permission_classes,
    authentication_classes,
    api_view,
)

from api.serializers.users_serializers import UserSerializer
from api.utils import debugPrint
from api.utils.fileUtils import ROOT_PATH, getAlarms
from api.utils.query_utils import (
    getFoodWaterID,
    getMainTable,
    getMainTotals,
    getMainTotalsByHouses,
    getPrevFoodWaterAmount,
)


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def Dashboard(request):
    total_users = User.objects.count()
    last_user = User.objects.order_by("date_joined").last()

    try:
        iniFilePath = ROOT_PATH + "/ovalGrowth.ini"

        if not os.path.isfile(iniFilePath):
            debugPrint("'{0}' not found".format(iniFilePath))
            raise AssertionError("'{0}' not found".format(iniFilePath))
        # endif

        config = configparser.ConfigParser()
        config.read(iniFilePath)

        try:
            camera_section = config["camera"]
            alarm_section = config["alarm"]
        except KeyError:
            debugPrint("Cannot parse configuration file, no section 'alarm/camera'. ")
            raise
        # endtry
        g_AlarmEggCount = alarm_section["eggcount"]  # ALRAM_LEVEL: egg count
        g_AlarmTempUpper = alarm_section["temp_upper"]  # ALARM_LEVEL: temp upper
        g_AlarmTempLower = alarm_section["temp_lower"]  # ALARM_LEVEL: temp lower
        g_AlarmFood = alarm_section["food"]  # ALARM_LEVEL: food
        g_AlarmWater = alarm_section["water"]  # ALARM_LEVEL: water

        g_CameraAddress = camera_section["Ip_address"]

        date = request.query_params.get("date")

        alarms = getAlarms()
        rows = getMainTable(date)
        tot_chiken, tot_adult_chiken = getMainTotals(date)

        alarmTempUpper = int(g_AlarmTempUpper)
        alarmTempLower = int(g_AlarmTempLower)
        alarmFood = int(g_AlarmFood)
        alarmWater = int(g_AlarmWater)

        # print(len(rows))

        if len(rows) > 0:
            for row in rows:
                fw_id = getFoodWaterID(row["LOT_ID"])
                prev_food, prev_water = getPrevFoodWaterAmount(fw_id)
                row["ALARM_FOOD"] = False
                if prev_food != -1:
                    f_min = prev_food - prev_food * (alarmFood / 100)
                    f_max = prev_food + prev_food * (alarmFood / 100)
                    if row["FOOD_AMOUNT"] < f_min or row["FOOD_AMOUNT"] > f_max:
                        row["ALARM_FOOD"] = True
                    # endif
                # endif

                row["ALARM_WATER"] = False
                if prev_water != -1:
                    w_min = prev_water - prev_water * (alarmWater / 100)
                    w_max = prev_water + prev_water * (alarmWater / 100)
                    if row["WATER_AMOUNT"] < f_min or row["WATER_AMOUNT"] > f_max:
                        row["ALARM_WATER"] = True
                    # endif
                # endif
            # endfor
        # endif

        curr_count_by_houses = getMainTotalsByHouses(date)

        return Response(
            {
                "alarms": alarms,
                "rows": rows,
                "tot_chiken": tot_chiken,
                "tot_adult_chiken": tot_adult_chiken,
                "alarmTempUpper": alarmTempUpper,
                "alarmTempLower": alarmTempLower,
                "total_users": total_users,
                "last_user": UserSerializer(
                    last_user, context={"request": request}
                ).data,
                "debug": "test",
                "curr_count_by_houses": curr_count_by_houses,
            }
        )
    except Exception as e:
        print(e)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def test_dashboard(request):
    try:

        rows = getMainTable()

        return Response({"rows": rows, "debug": "test"})
    except Exception as e:
        print(e)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )
