from collections import namedtuple
import datetime
import logging
import time
from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.response import Response
from rest_framework.decorators import (
    permission_classes,
    authentication_classes,
    api_view,
)

from api import abstractions
from api.utils.common import getTabsData
from api.utils.query_utils import (
    getBatchInfoByLotID,
    getFooderInfo,
    getFooderNoList,
    getHouseNameByLotID,
    getLotList,
    getRegFooderInfo,
    insertFooderst,
)


Color = namedtuple("Color", "red green blue")
Margin = namedtuple("Margin", "left right top bottom")


@api_view(["GET", "POST"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def fooder_reg(request, id=1):
    house = getHouseNameByLotID(id)

    lots = getLotList()
    tabs = []

    for lot in lots:
        batch = getBatchInfoByLotID(lot["LOT_ID"])
        tab = abstractions.Tab(
            lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "/fooder_reg"
        )
        tab.visibility = False
        if batch:
            tab.visibility = True
        tabs.append(tab)

    current_tab = tabs[0]

    for tab in tabs:
        if tab.id == id:
            current_tab = tab
            break

    if current_tab.visibility == False:
        for tab in tabs:
            if tab.visibility == True:
                current_tab = tab
                break

    id = current_tab.id

    fooders = getFooderNoList(id)
    # print(len(fooders))
    if len(fooders) > 0:
        mfooder_id = -1
        mfooderst_id = -1
        sel_fooder = -1
        reg_start_time = ""
        reg_apply_date = ""
        r_msg = ""
        # print(request.method)

        try:
            if request.method == "POST":
                # print(request.form)
                if request.data["select"]:
                    mfooder_id = int(request.data["fooder_id"])
                    # print("mfooder_id=%s" % (mfooder_id))
                    sel_fooder = mfooder_id
                    reg_fooder = getRegFooderInfo(id, mfooder_id, reg_start_time)
                    if len(reg_fooder) > 0:
                        reg_start_time = reg_fooder[0]["START_TIME"]
                        reg_apply_date = reg_fooder[0]["APPLY_DATE"]
                        mfooderst_id = reg_fooder[0]["MFOODERST_ID"]
                    else:
                        mfooderst_id = -1
                    # endif
                # end if
                elif request.data["register"]:
                    reg_start_time = request.data["start_date"]
                    reg_apply_date = request.data["apply_date"]
                    # print(mfooder_id)
                    insertFooderst(
                        id,
                        mfooder_id,
                        reg_start_time,
                        reg_apply_date,
                        mfooderst_id,
                    )
                    r_msg = "Database updated successfully !!!"
                # end if
            elif request.method == "GET":
                # print(request.query_params)
                if request.query_params.get("select"):
                    # print("select")
                    mfooder_id = int(request.query_params.get("fooder_id"))
                    sel_fooder = mfooder_id
                elif request.query_params.get("search"):
                    # print("search")
                    reg_start_time = request.query_params.get("start_date")
                    # print(reg_start_time)
                    reg_fooder = getRegFooderInfo(id, mfooder_id, reg_start_time)
                    if len(reg_fooder) > 0:
                        reg_start_time = reg_fooder[0]["START_TIME"]
                        reg_apply_date = reg_fooder[0]["APPLY_DATE"]
                        mfooderst_id = reg_fooder[0]["MFOODERST_ID"]
                    else:
                        mfooderst_id = -1
                        reg_start_time = ""
                        reg_apply_date = ""
                    # endif
                elif request.query_params.get("register"):
                    # print("register")
                    reg_start_time = request.query_params.get("start_date")
                    reg_apply_date = request.query_params.get("apply_date")
                    mfooder_id = int(request.query_params.get("fooder_id"))
                    sel_fooder = mfooder_id

                    insertFooderst(
                        id,
                        mfooder_id,
                        reg_start_time,
                        reg_apply_date,
                        mfooderst_id,
                    )
                    r_msg = "Database updated successfully !!!"
                # end if
            else:
                sel_fooder = -1
                mfooderst_id = -1
            # endif
        except:
            r_msg = "Failed fooder processing !"
            logging.exception("Something awful happened!")
        # endtry

        rows = getFooderInfo(id)
        fdr = 1
        apd = None
        rows1 = []
        rows2 = []
        for row in rows:
            if row["FOODER_NO"] == fdr:
                if apd == None:
                    rows1.append(row)
                    apd = row["APPLY_DATE"]
                elif row["APPLY_DATE"] == apd:
                    rows1.append(row)
                else:
                    rows2.append(row)
                # endif
            else:
                fdr += 1
                apd = None
                rows1.append(row)
                apd = row["APPLY_DATE"]
            # endif
        # endfor
        data_f = True
    else:
        data_f = False
    # endif

    try:
        # print("lot_id=%s" % (id))
        if data_f:
            return Response(
                {
                    "id": id,
                    "tabs": getTabsData(tabs),
                    "house": house,
                    "mfooder_id": mfooder_id,
                    "fooders": fooders,
                    "reg_start_time": reg_start_time,
                    "reg_apply_date": reg_apply_date,
                    "r_msg": r_msg,
                    "rows1": rows1,
                    "rows2": rows2,
                    "data_f": data_f,
                }
            )
        else:
            return Response(
                {
                    "id": id,
                    "lots": lots,
                    "house": house,
                    "data_f": data_f,
                }
            )
        # endif
    except Exception as e:
        print(e)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )
