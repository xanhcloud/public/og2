from collections import namedtuple
import datetime
import logging
import time
from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.response import Response
from rest_framework.decorators import (
    permission_classes,
    authentication_classes,
    api_view,
)

from api import abstractions
from api.utils.common import getTabsData
from api.utils.query_utils import (
    getBatchInfoByLotID,
    getBatchInfoList,
    getComparisonGraphPast,
    getDailyGraphFw,
    getDailyGraphThermo,
    getHengrpInfoByLotID,
    getHouseNameByLotID,
    getLotList,
    getWeeklyGraph1,
    getWeeklyGraph2,
    getWeeklyHistGraph1,
    getWeeklyHistGraph2,
)


Color = namedtuple("Color", "red green blue")
Margin = namedtuple("Margin", "left right top bottom")


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def graph(request, id=1):
    try:
        house = getHouseNameByLotID(id)
        lots = getLotList()
        tabs = []

        today = request.GET.get("date", "")
        if today == "":
            today = datetime.datetime.today().strftime("%Y-%m-%d")
        start_tdate = today + " 00:00:00"
        end_tdate = today + " 23:59:59"

        current_batch = getBatchInfoByLotID(id)

        for lot in lots:
            batch = getBatchInfoByLotID(lot["LOT_ID"])
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "graph"
            )
            # rows = getDailyGraphFw(lot["LOT_ID"], start_tdate, end_tdate)
            tab.visibility = False
            if batch:
                tab.visibility = True
            tabs.append(tab)

        # current_tab = tabs[0]

        # for tab in tabs:
        #     if tab.id == id:
        #         current_tab = tab
        #         break

        # if current_tab.visibility == False:
        #     for tab in tabs:
        #         if tab.visibility == True:
        #             current_tab = tab
        #             break

        # id = current_tab.id
        rows = []
        graphs_data_x = []
        data_food = []
        data_water = []
        data_temp_in = []
        data_temp_out = []

        if current_batch:
            rows = getDailyGraphFw(id, start_tdate, end_tdate)

            if len(rows) > 0:
                for row in rows:
                    tdate = row["COL_TDATE"]
                    graphs_data_x.append(tdate)
                    data_food.append(float(row["FOOD_AMOUNT_U"]))
                    data_water.append(float(row["WATER_AMOUNT_U"]))

                # Get room temp and outer temp
                temps = getDailyGraphThermo(id, start_tdate, end_tdate)
                for temp in temps:
                    if temp["MTHERMO_NO"] == 1:
                        data_temp_in.append(temp["CURVAL"] / 10)
                    else:
                        data_temp_out.append(temp["CURVAL"] / 10)

                data_f = True
            else:
                data_f = False
            # endif
        else:
            data_f = False
        # endif

        left_data_y = {
            "food": data_food,
            "water": data_water,
        }
        right_data_y = {
            "temp_in": data_temp_in,
            "temp_out": data_temp_out,
        }

        return Response(
            {
                "id": id,
                "data_f": data_f,
                "house": house,
                "tabs": getTabsData(tabs),
                "timestamp": str(time.time()),
                "rows": rows,
                "left_data_y": left_data_y,
                "right_data_y": right_data_y,
                "graphs_data_x": graphs_data_x,
            }
        )
    except Exception as e:
        print(e)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def graph_weekly(request, id=1):
    try:
        house = getHouseNameByLotID(id)
        lots = getLotList()
        tabs = []

        growth = ""
        growth1 = False
        growth2 = False
        growth3 = False

        for lot in lots:
            batch = getBatchInfoByLotID(lot["LOT_ID"])
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "graph_weekly"
            )
            tab.visibility = False
            tabs.append(tab)
            if tab.batch:

                growth_kind = tab.batch[0]

                if growth_kind == 1:
                    growth = "Brooder"
                elif growth_kind == 2:
                    growth = "Pullet"
                else:
                    growth = "Layer"

                hengrp_info = getHengrpInfoByLotID(lot["LOT_ID"])
                batches = getBatchInfoList(hengrp_info[0])

                if request.method == "GET":
                    if request.query_params.get("growth1"):
                        growth = "Brooder"
                        growth_kind = 1
                    elif request.query_params.get("growth2"):
                        growth = "Pullet"
                        growth_kind = 2
                    else:
                        growth = "Layer"
                        growth_kind = 3

                batch_id = False

                for batch in batches:
                    if batch["GROWTH_KIND"] == growth_kind:
                        batch_id = batch["BATCH_ID"]
                        break

                if batch_id == False:
                    batch_id = batches[0]["BATCH_ID"]

                rows = getWeeklyHistGraph1(batch_id, 1, 100)

                if batch and len(rows) > 1:
                    tab.visibility = True

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        if current_tab.batch:

            growth_kind = current_tab.batch[0]

            if growth_kind == 1:
                growth = "Brooder"
            elif growth_kind == 2:
                growth = "Pullet"
            else:
                growth = "Layer"

            hengrp_info = getHengrpInfoByLotID(current_tab.id)
            batches = getBatchInfoList(hengrp_info[0])

            if request.method == "GET":
                if request.query_params.get("growth1"):
                    growth = "Brooder"
                    growth_kind = 1
                elif request.query_params.get("growth2"):
                    growth = "Pullet"
                    growth_kind = 2
                else:
                    growth = "Layer"
                    growth_kind = 3

                batch_id = False

                for batch in batches:
                    if batch["GROWTH_KIND"] == growth_kind:
                        print(batch["BATCH_ID"])
                        batch_id = batch["BATCH_ID"]
                        break

                if batch_id == False:
                    batch_id = batches[0]["BATCH_ID"]

                rows = getWeeklyHistGraph1(batch_id, 1, 100)

            growth1 = growth2 = growth3 = False
            for batch in batches:
                if batch["GROWTH_KIND"] == 1:
                    growth1 = True
                elif batch["GROWTH_KIND"] == 2:
                    growth2 = True
                elif batch["GROWTH_KIND"] == 3:
                    growth3 = True

            graphs_data_x = []
            data_survivalRate = []
            data_eggRate = []
            data_eggWeight = []
            data_eggVol = []
            data_survivalRateMan = []
            data_eggRateMan = []
            data_eggWeightMan = []
            data_eggVolMan = []
            data_henWeight = []
            data_henWeightMan = []

            graph1 = {
                "graphs_data_x": [],
                "left_data_y": {},
                "right_data_y": {},
            }
            graph2 = {
                "graphs_data_x": [],
                "left_data_y": {},
                "right_data_y": {},
            }

            if len(rows) > 1:
                for row in rows:
                    week_periods = row["WEEK_PERIODS"]
                    graphs_data_x.append(week_periods)
                    data_survivalRate.append(float(row["SURVIVAL_RATE"]))
                    data_eggRate.append(float(row["EGG_RATE"]))
                    data_eggWeight.append(float(row["EGG_WEIGHT"]))
                    data_eggVol.append(float(row["EGG_VOL"]))
                    data_survivalRateMan.append(float(row["SURVIVAL_RATE_MAN"]))
                    data_eggRateMan.append(float(row["EGG_RATE_MAN"]))
                    data_eggWeightMan.append(float(row["EGG_WEIGHT_MAN"]))
                    data_eggVolMan.append(float(row["EGG_VOL_MAN"]))
                    data_henWeight.append(float(row["HEN_WEIGHT"]))
                    data_henWeightMan.append(float(row["HEN_WEIGHT_MAN"]))

                graph1["graphs_data_x"] = graphs_data_x
                graph1["left_data_y"] = {
                    "survivalRate": data_survivalRate,
                    "survivalRateMan": data_survivalRateMan,
                    "eggRate": data_eggRate,
                    "eggRateMan": data_eggRateMan,
                    "eggWeight": data_eggWeight,
                    "eggWeightMan": data_eggWeightMan,
                    "eggVol": data_eggVol,
                    "eggVolMan": data_eggVolMan,
                }
                graph1["right_data_y"] = {
                    "henWeight": data_henWeight,
                    "henWeightMan": data_henWeightMan,
                }

                # Get remaining Weekly data
                rows = getWeeklyHistGraph2(batch_id, 1, 100)

                graphs_data_x = []
                data_food = []
                data_water = []
                data_foodDemand = []
                data_temp_in = []
                data_temp_out = []

                for row in rows:
                    week_periods = row["WEEK_PERIODS"]
                    graphs_data_x.append(week_periods)
                    data_food.append(float(row["FOOD_AMOUNT_U"]) / 1000)
                    data_water.append(float(row["WATER_AMOUNT_U"]))
                    if row["FOOD_DEMAND"]:
                        data_foodDemand.append(float(row["FOOD_DEMAND"]))
                    else:
                        data_foodDemand.append(0)

                    data_temp_in.append(float(row["ROOM_TEMP"]))
                    data_temp_out.append(float(row["OUTER_TEMP"]))

                graph2["graphs_data_x"] = graphs_data_x
                graph2["left_data_y"] = {
                    "food": data_food,
                    "water": data_water,
                    "foodDemand": data_foodDemand,
                }
                graph2["right_data_y"] = {
                    "temp_in": data_temp_in,
                    "temp_out": data_temp_out,
                }

                data_f = True
            else:
                data_f = False
            # endif
        else:
            data_f = False
        # endif

        return Response(
            {
                "id": id,
                "house": house,
                "growth": growth,
                "growth1": growth1,
                "growth2": growth2,
                "growth3": growth3,
                "data_f": data_f,
                "timestamp": str(time.time()),
                "tabs": getTabsData(tabs),
                "graph1": graph1,
                "graph2": graph2,
            }
        )
    except Exception as e:
        print(e)
        logging.exception("Something awful happened!")
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def graph_spec_after(request, id=1):
    try:

        house = getHouseNameByLotID(id)
        lots = getLotList()
        tabs = []

        from_week = request.query_params.get("from_week", "")
        if from_week == "":
            from_week = 1

        to_week = request.query_params.get("to_week", "")
        if to_week == "":
            to_week = 100

        for lot in lots:
            batch = getBatchInfoByLotID(lot["LOT_ID"])
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "graph_spec_after"
            )
            rows = getWeeklyGraph1(lot["LOT_ID"], from_week, to_week)
            tab.visibility = False

            if batch and len(rows) > 0:
                tab.visibility = True
            tabs.append(tab)

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        graph1 = {
            "graphs_data_x": [],
            "left_data_y": {},
            "right_data_y": {},
        }
        graph2 = {
            "graphs_data_x": [],
            "left_data_y": {},
            "right_data_y": {},
        }

        if len(rows) > 0:
            graphs_data_x = []
            data_survivalRate = []
            data_eggRate = []
            data_eggWeight = []
            data_eggVol = []
            data_survivalRateMan = []
            data_eggRateMan = []
            data_eggWeightMan = []
            data_eggVolMan = []
            for row in rows:
                week_periods = row["WEEK_PERIODS"]
                graphs_data_x.append(week_periods)
                data_survivalRate.append(float(row["SURVIVAL_RATE"]))
                data_eggRate.append(float(row["EGG_RATE"]))
                data_eggWeight.append(float(row["EGG_WEIGHT"]))
                data_eggVol.append(float(row["EGG_VOL"]))
                data_survivalRateMan.append(float(row["SURVIVAL_RATE_MAN"]))
                data_eggRateMan.append(float(row["EGG_RATE_MAN"]))
                data_eggWeightMan.append(float(row["EGG_WEIGHT_MAN"]))
                data_eggVolMan.append(float(row["EGG_VOL_MAN"]))

            graph1["graphs_data_x"] = graphs_data_x
            graph1["left_data_y"] = {
                "survivalRate": data_survivalRate,
                "survivalRateMan": data_survivalRateMan,
                "eggRate": data_eggRate,
                "eggRateMan": data_eggRateMan,
            }
            graph1["right_data_y"] = {
                "eggWeight": data_eggWeight,
                "eggWeightMan": data_eggWeightMan,
                "eggVol": data_eggVol,
                "eggVolMan": data_eggVolMan,
            }

            # ===============================================================================

            rows = getWeeklyGraph2(id, from_week, to_week)

            graphs_data_x = []
            data_food = []
            data_water = []
            data_foodDemand = []
            data_temp_in = []
            data_temp_out = []
            for row in rows:
                week_periods = row["WEEK_PERIODS"]
                graphs_data_x.append(week_periods)
                data_food.append(float(row["FOOD_AMOUNT_U"]) / 1000)
                data_water.append(float(row["WATER_AMOUNT_U"]))
                data_foodDemand.append(float(row["FOOD_DEMAND"] or 0.0))
                data_temp_in.append(float(row["ROOM_TEMP"]))
                data_temp_out.append(float(row["OUTER_TEMP"]))

            graph2["graphs_data_x"] = graphs_data_x
            graph2["left_data_y"] = {
                "food": data_food,
                "water": data_water,
                "foodDemand": data_foodDemand,
            }
            graph2["right_data_y"] = {
                "temp_in": data_temp_in,
                "temp_out": data_temp_out,
            }

            # ===============================================================================

            return Response(
                {
                    "id": id,
                    "house": house,
                    "from_week": from_week,
                    "to_week": to_week,
                    "graph1": graph1,
                    "graph2": graph2,
                    "timestamp": str(time.time()),
                    "tabs": getTabsData(tabs),
                }
            )
        else:
            data_f = False
            return Response(
                {
                    "id": id,
                    "house": house,
                    "from_week": from_week,
                    "to_week": to_week,
                    "graph1": graph1,
                    "graph2": graph2,
                    "timestamp": str(time.time()),
                    "tabs": getTabsData(tabs),
                    "data_f": data_f,
                }
            )
        # endif

    except Exception as e:
        print(e)
        logging.exception("Something awful happened!")
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def graph_comparison(request, id=1):
    try:
        house = getHouseNameByLotID(id)
        lots = getLotList()
        tabs = []

        rows_set_1 = getWeeklyGraph1(id, 1, 100)

        for lot in lots:
            batch = getBatchInfoByLotID(lot["LOT_ID"])
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "graph_comparison"
            )
            rows_set_1 = getWeeklyGraph1(lot["LOT_ID"], 1, 100)
            days = len(rows_set_1)

            graphs_data_x = []
            data_survivalRate = []
            data_eggRate = []
            data_eggWeight = []
            data_eggVol = []

            for row in rows_set_1:
                week_periods = row["WEEK_PERIODS"]
                graphs_data_x.append(week_periods)
                data_survivalRate.append(float(row["SURVIVAL_RATE"]))
                data_eggRate.append(float(row["EGG_RATE"]))
                data_eggWeight.append(float(row["EGG_WEIGHT"]))
                data_eggVol.append(float(row["EGG_VOL"]))

            week_p_e = rows_set_1[days - 1]["WEEK_PERIODS"]
            week_p_s = rows_set_1[0]["WEEK_PERIODS"]

            rows_set_2 = getComparisonGraphPast(lot["LOT_ID"], week_p_s, week_p_e)

            tab.visibility = False

            if len(rows_set_1) > 1:
                if len(rows_set_2) > 1:
                    tab.visibility = True

            tabs.append(tab)

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        graph1 = {
            "graphs_data_x": [],
            "left_data_y": {},
            "right_data_y": {},
        }
        graph2 = {
            "graphs_data_x": [],
            "left_data_y": {},
            "left_date_y_past": {},
            "right_data_y": {},
            "right_data_y_past": {},
        }

        if len(rows_set_1) > 1:
            graphs_data_x = []
            data_survivalRate = []
            data_eggRate = []
            data_eggWeight = []
            data_eggVol = []

            for row in rows_set_1:
                week_periods = row["WEEK_PERIODS"]
                graphs_data_x.append(week_periods)
                data_survivalRate.append(float(row["SURVIVAL_RATE"]))
                data_eggRate.append(float(row["EGG_RATE"]))
                data_eggWeight.append(float(row["EGG_WEIGHT"]))
                data_eggVol.append(float(row["EGG_VOL"]))

            week_p_e = rows_set_1[days - 1]["WEEK_PERIODS"]
            week_p_s = rows_set_1[0]["WEEK_PERIODS"]

            rows_set_2 = getComparisonGraphPast(id, week_p_s, week_p_e)

            data_survivalRatePast = []
            data_eggRatePast = []
            data_eggWeightPast = []
            data_eggVolPast = []

            if len(rows_set_2) > 1:
                # ===============================================================================
                # GRAPH 1
                # ===============================================================================

                for row in rows_set_2:
                    data_survivalRatePast.append(float(row["SURVIVAL_RATE"]))
                    data_eggRatePast.append(float(row["EGG_RATE_CUR"]))
                    data_eggWeightPast.append(float(row["EGG_WEIGHT"]))
                    data_eggVolPast.append(float(row["EGG_VOL"]))

                graph1["graphs_data_x"] = graphs_data_x
                graph1["left_data_y"] = {
                    "survivalRate": data_survivalRate,
                    "survivalRatePast": data_survivalRatePast,
                    "eggRate": data_eggRate,
                    "eggRatePast": data_eggRatePast,
                }
                graph1["right_data_y"] = {
                    "eggWeight": data_eggWeight,
                    "eggWeightPast": data_eggWeightPast,
                    "eggVol": data_eggVol,
                    "eggVolPast": data_eggVolPast,
                }

                # ===============================================================================
                # GRAPH 2
                # ===============================================================================

                rows = getWeeklyGraph2(id, 1, 100)

                if len(rows) > 1:
                    graphs_data_x = []
                    data_food = []
                    data_water = []
                    data_foodDemand = []
                    data_temp_in = []
                    data_temp_out = []
                    for row in rows:
                        week_periods = row["WEEK_PERIODS"]
                        graphs_data_x.append(week_periods)
                        data_food.append(float(row["FOOD_AMOUNT_U"]) / 1000)
                        data_water.append(float(row["WATER_AMOUNT_U"]))
                        data_foodDemand.append(float(row["FOOD_DEMAND"] or 0.0))
                        data_temp_in.append(float(row["ROOM_TEMP"]))
                        data_temp_out.append(float(row["OUTER_TEMP"]))

                    # pasts = getComparisonGraphPast(id, 1, 100)

                    data_foodPast = []
                    data_waterPast = []
                    data_foodDemandPast = []
                    data_temp_inPast = []
                    data_temp_outPast = []

                    for row in rows:
                        data_foodPast.append(float(row["FOOD_AMOUNT_U"]) / 1000)
                        data_waterPast.append(float(row["WATER_AMOUNT_U"]))
                        data_foodDemandPast.append(float(row["FOOD_DEMAND"] or 0.0))
                        data_temp_inPast.append(float(row["ROOM_TEMP"]))
                        data_temp_outPast.append(float(row["OUTER_TEMP"]))

                    graph2["graphs_data_x"] = graphs_data_x
                    graph2["left_data_y"] = {
                        "food": data_food,
                        "water": data_water,
                        "foodDemand": data_foodDemand,
                    }
                    graph2["left_date_y_past"] = {
                        "food": data_foodPast,
                        "water": data_waterPast,
                        "foodDemand": data_foodDemandPast,
                    }
                    graph2["right_data_y"] = {
                        "temp_in": data_temp_in,
                        "temp_out": data_temp_out,
                    }
                    graph2["right_data_y_past"] = {
                        "temp_in": data_temp_inPast,
                        "temp_out": data_temp_outPast,
                    }

                    # ===============================================================================

                    data_f = True
                # endif
            else:
                data_f = False
            # endif
        else:
            data_f = False
        # endif
        return Response(
            {
                "id": id,
                "data_f": data_f,
                "graph1": graph1,
                "graph2": graph2,
                "tabs": getTabsData(tabs),
                "timestamp": str(time.time()),
                "house": house,
            }
        )
    except Exception as e:
        print(e)
        logging.exception("Something awful happened!")
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def graph_3days(request, id=1):
    try:
        house = getHouseNameByLotID(id)
        lots = getLotList()
        tabs = []

        # today = datetime.datetime.today()
        today_str = request.GET.get("date", "")
        if today_str == "":
            today = datetime.datetime.today()
        else:
            today = datetime.datetime.strptime(today_str, "%Y-%m-%d")

        start_tdate = today - datetime.timedelta(days=2)
        end_tdate = today

        current_batch = getBatchInfoByLotID(id)
        for lot in lots:
            batch = getBatchInfoByLotID(lot["LOT_ID"])
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "graph_3days"
            )
            # temperatures = getDailyGraphThermo(lot["LOT_ID"], start_tdate, end_tdate)
            tab.visibility = False
            if batch:
                tab.visibility = True
            tabs.append(tab)

        # current_tab = tabs[0]

        # for tab in tabs:
        #     if tab.id == id:
        #         current_tab = tab
        #         break

        # if current_tab.visibility == False:
        #     for tab in tabs:
        #         if tab.visibility == True:
        #             current_tab = tab
        #             break

        # id = current_tab.id

        total_x_values = 0
        number_of_x_labels = 0
        graphs_data_x = []

        data_food = []
        data_water = []
        data_temp_in = []
        data_temp_out = []
        if current_batch:
            # Get Food and Water
            ##            day1s_tdate = start_tdate
            ##            day1e_tdate = datetime.datetime(start_tdate.year, start_tdate.month, start_tdate.day, 23, 59, 59)
            ##            day2s_tdate = datetime.datetime(start_tdate.year, start_tdate.month, start_tdate.day+1, 0, 0, 0)
            ##            day2e_tdate = datetime.datetime(start_tdate.year, start_tdate.month, start_tdate.day+1, 23, 59, 59)
            ##            day3s_tdate = datetime.datetime(start_tdate.year, start_tdate.month, start_tdate.day+2, 0, 0, 0)
            ##            day3e_tdate = datetime.datetime(start_tdate.year, start_tdate.month, start_tdate.day+2, 23, 59, 59)
            ##            day4s_tdate = datetime.datetime(start_tdate.year, start_tdate.month, start_tdate.day+3, 0, 0, 0)
            ##            day4e_tdate = end_tdate

            # day 1
            day1s_tdate = start_tdate
            day1e_tdate = datetime.datetime(
                start_tdate.year, start_tdate.month, start_tdate.day, 23, 59, 59
            )

            # day 2
            day2s_tdate = datetime.datetime(
                start_tdate.year, start_tdate.month, start_tdate.day, 0, 0, 0
            ) + datetime.timedelta(days=1)
            day2e_tdate = datetime.datetime(
                start_tdate.year, start_tdate.month, start_tdate.day, 23, 59, 59
            ) + datetime.timedelta(days=1)

            # day 3
            day3s_tdate = datetime.datetime(
                start_tdate.year, start_tdate.month, start_tdate.day, 0, 0, 0
            ) + datetime.timedelta(days=2)
            day3e_tdate = datetime.datetime(
                start_tdate.year, start_tdate.month, start_tdate.day, 23, 59, 59
            ) + datetime.timedelta(days=2)

            rows1 = getDailyGraphFw(id, day1s_tdate, day1e_tdate)
            rows2 = getDailyGraphFw(id, day2s_tdate, day2e_tdate)
            rows3 = getDailyGraphFw(id, day3s_tdate, day3e_tdate)

            for row1 in rows1:
                tdate = row1["COL_TDATE"]
                graphs_data_x.append(tdate)
                data_food.append(float(row1["FOOD_AMOUNT_U"]))
                data_water.append(float(row1["WATER_AMOUNT_U"]))

            # print(len(rows1))
            for row2 in rows2:
                tdate = row2["COL_TDATE"]
                graphs_data_x.append(tdate)
                data_food.append(float(row2["FOOD_AMOUNT_U"]))
                data_water.append(float(row2["WATER_AMOUNT_U"]))

            # print(len(rows2))
            for row3 in rows3:
                tdate = row3["COL_TDATE"]
                graphs_data_x.append(tdate)
                data_food.append(float(row3["FOOD_AMOUNT_U"]))
                data_water.append(float(row3["WATER_AMOUNT_U"]))

            # print(len(rows3))

            # Get room temp and outer temp
            temps = getDailyGraphThermo(id, day1s_tdate, day3e_tdate)
            if len(temps) > 0:
                for temp in temps:
                    if not temp["CURVAL"]:
                        temp["CURVAL"] = 0
                    # endif
                    if temp["MTHERMO_NO"] == 1:
                        data_temp_in.append(temp["CURVAL"] / 10)
                    else:
                        data_temp_out.append(temp["CURVAL"] / 10)
                    # endif
                # endfor

                # ===============================================================================
                # save3DaysGraph(graphs_data_x, left_data_y, left_colors, right_data_y, right_colors, outFile)

                total_x_values = len(graphs_data_x)
                number_of_x_labels = int(total_x_values / 3)

                # print(number_of_x_labels)
                # ===============================================================================

                data_f = True
            else:
                data_f = False
            # endif
        else:
            data_f = False
        # endif

        left_data_y = {
            "food": data_food,
            "water": data_water,
        }

        right_data_y = {
            "temp_in": data_temp_in,
            "temp_out": data_temp_out,
        }

        return Response(
            {
                "id": id,
                "house": house,
                "tabs": getTabsData(tabs),
                "timestamp": str(time.time()),
                "data_f": data_f,
                "graphs_data_x": graphs_data_x,
                "left_data_y": left_data_y,
                "right_data_y": right_data_y,
                "total_x_values": total_x_values,
                "number_of_x_labels": number_of_x_labels,
            },
            200,
        )
    except Exception as e:
        print(e)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )
