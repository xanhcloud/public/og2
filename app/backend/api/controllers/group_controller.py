from collections import namedtuple
import datetime
import logging
import time
from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.response import Response
from rest_framework.decorators import (
    permission_classes,
    authentication_classes,
    api_view,
)

from api import abstractions
from api.utils.common import getTabsData
from api.utils.query_utils import (
    getBatchInfo,
    getBatchInfoList,
    getLatestBatchInfoList,
    getHenGroupList,
    getHenList,
    getLotIdByHouseId,
    getLotIdByHouseName,
    getLotList,
    getLotListNotUsed,
    insertBatchTable,
    insertHenGroup,
    updateBatchTable,
)


Color = namedtuple("Color", "red green blue")
Margin = namedtuple("Margin", "left right top bottom")


@api_view(["GET", "POST"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def hengrp_his(request):
    batchs = []
    sel_batch = -1
    p_hengrp = -1
    if request.method == "POST":
        data = request.data
        if "sel_batch" in data:
            sel_batch = int(data["sel_batch"])

        try:
            if sel_batch != -1:
                b_info = getBatchInfo(sel_batch)
                param_hengrp = b_info[0]["HENGRP_ID"]
            else:
                param_hengrp = data["hen_group"]
            # endif
            p_hengrp = int(param_hengrp)
            batchs = getBatchInfoList(param_hengrp)
            for batch in batchs:
                if batch["END_DATE"] == datetime.date(9999, 12, 31):
                    batch["END_DATE"] = "----------"
            # endif
        except Exception as e:
            print(e)
            return Response(
                {
                    "message": "Something awful happened!",
                    "detail": e.args,
                },
                400,
            )
        # endtry
    else:
        if request.query_params.get("sel_batch"):
            sel_batch = int(request.query_params.get("sel_batch"))
        if request.query_params.get("hen_group"):
            p_hengrp = int(request.query_params.get("hen_group"))
        if sel_batch != -1:
            b_info = getBatchInfo(sel_batch)
            param_hengrp = b_info[0]["HENGRP_ID"]
        else:
            param_hengrp = p_hengrp
        # endif
        # filter params
        params = {}
        # lot
        if request.query_params.get("lot_id"):
            params["lot_id"] = request.query_params.get("lot_id")
        # house id
        if request.query_params.get("house_id"):
            lot_id = getLotIdByHouseId(request.query_params.get("house_id"))
            if lot_id:
                params["lot_id"] = lot_id
        # start date
        if request.query_params.get("start_date"):
            params["start_date"] = request.query_params.get("start_date")
        # end date
        if request.query_params.get("end_date"):
            params["end_date"] = request.query_params.get("end_date")
        # feeding date
        if request.query_params.get("feeding_date"):
            params["feeding_date"] = request.query_params.get("feeding_date")
        # hen id
        if request.query_params.get("hen_id"):
            params["hen_id"] = int(request.query_params.get("hen_id"))
        # growth kind
        if request.query_params.get("growth_kind"):
            params["growth_kind"] = int(request.query_params.get("growth_kind"))
        # get batch
        batchs = getBatchInfoList(param_hengrp, params)
        for batch in batchs:
            if batch["END_DATE"] == datetime.date(9999, 12, 31):
                batch["END_DATE"] = "----------"
            # endif
    # endif
    hengrps = getHenGroupList()
    # house lot list
    lots = getLotList()
    # hen list
    hens = getHenList()
    try:
        return Response(
            {
                "batchs": batchs,
                "hengrps": hengrps,
                "p_hengrp": p_hengrp,
                "lots": lots,
                "hens": hens,
            }
        )
    except Exception as e:
        print(e)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )

@api_view(["GET", "POST"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def batch_reg(request):
    batchs = []
    sel_batch = -1
    p_hengrp = -1
    if request.method == "POST":
        data = request.data
        if "sel_batch" in data:
            sel_batch = int(data["sel_batch"])

        try:
            if sel_batch != -1:
                b_info = getBatchInfo(sel_batch)
                param_hengrp = b_info[0]["HENGRP_ID"]
            else:
                param_hengrp = data["hen_group"]
            # endif
            p_hengrp = int(param_hengrp)
            batchs = getBatchInfoList(param_hengrp)
            for batch in batchs:
                if batch["END_DATE"] == datetime.date(9999, 12, 31):
                    batch["END_DATE"] = "----------"
            # endif
        except Exception as e:
            print(e)
            return Response(
                {
                    "message": "Something awful happened!",
                    "detail": e.args,
                },
                400,
            )
        # endtry
    else:
        if request.query_params.get("sel_batch"):
            sel_batch = int(request.query_params.get("sel_batch"))
        if request.query_params.get("hen_group"):
            p_hengrp = int(request.query_params.get("hen_group"))
        if sel_batch != -1:
            b_info = getBatchInfo(sel_batch)
            param_hengrp = b_info[0]["HENGRP_ID"]
        else:
            param_hengrp = p_hengrp
        # endif
        # filter params
        params = {}
        # lot
        if request.query_params.get("lot_id"):
            params["lot_id"] = request.query_params.get("lot_id")
        # house id
        if request.query_params.get("house_id"):
            lot_id = getLotIdByHouseId(request.query_params.get("house_id"))
            if lot_id:
                params["lot_id"] = lot_id
        # start date
        if request.query_params.get("start_date"):
            params["start_date"] = request.query_params.get("start_date")
        # end date
        if request.query_params.get("end_date"):
            params["end_date"] = request.query_params.get("end_date")
        # feeding date
        if request.query_params.get("feeding_date"):
            params["feeding_date"] = request.query_params.get("feeding_date")
        # hen id
        if request.query_params.get("hen_id"):
            params["hen_id"] = int(request.query_params.get("hen_id"))
        # growth kind
        if request.query_params.get("growth_kind"):
            params["growth_kind"] = int(request.query_params.get("growth_kind"))
        # get batch
        # batchs = getBatchInfoList(param_hengrp, params)
        batchs = getLatestBatchInfoList(param_hengrp, params)
        for batch in batchs:
            if batch["END_DATE"] == datetime.date(9999, 12, 31):
                batch["END_DATE"] = ""
            # endif
    # endif
    hengrps = getHenGroupList()
    # house lot list
    lots = getLotList()
    # hen list
    hens = getHenList()
    try:
        return Response(
            {
                "batchs": batchs,
                "hengrps": hengrps,
                "p_hengrp": p_hengrp,
                "lots": lots,
                "hens": hens,
            }
        )
    except Exception as e:
        print(e)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )


@api_view(["GET", "POST"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def hengrp_upd(request):
    upd_batch = []
    if request.method == "POST":
        try:
            data = request.data
            # debugPrint("POST")
            batch_id = int(data["sel_batch"])
            hengrp_id = data["hengrp_id"]
            lot_id = data["lot_id"]
            hen_id = data["hen_id"]
            growth_kind = data["growth_kind"]
            start_date = data["start_date"]
            end_date = data["end_date"]
            if not end_date:
                # print("+*+*+*+*+*")
                end_date = "9999-12-31"
            initial_count = data["initial_count"]
            feeding_date = data["feeding_date"]
            # 2014-01-26
            eggcnt_stime = data["eggcnt_stime"]
            eggcnt_etime = data["eggcnt_etime"]
            upd_batch = [
                batch_id,
                hengrp_id,
                lot_id,
                hen_id,
                growth_kind,
                start_date,
                end_date,
                initial_count,
                feeding_date,
                eggcnt_stime,
                eggcnt_etime,
            ]

            updateBatchTable(upd_batch)
            # move to hengrp_his
            return Response({"status": "DONE", "batch": upd_batch})

        except Exception as e:
            print(e)
            return Response(
                {"message": "Something awful happened!", "detail": e.args}, 400
            )
        # endtry
    # endif
    if request.query_params.get("sel_batch") is not None:
        batch_id = int(request.query_params.get("sel_batch"))
    else:
        batch_id = -1
    batch = getBatchInfo(batch_id)
    lots = getLotList()
    hengrps = getHenGroupList()
    hens = getHenList()
    try:
        return Response(
            {
                "batch": batch,
                "lots": lots,
                "hengrps": hengrps,
                "hens": hens,
            }
        )
    except Exception as e:
        print(e)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )


@api_view(["GET", "POST"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def hengrp_reg(request):
    batchs = []
    new_batch = []
    r_msg = ""
    lots = getLotListNotUsed()
    # print(lots)
    hengrps = getHenGroupList()
    hens = getHenList()
    if request.method == "POST":
        try:
            data = request.data

            # debugPrint("POST")
            # Insert new hengrp
            hengrp_name = data["hengrp_name"]
            # debugPrint(hengrp_name)
            if hengrp_name != "" and hengrp_name is not None:
                hengrp_id = insertHenGroup(hengrp_name)
                if hengrp_id == 0:
                    r_msg = "Error: Duplicated henGroup name"
                    return Response(
                        {"hengrps": hengrps, "lots": lots, "hens": hens, "r_msg": r_msg}
                    )
            else:
                hengrp_id = data["hengrp_id"]
            # endif
            lot_id = data["lot_id"]
            hen_id = data["hen_id"]
            growth_kind = data["growth_kind"]
            start_date = data["start_date"]
            initial_count = data["initial_count"]
            feeding_date = data["feeding_date"]
            # 2014-01-26
            eggcnt_stime = data["eggcnt_stime"]
            eggcnt_etime = data["eggcnt_etime"]
            new_batch = [
                hengrp_id,
                lot_id,
                hen_id,
                growth_kind,
                start_date,
                initial_count,
                feeding_date,
                eggcnt_stime,
                eggcnt_etime,
            ]
            # print(new_batch)
            if insertBatchTable(new_batch) == 1:
                r_msg = "Completed  registration !!"
            else:
                r_msg = "Error: failed registration !"
        except Exception as e:
            print(e)
            return Response(
                {
                    "message": "Something awful happened!",
                    "detail": e.args,
                }
            )
        # endtry
    # endif

    return Response({"hengrps": hengrps, "lots": lots, "hens": hens, "r_msg": r_msg})
