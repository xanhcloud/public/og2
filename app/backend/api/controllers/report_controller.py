from collections import namedtuple
import datetime
import json
import logging
import time
from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.response import Response
from rest_framework.decorators import (
    permission_classes,
    authentication_classes,
    api_view,
)

from api import abstractions
from api.utils.common import getSetting, getTabsData, saveSetting
from api.utils.fileUtils import (
    getDailyViewItem,
    getWeeklyViewItem,
    loadDailyViewItem,
    loadFileSettings,
    loadWeeklyViewItem,
    saveDailyViewItem,
    saveWeeklyViewItem,
)
from api.utils.query_utils import (
    getBatchHisInfoByLotID,
    getBatchInfoByLotID,
    getBatchInfoList,
    getDailyEggCtrList,
    getDailyEggCtrListPeriods,
    getDailyHistTable,
    getDailyPastTable,
    getDailyPastTableByBatchID,
    getDailyTable,
    getDailyTablePeriods,
    getEggCtrInfo,
    getEggInformation,
    getHengrpInfoByLotID,
    getHouseNameByLotID,
    getLotList,
    getTodayDailyTable,
    getTodayEggCtrList,
    getTodayTotalEggCount,
    getTotalEggCountByDate,
    getWeeklyHistTable,
    getWeeklyPastTable,
    getWeeklyPastTableByBatchID,
    getWeeklyTable,
    getWeeklyTablePeriods,
    updateEggInformation,
    updateSummaryEggData,
)


Color = namedtuple("Color", "red green blue")
Margin = namedtuple("Margin", "left right top bottom")


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def dailytable(request, id=1):
    DAILY_TABLE_LINES = 1000

    try:
        house = getHouseNameByLotID(id)
        lots = getLotList()
        info = loadDailyViewItem()
        rows = []
        tabs = []
        today_rows = []
        growth1 = False
        growth2 = False
        growth3 = False
        growth = ""

        for lot in lots:
            batch = getBatchInfoByLotID(int(lot["LOT_ID"]))
            tab = abstractions.Tab(
                int(lot["LOT_ID"]), lot["HOUSE_NAME"], False, batch, "dailytable"
            )
            tab.visibility = False
            if batch:
                tab.visibility = True
            tabs.append(tab)

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        if current_tab.batch:
            batch_curr = current_tab.batch[2]
            growth_kind = current_tab.batch[0]

            if growth_kind == 1:
                growth = "brooder"
            elif growth_kind == 2:
                growth = "pullet"
            else:
                growth = "layer"

            hen_group_information = getHengrpInfoByLotID(id)
            batches = getBatchInfoList(hen_group_information[0])

            for batch in batches:
                if int(batch["GROWTH_KIND"]) == 1:
                    growth1 = True
                    growth == "brooder"
                    growth_kind == 1
                elif int(batch["GROWTH_KIND"]) == 2:
                    growth2 = True
                    growth == "pullet"
                    growth_kind == 2
                elif int(batch["GROWTH_KIND"]) == 3:
                    growth3 = True
                    growth == "layer"
                    growth_kind == 3

            if request.method == "GET":

                batch_id = -1
                for batch in batches:
                    if batch["GROWTH_KIND"] == growth_kind:
                        batch_id = batch["BATCH_ID"]
                        break

                if batch_id == batch_curr:
                    rows = getDailyTable(id, DAILY_TABLE_LINES)
                else:
                    rows = getDailyHistTable(batch_id)

            today_rows = getTodayDailyTable(id)

            data_f = True
        else:
            data_f = False
        # endif

        return Response(
            {
                "id": id,
                # "tabs": tabs,
                "tabs": getTabsData(tabs),
                "rows": rows,
                "today_rows": today_rows,
                "house": house,
                "items": info[0],
                "flags": info[1],
                "growth": growth,
                "growth1": growth1,
                "growth2": growth2,
                "growth3": growth3,
                "data_f": data_f,
            }
        )
    except:
        logging.exception("Something awful happened!")

    return Response({})


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def setup_dailytable(request):
    category = "report"
    name = "dailytable"
    user_id = request.query_params.get("user_id")
    # items = getDailyViewItem()
    setting = getSetting(category, name, user_id)
    if setting:
        items = json.loads(setting["VALUE"])
    else:
        items = {}
    return Response(
        {
            "items": items,
        }
    )


@api_view(["POST"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def save_setup_dailytable(request):
    category = "report"
    name = "dailytable"
    data = request.data
    user_id = data.get("user_id")
    try:
        # saveDailyViewItem(data)
        # items = getDailyViewItem()
        saveSetting(category, name, json.dumps(data.get("items")), user_id)
        setting = getSetting(category, name, user_id)
        if setting:
            items = json.loads(setting["VALUE"])
        else:
            items = {}
        return Response(
            {
                "items": items,
            }
        )
    except Exception as e:
        print(e)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def d_comparison(request, id=1):
    DAILY_TABLE_LINES = 1000
    try:
        house = getHouseNameByLotID(id)
        lots = getLotList()
        info = loadDailyViewItem()
        tabs = []
        nrows = []
        tdate_c = ""
        tdate_p = ""
        fdate_c = ""
        fdate_p = ""
        exist_f = False
        comparison_batch_id = request.query_params.get("batch_id")

        for lot in lots:
            batch = getDailyTable(lot["LOT_ID"], DAILY_TABLE_LINES)
            tab = abstractions.Tab(
                int(lot["LOT_ID"]), lot["HOUSE_NAME"], False, batch, "d_comparison"
            )
            tab.visibility = False
            if batch:
                tab.visibility = True
            tabs.append(tab)

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        rows = current_tab.batch

        if len(rows) > 0:
            days = len(rows)

            day_p_s = rows[days - 1]["DAY_PERIODS"]
            day_p_e = rows[0]["DAY_PERIODS"]
            if comparison_batch_id:
                rows_p = getDailyPastTableByBatchID(comparison_batch_id, day_p_s, day_p_e)
            else:
                rows_p = getDailyPastTable(id, day_p_s, day_p_e)

            mrows = []
            if len(rows_p) > 0:

                for i in range(len(rows)):
                    mrow = []
                    mrow.append(rows[i])
                    exist_f = False
                    for j in range(len(rows_p)):
                        if rows[i]["DAY_PERIODS"] == rows_p[j]["DAY_PERIODS"]:
                            mrow.append(rows_p[j])
                            exist_f = True
                            break

                    if not exist_f:
                        mrow.append([])

                    nrows.append(mrow)

                tdate_c = rows[0]["DAILY_DATE"]
                fdate_c = rows[len(rows) - 1]["DAILY_DATE"]
                tdate_p = rows_p[0]["DAILY_DATE"]
                fdate_p = rows_p[len(rows_p) - 1]["DAILY_DATE"]
            else:
                mlen = len(rows)
                for i in range(len(rows)):
                    mrow = []
                    mrow.append(rows[i])
                    nrows.append(mrow)

                tdate_c = nrows[0][0]["DAILY_DATE"]
                fdate_c = nrows[mlen - 1][0]["DAILY_DATE"]
                fdate_p = ""
                tdate_p = ""

        return Response(
            {
                "id": id,
                "tabs": getTabsData(tabs),
                "nrows": nrows,
                "house": house,
                "items": info[0],
                "flags": info[1],
                "from_date_c": fdate_c,
                "to_date_c": tdate_c,
                "from_date_p": fdate_p,
                "to_date_p": tdate_p,
                "exist_f": exist_f,
            }
        )
    except Exception as error:
        print("d_comparison error:", error)
        return Response({"message": "Something awful happened!"}, 400)


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def d_spec_after(request, id=1):
    try:
        tabs = []
        info = loadDailyViewItem()
        lots = getLotList()

        for lot in lots:
            batch = getBatchInfoByLotID(lot["LOT_ID"])
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "d_specification"
            )
            tab.visibility = False
            if batch:
                tab.visibility = True
            tabs.append(tab)

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        from_date = request.query_params.get("from_date")
        if from_date is None:
            from_date = datetime.datetime.today().strftime("%Y-%m-%d")

        to_date = request.query_params.get("to_date")
        if to_date is None:
            to_date = datetime.datetime.today().strftime("%Y-%m-%d")

        rows = getDailyTablePeriods(id, from_date, to_date)

        house = getHouseNameByLotID(id)
        info = loadDailyViewItem()

        try:
            return Response(
                {
                    "id": id,
                    "tabs": getTabsData(tabs),
                    "rows": rows,
                    "house": house,
                    "from_date": from_date,
                    "to_date": to_date,
                    "items": info[0],
                    "flags": info[1],
                }
            )
        except Exception as error:
            print("d_spec_after error:", error)
            return Response({"message": "Something awful happened!"}, 400)

    except Exception as error:
        print("d_spec_after error:", error)
        return Response({"message": "Something awful happened!"}, 400)


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def weeklytable(request, id=1):
    DAILY_TABLE_LINES = 1000

    try:
        house = getHouseNameByLotID(id)
        lots = getLotList()
        info = loadWeeklyViewItem()
        rows = []
        growth1 = False
        growth2 = False
        growth3 = False
        growth = ""
        tabs = []

        for lot in lots:
            batch = getBatchInfoByLotID(lot["LOT_ID"])
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "weeklytable"
            )
            tab.visibility = False
            if batch:
                tab.visibility = True
            tabs.append(tab)

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        batch = current_tab.batch

        if batch != None:
            batch_curr = batch[2]
            growth_kind = batch[0]
            if growth_kind == 1:
                growth = "Brooder"
            elif growth_kind == 2:
                growth = "Pullet"
            else:
                growth = "Layer"
            # endif
            ##        rows = getWeeklyTable(id, DAILY_TABLE_LINES)

            hengrp_info = getHengrpInfoByLotID(id)
            batchs = getBatchInfoList(hengrp_info[0])

            if request.method == "GET":
                if request.query_params.get("growth1") is not None:
                    growth = "Brooder"
                    growth_kind = 1
                elif request.query_params.get("growth2") is not None:
                    growth = "Pullet"
                    growth_kind = 2
                else:
                    growth = "Layer"
                    growth_kind = 3

                batch_id = False

                for batch in batchs:
                    if batch["GROWTH_KIND"] == growth_kind:
                        print(batch["BATCH_ID"])
                        batch_id = batch["BATCH_ID"]
                        break

                if batch_id == False:
                    batch_id = batchs[0]["BATCH_ID"]

                if batch_id == batch_curr:
                    rows = getWeeklyTable(id, DAILY_TABLE_LINES)
                else:
                    rows = getWeeklyHistTable(batch_id)
                # endif
                # print(rows)
            # endif

            # Get weight chage rate
            w_weight = 0
            # print(len(rows))
            for i in range(len(rows)):
                row = rows[len(rows) - i - 1]
                # 2014-01-07
                # if w_weight == 0:
                if w_weight == 0 or not row["HEN_WEIGHT"]:
                    w = 0
                else:
                    w = row["HEN_WEIGHT"] - w_weight
                # endif
                w_weight = row["HEN_WEIGHT"]
                # print(w_weight)
                row["WEIGHTCHG_RATE"] = round(w, 1)
                # case when (EGG_COUNT > 0 and CURR_COUNT > 0) then ROUND(AVG(dbt_summary.EGG_WEIGHT / 10) * EGG_COUNT / CURR_COUNT, 1) else 0 end AS EGG_VOL
                if row["CURR_COUNT"] > 0:
                    row["EGG_VOL"] = round(row["EGG_WEIGHT"] * row["EGG_COUNT"] / row["CURR_COUNT"], 1)
                else:
                    row["EGG_VOL"] = 0
                # case when (CURR_COUNT > 0) then ROUND(FOOD_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS FOOD_AMOUNT_U
                if row["CURR_COUNT"] > 0:
                    row["FOOD_AMOUNT_U"] = round(row["FOOD_AMOUNT"] / row["CURR_COUNT"] * 1000, 1)
                else:
                    row["FOOD_AMOUNT_U"] = 0
                # case when (INITIAL_COUNT > 0) then ROUND(CURR_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS SURVIVAL_RATE
                if row["INITIAL_COUNT"] > 0:
                    row["SURVIVAL_RATE"] = round(row["CURR_COUNT"] / row["INITIAL_COUNT"] * 100, 1)
                else:
                    row["SURVIVAL_RATE"] = 0
                # case when (CURR_COUNT > 0) then ROUND(EGG_COUNT / CURR_COUNT * 100, 1) else 0 end AS EGG_RATE_CUR
                if row["CURR_COUNT"] > 0:
                    row["EGG_RATE_CUR"] = round(row["EGG_COUNT"] / row["CURR_COUNT"] * 100, 1)
                else:
                    row["EGG_RATE_CUR"] = 0
                # case when (INITIAL_COUNT > 0) then ROUND(EGG_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS EGG_RATE_INI
                if row["INITIAL_COUNT"] > 0:
                    row["EGG_RATE_INI"] = round(row["EGG_COUNT"] / row["INITIAL_COUNT"] * 100, 1)
                else:
                    row["EGG_RATE_INI"] = 0
                # case when (dbt_summary.HEN_WEIGHT > 0 and FOOD_AMOUNT > 0 and CURR_COUNT > 0) 
                # then ROUND(AVG(dbt_summary.HEN_WEIGHT) / (FOOD_AMOUNT / CURR_COUNT * 1000), 1) 
                # else 0 end AS FOOD_EFFICEINCY
                if row["HEN_WEIGHT"] > 0 and row["FOOD_AMOUNT"] > 0 and row["CURR_COUNT"] > 0:
                    row["FOOD_EFFICEINCY"] = round(row["HEN_WEIGHT"] / (row["FOOD_AMOUNT"] / row["CURR_COUNT"] * 1000), 1)
                else:
                    row["FOOD_EFFICEINCY"] = 0
                # case when (EGG_COUNT > 0 and dbt_summary.EGG_WEIGHT > 0) then ROUND(FOOD_AMOUNT / (EGG_COUNT * AVG(dbt_summary.EGG_WEIGHT / 10)) * 1000, 1) else 0 end AS FOOD_DEMAND
                if row["EGG_COUNT"] > 0 and row["EGG_WEIGHT"] > 0:
                    row["FOOD_DEMAND"] = round(row["FOOD_AMOUNT"] / (row["EGG_COUNT"] * row["EGG_WEIGHT"]) * 1000, 1)
                else:
                    row["FOOD_DEMAND"] = 0
                # case when (CURR_COUNT > 0) then ROUND(WATER_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS WATER_AMOUNT_U
                if row["CURR_COUNT"] > 0:
                    row["WATER_AMOUNT_U"] = round(row["WATER_AMOUNT"] / row["CURR_COUNT"] * 1000, 1)
                else:
                    row["WATER_AMOUNT_U"] = 0
                # print(row['WEIGHTCHG_RATE'])
            # endfor
            """
            # Get weight chage rate
            w_weight = 0
            weightchg_rate = []
            for row in rows:
                if w_weight == 0:
                    w = 100
                else:
                    w = (w_weight - row['HEN_WEIGHT'])/100
                #endif
                row['WEIGHTCHG_RATE'] = round(w, 1)
                w_weight = row['HEN_WEIGHT']
            #endfor
            #print(rows)
            """

            for batch in batchs:
                if batch["GROWTH_KIND"] == 1:
                    growth1 = True
                elif batch["GROWTH_KIND"] == 2:
                    growth2 = True
                elif batch["GROWTH_KIND"] == 3:
                    growth3 = True
                # endif
            # endfor
            data_f = True
        else:
            data_f = False
        # endif

        return Response(
            {
                "id": id,
                "tabs": getTabsData(tabs),
                "rows": rows,
                "house": house,
                "items": info[0],
                "flags": info[1],
                "growth": growth,
                "growth1": growth1,
                "growth2": growth2,
                "growth3": growth3,
                "data_f": data_f,
            }
        )
    except Exception as e:
        print(e)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def w_comparison(request, id=1):
    DAILY_TABLE_LINES = 1000
    try:
        house = getHouseNameByLotID(id)
        lots = getLotList()
        info = loadWeeklyViewItem()

        nrows = []
        tdate_c = tdate_p = ""
        fdate_c = fdate_p = ""

        tabs = []
        comparison_batch_id = request.query_params.get("batch_id")

        for lot in lots:
            batch = getWeeklyTable(lot["LOT_ID"], DAILY_TABLE_LINES)
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "/w_comparison"
            )
            tab.visibility = False
            if batch:
                tab.visibility = True
            tabs.append(tab)

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        rows = current_tab.batch

        if len(rows) > 0:
            days = len(rows)
            # print("days=%s" % (days))

            # Get weight chage rate
            w_weight = 0
            # print(len(rows))
            for i in range(len(rows)):
                row = rows[len(rows) - i - 1]
                if w_weight == 0:
                    w = 0
                else:
                    #                    w = (row['HEN_WEIGHT']-w_weight)/100
                    w = row["HEN_WEIGHT"] - w_weight
                # endif
                w_weight = row["HEN_WEIGHT"]
                # print(w_weight)
                row["WEIGHTCHG_RATE"] = round(w, 1)
                # case when (EGG_COUNT > 0 and CURR_COUNT > 0) then ROUND(AVG(dbt_summary.EGG_WEIGHT / 10) * EGG_COUNT / CURR_COUNT, 1) else 0 end AS EGG_VOL
                if row["CURR_COUNT"] > 0:
                    row["EGG_VOL"] = round(row["EGG_WEIGHT"] * row["EGG_COUNT"] / row["CURR_COUNT"], 1)
                else:
                    row["EGG_VOL"] = 0
                # case when (CURR_COUNT > 0) then ROUND(FOOD_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS FOOD_AMOUNT_U
                if row["CURR_COUNT"] > 0:
                    row["FOOD_AMOUNT_U"] = round(row["FOOD_AMOUNT"] / row["CURR_COUNT"] * 1000, 1)
                else:
                    row["FOOD_AMOUNT_U"] = 0
                # case when (INITIAL_COUNT > 0) then ROUND(CURR_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS SURVIVAL_RATE
                if row["INITIAL_COUNT"] > 0:
                    row["SURVIVAL_RATE"] = round(row["CURR_COUNT"] / row["INITIAL_COUNT"] * 100, 1)
                else:
                    row["SURVIVAL_RATE"] = 0
                # case when (CURR_COUNT > 0) then ROUND(EGG_COUNT / CURR_COUNT * 100, 1) else 0 end AS EGG_RATE_CUR
                if row["CURR_COUNT"] > 0:
                    row["EGG_RATE_CUR"] = round(row["EGG_COUNT"] / row["CURR_COUNT"] * 100, 1)
                else:
                    row["EGG_RATE_CUR"] = 0
                # case when (INITIAL_COUNT > 0) then ROUND(EGG_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS EGG_RATE_INI
                if row["INITIAL_COUNT"] > 0:
                    row["EGG_RATE_INI"] = round(row["EGG_COUNT"] / row["INITIAL_COUNT"] * 100, 1)
                else:
                    row["EGG_RATE_INI"] = 0
                # case when (dbt_summary.HEN_WEIGHT > 0 and FOOD_AMOUNT > 0 and CURR_COUNT > 0) 
                # then ROUND(AVG(dbt_summary.HEN_WEIGHT) / (FOOD_AMOUNT / CURR_COUNT * 1000), 1) 
                # else 0 end AS FOOD_EFFICEINCY
                if row["HEN_WEIGHT"] > 0 and row["FOOD_AMOUNT"] > 0 and row["CURR_COUNT"] > 0:
                    row["FOOD_EFFICEINCY"] = round(row["HEN_WEIGHT"] / (row["FOOD_AMOUNT"] / row["CURR_COUNT"] * 1000), 1)
                else:
                    row["FOOD_EFFICEINCY"] = 0
                # case when (EGG_COUNT > 0 and dbt_summary.EGG_WEIGHT > 0) then ROUND(FOOD_AMOUNT / (EGG_COUNT * AVG(dbt_summary.EGG_WEIGHT / 10)) * 1000, 1) else 0 end AS FOOD_DEMAND
                if row["EGG_COUNT"] > 0 and row["EGG_WEIGHT"] > 0:
                    row["FOOD_DEMAND"] = round(row["FOOD_AMOUNT"] / (row["EGG_COUNT"] * row["EGG_WEIGHT"]) * 1000, 1)
                else:
                    row["FOOD_DEMAND"] = 0
                # case when (CURR_COUNT > 0) then ROUND(WATER_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS WATER_AMOUNT_U
                if row["CURR_COUNT"] > 0:
                    row["WATER_AMOUNT_U"] = round(row["WATER_AMOUNT"] / row["CURR_COUNT"] * 1000, 1)
                else:
                    row["WATER_AMOUNT_U"] = 0
                # print(row['WEIGHTCHG_RATE'])
            # endfor
            # print("rows=%s" % (rows))

            # 2013-10-16 Get range of weekly_periods
            week_p_s = rows[days - 1]["WEEK_PERIODS"]
            week_p_e = rows[0]["WEEK_PERIODS"]
            print(week_p_s)
            print(week_p_e)
            if comparison_batch_id:
                rows_p = getWeeklyPastTableByBatchID(comparison_batch_id, week_p_s, week_p_e)
            else:
                rows_p = getWeeklyPastTable(id, week_p_s, week_p_e)
            # print("rows_p=%s" % (rows_p))

            # Get weight chage rate
            w_weight = 0
            for i in range(len(rows_p)):
                row_p = rows_p[len(rows_p) - i - 1]
                if w_weight == 0:
                    w = 0
                else:
                    #                    w = (row_p['HEN_WEIGHT']-w_weight)/100
                    w = row_p["HEN_WEIGHT"] - w_weight
                # endif
                w_weight = row_p["HEN_WEIGHT"]
                # print(w_weight)
                row_p["WEIGHTCHG_RATE"] = round(w, 1)
                # case when (EGG_COUNT > 0 and CURR_COUNT > 0) then ROUND(AVG(dbt_summary.EGG_WEIGHT / 10) * EGG_COUNT / CURR_COUNT, 1) else 0 end AS EGG_VOL
                if row_p["CURR_COUNT"] > 0:
                    row_p["EGG_VOL"] = round(row_p["EGG_WEIGHT"] * row_p["EGG_COUNT"] / row_p["CURR_COUNT"], 1)
                else:
                    row_p["EGG_VOL"] = 0
                # case when (CURR_COUNT > 0) then ROUND(FOOD_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS FOOD_AMOUNT_U
                if row_p["CURR_COUNT"] > 0:
                    row_p["FOOD_AMOUNT_U"] = round(row_p["FOOD_AMOUNT"] / row_p["CURR_COUNT"] * 1000, 1)
                else:
                    row_p["FOOD_AMOUNT_U"] = 0
                # case when (INITIAL_COUNT > 0) then ROUND(CURR_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS SURVIVAL_RATE
                if row_p["INITIAL_COUNT"] > 0:
                    row_p["SURVIVAL_RATE"] = round(row_p["CURR_COUNT"] / row_p["INITIAL_COUNT"] * 100, 1)
                else:
                    row_p["SURVIVAL_RATE"] = 0
                # case when (CURR_COUNT > 0) then ROUND(EGG_COUNT / CURR_COUNT * 100, 1) else 0 end AS EGG_RATE_CUR
                if row_p["CURR_COUNT"] > 0:
                    row_p["EGG_RATE_CUR"] = round(row_p["EGG_COUNT"] / row_p["CURR_COUNT"] * 100, 1)
                else:
                    row_p["EGG_RATE_CUR"] = 0
                # case when (INITIAL_COUNT > 0) then ROUND(EGG_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS EGG_RATE_INI
                if row_p["INITIAL_COUNT"] > 0:
                    row_p["EGG_RATE_INI"] = round(row_p["EGG_COUNT"] / row_p["INITIAL_COUNT"] * 100, 1)
                else:
                    row_p["EGG_RATE_INI"] = 0
                # case when (dbt_summary.HEN_WEIGHT > 0 and FOOD_AMOUNT > 0 and CURR_COUNT > 0) 
                # then ROUND(AVG(dbt_summary.HEN_WEIGHT) / (FOOD_AMOUNT / CURR_COUNT * 1000), 1) 
                # else 0 end AS FOOD_EFFICEINCY
                if row_p["HEN_WEIGHT"] > 0 and row_p["FOOD_AMOUNT"] > 0 and row_p["CURR_COUNT"] > 0:
                    row_p["FOOD_EFFICEINCY"] = round(row_p["HEN_WEIGHT"] / (row_p["FOOD_AMOUNT"] / row_p["CURR_COUNT"] * 1000), 1)
                else:
                    row_p["FOOD_EFFICEINCY"] = 0
                # case when (EGG_COUNT > 0 and dbt_summary.EGG_WEIGHT > 0) then ROUND(FOOD_AMOUNT / (EGG_COUNT * AVG(dbt_summary.EGG_WEIGHT / 10)) * 1000, 1) else 0 end AS FOOD_DEMAND
                if row_p["EGG_COUNT"] > 0 and row_p["EGG_WEIGHT"] > 0:
                    row_p["FOOD_DEMAND"] = round(row_p["FOOD_AMOUNT"] / (row_p["EGG_COUNT"] * row_p["EGG_WEIGHT"]) * 1000, 1)
                else:
                    row_p["FOOD_DEMAND"] = 0
                # case when (CURR_COUNT > 0) then ROUND(WATER_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS WATER_AMOUNT_U
                if row_p["CURR_COUNT"] > 0:
                    row_p["WATER_AMOUNT_U"] = round(row_p["WATER_AMOUNT"] / row_p["CURR_COUNT"] * 1000, 1)
                else:
                    row_p["WATER_AMOUNT_U"] = 0
                # print(row_p['WEIGHTCHG_RATE'])
            # endfor
            # print(rows_p)

            nrows = []
            if len(rows_p) > 0:
                # 2013-10-16
                for i in range(len(rows)):
                    mrow = []
                    mrow.append(rows[i])
                    exist_f = False
                    for j in range(len(rows_p)):
                        if rows[i]["WEEK_PERIODS"] == rows_p[j]["WEEK_PERIODS"]:
                            mrow.append(rows_p[j])
                            exist_f = True
                            break
                        # endif
                    # endfor
                    if not exist_f:
                        mrow.append([])
                    # endif
                    nrows.append(mrow)
                # endfor
                tdate_c = rows[0]["WEEKLY_DATE"]
                fdate_c = rows[len(rows) - 1]["WEEKLY_DATE"]
                tdate_p = rows_p[0]["WEEKLY_DATE"]
                fdate_p = rows_p[len(rows_p) - 1]["WEEKLY_DATE"]
            else:
                # ToDo
                for i in range(len(rows)):
                    mrow = []
                    mrow.append(rows[i])
                    nrows.append(mrow)
                # endfor
                tdate_c = nrows[0][0]["WEEKLY_DATE"]
                fdate_c = nrows[len(rows) - 1][0]["WEEKLY_DATE"]
                fdate_p = ""
                tdate_p = ""
            # endfor
            # print(nrows)
        # endif

        return Response(
            {
                "id": id,
                "tabs": getTabsData(tabs),
                "nrows": nrows,
                "house": house,
                "items": info[0],
                "flags": info[1],
                "from_date_c": fdate_c,
                "to_date_c": tdate_c,
                "from_date_p": fdate_p,
                "to_date_p": tdate_p,
            }
        )
    except Exception as e:
        print(e)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def w_spec_after(request, id=1):
    try:

        lots = getLotList()
        tabs = []

        for lot in lots:
            batch = getBatchInfoByLotID(lot["LOT_ID"])
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "/w_spec_after"
            )
            tab.visibility = False
            if batch:
                tab.visibility = True
            tabs.append(tab)

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        from_week = request.query_params.get("from_week")
        if from_week is None:
            from_week = 1
        # endif
        to_week = request.query_params.get("to_week")
        if to_week is None:
            to_week = 100

        # print(from_week, to_week)
        rows = getWeeklyTablePeriods(id, from_week, to_week)
        # print(rows)

        # Get weight chage rate
        w_weight = 0
        # print(len(rows))
        for i in range(len(rows)):
            row = rows[len(rows) - i - 1]
            if w_weight == 0:
                w = 0
            else:
                #                w = (row['HEN_WEIGHT']-w_weight)/100
                w = row["HEN_WEIGHT"] - w_weight
            # endif
            w_weight = row["HEN_WEIGHT"]
            # print(w_weight)
            row["WEIGHTCHG_RATE"] = round(w, 1)
            # case when (EGG_COUNT > 0 and CURR_COUNT > 0) then ROUND(AVG(dbt_summary.EGG_WEIGHT / 10) * EGG_COUNT / CURR_COUNT, 1) else 0 end AS EGG_VOL
            if row["CURR_COUNT"] > 0:
                row["EGG_VOL"] = round(row["EGG_WEIGHT"] * row["EGG_COUNT"] / row["CURR_COUNT"], 1)
            else:
                row["EGG_VOL"] = 0
            # case when (CURR_COUNT > 0) then ROUND(FOOD_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS FOOD_AMOUNT_U
            if row["CURR_COUNT"] > 0:
                row["FOOD_AMOUNT_U"] = round(row["FOOD_AMOUNT"] / row["CURR_COUNT"] * 1000, 1)
            else:
                row["FOOD_AMOUNT_U"] = 0
            # case when (INITIAL_COUNT > 0) then ROUND(CURR_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS SURVIVAL_RATE
            if row["INITIAL_COUNT"] > 0:
                row["SURVIVAL_RATE"] = round(row["CURR_COUNT"] / row["INITIAL_COUNT"] * 100, 1)
            else:
                row["SURVIVAL_RATE"] = 0
            # case when (CURR_COUNT > 0) then ROUND(EGG_COUNT / CURR_COUNT * 100, 1) else 0 end AS EGG_RATE_CUR
            if row["CURR_COUNT"] > 0:
                row["EGG_RATE_CUR"] = round(row["EGG_COUNT"] / row["CURR_COUNT"] * 100, 1)
            else:
                row["EGG_RATE_CUR"] = 0
            # case when (INITIAL_COUNT > 0) then ROUND(EGG_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS EGG_RATE_INI
            if row["INITIAL_COUNT"] > 0:
                row["EGG_RATE_INI"] = round(row["EGG_COUNT"] / row["INITIAL_COUNT"] * 100, 1)
            else:
                row["EGG_RATE_INI"] = 0
            # case when (dbt_summary.HEN_WEIGHT > 0 and FOOD_AMOUNT > 0 and CURR_COUNT > 0) 
            # then ROUND(AVG(dbt_summary.HEN_WEIGHT) / (FOOD_AMOUNT / CURR_COUNT * 1000), 1) 
            # else 0 end AS FOOD_EFFICEINCY
            if row["HEN_WEIGHT"] > 0 and row["FOOD_AMOUNT"] > 0 and row["CURR_COUNT"] > 0:
                row["FOOD_EFFICEINCY"] = round(row["HEN_WEIGHT"] / (row["FOOD_AMOUNT"] / row["CURR_COUNT"] * 1000), 1)
            else:
                row["FOOD_EFFICEINCY"] = 0
            # case when (EGG_COUNT > 0 and dbt_summary.EGG_WEIGHT > 0) then ROUND(FOOD_AMOUNT / (EGG_COUNT * AVG(dbt_summary.EGG_WEIGHT / 10)) * 1000, 1) else 0 end AS FOOD_DEMAND
            if row["EGG_COUNT"] > 0 and row["EGG_WEIGHT"] > 0:
                row["FOOD_DEMAND"] = round(row["FOOD_AMOUNT"] / (row["EGG_COUNT"] * row["EGG_WEIGHT"]) * 1000, 1)
            else:
                row["FOOD_DEMAND"] = 0
            # case when (CURR_COUNT > 0) then ROUND(WATER_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS WATER_AMOUNT_U
            if row["CURR_COUNT"] > 0:
                row["WATER_AMOUNT_U"] = round(row["WATER_AMOUNT"] / row["CURR_COUNT"] * 1000, 1)
            else:
                row["WATER_AMOUNT_U"] = 0
            # print(row['WEIGHTCHG_RATE'])
        # endfor

        lots = getLotList()
        house = getHouseNameByLotID(id)
        info = loadWeeklyViewItem()
        # print(info)
        try:
            return Response(
                {
                    "id": id,
                    "tabs": getTabsData(tabs),
                    "rows": rows,
                    "house": house,
                    "from_week": from_week,
                    "to_week": to_week,
                    "items": info[0],
                    "flags": info[1],
                }
            )
        except Exception as e:
            print(e)
            return Response(
                {
                    "message": "Something awful happened!",
                    "detail": e.args,
                },
                400,
            )

    except Exception as e2:
        print(e2)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e2.args,
            },
            400,
        )


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def w_history(request, id=1):
    try:
        house = getHouseNameByLotID(id)
        info = loadDailyViewItem()

        lots = getLotList()
        tabs = []

        for lot in lots:
            batch = getBatchInfoByLotID(lot["LOT_ID"])
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "/w_history"
            )
            tab.visibility = False
            if batch:
                tab.visibility = True
            tabs.append(tab)

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        # get batch of growth_kind=3
        batchs = getBatchHisInfoByLotID(id)
        if len(batchs) > 0:
            data_f = True
        else:
            data_f = False
        # endif

        return Response(
            {
                "tabs": getTabsData(tabs),
                "id": id,
                "house": house,
                "batchs": batchs,
                "data_f": data_f,
            }
        )
    except Exception as e:
        print(e)
        return Response(
            {
                "message": "Something awful happened!",
                "detail": e.args,
            },
            400,
        )


@api_view(["GET", "POST"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def w_setup(request):
    category = "report"
    name = "weeklytable"
    if request.method == "POST":
        data = request.data
        user_id = data.get("user_id")
        try:
            # saveWeeklyViewItem(data)
            # items = getWeeklyViewItem()
            saveSetting(category, name, json.dumps(data.get("items")), user_id)
            setting = getSetting(category, name, user_id)
            if setting:
                items = json.loads(setting["VALUE"])
            else:
                items = {}
            return Response(
                {
                    "items": items,
                }
            )
        except Exception as e:
            print(e)
            return Response(
                {
                    "message": "Something awful happened!",
                    "detail": e.args,
                },
                400,
            )
    else:
        # items = getWeeklyViewItem()
        user_id = request.query_params.get("user_id")
        setting = getSetting(category, name, user_id)
        if setting:
            items = json.loads(setting["VALUE"])
        else:
            items = {}
        return Response({"items": items})


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def eggtable(request, id=1):
    config = loadFileSettings()
    camera_section = config["camera"]
    alarm_section = config["alarm"]
    g_AlarmEggCount = alarm_section["eggcount"]

    lots = getLotList()
    tabs = []

    for lot in lots:
        batch = getBatchInfoByLotID(lot["LOT_ID"])
        tab = abstractions.Tab(
            lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "/eggtable"
        )
        tab.visibility = False
        if batch:
            tab.visibility = True
        tabs.append(tab)

    current_tab = tabs[0]

    for tab in tabs:
        if tab.id == id:
            current_tab = tab
            break

    if current_tab.visibility == False:
        for tab in tabs:
            if tab.visibility == True:
                current_tab = tab
                break

    id = current_tab.id

    today = request.query_params.get("date")
    if today is None:
        today = datetime.datetime.today().strftime("%Y-%m-%d")

    house = getHouseNameByLotID(id)
    total_ecount = getTotalEggCountByDate(0, today)
    lot_ecount = getTotalEggCountByDate(id, today)
    r_infos = getEggCtrInfo(id, 0)
    rnum = len(r_infos)
    t_infos = getEggCtrInfo(id, 1)
    tnum = len(t_infos)
    eggs = getTodayEggCtrList(id, today)
    number_of_cages = rnum * tnum
    number_of_hens = 0
    lot_laying_rate = 0

    teggs = [[0 for j in range(tnum)] for i in range(rnum)]
    laying_rates = [[0 for j in range(tnum)] for i in range(rnum)]

    max_count = 0
    min_count = 99999
    for egg in eggs:
        r = egg["CAGE_ROW"] - 1
        t = tnum - egg["CAGE_TIER"]
        teggs[r][t] = egg["EGG_COUNT"]
        # laying rate
        if egg["CURR_COUNT"] > 0:
            laying_rates[r][t] = round(egg["EGG_COUNT"] / (egg["CURR_COUNT"] / number_of_cages) * 100, 1)
        else:
            laying_rates[r][t] = 0

        if egg["EGG_COUNT"] > max_count:
            max_count = egg["EGG_COUNT"]
        if egg["EGG_COUNT"] < min_count:
            min_count = egg["EGG_COUNT"]

        number_of_hens = egg["CURR_COUNT"]
        # print(teggs[r][t])
    # endfor
    # print(teggs)
    # print(min_count)
    # print(max_count)

    # Get Total row/teir EggCount
    r_eggs = [0 for i in range(rnum)]
    r_laying_rates = [0 for i in range(rnum)]
    for i in range(rnum):
        sum = 0
        for j in range(tnum):
            sum = sum + teggs[i][j]
        # endfor
        r_eggs[i] = sum
        if number_of_hens > 0:
            r_laying_rates[i] = round(sum / (number_of_hens / rnum) * 100, 1)
    # endfor
    # print(r_eggs)
    t_eggs = [0 for i in range(tnum)]
    t_laying_rates = [0 for i in range(tnum)]
    for i in range(tnum):
        sum = 0
        for j in range(rnum):
            sum = sum + teggs[j][i]
        # endfor
        t_eggs[i] = sum
        if number_of_hens > 0:
            t_laying_rates[i] = round(sum / (number_of_hens / tnum) * 100, 1)
        else:
            t_laying_rates[i] = 0
    # endfor
    # print(t_eggs)

    if number_of_hens > 0:
        lot_laying_rate = round(lot_ecount / number_of_hens * 100, 1)

    # Get average egg count except max/min count
    total = num = 0
    for i in range(rnum):
        for j in range(tnum):
            count = teggs[i][j]
            if (count != max_count) and (count != min_count):
                total = total + count
                num = num + 1
            # endif
        # endfor
    # endfor

    # Get alarm level
    norm_min = 0
    if num > 0:
        ave_count = total / num
        norm_avg = round(ave_count * 10) / 10
        norm_min = norm_avg - norm_avg * (int(g_AlarmEggCount) / 100)
        norm_min = round(norm_min * 10) / 10
    # endif
    # print(norm_min)

    try:
        return Response(
            {
                "id": id,
                "house": house,
                "tabs": getTabsData(tabs),
                "total_ecount": total_ecount,
                "lot_ecount": lot_ecount,
                "lot_laying_rate": lot_laying_rate,
                "r_infos": r_infos,
                "tnum": tnum,
                "t_infos": t_infos,
                "teggs": teggs,
                "laying_rates": laying_rates,
                "r_eggs": r_eggs,
                "r_laying_rates": r_laying_rates,
                "t_eggs": t_eggs,
                "t_laying_rates": t_laying_rates,
                "norm_min": norm_min,
            }
        )
    except Exception as e:
        print(e)
        return Response({"message": "Something awful happened!", "detail": e.args}, 400)


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def egg_d_table(request, id=1):
    try:
        lots = getLotList()
        tabs = []

        for lot in lots:
            batch = getBatchInfoByLotID(lot["LOT_ID"])
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "/egg_d_table"
            )
            tab.visibility = False
            if batch:
                tab.visibility = True
            tabs.append(tab)

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        house = getHouseNameByLotID(id)
        total_ecount = getTodayTotalEggCount(0)
        r_infos = getEggCtrInfo(id, 0)
        rnum = len(r_infos)
        t_infos = getEggCtrInfo(id, 1)
        tnum = len(t_infos)

        d = datetime.datetime.today()
        today = d.strftime("%Y-%m-%d")
        eggs = getTodayEggCtrList(id, today)
        # print(rnum)
        # print(tnum)
        today = d.strftime("%m-%d")
        teggs = [[0 for j in range(tnum)] for i in range(rnum)]
        for egg in eggs:
            r = egg["CAGE_ROW"] - 1
            t = egg["CAGE_TIER"] - 1
            print("r=%s" % r)
            print("t=%s" % t)
            teggs[r][t] = egg["EGG_COUNT"]
            print(teggs[r][t])
        # endfor
        # print(teggs)

        # Get days from feeding date to yesterday
        result = getBatchInfoByLotID(id)
        days = 0
        if result != None:
            feeding_date = result[1]
            yesterday = datetime.date.today() - datetime.timedelta(1)
            days = (yesterday - feeding_date).days + 1
        # endif

        # Get daily EggCount
        d_eggs = getDailyEggCtrList(id)
        print(d_eggs)
        dnum = len(d_eggs)
        # print(days)

        if days > 0:
            day_p = -1
            dat_p = 0
            rownum = 1 + 2 * tnum * rnum
            deggs = [[0 for d in range(days)] for d in range(rownum)]
            egg_day = "00-00"
            for d_egg in d_eggs:
                ed = d_egg["EGG_DATE"].strftime("%m-%d")
                if ed != egg_day:
                    day_p += 1
                    deggs[0][day_p] = ed
                    egg_day = ed
                # endif
                r = d_egg["CAGE_ROW"]
                t = d_egg["CAGE_TIER"]
                pos = ((r - 1) * tnum + t - 1) * 2
                # print(pos)
                deggs[pos + 1][day_p] = d_egg["EGG_COUNT"]
                deggs[pos + 2][day_p] = d_egg["EGG_RATE_CUR"]
            # endfor
        # endif

        request.session["egg_upd_lot"] = id

        return Response(
            {
                "id": id,
                "house": house,
                "tabs": getTabsData(tabs),
                "total_ecount": total_ecount,
                "rnum": rnum,
                "tnum": tnum,
                "today": today,
                "r_infos": r_infos,
                "t_infos": t_infos,
                "teggs": teggs,
                "days": days,
                "deggs": deggs,
            }
        )
    except Exception as e:
        print(e)
        return Response({"message": "Something awful happened!", "detail": e.args}, 400)


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def egg_spec_after(request, id=1):
    try:
        lots = getLotList()
        tabs = []

        for lot in lots:
            batch = getBatchInfoByLotID(lot["LOT_ID"])
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "/egg_spec_after"
            )
            tab.visibility = False
            if batch:
                tab.visibility = True
            tabs.append(tab)

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        from_date = request.query_params.get("from_date")
        if from_date is None:
            from_date = datetime.datetime.today().strftime("%Y-%m-%d")
        # endif
        to_date = request.query_params.get("to_date")
        if to_date is None:
            to_date = datetime.datetime.today().strftime("%Y-%m-%d")

        house = getHouseNameByLotID(id)
        lots = getLotList()
        total_ecount = getTodayTotalEggCount(0)
        r_infos = getEggCtrInfo(id, 0)
        rnum = len(r_infos)
        t_infos = getEggCtrInfo(id, 1)
        tnum = len(t_infos)
        d_eggs = getDailyEggCtrListPeriods(id, from_date, to_date)
        # dnum = len(d_eggs)
        f_date = datetime.datetime.strptime(from_date, "%Y-%m-%d")
        t_date = datetime.datetime.strptime(to_date, "%Y-%m-%d")
        day_num = (t_date - f_date).days + 1

        day_p = -1
        dat_p = 0
        rownum = 1 + 2 * tnum * rnum
        deggs = [[0 for d in range(day_num)] for d in range(rownum)]
        egg_day = "00-00"
        for d_egg in d_eggs:
            ed = d_egg["EGG_DATE"].strftime("%m-%d")
            if ed != egg_day:
                day_p += 1
                deggs[0][day_p] = ed
                egg_day = ed
            # endif
            r = d_egg["CAGE_ROW"]
            t = d_egg["CAGE_TIER"]
            pos = ((r - 1) * tnum + t - 1) * 2
            # print("pos=%s" % (pos))
            # print("day_p=%s" % (day_p))
            deggs[pos + 1][day_p] = d_egg["EGG_COUNT"]
            deggs[pos + 2][day_p] = d_egg["EGG_RATE_CUR"]
        # endfor
        days = day_p + 1

        return Response(
            {
                "id": id,
                "house": house,
                "tabs": getTabsData(tabs),
                "total_ecount": total_ecount,
                "r_infos": r_infos,
                "t_infos": t_infos,
                "days": days,
                "deggs": deggs,
                "from_date": from_date,
                "to_date": to_date,
            }
        )
    except Exception as e:
        print(e)
        return Response({"message": "Something awful happened!", "detail": e.args}, 400)


@api_view(["GET", "POST"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def w_his_after(request, id=1):
    try:
        house = getHouseNameByLotID(id)
        info = loadWeeklyViewItem()
        # get batch of growth_kind=3
        batchs = getBatchHisInfoByLotID(id)
        # print(batchs)

        lots = getLotList()
        tabs = []

        for lot in lots:
            batch = getBatchInfoByLotID(lot["LOT_ID"])
            tab = abstractions.Tab(
                lot["LOT_ID"], lot["HOUSE_NAME"], False, batch, "/w_his_after"
            )
            tab.visibility = False
            if batch:
                tab.visibility = True
            tabs.append(tab)

        current_tab = tabs[0]

        for tab in tabs:
            if tab.id == id:
                current_tab = tab
                break

        if current_tab.visibility == False:
            for tab in tabs:
                if tab.visibility == True:
                    current_tab = tab
                    break

        id = current_tab.id

        rows = []
        data_f = False
        if request.method == "GET":
            if request.args.get("history"):
                batch_id = request.args.get("batch")
                if batch_id != "":
                    rows = getWeeklyHistTable(batch_id)
                    # print(rows)

                    # Get weight chage rate
                    w_weight = 0
                    # print(len(rows))
                    for i in range(len(rows)):
                        row = rows[len(rows) - i - 1]
                        if w_weight == 0:
                            w = 0
                        else:
                            #                            w = (row['HEN_WEIGHT']-w_weight)/100
                            w = row["HEN_WEIGHT"] - w_weight
                        # endif
                        w_weight = row["HEN_WEIGHT"]
                        # print(w_weight)
                        row["WEIGHTCHG_RATE"] = round(w, 1)
                        # print(row['WEIGHTCHG_RATE'])
                    # endfor
                    data_f = True
                # endif
            # endif
        # endif

        return Response(
            {
                "id": id,
                "tabs": getTabsData(tabs),
                "rows": rows,
                "house": house,
                "items": info[0],
                "flags": info[1],
                "batchs": batchs,
                "data_f": data_f,
            }
        )
    except Exception as e:
        print(e)
        return Response({"message": "Something awful happened!", "detail": e.args}, 400)


@api_view(["GET", "POST"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def egg_input(request):
    id = request.query_params.get("id")
    if id == None:
        return Response({"message": "id is not specified"}, 400)

    r_infos = getEggCtrInfo(id, 0)
    rnum = len(r_infos)
    t_infos = getEggCtrInfo(id, 1)
    tnum = len(t_infos)
    teggs = [[0 for j in range(tnum)] for i in range(rnum)]
    egg_date = ""

    if request.method == "GET":
        msg = ""
        if request.query_params.get("get_egg"):
            egg_date = request.query_params.get("egg_date")
            # print(egg_date)
            eggs = getTodayEggCtrList(id, egg_date)
            teggs = [[0 for j in range(tnum)] for i in range(rnum)]
            for egg in eggs:
                r = egg["CAGE_ROW"] - 1
                t = tnum - egg["CAGE_TIER"]
                teggs[r][t] = egg["EGG_COUNT"]
                # print(teggs[r][t])
            # endfor
        # endif
        if request.query_params.get("update_egg"):
            try:
                cage_row = request.query_params.get("cage_row")
                cage_tier = request.query_params.get("cage_tier")
                egg_count = request.query_params.get("egg_count")
                result = getEggInformation(id, egg_date, cage_row, cage_tier)
                collect_id = result["COLLECT_ID"]
                tegg_id = result["TEGG_ID"]
                # print(tegg_id)
                # print(egg_count)
                updateEggInformation(tegg_id, egg_count)

                # Re-calculate total egg count
                eggs = getTodayEggCtrList(id, egg_date)
                teggs = [[0 for j in range(tnum)] for i in range(rnum)]
                sum = 0
                for egg in eggs:
                    r = egg["CAGE_ROW"] - 1
                    t = tnum - egg["CAGE_TIER"]
                    teggs[r][t] = egg["EGG_COUNT"]
                    sum = sum + egg["EGG_COUNT"]
                    # print(teggs[r][t])
                # endfor

                # Update summary egg data
                # print(collect_id)
                updateSummaryEggData(collect_id, sum)
                msg = "Database updated successfully !!!"
            except:
                msg = "Failed DB updation !"
                logging.exception("Failed DB updation !")
            # endtry
        # endif
    # endif

    return Response(
        {
            "r_infos": r_infos,
            "rnum": rnum,
            "t_infos": t_infos,
            "tnum": tnum,
            "teggs": teggs,
            "egg_date": egg_date,
            "msg": msg,
        }
    )
