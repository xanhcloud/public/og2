from api.utils.debugPrint import debugPrintEx

from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.response import Response
from rest_framework.decorators import (
    permission_classes,
    authentication_classes,
    api_view,
)

from api.utils.query_utils import getDailySummary, updateDailySummary


@api_view(["POST"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def daily_input(request):
    data = request.data
    result = updateDailySummary(data)
    return Response({"message": result})


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def daily_input_date(request):
    try:
        # get data from request, save to DB
        upd_date = request.query_params.get("upd_date")
        lot_id = request.query_params.get("lot_id")
        # print(upd_date)
        # print(lot_id)
        result = getDailySummary(upd_date, lot_id)
        # print(result)

        if not result:
            debugPrintEx("***** NG")
            result = {}
        # endif
    except Exception as e:
        return Response({"message": "Something awful happened!", "detail": e.args}, 500)
    # endtry

    return Response({"data": result})
