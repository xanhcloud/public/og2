from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.response import Response
from rest_framework.decorators import (
    permission_classes,
    authentication_classes,
    api_view,
)

from api.utils.query_utils import getHouseList, getLotList, getLotListByHouseID, getLotListNotUsed, getLotListNotUsedByHouseID


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def lots_and_houses(request):
    lots = getLotList()

    return Response({"lots": lots})


@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def houses(request):
    houses = getHouseList()

    return Response({"houses": houses})

@api_view(["GET"])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def lots(request):
    house_id = 0
    if request.query_params.get("house_id"):
        house_id = int(request.query_params.get("house_id"))

    not_used = 0
    if request.query_params.get("not_used"):
        not_used = int(request.query_params.get("not_used"))

    if house_id > 0:
        if not_used == 1:
            lots = getLotListNotUsedByHouseID(house_id)
        else:
            lots = getLotListByHouseID(house_id)
    else:
        if not_used == 1:
            lots = getLotListNotUsed()
        else:
            lots = getLotList()

    return Response({"lots": lots})
