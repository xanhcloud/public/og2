from rest_framework import permissions, viewsets, generics

from api.models import DbmHouse, DbmLot, DbtDead
from api.serializers.model_serializers import (
    DbmHouseSerializer,
    DbmLotSerializer,
    DbtDeadSerializer,
)


class DbmLotViewSet(viewsets.ModelViewSet):
    queryset = DbmLot.objects.all().order_by("lot_id")
    serializer_class = DbmLotSerializer
    permission_classes = [permissions.IsAuthenticated]


class DbmHouseViewSet(viewsets.ModelViewSet):
    queryset = DbmHouse.objects.all().order_by("house_id")
    serializer_class = DbmHouseSerializer
    permission_classes = [permissions.IsAuthenticated]


class DbtDeadViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = DbtDeadSerializer

    queryset = DbtDead.objects.all().order_by('-check_date', '-check_time')

    def get_queryset(self):
        queryset = self.queryset
        house = self.request.query_params.get("house_id")
        if house is not None:
            queryset = queryset.filter(house=house)
        
        lot = self.request.query_params.get("lot_id")
        if lot is not None:
            queryset = queryset.filter(lot=lot)
        return queryset
