from api.collect import collect_data_loop

from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    def handle(self, *args, **options):
        collect_data_loop()
