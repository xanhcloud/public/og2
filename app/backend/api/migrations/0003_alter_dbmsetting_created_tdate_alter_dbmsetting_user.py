# Generated by Django 5.0.6 on 2024-09-19 05:00

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_dbmsetting'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dbmsetting',
            name='created_tdate',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='dbmsetting',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='api.dbmuser'),
        ),
    ]
