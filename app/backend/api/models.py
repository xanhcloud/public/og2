from django.db import models
from django.contrib.auth.models import User


class DbmDic(models.Model):
    dic_id = models.AutoField(primary_key=True)
    term_name = models.CharField(max_length=32, blank=True, null=True)
    jp_name = models.CharField(max_length=64, blank=True, null=True)
    en_name = models.CharField(max_length=64, blank=True, null=True)
    pt_name = models.CharField(max_length=64, blank=True, null=True)
    es_name = models.CharField(max_length=64, blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbm_dic"


class DbmEgg(models.Model):
    megg_id = models.AutoField(primary_key=True)
    lot = models.ForeignKey("DbmLot", models.DO_NOTHING)
    eggctr_no = models.SmallIntegerField()
    cage_row = models.SmallIntegerField()
    cage_tier = models.SmallIntegerField()
    reg_egg = models.CharField(max_length=16, blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbm_egg"


class DbmFooder(models.Model):
    mfooder_id = models.AutoField(primary_key=True)
    lot = models.ForeignKey("DbmLot", models.DO_NOTHING)
    fooder_no = models.SmallIntegerField()
    fooder_type = models.SmallIntegerField()
    reg_dep = models.CharField(max_length=16, blank=True, null=True)
    reg_arv = models.CharField(max_length=16, blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbm_fooder"


class DbmFooderst(models.Model):
    mfooderst_id = models.AutoField(primary_key=True)
    mfooder = models.ForeignKey(DbmFooder, models.DO_NOTHING)
    start_time = models.TimeField()
    apply_date = models.DateField()
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbm_fooderst"


class DbmFw(models.Model):
    mfw_id = models.AutoField(primary_key=True)
    lot = models.ForeignKey("DbmLot", models.DO_NOTHING)
    fw_no = models.SmallIntegerField()
    reg_food_up = models.CharField(max_length=16, blank=True, null=True)
    reg_food_lo = models.CharField(max_length=16, blank=True, null=True)
    reg_water = models.CharField(max_length=16, blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbm_fw"


class DbmHen(models.Model):
    hen_id = models.AutoField(primary_key=True)
    hen_name = models.CharField(max_length=128, blank=True, null=True)
    created_tdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = "dbm_hen"


class DbmHouse(models.Model):
    house_id = models.AutoField(primary_key=True)
    house_name = models.CharField(max_length=64, blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbm_house"


class DbmLot(models.Model):
    lot_id = models.AutoField(primary_key=True)
    lot_name = models.CharField(max_length=32, blank=True, null=True)
    house = models.ForeignKey(DbmHouse, models.DO_NOTHING)
    cage_num = models.IntegerField(blank=True, null=True)
    equip_type = models.SmallIntegerField()
    comm_type = models.SmallIntegerField()
    ip_address = models.CharField(max_length=32, blank=True, null=True)
    food_input = models.SmallIntegerField()
    eggco = models.SmallIntegerField()
    stop_count = models.SmallIntegerField()
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbm_lot"


class DbmLotBackup(models.Model):
    lot_id = models.IntegerField()
    lot_name = models.CharField(max_length=32, blank=True, null=True)
    house_id = models.IntegerField()
    cage_num = models.IntegerField(blank=True, null=True)
    equip_type = models.SmallIntegerField()
    comm_type = models.SmallIntegerField()
    ip_address = models.CharField(max_length=32, blank=True, null=True)
    food_input = models.SmallIntegerField()
    stop_count = models.SmallIntegerField()
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbm_lot_backup"


class DbmManual(models.Model):
    manul_id = models.AutoField(primary_key=True)
    hen = models.ForeignKey(DbmHen, models.DO_NOTHING)
    week_periods = models.SmallIntegerField()
    survival_rate = models.SmallIntegerField(blank=True, null=True)
    hen_weight = models.SmallIntegerField(blank=True, null=True)
    egg_weight = models.SmallIntegerField(blank=True, null=True)
    egg_rate = models.SmallIntegerField(blank=True, null=True)
    egg_vol = models.SmallIntegerField(blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbm_manual"


class DbmPrice(models.Model):
    price_id = models.AutoField(primary_key=True)
    price_date = models.DateField()
    food_price = models.IntegerField(blank=True, null=True)
    stdegg_price = models.IntegerField(blank=True, null=True)
    outegg_price = models.IntegerField(blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbm_price"


class DbmThermo(models.Model):
    mthermo_id = models.AutoField(primary_key=True)
    lot = models.ForeignKey(DbmLot, models.DO_NOTHING)
    mthermo_type = models.SmallIntegerField()
    mthermo_no = models.SmallIntegerField()
    reg_cur = models.CharField(max_length=16, blank=True, null=True)
    reg_high = models.CharField(max_length=16, blank=True, null=True)
    reg_low = models.CharField(max_length=16, blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbm_thermo"


class DbmUser(models.Model):
    user_id = models.CharField(primary_key=True, max_length=8)
    password = models.CharField(max_length=8)
    user_name = models.CharField(max_length=64, blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbm_user"


class DbtBatch(models.Model):
    batch_id = models.AutoField(primary_key=True)
    lot = models.ForeignKey(DbmLot, models.DO_NOTHING)
    hen = models.ForeignKey(DbmHen, models.DO_NOTHING)
    hengrp = models.ForeignKey("DbtHengrp", models.DO_NOTHING)
    growth_kind = models.SmallIntegerField(blank=True, null=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    initial_count = models.IntegerField(blank=True, null=True)
    feeding_date = models.DateField(blank=True, null=True)
    eggcnt_stime = models.SmallIntegerField(blank=True, null=True)
    created_tdate = models.DateTimeField()
    eggcnt_etime = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = "dbt_batch"


class DbtCollection(models.Model):
    collect_id = models.AutoField(primary_key=True)
    batch = models.ForeignKey(DbtBatch, models.DO_NOTHING)
    col_tdate = models.DateTimeField()
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbt_collection"


class DbtEgg(models.Model):
    tegg_id = models.AutoField(primary_key=True)
    collect = models.ForeignKey(DbtCollection, models.DO_NOTHING)
    megg = models.ForeignKey(DbmEgg, models.DO_NOTHING)
    egg_count = models.IntegerField(blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbt_egg"


class DbtEggCopy(models.Model):
    tegg_id = models.AutoField(primary_key=True)
    collect = models.ForeignKey(DbtCollection, models.DO_NOTHING)
    megg = models.ForeignKey(DbmEgg, models.DO_NOTHING)
    egg_count = models.IntegerField(blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbt_egg_copy"


class DbtEggchg(models.Model):
    eggchg_id = models.AutoField(primary_key=True)
    summary = models.ForeignKey("DbtSummary", models.DO_NOTHING)
    megg = models.ForeignKey(DbmEgg, models.DO_NOTHING)
    egg_count = models.IntegerField(blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbt_eggchg"


class DbtFooder(models.Model):
    tfooder_id = models.AutoField(primary_key=True)
    collect = models.ForeignKey(DbtCollection, models.DO_NOTHING)
    mfooder = models.ForeignKey(DbmFooder, models.DO_NOTHING)
    departure_time = models.TimeField(blank=True, null=True)
    arrival_time = models.TimeField(blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbt_fooder"


class DbtFw(models.Model):
    tfw_id = models.AutoField(primary_key=True)
    collect = models.ForeignKey(DbtCollection, models.DO_NOTHING)
    mfw = models.ForeignKey(DbmFw, models.DO_NOTHING, blank=True, null=True)
    food_amount = models.IntegerField(blank=True, null=True)
    water_amount = models.IntegerField(blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbt_fw"


class DbtHengrp(models.Model):
    hengrp_id = models.AutoField(primary_key=True)
    hengrp_name = models.CharField(max_length=64, blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbt_hengrp"


class DbtSummary(models.Model):
    summary_id = models.AutoField(primary_key=True)
    batch = models.ForeignKey(DbtBatch, models.DO_NOTHING)
    summary_date = models.DateField()
    day_periods = models.SmallIntegerField(blank=True, null=True)
    week_periods = models.SmallIntegerField(blank=True, null=True)
    collect = models.ForeignKey(DbtCollection, models.DO_NOTHING)
    egg_count = models.IntegerField(blank=True, null=True)
    food_amount = models.IntegerField(blank=True, null=True)
    water_amount = models.IntegerField(blank=True, null=True)
    curr_count = models.IntegerField(blank=True, null=True)
    dead_count = models.IntegerField(blank=True, null=True)
    hen_weight = models.SmallIntegerField(blank=True, null=True)
    egg_weight = models.SmallIntegerField(blank=True, null=True)
    defection = models.SmallIntegerField(blank=True, null=True)
    coeff = models.SmallIntegerField(blank=True, null=True)
    daylight_hours = models.SmallIntegerField(blank=True, null=True)
    preset_temp = models.SmallIntegerField(blank=True, null=True)
    room_temp = models.SmallIntegerField(blank=True, null=True)
    room_temp_hi = models.SmallIntegerField(blank=True, null=True)
    room_temp_lo = models.SmallIntegerField(blank=True, null=True)
    outer_temp = models.SmallIntegerField(blank=True, null=True)
    outer_temp_hi = models.SmallIntegerField(blank=True, null=True)
    outer_temp_lo = models.SmallIntegerField(blank=True, null=True)
    humidity = models.SmallIntegerField(blank=True, null=True)
    humidity_upl = models.SmallIntegerField(blank=True, null=True)
    humidity_lol = models.SmallIntegerField(blank=True, null=True)
    min_ventilation = models.SmallIntegerField(blank=True, null=True)
    bmin_ventilation = models.SmallIntegerField(blank=True, null=True)
    comments = models.CharField(max_length=256, blank=True, null=True)
    remv_excr = models.CharField(max_length=256, blank=True, null=True)
    ventilation_setup = models.SmallIntegerField(blank=True, null=True)
    ventilation = models.SmallIntegerField(blank=True, null=True)
    food_interval = models.CharField(max_length=256, blank=True, null=True)
    heating = models.CharField(max_length=256, blank=True, null=True)
    water_pos = models.CharField(max_length=256, blank=True, null=True)
    aircon_system = models.CharField(max_length=256, blank=True, null=True)
    lighting_time = models.CharField(max_length=256, blank=True, null=True)
    food = models.CharField(max_length=256, blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbt_summary"


class DbtThermo(models.Model):
    tthermo_id = models.AutoField(primary_key=True)
    collect = models.ForeignKey(DbtCollection, models.DO_NOTHING)
    mthermo = models.ForeignKey(DbmThermo, models.DO_NOTHING)
    cur_val = models.SmallIntegerField(blank=True, null=True)
    high_val = models.SmallIntegerField(blank=True, null=True)
    low_val = models.SmallIntegerField(blank=True, null=True)
    created_tdate = models.DateTimeField()

    class Meta:
        managed = True
        db_table = "dbt_thermo"


class DbmSetting(models.Model):
    setting_id = models.AutoField(primary_key=True)
    created_tdate = models.DateTimeField(blank=True, null=True)
    user = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True)
    category = models.CharField(max_length=256, blank=False, null=False)
    name = models.CharField(max_length=256, blank=False, null=False)
    value = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = "dbm_setting"
        constraints = [
            models.UniqueConstraint(
                fields=["category", "name", "user"], name="unique_setting"
            )
        ]


class DbtDead(models.Model):
    tdead_id = models.AutoField(primary_key=True)
    dead_count = models.IntegerField(default=0)
    check_date = models.DateField(blank=True, null=True)
    check_time = models.TimeField(blank=True, null=True)
    house = models.ForeignKey(DbmHouse, models.DO_NOTHING)
    created_tdate = models.DateTimeField(blank=True, null=True)
    lot = models.ForeignKey(DbmLot, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = True
        db_table = "dbt_dead"


class DbtJson(models.Model):
    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    data = models.JSONField(blank=True, null=True)
    used = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = "dbt_json"


class DbtError(models.Model):
    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    data = models.JSONField(blank=True, null=True)
    used = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = "dbt_error"
