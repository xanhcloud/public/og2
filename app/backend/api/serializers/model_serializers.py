from rest_framework import serializers

from api.models import DbmHouse, DbmLot, DbtDead


class DbmLotSerializer(serializers.ModelSerializer):
    class Meta:
        model = DbmLot
        fields = "__all__"
        read_only_fields = ["lot_id"]


class DbmHouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = DbmHouse
        fields = "__all__"
        read_only_fields = ["house_id"]


class DbtDeadSerializer(serializers.ModelSerializer):
    class Meta:
        model = DbtDead
        read_only_fields = ["tdead_id"]
        fields = "__all__"
