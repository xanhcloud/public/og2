from rest_framework import serializers


class TabSerializers(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    visibility = serializers.BooleanField()
    batch = serializers.JSONField()
    prefix = serializers.CharField()

    class Meta:
        managed = False
        fields = "__all__"
