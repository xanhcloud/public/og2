import json
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync


@receiver(post_save, sender=User)
async def user_post_save_handler(sender, instance, **kwargs):
    channel_layer = get_channel_layer()
    await channel_layer.group_send(
        "database_activity",
        {
            "type": "database.changed",
            "message": {
                "type": "post_save",
                "data": {
                    "model": "User",
                    "id": instance.id,
                },
            },
        },
    )
