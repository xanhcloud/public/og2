from django.urls import path, include
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token

from . import views
from api.controllers import (
    dashboard_controller,
    fooder_controller,
    graph_controller,
    group_controller,
    setting_controller,
    users_controller,
    util_controller,
    viewset_controller,
)
from api.controllers import auth_controller, report_controller


router = routers.DefaultRouter()
router.register(r"users", users_controller.UserViewSet)
# router.register(r"groups", users_controller.GroupViewSet)
# router.register(r"dbt-lots", viewset_controller.DbmLotViewSet)
# router.register(r"dbt-houses", viewset_controller.DbmHouseViewSet)
router.register(r"dbt-deads", viewset_controller.DbtDeadViewSet)

urlpatterns = [
    path("", views.index, name="index"),
    path("auth/token", obtain_auth_token),
    path("auth/user", auth_controller.Profile),
    path("dashboard", dashboard_controller.Dashboard),
    path("dailytable/<int:id>", report_controller.dailytable),
    path("setup-dailytable", report_controller.setup_dailytable),
    path("save-setup-dailytable", report_controller.save_setup_dailytable),
    path("d-comparison/<int:id>", report_controller.d_comparison),
    path("d-spec-after/<int:id>", report_controller.d_spec_after),
    path("weeklytable/<int:id>", report_controller.weeklytable),
    path("w-comparison/<int:id>", report_controller.w_comparison),
    path("w-spec-after/<int:id>", report_controller.w_spec_after),
    path("w-history/<int:id>", report_controller.w_history),
    path("w-setup", report_controller.w_setup),
    path("eggtable/<int:id>", report_controller.eggtable),
    path("egg-d-table/<int:id>", report_controller.egg_d_table),
    path("egg-spec-after/<int:id>", report_controller.egg_spec_after),
    path("graph/<int:id>", graph_controller.graph),
    path("graph-3days/<int:id>", graph_controller.graph_3days),
    path("graph-weekly/<int:id>", graph_controller.graph_weekly),
    path("graph-spec-after/<int:id>", graph_controller.graph_spec_after),
    path("graph-comparison/<int:id>", graph_controller.graph_comparison),
    path("fooder-reg/<int:id>", fooder_controller.fooder_reg),
    path("hengrp-his", group_controller.hengrp_his),
    path("hengrp-upd", group_controller.hengrp_upd),
    path("batch-reg", group_controller.batch_reg),
    path("daily-input", setting_controller.daily_input),
    path("daily-input-date", setting_controller.daily_input_date),
    path("w-his-after/<int:id>", report_controller.w_his_after),
    path("egg-input", report_controller.egg_input),
    path("hengrp-reg", group_controller.hengrp_reg),
    path("lots-houses", util_controller.lots_and_houses),
    path("houses", util_controller.houses),
    path("lots", util_controller.lots),
    path("test-dashboard", dashboard_controller.test_dashboard),
    # view set
    path("", include(router.urls)),
]
