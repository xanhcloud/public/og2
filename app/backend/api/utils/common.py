import codecs
import datetime
import os
import shutil
import sys
import threading

from api.serializers.tab_serializers import TabSerializers
from api.utils.query_utils import executeRawQuery, fetAllRawQuery, fetOneRawQuery

#
# "Constants"
#
TRACE = 0
DEBUG = 1
INFO = 2
WARN = 3
ERROR = 4
FATAL = 5
ALARM = 6

RESULT_CODE_OK = "0"
RESULT_CODE_NG = "1"

MAX_LOGFILE_SIZE = 50000000
MIN_LOGFILE_SIZE = 10000000

#
# Globals
#
g_ErrLvl = {
    TRACE: "TRACE",
    DEBUG: "DEBUG",
    INFO: "INFO",
    WARN: "WARN",
    ERROR: "ERROR",
    FATAL: "FATAL",
    ALARM: "ALARM",
}
g_DataBaseName = "ovalGrowthDB"  # this is real database name

g_log_path = "./"  # dummy, real in the ini file
g_comm_dir = "./"  # dummy, real in the ini file
g_collection_dir = "./"  # dummy, real in the ini file
g_db_user_id = "dummy_user"  # dummy name, real name in the ini file
g_db_pwd = "dummy_pass"  # dummy pass, real pass in the ini file
g_data_interval = 10 * 60  # dummy (10 min in seconds), real interval in the ini file
g_loggingLvl = DEBUG  # dummy, real in the ini file

g_DebugMode = True  # in final version should be changed to 'False'
g_Region = "en"  # only loading from ini file is implemented

g_FileCheckInterval = 10  # 10 sec (for debug)
g_CurHouseAndLot = "--:--:"  # for log and alarm
g_ResultCode = RESULT_CODE_OK  # lot import result

g_LastLogCheckDate = None

g_record_format1 = "hhhhhh"  # 6 fields, total 6
g_record_format2 = "iiiiiii"  # 7 fields (last is food amount), total 13
#                   ddaaddaaddaaddaaddaaddaaddaaddaaddaa    d - departure, a - arrival
#                   111122223333444455556666777788889999    fooder no
#                   hmhmhmhmhmhmhmhmhmhmhmhmhmhmhmhmhmhm    h - hours, m - minutes
g_record_format3 = (
    "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"  # 18 fields (but 36 values), total 31
)
#                             1         2         3         4         5     55
#                   0         0         0         0         0         0     67
g_record_format4 = (
    "iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii"  # 56 fields, total 87
)
g_record_format5 = "i"  # water amount, one field, total 88
g_FileSize = 340  # will be recalculated

g_isPowerFailure = [False] * 99
g_datPrevList = None
g_timeCmd = "####-##-## --:--"


def getTabsData(tabs):
    tabData = []
    for t in tabs:
        tabData.append(TabSerializers(t).data)

    return tabData


def debugPrint(msg, fromLog=0):
    if g_DebugMode:
        # getframeinfo returns a named tuple Traceback(filename, lineno, function, code_context, index)
        import inspect

        if hasattr(sys, "_getframe"):
            frame = sys._getframe(2) if fromLog == 1 else sys._getframe(1)
            try:
                print(frame.f_code.co_name + ":" + str(frame.f_lineno) + ": " + msg)
            except UnicodeEncodeError:
                import unicodedata

                msg1 = str(
                    unicodedata.normalize("NFKD", msg).encode("ascii", "replace")
                )
                print(
                    frame.f_code.co_name
                    + ":"
                    + str(frame.f_lineno)
                    + ": "
                    + msg1
                    + " <-UnicodeEncodeError!"
                )
            # endtry
        # endif
    # endif


def log(level, msg, noFormat=False):
    global g_loggingLvl
    global g_log_path
    global g_CurHouseAndLot
    global g_ResultCode
    global g_LastLogCheckDate
    global g_timeCmd

    # Log file output
    if g_log_path[-1] == "/":
        log_path = g_log_path + "result.info"
        alarm_path = g_log_path + "alarms.info"
    else:
        log_path = g_log_path + "/result.info"
        alarm_path = g_log_path + "/alarms.info"
    # endif

    # if date changed (midnight) check log file size and remove old records
    # also remove old alarms
    cur_date = datetime.datetime.today().strftime("%Y-%m-%d")
    if cur_date != g_LastLogCheckDate:
        g_LastLogCheckDate = cur_date

        try:
            statinfo = os.stat(log_path)
        except:
            open(log_path, "w+b").close()
            statinfo = os.stat(log_path)
        # endtry
        if statinfo.st_size > MAX_LOGFILE_SIZE:
            # log file become too big, copy tail to the new file
            log_path_new = log_path + ".new"
            # 2013-10-16
            with codecs.open(log_path, "r", "utf-8") as src:
                # with open(log_path) as src:
                offset = statinfo.st_size - MIN_LOGFILE_SIZE
                while True:
                    # handle situation when we jump into multi byte sequence
                    try:
                        # skip the part to be removed
                        src.seek(offset, 0)
                        # align to beginning of the next line
                        src.readline()
                        break
                    except UnicodeDecodeError:
                        offset += 1
                        # handle EOF situation
                        if offset == statinfo.st_size:
                            break
                        # end if
                    # end try
                # end while

                # hand the rest to shutil.copyfileobj
                # 2013-10-16
                with codecs.open(log_path_new, "w", "utf-8") as dst:
                    # with open(log_path_new, 'w') as dst:
                    shutil.copyfileobj(src, dst)
                # endwith
            # endwith

            os.remove(log_path)
            os.rename(log_path_new, log_path)
        # endif

        if os.path.isfile(alarm_path):
            os.remove(alarm_path)
        # endif
    # endif

    # set result code
    if level >= ERROR:
        g_ResultCode = RESULT_CODE_NG
    # endif

    # if logging level high enough write message to the log
    if level >= g_loggingLvl:
        #        fp = open(log_path, 'a')
        fp = codecs.open(log_path, "a", "utf-8")
        if noFormat:
            debugPrint("{0} Code={1}".format(g_CurHouseAndLot, msg), 1)
            fp.write("{0} Code={1}\n".format(g_CurHouseAndLot, msg))
        else:
            debugPrint(g_ErrLvl[level] + ": " + msg, 1)
            ##            fp.write("{0},{1} {2} \"{3}\"\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), g_ErrLvl[level], g_CurHouseAndLot, msg))
            ##            fp.write("[{0}] {1},{2} {3} \"{4}\"\n".format(threading.get_ident(), datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), g_ErrLvl[level], g_CurHouseAndLot, msg))
            timeNow = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            timeCmd = g_timeCmd[11:]
            fp.write(
                '[{0}] {1}({2}),{3} {4} "{5}"\n'.format(
                    threading.get_ident(),
                    timeNow,
                    timeCmd,
                    g_ErrLvl[level],
                    g_CurHouseAndLot,
                    msg,
                )
            )
        # endif
        fp.close()
    # endif

    # if logging level ALARM, FATAL or ERROR write messageto the alarms.info
    if level >= ERROR:
        #        fp = open(alarm_path, 'a')
        fp = codecs.open(alarm_path, "a", "utf-8")
        if noFormat:
            fp.write("{0} Code={1}\n".format(g_CurHouseAndLot, msg))
        else:
            ##            fp.write("{0},{1} {2} \"{3}\"\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), g_ErrLvl[level], g_CurHouseAndLot, msg))
            timeNow = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            timeCmd = g_timeCmd[11:]
            fp.write(
                '{0}({1}),{2} {3} "{4}"\n'.format(
                    timeNow, g_ErrLvl[level], timeCmd, g_CurHouseAndLot, msg
                )
            )
        # endif
        fp.close()
    # endif


def getCurrentPid():
    pid = 0
    tid = 0

    if g_comm_dir[-1] == "/":
        procPath = g_comm_dir + "current.pid"
    else:
        procPath = g_comm_dir + "/current.pid"
    # endif

    if os.path.exists(procPath):
        fp = open(procPath, "r+")
        data = fp.read()
        fp.close()

        words = data.split(":")
        if len(words) > 0:
            pid = int(words[0])
            if len(words) > 1:
                tid = int(words[1])
            # endif
        # endif

    # endif

    return (pid, tid)


def getSettings(category, name="", user_id=None):
    sql = """
    SELECT name, value FROM dbm_setting
    WHERE category = '{0}'
    """.format(
        category
    )

    if name:
        sql += " AND name = '{0}'".format(name)

    if user_id:
        sql += " AND user_id = '{0}'".format(user_id)

    records = fetAllRawQuery(sql)

    result = []
    for record in records:
        result[record["name"]] = record["value"]

    return result


def getSetting(category, name, user_id=None):
    sql = """
    SELECT setting_id, name, value FROM dbm_setting
    WHERE category = '{0}' AND name = '{1}'
    """.format(
        category, name
    )

    if user_id:
        sql += " AND user_id = '{0}'".format(user_id)
    else:
        sql += " AND user_id IS NULL"

    return fetOneRawQuery(sql)


def saveSetting(category, name, value, user_id=None):
    setting = getSetting(category, name, user_id)
    if setting == None:
        if user_id:
            sql = """
            INSERT INTO dbm_setting
            (category, name, value, user_id, created_tdate)
            VALUES
            ('{0}', '{1}', '', '{2}', '{3}')
            """.format(
                category,
                name,
                user_id,
                datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            )
        else:
            sql = """
            INSERT INTO dbm_setting
            (category, name, value, created_tdate)
            VALUES
            ('{0}', '{1}', '', '{2}')
            """.format(
                category, name, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            )

        executeRawQuery(sql)

    sql = """
    UPDATE dbm_setting
    SET value = '{0}'
    WHERE category = '{1}' AND name = '{2}'
    """.format(
        value, category, name
    )

    if user_id:
        sql += " AND user_id = '{0}'".format(user_id)
    else:
        sql += " AND user_id IS NULL"

    executeRawQuery(sql)
