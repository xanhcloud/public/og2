﻿# -*- coding: utf-8 -*-

import sys
import inspect


def debugPrint(msg):
    # getframeinfo returns a named tuple Traceback(filename, lineno, function, code_context, index)
    if hasattr(sys, "_getframe"):
        frame = sys._getframe(1)
        try:
            print(frame.f_code.co_name+"():"+str(frame.f_lineno)+": "+msg)
        except UnicodeEncodeError:
            import unicodedata
            msg1 = str(unicodedata.normalize('NFKD', msg).encode('ascii','replace'))
            print(frame.f_code.co_name+"():"+str(frame.f_lineno)+": "+msg1+" <-UnicodeEncodeError!")
        #endtry
    #endif
#end def debugPrint()


def debugPrintEx(msg):
    # getframeinfo returns a named tuple Traceback(filename, lineno, function, code_context, index)
    if hasattr(sys, "_getframe"):
        frame = sys._getframe(1)
        try:
            print(frame.f_globals["__name__"]+"."+frame.f_code.co_name+"():"+str(frame.f_lineno)+": "+msg)
        except UnicodeEncodeError:
            import unicodedata
            msg1 = str(unicodedata.normalize('NFKD', msg).encode('ascii','replace'))
            print(frame.f_globals["__name__"]+"."+frame.f_code.co_name+"():"+str(frame.f_lineno)+": "+msg1+" <-UnicodeEncodeError!")
        #endtry
    #endif
#end def debugPrint()


def funcLine():
    # getframeinfo returns a named tuple Traceback(filename, lineno, function, code_context, index)
    if hasattr(sys, "_getframe"):
        frame = sys._getframe(1)
        return frame.f_globals["__name__"]+"."+frame.f_code.co_name+"():"+str(frame.f_lineno)
    #endif
#end def debugPrint()
