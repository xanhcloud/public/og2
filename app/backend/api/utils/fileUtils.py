﻿# -*- coding: utf-8 -*-

#
# Imports
#
import os
import configparser
import codecs
import re
import string

from api.utils.debugPrint import debugPrint, debugPrintEx


ROOT_PATH = os.path.abspath(os.path.dirname(__file__)) + "/.."


def get_preferred_language():
    return "Eng"
    filename = ROOT_PATH + "/i18n/preferred_language"
    file_contents = open(filename).read()
    preferred_language = re.findall("(\w+)", file_contents)
    return string.capwords(preferred_language[0])


#
# Globals
#


"""
def getAlarms():
    debugPrintEx("***")
    alarms = []

    if os.path.isfile(resultFilePath):
        fp = open(resultFilePath, 'r')
        fp_content = fp.read()
        myLines = []
        for line in fp_content.splitlines():
            if line.find(",ALARM,") > -1:
                alarms.append(line)
            #endif
        #endfor
        fp.close()
    #endif

    debugPrint("*** {0} alarms found".format(len(alarms)))
    alarms.reverse()
    return alarms
#end def dbConnect()
"""


def getAlarms():
    debugPrint("***")
    alarms = []

    if os.path.isfile(g_AlarmFilePath):
        # 2013-10-16
        fp = codecs.open(g_AlarmFilePath, "r", "utf-8")
        #       fp = open(g_AlarmFilePath, 'r')
        fp_content = fp.read()
        myLines = []
        for line in fp_content.splitlines():
            ##            if line.find(",ALARM,") > -1:
            ##                alarms.append(line)
            ##            #endif
            alarms.append(line)
        # endfor
        fp.close()
    # endif

    debugPrint("*** {0} alarms found".format(len(alarms)))
    alarms.reverse()
    return alarms


# end def dbConnect()


# View item information
#
def getDailyViewItem():
    debugPrintEx("*******")
    # ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
    infoPath = ROOT_PATH + "/dailyViewItem" + get_preferred_language() + ".info"
    info = configparser.ConfigParser()
    info.read(infoPath)
    daily_section = info["daily"]
    d_info = {}
    for key in daily_section:
        d_info[key] = daily_section[key].split(",")
    return d_info


# end def getDailyViewItem()


def getWeeklyViewItem():
    debugPrintEx("*******")
    # ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
    infoPath = ROOT_PATH + "/weeklyViewItem" + get_preferred_language() + ".info"
    info = configparser.ConfigParser()
    info.read(infoPath)
    weekly_section = info["weekly"]
    w_info = {}
    for key in weekly_section:
        w_info[key] = weekly_section[key].split(",")
    return w_info


# end def getWeeklyViewItem()


def getLineViewItem(keywd, i_line, data):
    o_line = ""
    keywd = keywd.rstrip()
    i_line = i_line.rstrip()
    if keywd in data:
        if i_line[-3:] == "OFF":
            o_line = i_line.replace("OFF", data[keywd])
        elif i_line[-2:] == "ON":
            o_line = i_line.replace("ON", data[keywd])
        else:
            o_line = i_line
    else:
        o_line = i_line
    return o_line


# end def getlineViewItem()


# save/load View item information
#
def saveDailyViewItem(data):
    debugPrintEx("*******")
    # ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
    srcPath = ROOT_PATH + "/dailyViewItem" + get_preferred_language() + ".info"
    desPath = ROOT_PATH + "/dailyViewItem" + get_preferred_language() + ".new"
    ""
    srcf = open(srcPath, "r")
    desf = open(desPath, "w")
    desf.write("[daily]\n")
    oline = ""
    for line in srcf:
        if line.startswith("["):
            continue
        elt = line[:-1].split("=")
        desf.write(getLineViewItem(elt[0], line, data))
        desf.write("\n")
    # end for loop
    srcf.close()
    desf.close()

    # delete info file
    os.remove(srcPath)
    # rename new info file
    os.rename(desPath, srcPath)


# end def saveDailyViewItem()


def loadDailyViewItem():
    debugPrintEx("*******")
    # ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
    infoPath = ROOT_PATH + "/dailyViewItem" + get_preferred_language() + ".info"

    info_name = []
    info_flag = []
    info_f = open(infoPath, "r")
    for line in info_f:
        if line.startswith("["):
            continue
        line = line.replace("'", "")
        line = line.replace("\n", "")
        elt = line[:-1].split("=")
        param = elt[1][:-1].split(",")
        info_name.append(param[0])
        if line[-2:] == "ON":
            info_flag.append(1)
        else:
            info_flag.append(0)
        # endif
    # end for loop
    # print(info_name)
    # print(info_flag)
    return info_name, info_flag


# end def saveDailyViewItem()


def saveWeeklyViewItem(data):
    debugPrintEx("*******")
    # ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
    srcPath = ROOT_PATH + "/weeklyViewItem" + get_preferred_language() + ".info"
    desPath = ROOT_PATH + "/weeklyViewItem" + get_preferred_language() + ".new"
    ""
    srcf = open(srcPath, "r")
    desf = open(desPath, "w")
    desf.write("[weekly]\n")
    oline = ""
    for line in srcf:
        if line.startswith("["):
            continue
        elt = line[:-1].split("=")
        desf.write(getLineViewItem(elt[0], line, data))
        desf.write("\n")
    # end for loop
    srcf.close()
    desf.close()

    # delete info file
    os.remove(srcPath)
    # rename new info file
    os.rename(desPath, srcPath)


# end def saveWeeklyViewItem()


def loadWeeklyViewItem():
    debugPrintEx("*******")
    # ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
    infoPath = ROOT_PATH + "/weeklyViewItem" + get_preferred_language() + ".info"

    info_name = []
    info_flag = []
    info_f = open(infoPath, "r")
    for line in info_f:
        if line.startswith("["):
            continue
        line = line.replace("'", "")
        line = line.replace("\n", "")
        elt = line[:-1].split("=")
        param = elt[1][:-1].split(",")
        info_name.append(param[0])
        if line[-2:] == "ON":
            info_flag.append(1)
        else:
            info_flag.append(0)
        # endif
    # end for loop
    # print(info_name)
    # print(info_flag)
    return info_name, info_flag


# end def loadWeeklyViewItem()


def loadFileSettings():
    iniFilePath = ROOT_PATH + "/ovalGrowth.ini"
    if not os.path.isfile(iniFilePath):
        debugPrint("'{0}' not found".format(iniFilePath))
        raise AssertionError("'{0}' not found".format(iniFilePath))

    config = configparser.ConfigParser()
    config.read(iniFilePath)

    if "camera" in config and "alarm" in config:
        return config
    else:
        debugPrint("Cannot parse configuration file, no section 'alarm/camera'. ")
        raise


################################################################################
# read parameters from ini-file
#
# ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
iniFilePath = ROOT_PATH + "/ovalGrowth.ini"

if not os.path.isfile(iniFilePath):
    debugPrint("'{0}' not found".format(iniFilePath))
    raise AssertionError("'{0}' not found".format(iniFilePath))
# endif

config = configparser.ConfigParser()
config.read(iniFilePath)

try:
    sys_section = config["system"]
except KeyError:
    debugPrint("Cannot parse configuration file, no section 'system'. ")
    raise AssertionError("Cannot parse configuration file, no section 'system'. ")
# endtry

g_log_path = sys_section["log_path"]  # path to the log  file

if g_log_path[-1] == "/":
    g_AlarmFilePath = g_log_path + "alarms.info"
else:
    g_AlarmFilePath = g_log_path + "/alarms.info"
# endif


if __name__ == "__main__":
    """
    getDailyViewItem()
    ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
    srcPath = ROOT_PATH + "/viewItem.info"
    desPath = ROOT_PATH + "/viewItem.bak"
    os.rename(srcPath, desPath)


    ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
    srcPath = ROOT_PATH + "/dailyViewItem" + get_preferred_language()+ ".info"
    srcf = open(srcPath, "r")
    for line in srcf:
        oline = getLineViewItem("CURR_COUNT", line)
    """
    """
    ret =loadDailyViewItem()
    print(ret)
    i = 1
    """
    alarms = getAlarms()
    i = 1
