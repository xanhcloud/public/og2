import datetime
from decimal import Decimal
import math
import time
from django.db import connection
from api.utils.debugPrint import debugPrintEx


def executeRawQuery(sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    lastrowid = cursor.lastrowid
    rowcount = cursor.rowcount
    cursor.close()
    return {
        "last_id": lastrowid,
        "count": rowcount,
    }


def fetAllRawQuery(sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    columns = [col[0].upper() for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]


def fetAllRawQueryNoColumns(sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    columns = [col[0].upper() for col in cursor.description]
    rows = [row for row in cursor.fetchall()]
    return {"columns": columns, "rows": rows}


def fetOneRawQuery(sql):
    try:
        cursor = connection.cursor()
        cursor.execute(sql)
        columns = [col[0].upper() for col in cursor.description]
        row = cursor.fetchone()
        if row:
            return dict(zip(columns, row))
        return None
    except:
        return None


def fetOneRawQueryNoColumns(sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    row = cursor.fetchone()
    if row:
        return row
    return None


def getBatchInfoByLotID(lot_id):
    sql = """
    SELECT GROWTH_KIND, FEEDING_DATE, BATCH_ID
    FROM dbt_batch
    WHERE dbt_batch.LOT_ID = %s AND dbt_batch.END_DATE > CURRENT_DATE
     """
    sql = sql % (lot_id)

    return fetOneRawQueryNoColumns(sql)


def getDailyTable(lot_id, daily_table_lines):
    debugPrintEx("***")
    result = []

    if daily_table_lines == 0:
        print("++++today+++++")
        # get today daily table
        sql = """
        SELECT * FROM dbv_daily
        WHERE LOT_ID = %s
        AND DAILY_DATE = CURRENT_DATE
        """
        result = fetAllRawQuery(sql % (lot_id))
    else:
        result = getBatchInfoByLotID(lot_id)
        if result != None:
            feeding_date = result[1].strftime("%Y-%m-%d")
            yesterday = (datetime.date.today() - datetime.timedelta(1)).strftime(
                "%Y-%m-%d"
            )

            result = []

            sql = """
            SELECT * FROM dbv_daily
            WHERE LOT_ID = %s AND DAILY_DATE BETWEEN '%s' AND '%s' ORDER BY DAILY_DATE DESC
            """
            # get weekly table for last weekly_table_lines days
            sql = sql % (lot_id, feeding_date, yesterday)
            # print("---------" + sql + "--------------")

            result = fetAllRawQuery(sql)
        else:
            result = []

    return result


def getHouseNameByLotID(lot_id):
    sql = """
    SELECT
    dbm_house.HOUSE_NAME AS HOUSE_NAME
    FROM dbm_lot, dbm_house
    WHERE dbm_lot.LOT_ID = %s AND dbm_house.HOUSE_ID = dbm_lot.HOUSE_ID
    """
    sql = sql % (lot_id)

    return fetOneRawQueryNoColumns(sql)


def getLotIdByHouseName(house_name):
    sql = """
    SELECT
    dbm_lot.LOT_ID AS LOT_ID
    FROM dbm_house, dbm_lot
    WHERE dbm_house.HOUSE_NAME = %s AND dbm_house.HOUSE_ID = dbm_lot.HOUSE_ID
    """
    sql = sql % (house_name)

    lot = fetOneRawQuery(sql)

    if lot:
        return lot["LOT_ID"]

    return None


def getLotIdByHouseId(house_id):
    sql = """
    SELECT
    dbm_lot.LOT_ID AS LOT_ID
    FROM dbm_house, dbm_lot
    WHERE dbm_house.HOUSE_ID = %s AND dbm_house.HOUSE_ID = dbm_lot.HOUSE_ID
    """
    sql = sql % (house_id)

    lot = fetOneRawQuery(sql)

    if lot:
        return lot["LOT_ID"]

    return None


def getLotList():
    sql = """
    SELECT
    dbm_lot.LOT_ID AS LOT_ID,
    dbm_lot.LOT_NAME AS LOT_NAME,
    dbm_lot.HOUSE_ID AS HOUSE_ID,
    dbm_lot.CAGE_NUM AS CAGE_NUM,
    dbm_house.HOUSE_NAME AS HOUSE_NAME
    FROM dbm_lot
    INNER JOIN dbm_house ON dbm_lot.HOUSE_ID = dbm_house.HOUSE_ID
    ORDER BY LOT_ID ASC
    """

    return fetAllRawQuery(sql)


def getLotListByHouseID(house_id):
    sql = """
    SELECT
    dbm_lot.LOT_ID AS LOT_ID,
    dbm_lot.LOT_NAME AS LOT_NAME,
    dbm_lot.HOUSE_ID AS HOUSE_ID,
    dbm_lot.CAGE_NUM AS CAGE_NUM,
    dbm_house.HOUSE_NAME AS HOUSE_NAME
    FROM dbm_lot
    INNER JOIN dbm_house ON dbm_lot.HOUSE_ID = dbm_house.HOUSE_ID
    WHERE dbm_house.HOUSE_ID = %s
    ORDER BY LOT_ID ASC
    """

    sql = sql % (house_id)

    return fetAllRawQuery(sql)


def getHouseList():
    sql = """
    SELECT
    dbm_house.HOUSE_ID AS HOUSE_ID,
    dbm_house.HOUSE_NAME AS HOUSE_NAME
    FROM dbm_house
    ORDER BY HOUSE_ID ASC
    """

    return fetAllRawQuery(sql)


def getHengrpInfoByLotID(lot_id):
    sql = """
    SELECT HENGRP_ID
    FROM dbt_batch
    WHERE dbt_batch.LOT_ID = %s AND dbt_batch.END_DATE > CURRENT_DATE
     """
    sql = sql % (lot_id)

    return fetOneRawQueryNoColumns(sql)


def getBatchInfoList(hengrp_id, params={}):
    sql = """
    SELECT
    dbt_batch.BATCH_ID AS BATCH_ID,
    dbm_hen.HEN_NAME AS HEN_NAME,
    dbt_batch.GROWTH_KIND AS GROWTH_KIND,
    dbt_batch.START_DATE AS START_DATE,
    dbt_batch.FEEDING_DATE AS FEEDING_DATE,
    dbt_batch.INITIAL_COUNT AS INITIAL_COUNT,
    dbt_batch.END_DATE AS END_DATE,
    dbt_batch.EGGCNT_STIME,
    dbt_batch.EGGCNT_ETIME,
    dbt_hengrp.HENGRP_NAME AS HENGRP_NAME,
    dbm_house.HOUSE_NAME AS HOUSE_NAME,
    dbm_lot.LOT_NAME AS LOT_NAME
    FROM
    dbt_batch
    INNER JOIN dbm_hen ON dbm_hen.HEN_ID = dbt_batch.HEN_ID
    INNER JOIN dbt_hengrp ON dbt_hengrp.HENGRP_ID = dbt_batch.HENGRP_ID
    INNER JOIN dbm_lot ON dbm_lot.LOT_ID = dbt_batch.LOT_ID
    INNER JOIN dbm_house ON dbm_house.HOUSE_ID = dbm_lot.HOUSE_ID
    WHERE 1 = 1
    -- dbt_batch.HENGRP_ID = %d
    """

    # sql = sql % (hengrp_id)

    if hengrp_id > 0:
        sql = sql + "AND dbt_batch.HENGRP_ID = %d " % (hengrp_id)

    if params.get("lot_id") and params["lot_id"] > 0:
        sql = sql + "AND dbt_batch.LOT_ID = %d " % (params["lot_id"])

    if params.get("feeding_date") and params["feeding_date"] != "":
        sql = sql + "AND dbt_batch.FEEDING_DATE = '%s' " % (params["feeding_date"])

    if params.get("hen_id") and params["hen_id"] > 0:
        sql = sql + "AND dbt_batch.HEN_ID = %d " % (params["hen_id"])

    if params.get("growth_kind") and params["growth_kind"] > 0:
        sql = sql + "AND dbt_batch.GROWTH_KIND = %d " % (params["growth_kind"])

    if (
        params.get("start_date")
        and params["start_date"] != ""
        and params.get("end_date")
        and params["end_date"] != ""
    ):
        sql = (
            sql
            + "AND"
            + """(
                (dbt_batch.START_DATE >= '%s' AND dbt_batch.START_DATE <= '%s')
                OR (dbt_batch.END_DATE >= '%s' AND dbt_batch.END_DATE <= '%s')
                OR (dbt_batch.START_DATE >= '%s' AND dbt_batch.END_DATE <= '%s')
            )"""
            % (
                params["start_date"],
                params["end_date"],
                params["start_date"],
                params["end_date"],
                params["start_date"],
                params["end_date"],
            )
        )
    elif params.get("start_date") and params["start_date"] != "":
        sql = sql + "AND dbt_batch.START_DATE >= '%s' " % (params["start_date"])
    elif params.get("end_date") and params.get("end_date") != "":
        sql = sql + "AND dbt_batch.END_DATE <= '%s' " % (params["end_date"])

    # print(sql)

    return fetAllRawQuery(sql)

def getLatestBatchInfoList(hengrp_id, params={}):
    sql = """
    SELECT
        DISTINCT ON (dbm_lot.lot_id)
        dbm_lot.LOT_ID AS LOT_ID,
        dbt_batch.BATCH_ID AS BATCH_ID,
        dbm_hen.HEN_NAME AS HEN_NAME,
        dbt_batch.GROWTH_KIND AS GROWTH_KIND,
        dbt_batch.START_DATE AS START_DATE,
        dbt_batch.FEEDING_DATE AS FEEDING_DATE,
        dbt_batch.INITIAL_COUNT AS INITIAL_COUNT,
        dbt_batch.END_DATE AS END_DATE,
        dbt_batch.EGGCNT_STIME,
        dbt_batch.EGGCNT_ETIME,
        dbt_hengrp.HENGRP_NAME AS HENGRP_NAME,
        dbm_house.HOUSE_NAME AS HOUSE_NAME,
        dbm_lot.LOT_NAME AS LOT_NAME
    FROM dbm_lot
    INNER JOIN dbt_batch ON dbm_lot.LOT_ID = dbt_batch.LOT_ID
    INNER JOIN dbt_hengrp ON dbt_hengrp.HENGRP_ID = dbt_batch.HENGRP_ID
    INNER JOIN dbm_house ON dbm_house.HOUSE_ID = dbm_lot.HOUSE_ID
    INNER JOIN dbm_hen ON dbm_hen.HEN_ID = dbt_batch.HEN_ID
    WHERE 1 = 1
    """

    # sql = sql % (hengrp_id)

    if hengrp_id > 0:
        sql = sql + "AND dbt_batch.HENGRP_ID = %d " % (hengrp_id)

    if params.get("lot_id") and params["lot_id"] > 0:
        sql = sql + "AND dbt_batch.LOT_ID = %d " % (params["lot_id"])

    if params.get("feeding_date") and params["feeding_date"] != "":
        sql = sql + "AND dbt_batch.FEEDING_DATE = '%s' " % (params["feeding_date"])

    if params.get("hen_id") and params["hen_id"] > 0:
        sql = sql + "AND dbt_batch.HEN_ID = %d " % (params["hen_id"])

    if params.get("growth_kind") and params["growth_kind"] > 0:
        sql = sql + "AND dbt_batch.GROWTH_KIND = %d " % (params["growth_kind"])

    if (
        params.get("start_date")
        and params["start_date"] != ""
        and params.get("end_date")
        and params["end_date"] != ""
    ):
        sql = (
            sql
            + "AND"
            + """(
                (dbt_batch.START_DATE >= '%s' AND dbt_batch.START_DATE <= '%s')
                OR (dbt_batch.END_DATE >= '%s' AND dbt_batch.END_DATE <= '%s')
                OR (dbt_batch.START_DATE >= '%s' AND dbt_batch.END_DATE <= '%s')
            )"""
            % (
                params["start_date"],
                params["end_date"],
                params["start_date"],
                params["end_date"],
                params["start_date"],
                params["end_date"],
            )
        )
    elif params.get("start_date") and params["start_date"] != "":
        sql = sql + "AND dbt_batch.START_DATE >= '%s' " % (params["start_date"])
    elif params.get("end_date") and params.get("end_date") != "":
        sql = sql + "AND dbt_batch.END_DATE <= '%s' " % (params["end_date"])

    sql = sql + "ORDER BY dbm_lot.LOT_ID, dbt_batch.END_DATE DESC "
    # print(sql)

    return fetAllRawQuery(sql)


def getDailyHistTable(batch_id):
    sql = """
    SELECT
    dbt_summary.SUMMARY_DATE AS DAILY_DATE,
    dbm_lot.LOT_ID AS LOT_ID,
    dbm_lot.LOT_NAME AS LOT_NAME,
    dbt_summary.DAY_PERIODS AS DAY_PERIODS,
    dbt_summary.EGG_COUNT AS EGG_COUNT,
    dbt_summary.CURR_COUNT AS CURR_COUNT,
    CAST(dbt_summary.EGG_WEIGHT/10 AS DECIMAL(5, 1)) AS EGG_WEIGHT,
    dbt_summary.HEN_WEIGHT AS HEN_WEIGHT,
    CAST(dbt_summary.FOOD_AMOUNT/1000 AS DECIMAL(5, 1)) AS FOOD_AMOUNT,
    ROUND(dbt_summary.FOOD_AMOUNT*1000/CURR_COUNT,1) AS FOOD_AMOUNT_U,
    ROUND(EGG_WEIGHT/10*dbt_summary.EGG_COUNT/dbt_summary.CURR_COUNT,1) AS EGG_VOL,
    dbt_batch.INITIAL_COUNT AS INITIAL_COUNT,
    ROUND(CURR_COUNT/INITIAL_COUNT*100,1) AS SURVIVAL_RATE,
    ROUND(EGG_COUNT/CURR_COUNT*100,1) AS EGG_RATE_CUR,
    ROUND(EGG_COUNT/INITIAL_COUNT*100,1) AS EGG_RATE_INI,
    ROUND(HEN_WEIGHT/(FOOD_AMOUNT*1000/CURR_COUNT),1) AS FOOD_EFFICEINCY,
    ROUND(FOOD_AMOUNT*1000/(EGG_COUNT*EGG_WEIGHT/10),1) AS FOOD_DEMAND,
    dbt_summary.WATER_AMOUNT AS WATER_AMOUNT,
    ROUND(WATER_AMOUNT*1000/CURR_COUNT,1) AS WATER_AMOUNT_U,
    CAST(dbt_summary.PRESET_TEMP/10 AS DECIMAL(5, 1)) AS PRESET_TEMP,
    CAST(dbt_summary.ROOM_TEMP/10 AS DECIMAL(5, 1)) AS ROOM_TEMP,
    CAST(dbt_summary.ROOM_TEMP_HI/10 AS DECIMAL(5, 1)) AS ROOM_TEMP_HI,
    CAST(dbt_summary.ROOM_TEMP_LO/10 AS DECIMAL(5, 1)) AS ROOM_TEMP_LO,
    CAST(dbt_summary.OUTER_TEMP/10 AS DECIMAL(5, 1)) AS OUTER_TEMP,
    CAST(dbt_summary.OUTER_TEMP_HI/10 AS DECIMAL(5, 1)) AS OUTER_TEMP_HI,
    CAST(dbt_summary.OUTER_TEMP_LO/10 AS DECIMAL(5, 1)) AS OUTER_TEMP_LO,
    CAST(dbt_summary.DAYLIGHT_HOURS/100 AS DECIMAL(5, 2)) AS DAYLIGHT_HOURS,
    CAST(dbt_summary.DEFECTION/100  AS DECIMAL(5, 2))AS DEFECTION,
    CAST(dbt_summary.COEFF/100  AS DECIMAL(5, 2)) AS COEFF,
    INITIAL_COUNT-CURR_COUNT AS TOTAL_DEAD,
    ROUND(dbt_summary.HUMIDITY/10,1) AS HUMIDITY,
    ROUND(dbt_summary.HUMIDITY_UPL/10,1) AS HUMIDITY_UPL,
    ROUND(dbt_summary.HUMIDITY_LOL/10,1) AS HUMIDITY_LOL,
    ROUND(dbt_summary.MIN_VENTILATION/100,2) AS MIN_VENTILATION,
    ROUND(dbt_summary.BMIN_VENTILATION/100,2) AS BMIN_VENTILATION,
    ROUND(dbt_summary.VENTILATION_SETUP/100,2) AS VENTILATION_SETUP,
    dbt_summary.REMV_EXCR AS REMV_EXCR,
    dbt_summary.HEATING AS HEATING,
    dbt_summary.WATER_POS AS WATER_POS,
    dbt_summary.AIRCON_SYSTEM AS AIRCON_SYSTEM,
    dbt_summary.LIGHTING_TIME AS LIGHTING_TIME,
    dbt_summary.FOOD AS FOOD,
    dbt_summary.COMMENTS AS COMMENTS
    FROM
    (dbm_lot
    INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
    INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID
    WHERE dbt_summary.BATCH_ID = %s ORDER BY DAILY_DATE DESC
    """
    # get daily table for last daily_table_lines days
    sql = sql % (batch_id)

    return fetAllRawQuery(sql)


def getTodayDailyTable(lot_id):
    return getDailyTable(lot_id, 0)


def getBatchInfoByGrowthKind(growth_kind):
    sql = """
    SELECT *
    FROM dbt_batch
    WHERE GROWTH_KIND = '%s' AND dbt_batch.END_DATE < CURRENT_DATE
    """
    return fetAllRawQuery(sql % (growth_kind))


def getDailyPastTable(lot_id, day_p_s, day_p_e):
    debugPrintEx("***")
    print(day_p_s)
    print(day_p_e)

    # Get growth_kind, feeding_date from lot_id
    row = getBatchInfoByLotID(lot_id)
    growth_kind = row[0]
    feeding_date = row[1]
    # print("growth_kind=%s" % (growth_kind))
    # print("feeding_date=%s" % (feeding_date))

    rows = getBatchInfoByGrowthKind(growth_kind)
    # print(rows)

    # Get BATCH_ID of most nearest feeding date
    month_c = feeding_date.month
    day_c = feeding_date.day
    mindays = 999
    batch_id = 0
    for row in rows:
        fdate = row["FEEDING_DATE"]
        year = fdate.year
        month = fdate.month
        day = fdate.day
        day_curr = datetime.date(year, month_c, day_c)
        day_past = datetime.date(year, month, day)
        periods = abs((day_curr - day_past).days)
        if periods < mindays:
            batch_id = row["BATCH_ID"]
            mindays = periods
        # endif
    # endfor

    result = []
    if batch_id > 0:
        lot_id = row["LOT_ID"]

        sql = """
        SELECT
        dbt_summary.SUMMARY_DATE AS DAILY_DATE,
        dbm_lot.LOT_ID AS LOT_ID,
        dbm_lot.LOT_NAME AS LOT_NAME,
        dbt_summary.DAY_PERIODS AS DAY_PERIODS,
        dbt_summary.EGG_COUNT AS EGG_COUNT,
        dbt_summary.CURR_COUNT AS CURR_COUNT,
        CAST(dbt_summary.EGG_WEIGHT / 10 AS DECIMAL(5, 1)) AS EGG_WEIGHT,
        dbt_summary.HEN_WEIGHT AS HEN_WEIGHT,
        dbt_summary.FOOD_AMOUNT/1000 AS FOOD_AMOUNT,
        case
            when (
            CURR_COUNT > 0
            and dbt_summary.FOOD_AMOUNT > 0
            ) then ROUND((dbt_summary.FOOD_AMOUNT / CURR_COUNT) * 1000, 1)
            else 0
        end AS FOOD_AMOUNT_U,
        case
            when (
            EGG_COUNT > 0
            and CURR_COUNT > 0
            ) then ROUND(EGG_WEIGHT / 10 * EGG_COUNT / CURR_COUNT, 1)
            else 0
        end AS EGG_VOL,
        dbt_batch.INITIAL_COUNT AS INITIAL_COUNT,
        case
            when (INITIAL_COUNT > 0) then ROUND(CURR_COUNT / INITIAL_COUNT * 100, 1)
            else 0
        end AS SURVIVAL_RATE,
        case
            when (CURR_COUNT > 0) then ROUND(EGG_COUNT / CURR_COUNT * 100, 1)
            else 0
        end AS EGG_RATE_CUR,
        case
            when (INITIAL_COUNT > 0) then ROUND(EGG_COUNT / INITIAL_COUNT * 100, 1)
            else 0
        end AS EGG_RATE_INI,
        case
            when (
            FOOD_AMOUNT > 0
            and CURR_COUNT > 0
            and HEN_WEIGHT > 0
            ) then ROUND(HEN_WEIGHT / (FOOD_AMOUNT / CURR_COUNT * 1000), 1)
            else 0
        end AS FOOD_EFFICEINCY,
        case
            when (
            EGG_COUNT > 0
            and EGG_WEIGHT > 0
            ) then ROUND(
            FOOD_AMOUNT * 1000 / (EGG_COUNT * EGG_WEIGHT / 10),
            1
            )
            else 0
        end AS FOOD_DEMAND,
        dbt_summary.WATER_AMOUNT AS WATER_AMOUNT,
        ROUND(WATER_AMOUNT * 1000 / CURR_COUNT, 1) AS WATER_AMOUNT_U,
        CAST(dbt_summary.PRESET_TEMP / 10 AS DECIMAL(5, 1)) AS PRESET_TEMP,
        CAST(dbt_summary.ROOM_TEMP / 10 AS DECIMAL(5, 1)) AS ROOM_TEMP,
        CAST(dbt_summary.ROOM_TEMP_HI / 10 AS DECIMAL(5, 1)) AS ROOM_TEMP_HI,
        CAST(dbt_summary.ROOM_TEMP_LO / 10 AS DECIMAL(5, 1)) AS ROOM_TEMP_LO,
        CAST(dbt_summary.OUTER_TEMP / 10 AS DECIMAL(5, 1)) AS OUTER_TEMP,
        CAST(dbt_summary.OUTER_TEMP_HI / 10 AS DECIMAL(5, 1)) AS OUTER_TEMP_HI,
        CAST(dbt_summary.OUTER_TEMP_LO / 10 AS DECIMAL(5, 1)) AS OUTER_TEMP_LO,
        CAST(dbt_summary.DAYLIGHT_HOURS / 100 AS DECIMAL(5, 2)) AS DAYLIGHT_HOURS,
        CAST(dbt_summary.DEFECTION / 100 AS DECIMAL(5, 2)) AS DEFECTION,
        CAST(dbt_summary.COEFF / 100 AS DECIMAL(5, 2)) AS COEFF,
        INITIAL_COUNT - CURR_COUNT AS TOTAL_DEAD,
        ROUND(dbt_summary.HUMIDITY / 10, 1) AS HUMIDITY,
        ROUND(dbt_summary.HUMIDITY_UPL / 10, 1) AS HUMIDITY_UPL,
        ROUND(dbt_summary.HUMIDITY_LOL / 10, 1) AS HUMIDITY_LOL,
        ROUND(dbt_summary.MIN_VENTILATION / 100, 2) AS MIN_VENTILATION,
        ROUND(dbt_summary.BMIN_VENTILATION / 100, 2) AS BMIN_VENTILATION,
        ROUND(dbt_summary.VENTILATION_SETUP / 100, 2) AS VENTILATION_SETUP,
        dbt_summary.REMV_EXCR AS REMV_EXCR,
        dbt_summary.HEATING AS HEATING,
        dbt_summary.WATER_POS AS WATER_POS,
        dbt_summary.AIRCON_SYSTEM AS AIRCON_SYSTEM,
        dbt_summary.LIGHTING_TIME AS LIGHTING_TIME,
        dbt_summary.FOOD AS FOOD,
        dbt_summary.COMMENTS AS COMMENTS
        FROM
        (dbm_lot
        INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
        INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID
        WHERE dbt_summary.BATCH_ID = %s
        AND dbt_summary.DAY_PERIODS BETWEEN %s AND %s ORDER BY DAILY_DATE DESC
        """
        sql = sql % (batch_id, day_p_s, day_p_e)
        result = fetAllRawQuery(sql)

    return result

def getDailyPastTableByBatchID(batch_id, day_p_s, day_p_e):
    debugPrintEx("***")
    print(day_p_s)
    print(day_p_e)

    result = []
    if batch_id:
        sql = """
        SELECT
        dbt_summary.SUMMARY_DATE AS DAILY_DATE,
        dbm_lot.LOT_ID AS LOT_ID,
        dbm_lot.LOT_NAME AS LOT_NAME,
        dbt_summary.DAY_PERIODS AS DAY_PERIODS,
        dbt_summary.EGG_COUNT AS EGG_COUNT,
        dbt_summary.CURR_COUNT AS CURR_COUNT,
        CAST(dbt_summary.EGG_WEIGHT / 10 AS DECIMAL(5, 1)) AS EGG_WEIGHT,
        dbt_summary.HEN_WEIGHT AS HEN_WEIGHT,
        dbt_summary.FOOD_AMOUNT/1000 AS FOOD_AMOUNT,
        case
            when (
            CURR_COUNT > 0
            and dbt_summary.FOOD_AMOUNT > 0
            ) then ROUND((dbt_summary.FOOD_AMOUNT / CURR_COUNT) * 1000, 1)
            else 0
        end AS FOOD_AMOUNT_U,
        case
            when (
            EGG_COUNT > 0
            and CURR_COUNT > 0
            ) then ROUND(EGG_WEIGHT / 10 * EGG_COUNT / CURR_COUNT, 1)
            else 0
        end AS EGG_VOL,
        dbt_batch.INITIAL_COUNT AS INITIAL_COUNT,
        case
            when (INITIAL_COUNT > 0) then ROUND(CURR_COUNT / INITIAL_COUNT * 100, 1)
            else 0
        end AS SURVIVAL_RATE,
        case
            when (CURR_COUNT > 0) then ROUND(EGG_COUNT / CURR_COUNT * 100, 1)
            else 0
        end AS EGG_RATE_CUR,
        case
            when (INITIAL_COUNT > 0) then ROUND(EGG_COUNT / INITIAL_COUNT * 100, 1)
            else 0
        end AS EGG_RATE_INI,
        case
            when (
            FOOD_AMOUNT > 0
            and CURR_COUNT > 0
            and HEN_WEIGHT > 0
            ) then ROUND(HEN_WEIGHT / (FOOD_AMOUNT / CURR_COUNT * 1000), 1)
            else 0
        end AS FOOD_EFFICEINCY,
        case
            when (
            EGG_COUNT > 0
            and EGG_WEIGHT > 0
            ) then ROUND(
            FOOD_AMOUNT * 1000 / (EGG_COUNT * EGG_WEIGHT / 10),
            1
            )
            else 0
        end AS FOOD_DEMAND,
        dbt_summary.WATER_AMOUNT AS WATER_AMOUNT,
        ROUND(WATER_AMOUNT * 1000 / CURR_COUNT, 1) AS WATER_AMOUNT_U,
        CAST(dbt_summary.PRESET_TEMP / 10 AS DECIMAL(5, 1)) AS PRESET_TEMP,
        CAST(dbt_summary.ROOM_TEMP / 10 AS DECIMAL(5, 1)) AS ROOM_TEMP,
        CAST(dbt_summary.ROOM_TEMP_HI / 10 AS DECIMAL(5, 1)) AS ROOM_TEMP_HI,
        CAST(dbt_summary.ROOM_TEMP_LO / 10 AS DECIMAL(5, 1)) AS ROOM_TEMP_LO,
        CAST(dbt_summary.OUTER_TEMP / 10 AS DECIMAL(5, 1)) AS OUTER_TEMP,
        CAST(dbt_summary.OUTER_TEMP_HI / 10 AS DECIMAL(5, 1)) AS OUTER_TEMP_HI,
        CAST(dbt_summary.OUTER_TEMP_LO / 10 AS DECIMAL(5, 1)) AS OUTER_TEMP_LO,
        CAST(dbt_summary.DAYLIGHT_HOURS / 100 AS DECIMAL(5, 2)) AS DAYLIGHT_HOURS,
        CAST(dbt_summary.DEFECTION / 100 AS DECIMAL(5, 2)) AS DEFECTION,
        CAST(dbt_summary.COEFF / 100 AS DECIMAL(5, 2)) AS COEFF,
        INITIAL_COUNT - CURR_COUNT AS TOTAL_DEAD,
        ROUND(dbt_summary.HUMIDITY / 10, 1) AS HUMIDITY,
        ROUND(dbt_summary.HUMIDITY_UPL / 10, 1) AS HUMIDITY_UPL,
        ROUND(dbt_summary.HUMIDITY_LOL / 10, 1) AS HUMIDITY_LOL,
        ROUND(dbt_summary.MIN_VENTILATION / 100, 2) AS MIN_VENTILATION,
        ROUND(dbt_summary.BMIN_VENTILATION / 100, 2) AS BMIN_VENTILATION,
        ROUND(dbt_summary.VENTILATION_SETUP / 100, 2) AS VENTILATION_SETUP,
        dbt_summary.REMV_EXCR AS REMV_EXCR,
        dbt_summary.HEATING AS HEATING,
        dbt_summary.WATER_POS AS WATER_POS,
        dbt_summary.AIRCON_SYSTEM AS AIRCON_SYSTEM,
        dbt_summary.LIGHTING_TIME AS LIGHTING_TIME,
        dbt_summary.FOOD AS FOOD,
        dbt_summary.COMMENTS AS COMMENTS
        FROM
            (dbm_lot
            INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
            INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID
        WHERE dbt_summary.BATCH_ID = %s
            AND dbt_summary.DAY_PERIODS BETWEEN %s AND %s ORDER BY DAILY_DATE DESC
        """
        sql = sql % (batch_id, day_p_s, day_p_e)
        result = fetAllRawQuery(sql)

    return result

def getDailyTablePeriods(lot_id, s_date, e_date):
    debugPrintEx("***")
    sql = """
    SELECT * FROM dbv_daily
    WHERE
    LOT_ID = %s AND DAILY_DATE BETWEEN '%s' AND '%s'
    ORDER BY DAILY_DATE DESC
    """
    return fetAllRawQuery(sql % (lot_id, s_date, e_date))


def getWeeklyTable(lot_id, daily_table_lines):
    debugPrintEx("***")
    result = []

    # print("++++detail+++++")
    # Get feeding_date from lot_id
    result = getBatchInfoByLotID(lot_id)
    if result != None:
        #        feeding_date = result[1].strftime('%Y-%m-%d')
        #        yesterday = (datetime.date.today() -datetime.timedelta(1)).strftime('%Y-%m-%d')

        result = []

        sql = """
        SELECT
            MAX(dbt_summary.SUMMARY_DATE) AS WEEKLY_DATE,
            dbt_summary.WEEK_PERIODS AS WEEK_PERIODS,
            dbm_lot.LOT_ID AS LOT_ID,
            dbm_lot.LOT_NAME AS LOT_NAME,
            dbm_house.HOUSE_ID AS HOUSE_ID,
            dbm_house.HOUSE_NAME AS HOUSE_NAME,
            ROUND(AVG(dbt_summary.EGG_COUNT), 0) AS EGG_COUNT,
            ROUND(AVG(dbt_summary.CURR_COUNT), 0) AS CURR_COUNT,
            ROUND(AVG(dbt_summary.EGG_WEIGHT) / 10, 1) AS EGG_WEIGHT,
            -- case when (EGG_COUNT > 0 and CURR_COUNT > 0) then ROUND(AVG(dbt_summary.EGG_WEIGHT / 10) * EGG_COUNT / CURR_COUNT, 1) else 0 end AS EGG_VOL,
            MIN(dbt_batch.INITIAL_COUNT) AS INITIAL_COUNT,
            ROUND(AVG(dbt_summary.HEN_WEIGHT), 0) AS HEN_WEIGHT,
            CAST(
                ROUND(AVG(dbt_summary.FOOD_AMOUNT), 0) / 1000 AS DECIMAL(5, 1)
            ) AS FOOD_AMOUNT,
            -- case when (CURR_COUNT > 0) then ROUND(FOOD_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS FOOD_AMOUNT_U,
            -- case when (INITIAL_COUNT > 0) then ROUND(CURR_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS SURVIVAL_RATE,
            -- case when (CURR_COUNT > 0) then  ROUND(EGG_COUNT / CURR_COUNT * 100, 1) else 0 end AS EGG_RATE_CUR,
            -- case when (INITIAL_COUNT > 0) then ROUND(EGG_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS EGG_RATE_INI,
            -- case when (dbt_summary.HEN_WEIGHT > 0 and FOOD_AMOUNT > 0 and CURR_COUNT > 0) then ROUND(AVG(dbt_summary.HEN_WEIGHT) / (FOOD_AMOUNT / CURR_COUNT * 1000), 1) else 0 end AS FOOD_EFFICEINCY,
            -- case when (EGG_COUNT > 0 and dbt_summary.EGG_WEIGHT > 0) then ROUND(FOOD_AMOUNT / (EGG_COUNT * AVG(dbt_summary.EGG_WEIGHT / 10)) * 1000, 1) else 0 end AS FOOD_DEMAND,
            ROUND(AVG(dbt_summary.PRESET_TEMP) / 10, 1) AS PRESET_TEMP,
            ROUND(AVG(dbt_summary.DAYLIGHT_HOURS) / 100, 2) AS DAYLIGHT_HOURS,
            ROUND(AVG(dbt_summary.WATER_AMOUNT), 0) AS WATER_AMOUNT,
            -- case when (CURR_COUNT > 0) then ROUND(WATER_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS WATER_AMOUNT_U,
            ROUND(AVG(dbt_summary.ROOM_TEMP) / 10, 1) AS ROOM_TEMP,
            CAST(
                MAX(dbt_summary.ROOM_TEMP_HI) / 10 AS DECIMAL(5, 1)
            ) AS ROOM_TEMP_HI,
            CAST(
                MIN(dbt_summary.ROOM_TEMP_LO) / 10 AS DECIMAL(5, 1)
            ) AS ROOM_TEMP_LO,
            ROUND(AVG(dbt_summary.OUTER_TEMP) / 10, 1) AS OUTER_TEMP,
            CAST(
                MAX(dbt_summary.OUTER_TEMP_HI) / 10 AS DECIMAL(5, 1)
            ) AS OUTER_TEMP_HI,
            CAST(
                MIN(dbt_summary.OUTER_TEMP_LO) / 10 AS DECIMAL(5, 1)
            ) AS OUTER_TEMP_LO,
            ROUND(AVG(dbt_summary.DEFECTION) / 100, 2) AS DEFECTION,
            ROUND(AVG(dbt_summary.COEFF) / 100, 2) AS COEFF,
            MIN(dbt_batch.INITIAL_COUNT) - MIN(dbt_summary.CURR_COUNT) AS TOTAL_DEAD,
            ROUND(AVG(dbt_summary.HUMIDITY) / 10, 1) AS HUMIDITY,
            ROUND(MAX(dbt_summary.HUMIDITY_UPL) / 10, 1) AS HUMIDITY_UPL,
            ROUND(MIN(dbt_summary.HUMIDITY_LOL) / 10, 1) AS HUMIDITY_LOL,
            ROUND(AVG(dbt_summary.MIN_VENTILATION) / 100, 2) AS MIN_VENTILATION,
            ROUND(AVG(dbt_summary.BMIN_VENTILATION) / 100, 2) AS BMIN_VENTILATION,
            ROUND(AVG(dbt_summary.VENTILATION_SETUP) / 100, 2) AS VENTILATION_SETUP,
            dbm_hen.HEN_NAME AS HEN_NAME,
            MAX(dbm_manual.HEN_WEIGHT) AS HEN_WEIGHT_MAN,
            ROUND(MAX(dbm_manual.EGG_WEIGHT)/ 10, 1) AS EGG_WEIGHT_MAN,
            ROUND(MAX(dbm_manual.EGG_RATE)/ 10, 1) AS EGG_RATE_MAN,
            ROUND(MAX(dbm_manual.EGG_VOL)/ 10, 1) AS EGG_VOL_MAN
        FROM
            ((((dbm_lot
            INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
            INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID)
            INNER JOIN dbm_house ON dbm_lot.HOUSE_ID = dbm_house.HOUSE_ID)
            INNER JOIN dbm_manual ON dbt_batch.HEN_ID = dbm_manual.HEN_ID AND dbt_summary.WEEK_PERIODS = dbm_manual.WEEK_PERIODS)
            INNER JOIN dbm_hen ON dbt_batch.HEN_ID = dbm_hen.HEN_ID
        WHERE dbm_lot.LOT_ID = %s AND dbt_batch.END_DATE > CURRENT_DATE
        GROUP BY 
            dbm_lot.LOT_ID, 
            dbm_house.HOUSE_ID,
            dbm_hen.HEN_NAME,
            dbt_summary.WEEK_PERIODS
        ORDER BY WEEK_PERIODS DESC
        """
        # get weekly table for last weekly_table_lines days
        sql = sql % (lot_id)
        return fetAllRawQuery(sql)
        # endfor
    else:
        result = []
    # endif

    return result


def getWeeklyHistTable(batch_id):
    debugPrintEx("***")

    sql = """
    SELECT
        MAX(dbt_summary.SUMMARY_DATE) AS WEEKLY_DATE,
        dbt_summary.WEEK_PERIODS AS WEEK_PERIODS,
        dbm_lot.LOT_ID AS LOT_ID,
        dbm_lot.LOT_NAME AS LOT_NAME,
        dbm_house.HOUSE_ID AS HOUSE_ID,
        dbm_house.HOUSE_NAME AS HOUSE_NAME,
        ROUND(AVG(dbt_summary.EGG_COUNT), 0) AS EGG_COUNT,
        ROUND(AVG(dbt_summary.CURR_COUNT), 0) AS CURR_COUNT,
        ROUND(AVG(dbt_summary.EGG_WEIGHT) / 10, 1) AS EGG_WEIGHT,
        -- case when (EGG_COUNT > 0 and CURR_COUNT > 0) then ROUND(AVG(dbt_summary.EGG_WEIGHT / 10) * EGG_COUNT / CURR_COUNT, 1) else 0 end AS EGG_VOL,
        MIN(dbt_batch.INITIAL_COUNT) AS INITIAL_COUNT,
        ROUND(AVG(dbt_summary.HEN_WEIGHT), 0) AS HEN_WEIGHT,
        CAST(
            ROUND(AVG(dbt_summary.FOOD_AMOUNT), 0) / 1000 AS DECIMAL(5, 1)
        ) AS FOOD_AMOUNT,
        -- case when (CURR_COUNT > 0) then ROUND(FOOD_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS FOOD_AMOUNT_U,
        -- case when (INITIAL_COUNT > 0) then ROUND(CURR_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS SURVIVAL_RATE,
        -- case when (CURR_COUNT > 0) then  ROUND(EGG_COUNT / CURR_COUNT * 100, 1) else 0 end AS EGG_RATE_CUR,
        -- case when (INITIAL_COUNT > 0) then ROUND(EGG_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS EGG_RATE_INI,
        -- case when (dbt_summary.HEN_WEIGHT > 0 and FOOD_AMOUNT > 0 and CURR_COUNT > 0) then ROUND(AVG(dbt_summary.HEN_WEIGHT) / (FOOD_AMOUNT / CURR_COUNT * 1000), 1) else 0 end AS FOOD_EFFICEINCY,
        -- case when (EGG_COUNT > 0 and dbt_summary.EGG_WEIGHT > 0) then ROUND(FOOD_AMOUNT / (EGG_COUNT * AVG(dbt_summary.EGG_WEIGHT / 10)) * 1000, 1) else 0 end AS FOOD_DEMAND,
        ROUND(AVG(dbt_summary.PRESET_TEMP) / 10, 1) AS PRESET_TEMP,
        ROUND(AVG(dbt_summary.DAYLIGHT_HOURS) / 100, 2) AS DAYLIGHT_HOURS,
        ROUND(AVG(dbt_summary.WATER_AMOUNT), 0) AS WATER_AMOUNT,
        -- case when (CURR_COUNT > 0) then ROUND(WATER_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS WATER_AMOUNT_U,
        ROUND(AVG(dbt_summary.ROOM_TEMP) / 10, 1) AS ROOM_TEMP,
        CAST(
            MAX(dbt_summary.ROOM_TEMP_HI) / 10 AS DECIMAL(5, 1)
        ) AS ROOM_TEMP_HI,
        CAST(
            MIN(dbt_summary.ROOM_TEMP_LO) / 10 AS DECIMAL(5, 1)
        ) AS ROOM_TEMP_LO,
        ROUND(AVG(dbt_summary.OUTER_TEMP) / 10, 1) AS OUTER_TEMP,
        CAST(
            MAX(dbt_summary.OUTER_TEMP_HI) / 10 AS DECIMAL(5, 1)
        ) AS OUTER_TEMP_HI,
        CAST(
            MIN(dbt_summary.OUTER_TEMP_LO) / 10 AS DECIMAL(5, 1)
        ) AS OUTER_TEMP_LO,
        ROUND(AVG(dbt_summary.DEFECTION) / 100, 2) AS DEFECTION,
        ROUND(AVG(dbt_summary.COEFF) / 100, 2) AS COEFF,
        MIN(dbt_batch.INITIAL_COUNT) - MIN(dbt_summary.CURR_COUNT) AS TOTAL_DEAD,
        ROUND(AVG(dbt_summary.HUMIDITY) / 10, 1) AS HUMIDITY,
        ROUND(MAX(dbt_summary.HUMIDITY_UPL) / 10, 1) AS HUMIDITY_UPL,
        ROUND(MIN(dbt_summary.HUMIDITY_LOL) / 10, 1) AS HUMIDITY_LOL,
        ROUND(AVG(dbt_summary.MIN_VENTILATION) / 100, 2) AS MIN_VENTILATION,
        ROUND(AVG(dbt_summary.BMIN_VENTILATION) / 100, 2) AS BMIN_VENTILATION,
        ROUND(AVG(dbt_summary.VENTILATION_SETUP) / 100, 2) AS VENTILATION_SETUP,
        dbm_hen.HEN_NAME AS HEN_NAME,
        MAX(dbm_manual.HEN_WEIGHT) AS HEN_WEIGHT_MAN,
        ROUND(MAX(dbm_manual.EGG_WEIGHT)/ 10, 1) AS EGG_WEIGHT_MAN,
        ROUND(MAX(dbm_manual.EGG_RATE)/ 10, 1) AS EGG_RATE_MAN,
        ROUND(MAX(dbm_manual.EGG_VOL)/ 10, 1) AS EGG_VOL_MAN
    FROM
    ((((dbm_lot
        INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
        INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID)
        INNER JOIN dbm_house ON dbm_lot.HOUSE_ID = dbm_house.HOUSE_ID)
        INNER JOIN dbm_manual ON dbt_batch.HEN_ID = dbm_manual.HEN_ID AND dbt_summary.WEEK_PERIODS = dbm_manual.WEEK_PERIODS)
        INNER JOIN dbm_hen ON dbt_batch.HEN_ID = dbm_hen.HEN_ID
    WHERE dbt_batch.BATCH_ID = %s
    GROUP BY 
        dbm_lot.LOT_ID, 
        dbm_house.HOUSE_ID,
        dbm_hen.HEN_NAME,
        dbt_summary.WEEK_PERIODS
    ORDER BY WEEK_PERIODS DESC
    """
    # get weekly table for last weekly_table_lines days
    sql = sql % (batch_id)
    # print(sql)
    return fetAllRawQuery(sql)


def getWeeklyPastTable(lot_id, week_p_s, week_p_e):
    debugPrintEx("***")
    # Get growth_kind, feeding_date from lot_id
    row = getBatchInfoByLotID(lot_id)
    growth_kind = row[0]
    feeding_date = row[1]
    # print("growth_kind=%s" % (growth_kind))
    # print("feeding_date=%s" % (feeding_date))

    rows = getBatchInfoByGrowthKind(growth_kind)
    # print(rows)

    # Get BATCH_ID of most nearest feeding date
    month_c = feeding_date.month
    day_c = feeding_date.day
    mindays = 999
    batch_id = 0
    for row in rows:
        fdate = row["FEEDING_DATE"]
        year = fdate.year
        month = fdate.month
        day = fdate.day
        day_curr = datetime.date(year, month_c, day_c)
        day_past = datetime.date(year, month, day)
        periods = abs((day_curr - day_past).days)
        if periods < mindays:
            batch_id = row["BATCH_ID"]
            mindays = periods
        # endif
    # endfor

    result = []
    if batch_id > 0:
        result = []

        lot_id = row["LOT_ID"]
        #        feeding_date = row['FEEDING_DATE']
        #        pend_date = feeding_date + datetime.timedelta(days)

        sql = """
        SELECT
            MAX(dbt_summary.SUMMARY_DATE) AS WEEKLY_DATE,
            dbt_summary.WEEK_PERIODS AS WEEK_PERIODS,
            dbm_lot.LOT_ID AS LOT_ID,
            dbm_lot.LOT_NAME AS LOT_NAME,
            dbm_house.HOUSE_ID AS HOUSE_ID,
            dbm_house.HOUSE_NAME AS HOUSE_NAME,
            ROUND(AVG(dbt_summary.EGG_COUNT), 0) AS EGG_COUNT,
            ROUND(AVG(dbt_summary.CURR_COUNT), 0) AS CURR_COUNT,
            ROUND(AVG(dbt_summary.EGG_WEIGHT) / 10, 1) AS EGG_WEIGHT,
            -- case when (EGG_COUNT > 0 and CURR_COUNT > 0) then ROUND(AVG(dbt_summary.EGG_WEIGHT / 10) * EGG_COUNT / CURR_COUNT, 1) else 0 end AS EGG_VOL,
            MIN(dbt_batch.INITIAL_COUNT) AS INITIAL_COUNT,
            ROUND(AVG(dbt_summary.HEN_WEIGHT), 0) AS HEN_WEIGHT,
            CAST(
                ROUND(AVG(dbt_summary.FOOD_AMOUNT), 0) / 1000 AS DECIMAL(5, 1)
            ) AS FOOD_AMOUNT,
            -- case when (CURR_COUNT > 0) then ROUND(FOOD_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS FOOD_AMOUNT_U,
            -- case when (INITIAL_COUNT > 0) then ROUND(CURR_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS SURVIVAL_RATE,
            -- case when (CURR_COUNT > 0) then  ROUND(EGG_COUNT / CURR_COUNT * 100, 1) else 0 end AS EGG_RATE_CUR,
            -- case when (INITIAL_COUNT > 0) then ROUND(EGG_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS EGG_RATE_INI,
            -- case when (dbt_summary.HEN_WEIGHT > 0 and FOOD_AMOUNT > 0 and CURR_COUNT > 0) then ROUND(AVG(dbt_summary.HEN_WEIGHT) / (FOOD_AMOUNT / CURR_COUNT * 1000), 1) else 0 end AS FOOD_EFFICEINCY,
            -- case when (EGG_COUNT > 0 and dbt_summary.EGG_WEIGHT > 0) then ROUND(FOOD_AMOUNT / (EGG_COUNT * AVG(dbt_summary.EGG_WEIGHT / 10)) * 1000, 1) else 0 end AS FOOD_DEMAND,
            ROUND(AVG(dbt_summary.PRESET_TEMP) / 10, 1) AS PRESET_TEMP,
            ROUND(AVG(dbt_summary.DAYLIGHT_HOURS) / 100, 2) AS DAYLIGHT_HOURS,
            ROUND(AVG(dbt_summary.WATER_AMOUNT), 0) AS WATER_AMOUNT,
            -- case when (CURR_COUNT > 0) then ROUND(WATER_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS WATER_AMOUNT_U,
            ROUND(AVG(dbt_summary.ROOM_TEMP) / 10, 1) AS ROOM_TEMP,
            CAST(
                MAX(dbt_summary.ROOM_TEMP_HI) / 10 AS DECIMAL(5, 1)
            ) AS ROOM_TEMP_HI,
            CAST(
                MIN(dbt_summary.ROOM_TEMP_LO) / 10 AS DECIMAL(5, 1)
            ) AS ROOM_TEMP_LO,
            ROUND(AVG(dbt_summary.OUTER_TEMP) / 10, 1) AS OUTER_TEMP,
            CAST(
                MAX(dbt_summary.OUTER_TEMP_HI) / 10 AS DECIMAL(5, 1)
            ) AS OUTER_TEMP_HI,
            CAST(
                MIN(dbt_summary.OUTER_TEMP_LO) / 10 AS DECIMAL(5, 1)
            ) AS OUTER_TEMP_LO,
            ROUND(AVG(dbt_summary.DEFECTION) / 100, 2) AS DEFECTION,
            ROUND(AVG(dbt_summary.COEFF) / 100, 2) AS COEFF,
            MIN(dbt_batch.INITIAL_COUNT) - MIN(dbt_summary.CURR_COUNT) AS TOTAL_DEAD,
            ROUND(AVG(dbt_summary.HUMIDITY) / 10, 1) AS HUMIDITY,
            ROUND(MAX(dbt_summary.HUMIDITY_UPL) / 10, 1) AS HUMIDITY_UPL,
            ROUND(MIN(dbt_summary.HUMIDITY_LOL) / 10, 1) AS HUMIDITY_LOL,
            ROUND(AVG(dbt_summary.MIN_VENTILATION) / 100, 2) AS MIN_VENTILATION,
            ROUND(AVG(dbt_summary.BMIN_VENTILATION) / 100, 2) AS BMIN_VENTILATION,
            ROUND(AVG(dbt_summary.VENTILATION_SETUP) / 100, 2) AS VENTILATION_SETUP,
            dbm_hen.HEN_NAME AS HEN_NAME,
            MAX(dbm_manual.HEN_WEIGHT) AS HEN_WEIGHT_MAN,
            ROUND(MAX(dbm_manual.EGG_WEIGHT)/ 10, 1) AS EGG_WEIGHT_MAN,
            ROUND(MAX(dbm_manual.EGG_RATE)/ 10, 1) AS EGG_RATE_MAN,
            ROUND(MAX(dbm_manual.EGG_VOL)/ 10, 1) AS EGG_VOL_MAN
        FROM
        ((((dbm_lot
            INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
            INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID)
            INNER JOIN dbm_house ON dbm_lot.HOUSE_ID = dbm_house.HOUSE_ID)
            INNER JOIN dbm_manual ON dbt_batch.HEN_ID = dbm_manual.HEN_ID AND dbt_summary.WEEK_PERIODS = dbm_manual.WEEK_PERIODS)
            INNER JOIN dbm_hen ON dbt_batch.HEN_ID = dbm_hen.HEN_ID
        WHERE dbt_summary.BATCH_ID = %s AND dbt_summary.WEEK_PERIODS BETWEEN %s AND %s
        GROUP BY 
            dbm_lot.LOT_ID,
            dbm_house.HOUSE_ID,
            dbm_hen.HEN_NAME,
            dbt_summary.WEEK_PERIODS
        ORDER BY dbt_summary.WEEK_PERIODS DESC
        """
        # get daily table for range of weekly_periods
        sql = sql % (batch_id, week_p_s, week_p_e)
        # print(sql)
        return fetAllRawQuery(sql)
    # endif

    return result

def getWeeklyPastTableByBatchID(batch_id, week_p_s, week_p_e):
    debugPrintEx("***")

    result = []
    if batch_id:
        sql = """
        SELECT
            MAX(dbt_summary.SUMMARY_DATE) AS WEEKLY_DATE,
            dbt_summary.WEEK_PERIODS AS WEEK_PERIODS,
            dbm_lot.LOT_ID AS LOT_ID,
            dbm_lot.LOT_NAME AS LOT_NAME,
            dbm_house.HOUSE_ID AS HOUSE_ID,
            dbm_house.HOUSE_NAME AS HOUSE_NAME,
            ROUND(AVG(dbt_summary.EGG_COUNT), 0) AS EGG_COUNT,
            ROUND(AVG(dbt_summary.CURR_COUNT), 0) AS CURR_COUNT,
            ROUND(AVG(dbt_summary.EGG_WEIGHT) / 10, 1) AS EGG_WEIGHT,
            -- case when (EGG_COUNT > 0 and CURR_COUNT > 0) then ROUND(AVG(dbt_summary.EGG_WEIGHT / 10) * EGG_COUNT / CURR_COUNT, 1) else 0 end AS EGG_VOL,
            MIN(dbt_batch.INITIAL_COUNT) AS INITIAL_COUNT,
            ROUND(AVG(dbt_summary.HEN_WEIGHT), 0) AS HEN_WEIGHT,
            CAST(
                ROUND(AVG(dbt_summary.FOOD_AMOUNT), 0) / 1000 AS DECIMAL(5, 1)
            ) AS FOOD_AMOUNT,
            -- case when (CURR_COUNT > 0) then ROUND(FOOD_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS FOOD_AMOUNT_U,
            -- case when (INITIAL_COUNT > 0) then ROUND(CURR_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS SURVIVAL_RATE,
            -- case when (CURR_COUNT > 0) then  ROUND(EGG_COUNT / CURR_COUNT * 100, 1) else 0 end AS EGG_RATE_CUR,
            -- case when (INITIAL_COUNT > 0) then ROUND(EGG_COUNT / INITIAL_COUNT * 100, 1) else 0 end AS EGG_RATE_INI,
            -- case when (dbt_summary.HEN_WEIGHT > 0 and FOOD_AMOUNT > 0 and CURR_COUNT > 0) then ROUND(AVG(dbt_summary.HEN_WEIGHT) / (FOOD_AMOUNT / CURR_COUNT * 1000), 1) else 0 end AS FOOD_EFFICEINCY,
            -- case when (EGG_COUNT > 0 and dbt_summary.EGG_WEIGHT > 0) then ROUND(FOOD_AMOUNT / (EGG_COUNT * AVG(dbt_summary.EGG_WEIGHT / 10)) * 1000, 1) else 0 end AS FOOD_DEMAND,
            ROUND(AVG(dbt_summary.PRESET_TEMP) / 10, 1) AS PRESET_TEMP,
            ROUND(AVG(dbt_summary.DAYLIGHT_HOURS) / 100, 2) AS DAYLIGHT_HOURS,
            ROUND(AVG(dbt_summary.WATER_AMOUNT), 0) AS WATER_AMOUNT,
            -- case when (CURR_COUNT > 0) then ROUND(WATER_AMOUNT / CURR_COUNT * 1000, 1) else 0 end AS WATER_AMOUNT_U,
            ROUND(AVG(dbt_summary.ROOM_TEMP) / 10, 1) AS ROOM_TEMP,
            CAST(
                MAX(dbt_summary.ROOM_TEMP_HI) / 10 AS DECIMAL(5, 1)
            ) AS ROOM_TEMP_HI,
            CAST(
                MIN(dbt_summary.ROOM_TEMP_LO) / 10 AS DECIMAL(5, 1)
            ) AS ROOM_TEMP_LO,
            ROUND(AVG(dbt_summary.OUTER_TEMP) / 10, 1) AS OUTER_TEMP,
            CAST(
                MAX(dbt_summary.OUTER_TEMP_HI) / 10 AS DECIMAL(5, 1)
            ) AS OUTER_TEMP_HI,
            CAST(
                MIN(dbt_summary.OUTER_TEMP_LO) / 10 AS DECIMAL(5, 1)
            ) AS OUTER_TEMP_LO,
            ROUND(AVG(dbt_summary.DEFECTION) / 100, 2) AS DEFECTION,
            ROUND(AVG(dbt_summary.COEFF) / 100, 2) AS COEFF,
            MIN(dbt_batch.INITIAL_COUNT) - MIN(dbt_summary.CURR_COUNT) AS TOTAL_DEAD,
            ROUND(AVG(dbt_summary.HUMIDITY) / 10, 1) AS HUMIDITY,
            ROUND(MAX(dbt_summary.HUMIDITY_UPL) / 10, 1) AS HUMIDITY_UPL,
            ROUND(MIN(dbt_summary.HUMIDITY_LOL) / 10, 1) AS HUMIDITY_LOL,
            ROUND(AVG(dbt_summary.MIN_VENTILATION) / 100, 2) AS MIN_VENTILATION,
            ROUND(AVG(dbt_summary.BMIN_VENTILATION) / 100, 2) AS BMIN_VENTILATION,
            ROUND(AVG(dbt_summary.VENTILATION_SETUP) / 100, 2) AS VENTILATION_SETUP,
            dbm_hen.HEN_NAME AS HEN_NAME,
            MAX(dbm_manual.HEN_WEIGHT) AS HEN_WEIGHT_MAN,
            ROUND(MAX(dbm_manual.EGG_WEIGHT)/ 10, 1) AS EGG_WEIGHT_MAN,
            ROUND(MAX(dbm_manual.EGG_RATE)/ 10, 1) AS EGG_RATE_MAN,
            ROUND(MAX(dbm_manual.EGG_VOL)/ 10, 1) AS EGG_VOL_MAN
        FROM
        ((((dbm_lot
            INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
            INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID)
            INNER JOIN dbm_house ON dbm_lot.HOUSE_ID = dbm_house.HOUSE_ID)
            INNER JOIN dbm_manual ON dbt_batch.HEN_ID = dbm_manual.HEN_ID AND dbt_summary.WEEK_PERIODS = dbm_manual.WEEK_PERIODS)
            INNER JOIN dbm_hen ON dbt_batch.HEN_ID = dbm_hen.HEN_ID
        WHERE dbt_summary.BATCH_ID = %s AND dbt_summary.WEEK_PERIODS BETWEEN %s AND %s
        GROUP BY 
            dbm_lot.LOT_ID,
            dbm_house.HOUSE_ID,
            dbm_hen.HEN_NAME,
            dbt_summary.WEEK_PERIODS
        ORDER BY dbt_summary.WEEK_PERIODS DESC
        """
        # get daily table for range of weekly_periods
        sql = sql % (batch_id, week_p_s, week_p_e)
        # print(sql)
        return fetAllRawQuery(sql)
    # endif

    return result


def getWeeklyTablePeriods(lot_id, s_week, e_week):
    sql = """
    SELECT * FROM dbv_weekly
    WHERE
    LOT_ID = %s AND WEEK_PERIODS BETWEEN %s AND %s
    ORDER BY WEEK_PERIODS DESC
    """
    # print(sql % (lot_id, s_week, e_week))
    return fetAllRawQuery(sql % (lot_id, s_week, e_week))


def getBatchHisInfoByLotID(lot_id):
    debugPrintEx("***")

    sql = """
    SELECT *
    FROM dbt_batch
    WHERE dbt_batch.LOT_ID = %s AND dbt_batch.END_DATE < CURRENT_DATE
    """
    sql = sql % (lot_id)
    # print(sql)
    return fetAllRawQuery(sql)


def getTodayTotalEggCount(lot_id):
    debugPrintEx("***")

    if lot_id == 0:
        sql = """
        SELECT SUM(EGG_COUNT) FROM dbt_summary WHERE SUMMARY_DATE = CURRENT_DATE
        """
        row = fetOneRawQueryNoColumns(sql)
    else:
        sql = """
        SELECT SUM(EGG_COUNT)
        FROM dbt_summary,dbt_batch
        WHERE dbt_batch.LOT_ID = %s AND dbt_summary.BATCH_ID = dbt_batch.BATCH_ID AND dbt_summary.SUMMARY_DATE = CURRENT_DATE
        """
        row = fetOneRawQueryNoColumns(sql % (lot_id))

    return row[0]

def getTotalEggCountByDate(lot_id, date):
    debugPrintEx("***")

    if lot_id == 0:
        sql = """
        SELECT SUM(EGG_COUNT) FROM dbt_summary WHERE SUMMARY_DATE = '%s'
        """
        sql = sql % (date)
        print(sql)
        row = fetOneRawQueryNoColumns(sql)
    else:
        sql = """
        SELECT SUM(EGG_COUNT)
        FROM dbt_summary,dbt_batch
        WHERE dbt_batch.LOT_ID = %s AND dbt_summary.BATCH_ID = dbt_batch.BATCH_ID AND dbt_summary.SUMMARY_DATE = '%s'
        """
        sql = sql % (lot_id, date)
        print(sql)
        row = fetOneRawQueryNoColumns(sql)

    return row[0]


def getEggCtrInfo(lot_id, type):
    debugPrintEx("***")

    if type == 0:
        sql = """
        SELECT DISTINCT(CAGE_ROW)
        FROM dbm_egg
        WHERE dbm_egg.LOT_ID = %s ORDER BY CAGE_ROW ASC
        """
    else:
        sql = """
        SELECT DISTINCT(CAGE_TIER)
        FROM dbm_egg
        WHERE dbm_egg.LOT_ID = %s ORDER BY CAGE_TIER DESC
        """

    return fetAllRawQuery(sql % (lot_id))


def getTodayEggCtrList(lot_id, day):
    debugPrintEx("***")

    sql = """
    SELECT
    dbt_egg.MEGG_ID AS MEGG_ID,
    dbm_egg.CAGE_ROW AS CAGE_ROW,
    dbm_egg.CAGE_TIER AS CAGE_TIER,
    dbt_egg.TEGG_ID AS TEGG_ID,
	dbt_egg.EGG_COUNT AS EGG_COUNT,
    dbt_summary.CURR_COUNT AS CURR_COUNT
    FROM
    (((dbm_lot
    INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
    INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID)
    INNER JOIN dbt_egg ON dbt_summary.COLLECT_ID = dbt_egg.COLLECT_ID)
    INNER JOIN dbm_egg ON dbm_egg.MEGG_ID = dbt_egg.MEGG_ID AND dbm_egg.LOT_ID = %s
    WHERE
    dbt_batch.LOT_ID = %s AND dbt_summary.SUMMARY_DATE = '%s'
    """
    # endif
    sql = sql % (lot_id, lot_id, day)
    # print(sql)
    return fetAllRawQuery(sql)


def getCageNum(lot_id):
    debugPrintEx("***")

    sql = """
    SELECT MAX(dbm_egg.EGGCTR_NO) FROM dbm_egg WHERE dbm_egg.LOT_ID = %s
    """
    sql = sql % (lot_id)
    # print(sql)
    row = fetOneRawQueryNoColumns(sql)
    # print(row[0])

    return row[0]


def getDailyEggCtrList(lot_id):
    debugPrintEx("***")

    cagenum = getCageNum(lot_id)
    result = []

    result = getBatchInfoByLotID(lot_id)
    if result != None:
        feeding_date = result[1].strftime("%Y-%m-%d")
        yesterday = (datetime.date.today() - datetime.timedelta(1)).strftime("%Y-%m-%d")

        result = []

        sql = """
        SELECT
        dbt_summary.SUMMARY_DATE AS EGG_DATE,
        dbm_lot.LOT_ID AS LOT_ID,
        dbm_lot.LOT_NAME AS LOT_NAME,
        dbm_egg.CAGE_ROW AS CAGE_ROW,
        dbm_egg.CAGE_TIER AS CAGE_TIER,
        dbt_summary.EGG_COUNT AS EGG_COUNT_T,
        dbt_egg.EGG_COUNT AS EGG_COUNT,
        dbt_summary.CURR_COUNT AS CURR_COUNT,
       FLOOR(dbt_egg.EGG_COUNT/(dbt_summary.CURR_COUNT/%s)*100*POW(10,2))/POW(10,2) AS EGG_RATE_CUR
        FROM
        (((dbm_lot
        INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
        INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID)
        INNER JOIN dbt_egg ON dbt_summary.COLLECT_ID = dbt_egg.COLLECT_ID)
        INNER JOIN dbm_egg ON dbm_egg.MEGG_ID = dbt_egg.MEGG_ID AND dbm_egg.LOT_ID = %s
        WHERE dbm_lot.LOT_ID = %s AND dbt_summary.SUMMARY_DATE BETWEEN '%s' AND '%s'
        ORDER BY EGG_DATE DESC, CAGE_ROW ASC, CAGE_TIER ASC
        """
        # get egg table
        sql = sql % (cagenum, lot_id, lot_id, feeding_date, yesterday)
        print(sql)
        result = fetAllRawQuery(sql)
    else:
        result = []

    return result


def getDailyEggCtrListPeriods(lot_id, s_date, e_date):
    debugPrintEx("***")

    cagenum = getCageNum(lot_id)
    result = []

    result = getBatchInfoByLotID(lot_id)
    if result != None:
        sql = """
        SELECT
        dbt_summary.SUMMARY_DATE AS EGG_DATE,
        dbm_lot.LOT_ID AS LOT_ID,
        dbm_lot.LOT_NAME AS LOT_NAME,
        dbm_egg.CAGE_ROW AS CAGE_ROW,
        dbm_egg.CAGE_TIER AS CAGE_TIER,
        dbt_egg.EGG_COUNT AS EGG_COUNT,
        FLOOR(dbt_egg.EGG_COUNT/(dbt_summary.CURR_COUNT/%s)*100*POW(10,2))/POW(10,2) AS EGG_RATE_CUR
        FROM
        (((dbm_lot
        INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
        INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID)
        INNER JOIN dbt_egg ON dbt_summary.COLLECT_ID = dbt_egg.COLLECT_ID)
        INNER JOIN dbm_egg ON dbm_egg.MEGG_ID = dbt_egg.MEGG_ID
        WHERE dbm_lot.LOT_ID = %s AND dbt_summary.SUMMARY_DATE BETWEEN '%s' AND '%s'
        ORDER BY EGG_DATE DESC, CAGE_ROW ASC, CAGE_TIER ASC
        """
        # get egg table
        sql = sql % (cagenum, lot_id, s_date, e_date)
        # print(sql)
        result = fetAllRawQuery(sql)
        # endfor
    else:
        result = []

    return result


def getDailyGraphFw(lot_id, start_tdate, end_tdate):
    debugPrintEx("***")

    sql = """
    SELECT
    DISTINCT(dbt_collection.COL_TDATE) AS COL_TDATE,
    dbm_lot.LOT_ID AS LOT_ID,
    ROUND((dbt_fw.FOOD_AMOUNT)/dbt_summary.CURR_COUNT,1) AS FOOD_AMOUNT_U,
    ROUND(dbt_fw.WATER_AMOUNT*1000/dbt_summary.CURR_COUNT,1) AS WATER_AMOUNT_U
    FROM
    (((dbm_lot
    INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
    INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID)
    INNER JOIN dbt_collection ON dbt_collection.BATCH_ID = dbt_batch.BATCH_ID)
    INNER JOIN dbt_fw ON dbt_fw.COLLECT_ID = dbt_collection.COLLECT_ID
    WHERE
    dbm_lot.LOT_ID = %s
	AND dbt_summary.SUMMARY_DATE = '%s'
    AND dbt_collection.COL_TDATE BETWEEN '%s' AND '%s'
    ORDER BY COL_TDATE ASC
    """
    # print(isinstance(start_tdate,str))
    if isinstance(start_tdate, str):
        tdatetime = datetime.datetime.strptime(start_tdate, "%Y-%m-%d %H:%M:%S")
        start_date = tdatetime.strftime("%Y-%m-%d")
    else:
        start_date = start_tdate.strftime("%Y-%m-%d")
    ##        start_date = start_tdate.strftime("%Y-%m")
    # endif
    sql = sql % (lot_id, start_date, start_tdate, end_tdate)
    # print(sql)
    return fetAllRawQuery(sql)


def getDailyGraphThermo(lot_id, start_tdate, end_tdate):
    debugPrintEx("***")

    sql = """
    SELECT
    DISTINCT(dbt_collection.COL_TDATE) AS COL_TDATE,
    dbt_collection.COLLECT_ID AS COLLECT_ID,
    dbm_lot.LOT_ID AS LOT_ID,
    dbm_thermo.MTHERMO_NO AS MTHERMO_NO,
    dbt_thermo.CUR_VAL AS CURVAL
    FROM
    (((dbm_lot
    INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
    INNER JOIN dbt_collection ON dbt_collection.BATCH_ID = dbt_batch.BATCH_ID)
    INNER JOIN dbt_thermo ON dbt_thermo.COLLECT_ID = dbt_collection.COLLECT_ID)
    INNER JOIN dbm_thermo ON dbm_thermo.MTHERMO_ID = dbt_thermo.MTHERMO_ID
    WHERE
    dbm_lot.LOT_ID = %s
    AND dbt_collection.COL_TDATE BETWEEN '%s' AND '%s'
    ORDER BY COL_TDATE ASC
    """
    sql = sql % (lot_id, start_tdate, end_tdate)
    # print(sql)
    return fetAllRawQuery(sql)


def getWeeklyHistGraph1(batch_id, start_week, end_week):
    debugPrintEx("***")

    sql = """
    SELECT
        dbt_summary.WEEK_PERIODS AS WEEK_PERIODS,
        dbm_lot.LOT_ID AS LOT_ID,
        MAX(dbt_batch.INITIAL_COUNT) AS INITIAL_COUNT,
        ROUND(AVG(dbt_summary.EGG_COUNT), 0) AS EGG_COUNT,
        ROUND(AVG(dbt_summary.CURR_COUNT), 0) AS CURR_COUNT,
        ROUND(AVG(dbt_summary.CURR_COUNT) * 100 / MAX(dbt_batch.INITIAL_COUNT), 1) AS SURVIVAL_RATE,
        ROUND(AVG(dbt_summary.EGG_COUNT) *100 / AVG(dbt_summary.CURR_COUNT), 1) AS EGG_RATE,
        ROUND(AVG(dbt_summary.EGG_WEIGHT / 10), 0) AS EGG_WEIGHT,
        ROUND(AVG(dbt_summary.HEN_WEIGHT), 0) AS HEN_WEIGHT,
        ROUND(AVG(dbt_summary.EGG_WEIGHT / 10) * AVG(dbt_summary.EGG_COUNT) / AVG(dbt_summary.CURR_COUNT),1) AS EGG_VOL,
        ROUND(AVG(dbm_manual.SURVIVAL_RATE / 10), 1) AS SURVIVAL_RATE_MAN,
        ROUND(AVG(dbm_manual.EGG_RATE / 10), 1) AS EGG_RATE_MAN,
        ROUND(AVG(dbm_manual.EGG_WEIGHT / 10), 1) AS EGG_WEIGHT_MAN,
        ROUND(AVG(dbm_manual.EGG_VOL / 10), 1) AS EGG_VOL_MAN,
        AVG(dbm_manual.HEN_WEIGHT) AS HEN_WEIGHT_MAN
    FROM
    (((dbm_lot
        INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
        INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID)
        INNER JOIN dbm_manual ON dbt_batch.HEN_ID = dbm_manual.HEN_ID AND dbt_summary.WEEK_PERIODS = dbm_manual.WEEK_PERIODS)
        INNER JOIN dbm_hen ON dbt_batch.HEN_ID = dbm_hen.HEN_ID
    WHERE dbt_batch.BATCH_ID = %s AND dbt_summary.WEEK_PERIODS BETWEEN %s AND %s
    GROUP BY 
        dbt_batch.LOT_ID, 
        dbm_lot.LOT_ID,
        dbt_summary.WEEK_PERIODS
    ORDER BY dbt_summary.WEEK_PERIODS ASC
    """
    sql = sql % (batch_id, start_week, end_week)
    # print(sql)
    return fetAllRawQuery(sql)


def getWeeklyHistGraph2(batch_id, start_week, end_week):
    debugPrintEx("***")

    sql = """
    SELECT
        dbt_summary.WEEK_PERIODS AS WEEK_PERIODS,
        dbm_lot.LOT_ID AS LOT_ID,
        ROUND(AVG(dbt_summary.CURR_COUNT), 0) AS CURR_COUNT,
        CAST(ROUND(AVG(dbt_summary.FOOD_AMOUNT), 0) / 1000 AS DECIMAL(5, 1)) AS FOOD_AMOUNT,
        CASE WHEN (AVG(dbt_summary.CURR_COUNT) > 0) THEN ROUND(AVG(dbt_summary.FOOD_AMOUNT) * 1000 / AVG(dbt_summary.CURR_COUNT), 1) ELSE 0 END AS FOOD_AMOUNT_U,
        ROUND(AVG(dbt_summary.WATER_AMOUNT), 0) AS WATER_AMOUNT,
        ROUND(AVG(dbt_summary.WATER_AMOUNT) * 1000 / AVG(dbt_summary.CURR_COUNT), 1) AS WATER_AMOUNT_U,
        CASE WHEN (AVG(dbt_summary.EGG_COUNT) > 0 AND AVG(dbt_summary.EGG_WEIGHT) > 0) THEN ROUND(AVG(dbt_summary.FOOD_AMOUNT) * 1000 / (AVG(dbt_summary.EGG_COUNT) * AVG(dbt_summary.EGG_WEIGHT/10)), 1) ELSE 0 END AS FOOD_DEMAND,
        ROUND(AVG(dbt_summary.ROOM_TEMP) / 10, 1) AS ROOM_TEMP,
        ROUND(AVG(dbt_summary.OUTER_TEMP) / 10, 1) AS OUTER_TEMP
    FROM
    (dbm_lot
        INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
        INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID
    WHERE dbt_batch.BATCH_ID = %s
    AND dbt_summary.WEEK_PERIODS BETWEEN %s AND %s
    GROUP BY 
        dbt_batch.LOT_ID, 
        dbt_summary.WEEK_PERIODS,
        dbm_lot.LOT_ID
    ORDER BY dbt_summary.WEEK_PERIODS ASC
    """
    sql = sql % (batch_id, start_week, end_week)
    return fetAllRawQuery(sql)


def getWeeklyGraph1(lot_id, start_week, end_week):
    debugPrintEx("***")

    sql = """
    SELECT
    dbt_summary.WEEK_PERIODS AS WEEK_PERIODS,
    dbm_lot.LOT_ID AS LOT_ID,
    dbt_batch.INITIAL_COUNT AS INITIAL_COUNT,
    ROUND(AVG(dbt_summary.EGG_COUNT),0) AS EGG_COUNT,
    ROUND(AVG(dbt_summary.CURR_COUNT),0) AS CURR_COUNT,
    ROUND(CURR_COUNT/INITIAL_COUNT*100,1) AS SURVIVAL_RATE,
    ROUND(EGG_COUNT/CURR_COUNT*100,1) AS EGG_RATE,
    ROUND(AVG(dbt_summary.EGG_WEIGHT/10),0) AS EGG_WEIGHT,
    ROUND(AVG(dbt_summary.HEN_WEIGHT),0) AS HEN_WEIGHT,
    ROUND(AVG(dbt_summary.EGG_WEIGHT*0.1)*EGG_COUNT/CURR_COUNT,1) AS EGG_VOL,
    ROUND(dbm_manual.SURVIVAL_RATE/10,1) AS SURVIVAL_RATE_MAN,
    ROUND(dbm_manual.EGG_RATE/10,1) AS EGG_RATE_MAN,
    ROUND(dbm_manual.EGG_WEIGHT/10,1) AS EGG_WEIGHT_MAN,
    ROUND(dbm_manual.EGG_VOL/10,1) AS EGG_VOL_MAN,
    ROUND(dbm_manual.EGG_VOL/10,1) AS EGG_VOL_MAN,
    dbm_manual.HEN_WEIGHT AS HEN_WEIGHT_MAN
    FROM
    (((dbm_lot
    INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
    INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID)
    INNER JOIN dbm_manual ON dbt_batch.HEN_ID = dbm_manual.HEN_ID AND dbt_summary.WEEK_PERIODS = dbm_manual.WEEK_PERIODS)
    INNER JOIN dbm_hen ON dbt_batch.HEN_ID = dbm_hen.HEN_ID
    WHERE
    dbm_lot.LOT_ID = %s
    AND dbt_summary.WEEK_PERIODS BETWEEN %s AND %s
    GROUP BY 
        dbt_summary.WEEK_PERIODS,
        dbm_lot.LOT_ID,
        dbt_batch.INITIAL_COUNT,
        dbt_summary.CURR_COUNT,
        dbt_summary.egg_count,
        dbm_manual.survival_rate,
        dbm_manual.egg_rate,
        dbm_manual.egg_weight,
        dbm_manual.egg_vol,
        dbm_manual.hen_weight
    """
    sql = sql % (lot_id, start_week, end_week)
    # print(sql)
    return fetAllRawQuery(sql)


def getWeeklyGraph2(lot_id, start_week, end_week):
    debugPrintEx("***")

    sql = """
    SELECT
    dbt_summary.WEEK_PERIODS AS WEEK_PERIODS,
    dbm_lot.LOT_ID AS LOT_ID,
    ROUND(AVG(dbt_summary.CURR_COUNT),0) AS CURR_COUNT,
    ROUND(AVG(dbt_summary.EGG_COUNT),0) AS EGG_COUNT,
    CAST(ROUND(AVG(dbt_summary.FOOD_AMOUNT),0)/1000 AS DECIMAL(5, 1)) AS FOOD_AMOUNT,
    CASE WHEN (CURR_COUNT > 0) THEN ROUND(FOOD_AMOUNT/CURR_COUNT*1000,1) ELSE 0 END AS FOOD_AMOUNT_U,
    ROUND(AVG(dbt_summary.WATER_AMOUNT),0) AS WATER_AMOUNT,
    CASE WHEN (CURR_COUNT > 0) THEN ROUND(WATER_AMOUNT/CURR_COUNT*1000,1) ELSE 0 END AS WATER_AMOUNT_U,
    CASE WHEN (EGG_COUNT > 0 AND dbt_summary.EGG_WEIGHT > 0) THEN ROUND(FOOD_AMOUNT/(EGG_COUNT*AVG(dbt_summary.EGG_WEIGHT/10))*1000,1) ELSE 0 END AS FOOD_DEMAND,
    ROUND(AVG(dbt_summary.ROOM_TEMP)/10,1) AS ROOM_TEMP,
    ROUND(AVG(dbt_summary.OUTER_TEMP)/10,1) AS OUTER_TEMP
    FROM
    (dbm_lot
    INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
    INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID
    WHERE
    dbm_lot.LOT_ID = %s
    AND dbt_summary.WEEK_PERIODS BETWEEN %s AND %s
    GROUP BY 
        dbt_summary.WEEK_PERIODS,
        dbm_lot.LOT_ID,
        dbt_summary.food_amount,
        dbt_summary.curr_count,
        dbt_summary.water_amount,
        dbt_summary.egg_count,
        dbt_summary.egg_weight
    """
    sql = sql % (lot_id, start_week, end_week)
    # print(sql)
    return fetAllRawQuery(sql)


def getComparisonGraphPast(lot_id, start_week, end_week):
    debugPrintEx("***")
    result = []

    # Get growth_kind, feeding_date from lot_id
    row = getBatchInfoByLotID(lot_id)
    # print(row)
    if row:
        growth_kind = row[0]
        feeding_date = row[1]
        # print("growth_kind=%s" % (growth_kind))
        # print("feeding_date=%s" % (feeding_date))

        rows = getBatchInfoByGrowthKind(growth_kind)
        # print(rows)

        # Get BATCH_ID of most nearest feeding date
        month_c = feeding_date.month
        day_c = feeding_date.day
        mindays = 999
        batch_id = 0
        for row in rows:
            fdate = row["FEEDING_DATE"]
            year = fdate.year
            month = fdate.month
            day = fdate.day
            day_curr = datetime.date(year, month_c, day_c)
            day_past = datetime.date(year, month, day)
            periods = abs((day_curr - day_past).days)
            if periods < mindays:
                batch_id = row["BATCH_ID"]
                mindays = periods
            # endif
        # endfor

        result = []
        if batch_id > 0:
            # print("---found----")

            #            lot_id       = row['LOT_ID']
            #            feeding_date = row['FEEDING_DATE']
            #            pend_date = feeding_date + datetime.timedelta(700)

            sql = """
            SELECT
            dbt_summary.SUMMARY_DATE AS WEEKLY_DATE,
            dbt_summary.WEEK_PERIODS AS WEEK_PERIODS,
            dbt_batch.INITIAL_COUNT AS INITIAL_COUNT,
            ROUND(AVG(dbt_summary.EGG_COUNT),0) AS EGG_COUNT,
            ROUND(AVG(dbt_summary.CURR_COUNT),0) AS CURR_COUNT,
            ROUND(CURR_COUNT/INITIAL_COUNT*100,1) AS SURVIVAL_RATE,
            ROUND(EGG_COUNT/CURR_COUNT*100,1) AS EGG_RATE_CUR,
            ROUND(AVG(dbt_summary.EGG_WEIGHT*0.1),0) AS EGG_WEIGHT,
            ROUND(AVG(dbt_summary.HEN_WEIGHT),0) AS HEN_WEIGHT,
            ROUND(AVG(dbt_summary.EGG_WEIGHT/10)*EGG_COUNT/CURR_COUNT,1) AS EGG_VOL,
            ROUND(dbm_manual.SURVIVAL_RATE/10,1) AS SURVIVAL_RATE_MAN,
            ROUND(dbm_manual.EGG_RATE/10,1) AS EGG_RATE_MAN,
            ROUND(dbm_manual.EGG_WEIGHT/10,1) AS EGG_WEIGHT_MAN,
            ROUND(dbm_manual.EGG_VOL/10,1) AS EGG_VOL_MAN,
            dbm_manual.HEN_WEIGHT AS HEN_WEIGHT_MAN
            FROM
            ((((dbm_lot
            INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
            INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID)
            INNER JOIN dbm_house ON dbm_lot.HOUSE_ID = dbm_house.HOUSE_ID)
            INNER JOIN dbm_manual ON dbt_batch.HEN_ID = dbm_manual.HEN_ID AND dbt_summary.WEEK_PERIODS = dbm_manual.WEEK_PERIODS)
            INNER JOIN dbm_hen ON dbt_batch.HEN_ID = dbm_hen.HEN_ID
            WHERE dbt_summary.BATCH_ID = %s AND dbt_summary.WEEK_PERIODS BETWEEN %s AND %s
            GROUP BY 
                dbt_summary.BATCH_ID, 
                dbt_summary.WEEK_PERIODS,
                dbt_summary.summary_date,
                dbt_batch.initial_count,
                dbt_summary.curr_count,
                dbt_summary.egg_count,
                dbm_manual.survival_rate,
                dbm_manual.egg_rate,
                dbm_manual.egg_weight,
                dbm_manual.egg_vol,
                dbm_manual.hen_weight
            ORDER BY dbt_summary.WEEK_PERIODS asc
           """
            sql = sql % (batch_id, start_week, end_week)
            # print(sql)
            result = fetAllRawQuery(sql)
        # endif
    # endif

    return result


def getFooderNoList(lot_id):
    debugPrintEx("***")

    sql = """
    SELECT
    dbm_fooder.LOT_ID AS LOT_ID,
    dbm_fooder.MFOODER_ID AS MFOODER_ID,
    dbm_fooder.FOODER_NO AS FOODER_NO
    FROM dbm_fooder
    WHERE dbm_fooder.LOT_ID = %s
	ORDER BY FOODER_NO ASC
    """
    sql = sql % (lot_id)
    return fetAllRawQuery(sql)


def getRegFooderInfo(lot_id, fooder_id, start_time):
    debugPrintEx("***")

    if start_time != "":
        sql = """
        SELECT
        dbm_fooder.LOT_ID AS LOT_ID,
        dbm_fooder.MFOODER_ID AS MFOODER_ID,
        dbm_fooder.FOODER_NO AS FOODER_NO,
        dbm_fooderst.MFOODERST_ID AS MFOODERST_ID,
        dbm_fooderst.START_TIME AS START_TIME,
	    dbm_fooderst.APPLY_DATE AS APPLY_DATE
        FROM
        dbm_fooder
        JOIN dbm_fooderst ON dbm_fooderst.MFOODER_ID = dbm_fooder.MFOODER_ID
        WHERE
        dbm_fooder.LOT_ID = %s
        AND dbm_fooder.MFOODER_ID = %s
        AND dbm_fooderst.START_TIME = '%s'
        """
        sql = sql % (lot_id, fooder_id, start_time)
    else:
        sql = """
        SELECT
        dbm_fooder.LOT_ID AS LOT_ID,
        dbm_fooder.MFOODER_ID AS MFOODER_ID,
        dbm_fooder.FOODER_NO AS FOODER_NO,
        dbm_fooderst.MFOODERST_ID AS MFOODERST_ID,
        dbm_fooderst.START_TIME AS START_TIME,
	    dbm_fooderst.APPLY_DATE AS APPLY_DATE
        FROM
	    dbm_fooder
	    JOIN dbm_fooderst ON dbm_fooderst.MFOODER_ID = dbm_fooder.MFOODER_ID
        WHERE
	    dbm_fooder.LOT_ID = %s
        AND dbm_fooder.MFOODER_ID = %s
        AND dbm_fooderst.APPLY_DATE > CURRENT_DATE LIMIT 1
        """
        sql = sql % (lot_id, fooder_id)
    # endif
    print(sql)
    return fetAllRawQuery(sql)


def insertFooderst(lot_id, mfooder_id, start_time, apply_date, mfooderst_id):
    debugPrintEx("***")

    created_tdate = datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")

    if mfooderst_id != -1:
        print("update")
        sql = """
        UPDATE dbm_fooderst
        SET START_TIME = '%s', APPLY_DATE = '%s', CREATED_TDATE = '%s'
        WHERE MFOODERST_ID = %s
        """
        sql = sql % (start_time, apply_date, created_tdate, mfooderst_id)
    else:
        print("insert")
        sql = """
        INSERT INTO dbm_fooderst (
        mfooder_id, start_time, apply_date, created_tdate)
        VALUES (%s, '%s', '%s', '%s')
        """
        sql = sql % (mfooder_id, start_time, apply_date, created_tdate)
    # endif
    # print(sql)
    executeRawQuery(sql)

    return 1


def getFooderInfo(lot_id):
    debugPrintEx("***")

    sql = """
    SELECT
    dbm_fooder.MFOODER_ID AS MFOODER_ID,
    dbm_fooder.FOODER_NO AS FOODER_NO,
    dbm_fooderst.START_TIME AS START_TIME,
    dbm_fooderst.APPLY_DATE AS APPLY_DATE
    FROM dbm_fooder
	INNER JOIN dbm_fooderst ON dbm_fooderst.MFOODER_ID = dbm_fooder.MFOODER_ID
    WHERE dbm_fooder.LOT_ID = %s
	ORDER BY FOODER_NO ASC, APPLY_DATE DESC, START_TIME ASC
    """

    sql = sql % (lot_id)
    # print(sql)
    return fetAllRawQuery(sql)


def getHenGroupList():
    debugPrintEx("***")

    sql = """
    SELECT * FROM dbt_hengrp ORDER BY CREATED_TDATE ASC
    """

    return fetAllRawQuery(sql)


def getBatchInfo(batch_id):
    debugPrintEx("***")

    sql = """
    SELECT 
        dbt_batch.*,
        dbm_lot.HOUSE_ID AS HOUSE_ID
    FROM dbt_batch
    LEFT JOIN dbm_lot ON dbm_lot.LOT_ID = dbt_batch.LOT_ID
    WHERE dbt_batch.BATCH_ID = '%s'
    """

    return fetAllRawQuery(sql % (batch_id))


def updateBatchTable(upd_data):
    debugPrintEx("***")

    created_tdate = datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")
    if upd_data[5] == "" or upd_data[8] == "":
        return 0
    if upd_data[6] != "":
        sql = """
        UPDATE dbt_batch
        SET hengrp_id=%s, lot_id=%s, hen_id=%s, growth_kind=%s, start_date='%s', end_date='%s', initial_count=%s, feeding_date='%s',
        eggcnt_stime=%s, eggcnt_etime=%s, created_tdate='%s'
        WHERE batch_id=%s
        """
        sql = sql % (
            upd_data[1],
            upd_data[2],
            upd_data[3],
            upd_data[4],
            upd_data[5],
            upd_data[6],
            upd_data[7],
            upd_data[8],
            upd_data[9],
            upd_data[10],
            created_tdate,
            upd_data[0],
        )
    else:
        sql = """
        UPDATE dbt_batch
        SET hengrp_id=%s, lot_id=%s, hen_id=%s, growth_kind=%s, start_date='%s', end_date='9999-12-31', initial_count=%s, feeding_date='%s',
        eggcnt_stime=%s, eggcnt_etime=%s, created_tdate='%s'
        WHERE batch_id=%s
        """
        sql = sql % (
            upd_data[1],
            upd_data[2],
            upd_data[3],
            upd_data[4],
            upd_data[5],
            upd_data[7],
            upd_data[8],
            upd_data[9],
            upd_data[10],
            created_tdate,
            upd_data[0],
        )
    # endif
    # print(sql)
    executeRawQuery(sql)

    return 1


def getHenList():
    debugPrintEx("***")

    sql = """
    SELECT * FROM dbm_hen
    """

    return fetAllRawQuery(sql)


def getLastCollectionTime():
    row = fetOneRawQueryNoColumns(
        "SELECT col_tdate FROM dbt_collection ORDER BY COLLECT_ID DESC LIMIT 1"
    )
    if row == None:
        return None
    # endif
    lastCollectionTime = row[0]

    return time.mktime(lastCollectionTime.timetuple())


def getMainTable(date=None):
    debugPrintEx("***")
    if date == None:
        sql = """
        SELECT * FROM dbv_main
        """
        return fetAllRawQuery(sql)

    return getRawMainTable(date)


def getMainTotals(date=None):
    debugPrintEx("***")
    if date == None:
        date = datetime.datetime.today().strftime("%Y-%m-%d")
    sql = """
    SELECT
    dbt_batch.GROWTH_KIND AS GROWTH_KIND,
    dbt_summary.CURR_COUNT AS CURR_COUNT
    FROM
    dbt_batch
    INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID
    WHERE
    dbt_summary.SUMMARY_DATE = CURRENT_DATE OR dbt_summary.SUMMARY_DATE = '%s'
    """

    result = fetAllRawQuery(sql % (date))

    tot_chiken = 0
    tot_adult_chiken = 0
    for row in result:
        tot_chiken = tot_chiken + row["CURR_COUNT"]
        if row["GROWTH_KIND"] == 3:
            tot_adult_chiken = tot_adult_chiken + row["CURR_COUNT"]
        # endif
    # endfor

    return tot_chiken, tot_adult_chiken


def getMainTotalsByHouses(date=None):
    debugPrintEx("***")
    if date == None:
        date = datetime.datetime.today().strftime("%Y-%m-%d")
    sql = """
    SELECT
    SUM(dbt_summary.CURR_COUNT) AS CURR_COUNT,
    dbm_house.HOUSE_ID AS HOUSE_ID,
    dbm_house.HOUSE_NAME AS HOUSE_NAME
    FROM
    dbt_batch
    INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID
    INNER JOIN dbm_lot ON dbm_lot.LOT_ID = dbt_batch.LOT_ID
    INNER JOIN dbm_house ON dbm_house.HOUSE_ID = dbm_lot.HOUSE_ID
    WHERE
    dbt_summary.SUMMARY_DATE = CURRENT_DATE OR dbt_summary.SUMMARY_DATE = '%s'
    GROUP BY dbm_house.HOUSE_ID;
    """

    result = fetAllRawQuery(sql % (date))
    return result


def getRawMainTable(date):
    debugPrintEx("***")
    sql = """
    SELECT dbt_batch.batch_id,
    dbt_batch.lot_id,
    dbm_lot.lot_name,
    dbm_house.house_id,
    dbm_house.house_name,
    dbt_summary.summary_date,
    dbt_summary.week_periods,
    dbt_summary.day_periods,
    dbt_batch.feeding_date,
    dbt_batch.start_date,
    dbt_batch.initial_count,
    dbm_lot.cage_num,
    dbt_summary.curr_count,
    dbt_summary.dead_count,
    ((dbt_summary.egg_weight / 10))::numeric(5,1) AS egg_weight,
    dbt_summary.hen_weight,
    ((dbt_summary.food_amount / 1000))::numeric(5,1) AS food_amount,
    dbt_summary.water_amount,
    ((dbt_summary.preset_temp / 10))::numeric(5,1) AS preset_temp,
    ((dbt_summary.room_temp / 10))::numeric(5,1) AS room_temp,
    ((dbt_summary.room_temp_hi / 10))::numeric(5,1) AS room_temp_hi,
    ((dbt_summary.room_temp_lo / 10))::numeric(5,1) AS room_temp_lo,
    ((dbt_summary.outer_temp / 10))::numeric(5,1) AS outer_temp,
    ((dbt_summary.outer_temp_hi / 10))::numeric(5,1) AS outer_temp_hi,
    ((dbt_summary.outer_temp_lo / 10))::numeric(5,1) AS outer_temp_lo,
    dbt_summary.daylight_hours,
    dbt_summary.defection,
    round(((dbt_summary.min_ventilation / 100))::numeric, 2) AS min_ventilation,
    round(((dbt_summary.bmin_ventilation / 100))::numeric, 2) AS bmin_ventilation,
    round(((dbt_summary.humidity / 10))::numeric, 1) AS humidity,
    round(((dbt_summary.humidity_upl / 10))::numeric, 1) AS humidity_upl,
    round(((dbt_summary.humidity_lol / 10))::numeric, 1) AS humidity_lol,
    (EXTRACT(day FROM dbt_batch.start_date) - EXTRACT(day FROM dbt_batch.feeding_date)) AS sttday_periods,
        CASE
            WHEN (dbm_manual.hen_weight > 0) THEN round((((dbt_summary.hen_weight / dbm_manual.hen_weight) * 100))::numeric, 1)
            ELSE (0)::numeric
        END AS weightchg_rate
    FROM ((((dbm_lot
     JOIN dbm_house ON ((dbm_house.house_id = dbm_lot.house_id)))
     JOIN dbt_batch ON ((dbt_batch.lot_id = dbm_lot.lot_id)))
     JOIN dbt_summary ON ((dbt_summary.batch_id = dbt_batch.batch_id)))
     JOIN dbm_manual ON (((dbm_manual.hen_id = dbt_batch.hen_id) AND (dbm_manual.week_periods = dbt_summary.week_periods))))
    WHERE ((dbt_summary.summary_date = '%s'::date))
    ORDER BY dbm_lot.lot_id
    """

    return fetAllRawQuery(sql % (date))


def getFoodWaterID(lot_id):
    debugPrintEx("***")

    sql = """
    SELECT MFW_ID FROM dbm_fw WHERE LOT_ID = %s
    """
    sql = sql % (lot_id)
    row = fetOneRawQueryNoColumns(sql)

    return row[0]


def getPrevFoodWaterAmount(mfw_id):
    debugPrintEx("***")

    return (-1, -1)


def updateDailySummary(data):
    debugPrintEx("******")
    # print(data)

    # 2014-01-06 Bug for curr_count & dead_count
    row_p1 = getDailyTablePeriods(data["lot_id"], data["upd_date"], data["upd_date"])
    # print(row_p1)
    row_p = row_p1[0]

    # get record for given date
    upd_date = data["upd_date"]
    lot_id = data["lot_id"]
    add_sm = """
    SELECT summary_id
    FROM dbt_summary,dbt_batch
    WHERE summary_date = '%s' AND dbt_batch.lot_id = %s AND dbt_summary.batch_id = dbt_batch.batch_id
    """
    #    add_sm = """SELECT summary_id
    #    FROM dbt_summary,dbt_batch WHERE summary_date = '%s' AND dbt_batch.lot_id = %s AND dbt_summary.batch_id = dbt_batch.batch_id ORDER BY summary_date DESC"""
    sql = add_sm % (upd_date, lot_id)
    # print(sql)

    row = fetOneRawQueryNoColumns(sql)
    result = "Database update failed."
    # print(cursor.rowcount)

    if row:
        try:
            # print("-found-")
            # 1. found summary record for this date
            summary_id = row[0]
            # print(summary_id)
            # print(data)

            # dead_count        = Decimal(data['DEAD_COUNT'])
            # 2014-01-06
            dead_count = Decimal(data["DEAD_COUNT"])
            # print(row_p['CURR_COUNT'])
            # print(row_p['DEAD_COUNT'])
            curr_count = row_p["CURR_COUNT"] - (dead_count - row_p["DEAD_COUNT"])
            # 2014-01-08
            egg_count = Decimal(data["EGG_COUNT"])

            min_ventilation = math.floor(Decimal(data["MIN_VENTILATION"]) * 100)
            lighting_time = data["LIGHTING_TIME"]
            hen_weight = math.floor(Decimal(data["HEN_WEIGHT"]))
            bmin_ventilation = math.floor(Decimal(data["BMIN_VENTILATION"]) * 100)
            food = data["FOOD"]
            # 2013-09-26
            egg_weight = math.floor(Decimal(data["EGG_WEIGHT"]) * 10)

            comments = data["COMMENTS"]
            coeff = math.floor(Decimal(data["COEFF"]) * 100)
            remv_excr = data["REMV_EXCR"]
            defection = math.floor(Decimal(data["DEFECTION"]) * 100)
            ventilation_setup = math.floor(Decimal(data["VENTILATION_SETUP"]) * 100)
            daylight_hours = math.floor(Decimal(data["DAYLIGHT_HOURS"]) * 100)
            ventilation = math.floor(Decimal(data["VENTILATION"]) * 100)
            preset_temp = math.floor(Decimal(data["PRESET_TEMP"]) * 10)
            food_interval = data["FOOD_INTERVAL"]
            humidity = math.floor(Decimal(data["HUMIDITY"]) * 10)
            heating = data["HEATING"]
            humidity_upl = math.floor(Decimal(data["HUMIDITY_UPL"]) * 10)
            water_pos = data["WATER_POS"]
            humidity_lol = math.floor(Decimal(data["HUMIDITY_LOL"]) * 10)
            aircon_system = data["AIRCON_SYSTEM"]

            # 2013-09-22 Add
            food_amount = math.floor(Decimal(data["FOOD_AMOUNT"]))
            water_amount = math.floor(Decimal(data["WATER_AMOUNT"]))
            room_temp = math.floor(Decimal(data["ROOM_TEMP"]) * 10)
            outer_temp = math.floor(Decimal(data["OUTER_TEMP"]) * 10)

            add_sm = """
            UPDATE dbt_summary SET
            dead_count = %s,      min_ventilation = %s,   lighting_time = '%s',  hen_weight = %s,   bmin_ventilation = %s,
            food = '%s',          egg_weight = %s,        comments = '%s',       coeff = %s,        remv_excr = '%s',
            defection = %s,       ventilation_setup = %s, daylight_hours = %s,   ventilation = %s,  preset_temp = %s,
            food_interval = '%s', humidity = %s,          heating = '%s',        humidity_upl = %s, water_pos = '%s',
            humidity_lol = %s,    aircon_system = '%s',
            food_amount = %s,     water_amount = %s,      room_temp = %s,        outer_temp = %s,
            curr_count = %s,      egg_count = %s
            WHERE summary_id = %s
            """
            data_sm = (
                dead_count,
                min_ventilation,
                lighting_time,
                hen_weight,
                bmin_ventilation,
                food,
                egg_weight,
                comments,
                coeff,
                remv_excr,
                defection,
                ventilation_setup,
                daylight_hours,
                ventilation,
                preset_temp,
                food_interval,
                humidity,
                heating,
                humidity_upl,
                water_pos,
                humidity_lol,
                aircon_system,
                food_amount,
                water_amount,
                room_temp,
                outer_temp,
                curr_count,
                egg_count,
                summary_id,
            )
            sql = add_sm % (data_sm)
            # print(sql)

            executeRawQuery(sql)

            result = "Database updated successfully"
        except Exception as e:
            result = e.args

    return result


def getDailySummary(upd_date, lot_id):
    debugPrintEx("******")

    # get record for given date
    add_sm = """SELECT
    DEAD_COUNT,    MIN_VENTILATION,   LIGHTING_TIME, HEN_WEIGHT,  BMIN_VENTILATION,
    FOOD,          EGG_WEIGHT,        COMMENTS,      COEFF,       REMV_EXCR,
    DEFECTION,     VENTILATION_SETUP, DAYLIGHT_HOURS,VENTILATION, PRESET_TEMP,
    FOOD_INTERVAL, HUMIDITY,          HEATING,       HUMIDITY_UPL,WATER_POS,
    HUMIDITY_LOL,  AIRCON_SYSTEM,
    FOOD_AMOUNT,   WATER_AMOUNT,      ROOM_TEMP,     OUTER_TEMP,
    EGG_COUNT
    FROM dbt_summary,dbt_batch WHERE summary_date = '%s' AND dbt_batch.lot_id = %s AND dbt_summary.batch_id = dbt_batch.batch_id ORDER BY summary_date DESC
    """
    sql = add_sm % (upd_date, lot_id)
    # print(sql)

    result = fetOneRawQuery(sql)
    if result:
        # print(result)
        # 2013-10-24 Convert
        if result["PRESET_TEMP"]:
            result["PRESET_TEMP"] = result["PRESET_TEMP"] / 10
        else:
            result["PRESET_TEMP"] = 0
        # endif
        if result["HUMIDITY"]:
            result["HUMIDITY"] = result["HUMIDITY"] / 10
        else:
            result["HUMIDITY"] = 0
        # endif
        if result["HUMIDITY_UPL"]:
            result["HUMIDITY_UPL"] = result["HUMIDITY_UPL"] / 10
        else:
            result["HUMIDITY_UPL"] = 0
        # endif
        if result["HUMIDITY_LOL"]:
            result["HUMIDITY_LOL"] = result["HUMIDITY_LOL"] / 10
        else:
            result["HUMIDITY_LOL"] = 0
        # endif
        if result["MIN_VENTILATION"]:
            result["MIN_VENTILATION"] = result["MIN_VENTILATION"] / 100
        else:
            result["MIN_VENTILATION"] = 0
        # endif
        if result["BMIN_VENTILATION"]:
            result["BMIN_VENTILATION"] = result["BMIN_VENTILATION"] / 100
        else:
            result["BMIN_VENTILATION"] = 0
        # endif
        if result["VENTILATION"]:
            result["VENTILATION"] = result["VENTILATION"] / 100
        else:
            result["VENTILATION"] = 0
        # endif
        if result["VENTILATION_SETUP"]:
            result["VENTILATION_SETUP"] = result["VENTILATION_SETUP"] / 100
        else:
            result["VENTILATION_SETUP"] = 0
        # endif

        # 2013-09-22 Add
        # print(result['ROOM_TEMP'])
        # print(result['ROOM_TEMP']/10)
        # print(result['OUTER_TEMP'])
        # print(result['OUTER_TEMP']/10)

        if result["ROOM_TEMP"]:
            result["ROOM_TEMP"] = result["ROOM_TEMP"] / 10
        else:
            result["ROOM_TEMP"] = 0
        # endif
        if result["OUTER_TEMP"]:
            result["OUTER_TEMP"] = result["OUTER_TEMP"] / 10
        else:
            result["OUTER_TEMP"] = 0
        # endif
        if result["EGG_WEIGHT"]:
            result["EGG_WEIGHT"] = result["EGG_WEIGHT"] / 10
        else:
            result["EGG_WEIGHT"] = 0
        # endif
        if not result["HEN_WEIGHT"]:
            result["HEN_WEIGHT"] = 0
        # endif
        if result["DEFECTION"]:
            result["DEFECTION"] = result["DEFECTION"] / 100
        else:
            result["DEFECTION"] = 0
        # endif
        if result["COEFF"]:
            result["COEFF"] = result["COEFF"] / 100
        else:
            result["COEFF"] = 0
        # endif
        if result["DAYLIGHT_HOURS"]:
            result["DAYLIGHT_HOURS"] = result["DAYLIGHT_HOURS"] / 100
        else:
            result["DAYLIGHT_HOURS"] = 0
        # endif
    # endif

    return result


def getEggInformation(lot_id, summary_date, cage_row, cage_tier):
    debugPrintEx("***")

    sql = """
    SELECT
    dbm_lot.LOT_ID AS LOT_ID,
    dbt_summary.COLLECT_ID AS COLLECT_ID,
    dbt_egg.TEGG_ID AS TEGG_ID,
    dbm_egg.CAGE_ROW AS CAGE_ROW,
    dbm_egg.CAGE_TIER AS CAGE_TIER
    FROM
    (((dbm_lot
    INNER JOIN dbt_batch ON dbt_batch.LOT_ID = dbm_lot.LOT_ID)
    INNER JOIN dbt_summary ON dbt_summary.BATCH_ID = dbt_batch.BATCH_ID)
    INNER JOIN dbt_egg ON dbt_egg.COLLECT_ID = dbt_summary.COLLECT_ID)
    INNER JOIN dbm_egg ON dbm_egg.MEGG_ID = dbt_egg.MEGG_ID
    WHERE dbm_lot.LOT_ID = %s AND dbt_summary.SUMMARY_DATE = '%s'
    AND dbm_egg.CAGE_ROW = %s AND dbm_egg.CAGE_TIER = %s
    """
    sql = sql % (lot_id, summary_date, cage_row, cage_tier)
    # print(sql)
    result = fetAllRawQuery(sql)

    return result[0]


def updateEggInformation(tegg_id, egg_count):
    debugPrintEx("***")

    sql = """
    UPDATE dbt_egg
    SET EGG_COUNT = %s
    WHERE TEGG_ID = %s
    """
    sql = sql % (egg_count, tegg_id)
    # print(sql)
    executeRawQuery(sql)

    return 1


def updateSummaryEggData(collect_id, egg_count):
    debugPrintEx("***")

    sql = """
    UPDATE dbt_summary
    SET EGG_COUNT = %s
    WHERE COLLECT_ID = %s
    """
    sql = sql % (egg_count, collect_id)
    # print(sql)
    executeRawQuery(sql)

    return 1


def getLotListNotUsed():
    debugPrintEx("***")
    result = []

    sql = """
    SELECT
    DISTINCT(dbm_lot.LOT_ID) AS LOT_ID
    FROM dbm_lot, dbt_batch
    WHERE dbm_lot.LOT_ID = dbt_batch.LOT_ID AND dbt_batch.END_DATE > CURRENT_DATE
    ORDER BY LOT_ID ASC
    """

    result1 = fetAllRawQuery(sql)
    # print(result1)

    sql = """
    SELECT
        dbm_lot.LOT_ID AS LOT_ID,
        dbm_lot.LOT_NAME AS LOT_NAME,
        dbm_lot.HOUSE_ID AS HOUSE_ID
    FROM dbm_lot ORDER BY LOT_ID ASC
    """
    res = fetAllRawQueryNoColumns(sql)

    column_names = res["columns"]
    for row in res["rows"]:
        insert_value = row
        for used_lot_id in result1:
            if row[0] == used_lot_id["LOT_ID"]:
                insert_value = None
                break
            # endif
        # endfor
        if insert_value:
            result.append(dict(zip(column_names, row)))
    # endfor
    # print(result)

    return result

def getLotListNotUsedByHouseID(house_id):
    debugPrintEx("***")
    result = []

    sql = """
    SELECT
    DISTINCT(dbm_lot.LOT_ID) AS LOT_ID
    FROM dbm_lot, dbt_batch
    WHERE dbm_lot.LOT_ID = dbt_batch.LOT_ID AND dbt_batch.END_DATE > CURRENT_DATE AND dbm_lot.HOUSE_ID = %s
    ORDER BY LOT_ID ASC
    """

    sql = sql % (house_id)

    result1 = fetAllRawQuery(sql)
    # print(result1)

    sql = """
    SELECT
        dbm_lot.LOT_ID AS LOT_ID,
        dbm_lot.LOT_NAME AS LOT_NAME,
        dbm_lot.HOUSE_ID AS HOUSE_ID
    FROM dbm_lot 
    WHERE dbm_lot.HOUSE_ID = %s
    ORDER BY LOT_ID ASC
    """

    sql = sql % (house_id)

    res = fetAllRawQueryNoColumns(sql)

    column_names = res["columns"]
    for row in res["rows"]:
        insert_value = row
        for used_lot_id in result1:
            if row[0] == used_lot_id["LOT_ID"]:
                insert_value = None
                break
            # endif
        # endfor
        if insert_value:
            result.append(dict(zip(column_names, row)))
    # endfor
    # print(result)

    return result


def insertHenGroup(new_data):
    debugPrintEx("***")

    found = False
    hengrps = getHenGroupList()
    for hengrp in hengrps:
        if new_data == hengrp["HENGRP_NAME"]:
            found = True
            break
        # endif
    # endfor
    if found:
        return 0

    created_tdate = datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")
    sql = """
    INSERT INTO dbt_hengrp (
    hengrp_name, created_tdate)
    VALUES ('%s', '%s')
    """
    sql = sql % (new_data, created_tdate)
    # print(sql)
    executeRawQuery(sql)

    # Get max ID
    sql = "SELECT MAX(HENGRP_ID) FROM dbt_hengrp"
    row = fetOneRawQueryNoColumns(sql)

    return row[0]


def insertBatchTable(new_data):
    debugPrintEx("***")

    created_tdate = datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")
    if new_data[4] == "" or new_data[6] == "":
        return 0
    sql = """
    INSERT INTO dbt_batch (
    hengrp_id, lot_id, hen_id, growth_kind, start_date, end_date, initial_count, feeding_date, eggcnt_stime, eggcnt_etime, created_tdate)
    VALUES (%s, %s, %s, %s, '%s', '9999-12-31', %s, '%s', %s, %s, '%s')
    """
    sql = sql % (
        new_data[0],
        new_data[1],
        new_data[2],
        new_data[3],
        new_data[4],
        new_data[5],
        new_data[6],
        new_data[7],
        new_data[8],
        created_tdate,
    )
    # endif
    # print(sql)

    executeRawQuery(sql)

    return 1

def getDeadCountByDate(lot_id, date):
    debugPrintEx("***")

    if lot_id == 0:
        sql = """
        SELECT SUM(dead_count) FROM dbt_dead WHERE CHECK_DATE = '%s'
        """
        sql = sql % (date)
        print(sql)
        row = fetOneRawQueryNoColumns(sql)
    else:
        sql = """
        SELECT SUM(dead_count) FROM dbt_dead WHERE LOT_ID = %s AND CHECK_DATE = '%s'
        """
        sql = sql % (lot_id, date)
        print(sql)
        row = fetOneRawQueryNoColumns(sql)

    return row[0]
