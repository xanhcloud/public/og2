-- public.dbv_daily source

CREATE OR REPLACE VIEW public.dbv_daily
AS SELECT dbt_summary.summary_date AS daily_date,
    dbm_lot.lot_id,
    dbm_lot.lot_name,
    dbm_house.house_id,
    dbm_house.house_name,
    dbt_summary.day_periods,
    dbt_summary.egg_count,
    dbt_summary.curr_count,
    dbt_summary.egg_weight::numeric * 0.1 AS egg_weight,
    dbt_summary.hen_weight,
    round((dbt_summary.food_amount / 1000)::numeric, 1) AS food_amount,
    CASE
        WHEN COALESCE(dbt_summary.curr_count, 0) > 0 THEN round(((dbt_summary.food_amount / dbt_summary.curr_count * 10)::numeric + 0.5) / 10::numeric, 1)
        ELSE 0::numeric
    END AS food_amount_u,
    CASE
        WHEN COALESCE(dbt_summary.curr_count, 0) > 0 THEN round(((dbt_summary.egg_weight * dbt_summary.egg_count / dbt_summary.curr_count * 10)::numeric + 0.5) / 10::numeric, 1)
        ELSE 0::numeric
    END AS egg_vol,
    dbt_batch.initial_count,
    CASE
        WHEN COALESCE(dbt_batch.initial_count, 0) > 0 THEN round(((dbt_summary.curr_count / dbt_batch.initial_count * 100 * 10)::numeric + 0.5) / 10::numeric, 1)
        ELSE 0::numeric
    END AS survival_rate,
    CASE
        WHEN dbt_summary.curr_count > 0 THEN round(dbt_summary.egg_count * 100/ dbt_summary.curr_count, 1)
        ELSE 0
    END AS egg_rate_cur,
    CASE
        WHEN dbt_batch.initial_count > 0 THEN round(dbt_summary.egg_count * 100/ dbt_batch.initial_count, 1)
        ELSE 0
    END AS egg_rate_ini,
    CASE
        WHEN COALESCE(dbt_summary.curr_count, 0) > 0 AND COALESCE(dbt_summary.food_amount, 0) > 0 AND (dbt_summary.food_amount / dbt_summary.curr_count) > 0 THEN round(((dbt_summary.hen_weight / (dbt_summary.food_amount / dbt_summary.curr_count) * 10)::numeric + 0.5) / 10::numeric, 1)
        ELSE 0::numeric
    END AS food_efficeincy,
    CASE
        WHEN COALESCE(dbt_summary.egg_count, 0) > 0 AND COALESCE(dbt_summary.egg_weight::integer, 0) > 0 THEN round(((dbt_summary.food_amount / (dbt_summary.egg_count * dbt_summary.egg_weight) * 100 * 10)::numeric + 0.5) / 10::numeric, 1)
        ELSE 0::numeric
    END AS food_demand,
    dbt_summary.water_amount,
    CASE
        WHEN COALESCE(dbt_summary.curr_count, 0) > 0 THEN round(((dbt_summary.water_amount / dbt_summary.curr_count * 1000 * 10)::numeric + 0.5) / 10::numeric, 1)
        ELSE 0::numeric
    END AS water_amount_u,
    round(((dbt_summary.preset_temp / 10 * 10)::numeric + 0.5) / 10::numeric, 1) AS preset_temp,
    round(((dbt_summary.room_temp / 10 * 10)::numeric + 0.5) / 10::numeric, 1) AS room_temp,
    round(((dbt_summary.room_temp_hi / 10 * 10)::numeric + 0.5) / 10::numeric, 1) AS room_temp_hi,
    round(((dbt_summary.room_temp_lo / 10 * 10)::numeric + 0.5) / 10::numeric, 1) AS room_temp_lo,
    round(((dbt_summary.outer_temp / 10 * 10)::numeric + 0.5) / 10::numeric, 1) AS outer_temp,
    round(((dbt_summary.outer_temp_hi / 10 * 10)::numeric + 0.5) / 10::numeric, 1) AS outer_temp_hi,
    round(((dbt_summary.outer_temp_lo / 10 * 10)::numeric + 0.5) / 10::numeric, 1) AS outer_temp_lo,
    dbt_summary.daylight_hours,
    dbt_summary.defection,
    round(((dbt_summary.coeff / 10 * 10)::numeric + 0.5) / 10::numeric, 1) AS coeff,
    dbt_batch.initial_count - dbt_summary.curr_count AS total_dead,
    ( 
        SELECT 
            sum(dbt_dead.dead_count) AS sum
        FROM dbt_dead
        WHERE dbt_dead.lot_id = dbm_lot.lot_id AND dbt_dead.check_date = dbt_summary.summary_date
    ) AS dead_count,
    round(((dbt_summary.humidity / 10 * 10)::numeric + 0.5) / 10::numeric, 1) AS humidity,
    round(((dbt_summary.humidity_upl / 10 * 10)::numeric + 0.5) / 10::numeric, 1) AS humidity_upl,
    round(((dbt_summary.humidity_lol / 10 * 10)::numeric + 0.5) / 10::numeric, 1) AS humidity_lol,
    round(((dbt_summary.min_ventilation / 100 * 100)::numeric + 0.5) / 100::numeric, 2) AS min_ventilation,
    round(((dbt_summary.bmin_ventilation / 100 * 100)::numeric + 0.5) / 100::numeric, 2) AS bmin_ventilation,
    round(((dbt_summary.ventilation_setup / 100 * 100)::numeric + 0.5) / 100::numeric, 2) AS ventilation_setup,
    dbt_summary.remv_excr,
    dbt_summary.heating,
    dbt_summary.water_pos,
    dbt_summary.aircon_system,
    dbt_summary.lighting_time,
    dbt_summary.food,
    dbt_summary.comments
   FROM dbm_lot
     JOIN dbt_batch ON dbt_batch.lot_id = dbm_lot.lot_id
     JOIN dbt_summary ON dbt_summary.batch_id = dbt_batch.batch_id
     JOIN dbm_house ON dbm_lot.house_id = dbm_house.house_id;