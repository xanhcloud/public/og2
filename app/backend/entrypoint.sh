#!/bin/sh
set -e

. /env/bin/activate

# Apply database migrations (only first time)
if [[ "$APPLY_MIGRATIONS" == "true" ]]; then
  echo "Applying database migrations..."
  python manage.py migrate --noinput
fi

# Collect static files
echo "Collecting static files..."
python manage.py collectstatic --noinput

# Start Supervisor which in turn will manage NGINX and Gunicorn.
exec /usr/bin/supervisord -c /etc/supervisord.conf

# Start Gunicorn
#echo "Starting Gunicorn..."
#exec gunicorn --bind :5000 --workers 3 ovalgrowth_backend.wsgi:application

# Start nginx
#echo "Starting nginx ..."
#nginx -g 'daemon off;'

