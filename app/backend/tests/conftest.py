# app/tests/conftest.py

import pytest
from app.factories import DbmLotFactory

@pytest.fixture
def dbm_lot():
    return DbmLotFactory()
