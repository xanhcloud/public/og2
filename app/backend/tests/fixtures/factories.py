
from factory.django import DjangoModelFactory
from factory import Faker, SubFactory
from ...api import models

class DbmDicFactory(DjangoModelFactory):
  """Factory for creating DbmDic objects."""

  class Meta:
    model = DbmDic
    django_get_or_create = True  # Optional, allows creating or retrieving existing objects

  term_name = Faker('sentence', nb_words=2)  # Generate random term names
  jp_name = Faker('name', locale='ja_JP')  # Generate random Japanese names
  en_name = Faker('name', locale='en_US')  # Generate random English names
  pt_name = Faker('name', locale='pt_BR')  # Generate random Portuguese names
  es_name = Faker('name', locale='es_ES')  # Generate random Spanish names
  # Optionally set specific values for some fields
  # created_tdate = datetime.datetime.now()


class DbmEggFactory(DjangoModelFactory):
  """Factory for creating DbmEgg objects."""

  class Meta:
    model = DbmEgg
    django_get_or_create = True

  lot = SubFactory(DbmLotFactory)  # Creates a DbmLot object before DbmEgg
  eggctr_no = Faker('pyint', min_value=1)  # Generate random positive integers
  cage_row = Faker('pyint', min_value=1)  # Generate random positive integers
  cage_tier = Faker('pyint', min_value=1)  # Generate random positive integers
  reg_egg = Faker('word')  # Generate random word for registration egg

class DbmFooderFactory(DjangoModelFactory):
  """Factory for creating DbmFooder objects."""

  class Meta:
    model = DbmFooder
    django_get_or_create = True

  lot = SubFactory(DbmLotFactory)  # Creates a DbmLot object before DbmFooder
  fooder_no = Faker('pyint', min_value=1)  # Generate random positive integers
  fooder_type = Faker('pyint', min_value=1)  # Generate random integer for fooder type
  reg_dep = Faker('word')  # Generate random word for registration departure
  reg_arv = Faker('word')  # Generate random word for registration arrival

class DbmFooderstFactory(DjangoModelFactory):
  """Factory for creating DbmFooderst objects."""

  class Meta:
    model = DbmFooderst
    django_get_or_create = True

  mfooder = SubFactory(DbmFooderFactory)  # Creates a DbmFooder object before DbmFooderst
  start_time = Faker('time')  # Generate random time
  apply_date = Faker('date')  # Generate random date


class DbmFwFactory(DjangoModelFactory):
  """Factory for creating DbmFw objects."""

  class Meta:
    model = DbmFw
    django_get_or_create = True

  lot = SubFactory(DbmLotFactory)  # Creates a DbmLot object before DbmFw
  fw_no = Faker('pyint', min_value=1)  # Generate random positive integer
  reg_food_up = Faker('word')  # Generate random word for registration food up
  reg_food_lo = Faker('word')  # Generate random word for registration food low
  reg_water = Faker('word')  # Generate random word for registration water

class DbmHenFactory(DjangoModelFactory):
  """Factory for creating DbmHen objects."""

  class Meta:
    model = DbmHen
    django_get_or_create = True

  hen_name = Faker('name')  # Generate random name for hen

class DbmHouseFactory(DjangoModelFactory):
  """Factory for creating DbmHouse objects."""

  class Meta:
    model = DbmHouse
    django_get_or_create = True

  house_name = Faker('sentence', nb_words=2)  # Generate random house name


class DbmLotBackupFactory(DjangoModelFactory):
  """Factory for creating DbmLotBackup objects."""

  class Meta:
    model = DbmLotBackup
    django_get_or_create = True

  lot_id = Faker('pyint')  # Generate random integer for lot ID
  lot_name = Faker('sentence', nb_words=2)  # Generate random name for lot
  house_id = Faker('pyint')  # Generate random integer for house ID
  cage_num = Faker('pyint')  # Generate random integer for cage number
  equip_type = Faker('pyint')  # Generate random integer for equipment type
  comm_type = Faker('pyint')  # Generate random integer for communication type
  ip_address = Faker('ipv4')  # Generate random IP address
  food_input = Faker('pyint')  # Generate random integer for food input
  stop_count = Faker('pyint')  # Generate random integer for stop count

class DbmManualFactory(DjangoModelFactory):
  """Factory for creating DbmManual objects."""

  class Meta:
    model = DbmManual
    django_get_or_create = True

  hen = SubFactory(DbmHenFactory)  # Creates a DbmHen object before DbmManual
  week_periods = Faker('pyint')  # Generate random integer for week periods
  survival_rate = Faker('pyint')  # Generate random integer for survival rate
  hen_weight = Faker('pyint')  # Generate random integer for hen weight
  egg_weight = Faker('pyint')  # Generate random integer for egg weight
  egg_rate = Faker('pyint')  # Generate random integer for egg rate
  egg_vol = Faker('pyint')  # Generate random integer for egg volume

class DbmPriceFactory(DjangoModelFactory):
  """Factory for creating DbmPrice objects."""

  class Meta:
    model = DbmPrice
    django_get_or_create = True

  price_date = Faker('date')  # Generate random date
  food_price = Faker('pyint')  # Generate random integer for food price
  stdegg_price = Faker('pyint')  # Generate random integer for standard egg price
  outegg_price = Faker('pyint')  # Generate random integer for out egg price

class DbmThermoFactory(DjangoModelFactory):
  """Factory for creating DbmThermo objects."""

  class Meta:
    model = DbmThermo
    django_get_or_create = True

  lot = SubFactory(DbmLotFactory)  # Creates a DbmLot object before DbmThermo
  mthermo_type = Faker('pyint')  # Generate random integer for thermo type
  mthermo_no = Faker('pyint')  # Generate random integer for thermo number
  reg_cur = Faker('word')  # Generate random word for registration current
  reg_high = Faker('word')  # Generate random word for registration high
  reg_low = Faker('word')  # Generate random word for registration low

class DbmUserFactory(DjangoModelFactory):
  """Factory for creating DbmUser objects."""

  class Meta:
    model = DbmUser
    django_get_or_create = True

  user_id = Faker('bothify', text='????####')  # Generate random user ID with 4 letters and 4 digits
  password = Faker('password', length=8)  # Generate random password with length 8
  user_name = Faker('name')  # Generate random user name


class DbtBatchFactory(DjangoModelFactory):
  """Factory for creating DbtBatch objects."""

  class Meta:
    model = DbtBatch
    django_get_or_create = True

  lot = SubFactory(DbmLotFactory)  # Creates a DbmLot object before DbtBatch
  hen = SubFactory(DbmHenFactory)  # Creates a DbmHen object before DbtBatch
  hengrp = SubFactory(DbtHengrpFactory)  # Creates a DbtHengrp object before DbtBatch
  growth_kind = Faker('pyint')  # Generate random integer for growth kind
  start_date = Faker('date', past=True)  # Generate random past date for start date
  end_date = Faker('date', after=Faker('date', past=True))  # Generate random future date for end date
  initial_count = Faker('pyint')  # Generate random integer for initial count
  feeding_date = Faker('date', past=True)  # Generate random past date for feeding date
  eggcnt_stime = Faker('pyint', min_value=0, max_value=23)  # Generate random integer between 0 and 23 for egg count start time
  eggcnt_etime = Faker('pyint', min_value=eggcnt_stime, max_value=23)  # Generate random end time after start time

class DbtCollectionFactory(DjangoModelFactory):
  """Factory for creating DbtCollection objects."""

  class Meta:
    model = DbtCollection
    django_get_or_create = True

  batch = SubFactory(DbtBatchFactory)  # Creates a DbtBatch object before DbtCollection
  col_tdate = Faker('datetime')  # Generate random datetime for collection date

class DbtEggFactory(DjangoModelFactory):
  """Factory for creating DbtEgg objects."""

  class Meta:
    model = DbtEgg
    django_get_or_create = True

  collect = SubFactory(DbtCollectionFactory)  # Creates a DbtCollection object before DbtEgg
  megg = SubFactory(DbmEggFactory)  # Creates a DbmEgg object before DbtEgg
  egg_count = Faker('pyint')  # Generate random integer for egg count


class DbtEggCopyFactory(DjangoModelFactory):
  """Factory for creating DbtEggCopy objects."""

  class Meta:
    model = DbtEggCopy
    django_get_or_create = True

  collect = SubFactory(DbtCollectionFactory)  # Creates a DbtCollection object before DbtEggCopy
  megg = SubFactory(DbmEggFactory)  # Creates a DbmEgg object before DbtEggCopy
  egg_count = Faker('pyint')  # Generate random integer for egg count

class DbtEggchgFactory(DjangoModelFactory):
  """Factory for creating DbtEggchg objects."""

  class Meta:
    model = DbtEggchg
    django_get_or_create = True

  summary = SubFactory('DbtSummaryFactory')  # Replace with the appropriate factory name
  megg = SubFactory(DbmEggFactory)  # Creates a DbmEgg object before DbtEggchg
  egg_count = Faker('pyint')  # Generate random integer for egg count

class DbtFooderFactory(DjangoModelFactory):
  """Factory for creating DbtFooder objects."""

  class Meta:
    model = DbtFooder
    django_get_or_create = True

  collect = SubFactory(DbtCollectionFactory)  # Creates a DbtCollection object before DbtFooder
  mfooder = SubFactory(DbmFooderFactory)  # Creates a DbmFooder object before DbtFooder
  departure_time = Faker('time')  # Generate random time for departure
  arrival_time = Faker('time')  # Generate random time for arrival

class DbtFwFactory(DjangoModelFactory):
  """Factory for creating DbtFw objects."""

  class Meta:
    model = DbtFw
    django_get_or_create = True

  collect = SubFactory(DbtCollectionFactory)  # Creates a DbtCollection object before DbtFw
  mfw = SubFactory(DbmFwFactory)  # Creates a DbmFw object before DbtFw (can be None)
  food_amount = Faker('pyint')  # Generate random integer for food amount
  water_amount = Faker('pyint')  # Generate random integer for water amount

class DbtHengrpFactory(DjangoModelFactory):
  """Factory for creating DbtHengrp objects."""

  class Meta:
    model = DbtHengrp
    django_get_or_create = True

  hengrp_name = Faker('sentence', nb_words=2)  # Generate random name for hen group

class DbtSummaryFactory(DjangoModelFactory):
  """Factory for creating DbtSummary objects."""

  class Meta:
    model = DbtSummary
    django_get_or_create = True

  batch = SubFactory(DbtBatchFactory)  # Creates a DbtBatch object before DbtSummary
  summary_date = Faker('date')  # Generate random date for summary date
  day_periods = Faker('pyint')  # Generate random integer for day periods (can be None)
  week_periods = Faker('pyint')  # Generate random integer for week periods (can be None)
  collect = SubFactory(DbtCollectionFactory)  # Creates a DbtCollection object before DbtSummary
  egg_count = Faker('pyint')  # Generate random integer for egg count (can be None)
  food_amount = Faker('pyint')  # Generate random integer for food amount (can be None)
  water_amount = Faker('pyint')  # Generate random integer for water amount (can be None)
  curr_count = Faker('pyint')  # Generate random integer for current count (can be None)
  dead_count = Faker('pyint')  # Generate random integer for dead count (can be None)
  hen_weight = Faker('pyint')  # Generate random integer for hen weight (can be None)
  egg_weight = Faker('pyint')  # Generate random integer for egg weight (can be None)
  defection = Faker('pyint')  # Generate random integer for defection (can be None)
  coeff = Faker('pyint')  # Generate random integer for coeff (can be None)
  # ... (other fields with similar logic)
  comments = Faker('sentence', nb_words=5)  # Generate random comments
  created_tdate = Faker('datetime')  # Generate random datetime for creation

class DbtThermoFactory(DjangoModelFactory):
  """Factory for creating DbtThermo objects."""

  class Meta:
    model = DbtThermo
    django_get_or_create = True

  collect = SubFactory(DbtCollectionFactory)  # Creates a DbtCollection object before DbtThermo
  mthermo = SubFactory(DbmThermoFactory)  # Creates a DbmThermo object before DbtThermo
  cur_val = Faker('pyint')  # Generate random integer for current value (can be None)
  high_val = Faker('pyint')  # Generate random integer for high value (can be None)
  low_val = Faker('pyint')  # Generate random integer for low value (can be None)
  created_tdate = Faker('datetime')  # Generate random datetime for creation
