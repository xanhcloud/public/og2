import pytest
from ..fixtures import factories  # Import factories from fixtures directory
from ....api import models  # Import DbtLot model from api directory

@pytest.mark.django_db
def test_dbm_lot_creation(dbm_lot):
    assert DbmLot.objects.count() == 1
    assert dbm_lot.lot_name is not None
    assert dbm_lot.house is not None
    assert dbm_lot.cage_num is not None
    assert dbm_lot.equip_type is not None
    assert dbm_lot.comm_type is not None
    assert dbm_lot.ip_address is not None
    assert dbm_lot.food_input is not None
    assert dbm_lot.eggco is not None
    assert dbm_lot.stop_count is not None
    assert dbm_lot.created_tdate is not None

@pytest.mark.django_db
def test_dbm_lot_retrieval(dbm_lot):
    retrieved_lot = DbmLot.objects.get(lot_id=dbm_lot.lot_id)
    assert retrieved_lot == dbm_lot

@pytest.mark.django_db
def test_dbm_lot_update(dbm_lot):
    dbm_lot.lot_name = 'Updated Lot Name'
    dbm_lot.save()
    updated_lot = DbmLot.objects.get(lot_id=dbm_lot.lot_id)
    assert updated_lot.lot_name == 'Updated Lot Name'

@pytest.mark.django_db
def test_dbm_lot_deletion(dbm_lot):
    dbm_lot.delete()
    with pytest.raises(DbmLot.DoesNotExist):
        DbmLot.objects.get(lot_id=dbm_lot.lot_id)
