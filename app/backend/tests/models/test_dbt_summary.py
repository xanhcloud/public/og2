import pytest
from ..fixtures import factories  # Import factories from fixtures directory
from ....api import models  # Import DbtSummary model from api directory
from django.contrib.auth.models import User  # Assuming User model for ownership

pytestmark = pytest.mark.django_db  # Mark tests to use the Django database


@pytest.fixture
def user():
  """Fixture to create a user for ownership of DbtSummary."""
  return User.objects.create_user(username="testuser", password="password123")


@pytest.fixture
def dbt_summary_factory(user):
  """Fixture to create a DbtSummary factory with a user."""
  def factory_wrapper(**kwargs):
    kwargs["batch"] = factories.DbtBatchFactory()
    kwargs["collect"] = factories.DbtCollectionFactory()
    kwargs["created_by"] = user
    return factories.DbtSummaryFactory(**kwargs)
  return factory_wrapper


def test_create_dbt_summary(dbt_summary_factory):
  """Test creating a DbtSummary object with factory."""
  summary = dbt_summary_factory()
  assert summary.summary_date == summary.summary_date
  assert summary.batch is not None
  assert summary.collect is not None
  assert summary.created_by.username == "testuser"  # From fixture


@pytest.mark.parametrize(
    "field, value",
    [
        ("egg_count", 100),
        ("food_amount", 50),
        ("water_amount", 20),
        ("curr_count", 250),
    ],
)
def test_dbt_summary_field_values(dbt_summary_factory, field, value):
  """Test setting specific field values during DbtSummary creation."""
  summary = dbt_summary_factory(**{field: value})
  assert getattr(summary, field) == value


def test_dbt_summary_optional_fields(dbt_summary_factory):
  """Test DbtSummary object creation with optional fields being None."""
  summary = dbt_summary_factory(day_periods=None, week_periods=None)
  assert summary.day_periods is None
  assert summary.week_periods is None
