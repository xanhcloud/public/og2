# app/tests/test_views.py

import pytest
from unittest.mock import patch, MagicMock
from rest_framework.test import APIClient
from django.urls import reverse

# URL name for the dailytable view, adjust if different
DAILYTABLE_URL = reverse('dailytable', kwargs={'id': 1})

@pytest.mark.django_db
@patch('api.views.getHouseNameByLotID')
@patch('api.views.getLotList')
@patch('api.views.loadDailyViewItem')
@patch('api.views.getBatchInfoByLotID')
@patch('api.views.getHengrpInfoByLotID')
@patch('api.views.getBatchInfoList')
@patch('api.views.getDailyTable')
@patch('api.views.getDailyHistTable')
@patch('api.views.getTodayDailyTable')
def test_dailytable_view(
        mock_getTodayDailyTable,
        mock_getDailyHistTable,
        mock_getDailyTable,
        mock_getBatchInfoList,
        mock_getHengrpInfoByLotID,
        mock_getBatchInfoByLotID,
        mock_loadDailyViewItem,
        mock_getLotList,
        mock_getHouseNameByLotID
    ):

    client = APIClient()
    mock_getHouseNameByLotID.return_value = 'Test House'
    mock_getLotList.return_value = [{'LOT_ID': 1, 'HOUSE_NAME': 'Test House'}]
    mock_loadDailyViewItem.return_value = (['item1', 'item2'], ['flag1', 'flag2'])
    mock_getBatchInfoByLotID.return_value = ['Test Batch', 'Test Batch Info', 'batch_curr']
    mock_getHengrpInfoByLotID.return_value = ['Hengrp Info']
    mock_getBatchInfoList.return_value = [{'GROWTH_KIND': 1, 'BATCH_ID': 123}]
    mock_getDailyTable.return_value = [{'data': 'daily_table_row'}]
    mock_getDailyHistTable.return_value = [{'data': 'daily_hist_table_row'}]
    mock_getTodayDailyTable.return_value = [{'data': 'today_daily_table_row'}]

    response = client.get(DAILYTABLE_URL)
    assert response.status_code == 200
    assert 'id' in response.data
    assert 'tabs' in response.data
    assert 'rows' in response.data
    assert 'today_rows' in response.data
    assert 'house' in response.data
    assert 'items' in response.data
    assert 'flags' in response.data
    assert 'growth' in response.data
    assert 'growth1' in response.data
    assert 'growth2' in response.data
    assert 'growth3' in response.data
    assert 'data_f' in response.data
    assert response.data['id'] == 1
    assert response.data['house'] == 'Test House'
    assert response.data['items'] == ['item1', 'item2']
    assert response.data['flags'] == ['flag1', 'flag2']
    assert response.data['rows'] == [{'data': 'daily_table_row'}]
    assert response.data['today_rows'] == [{'data': 'today_daily_table_row'}]
    assert response.data['data_f'] is True
