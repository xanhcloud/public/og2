#!/bin/bash

# Load values for variables from the environment file
source .env

#-------------------------------------------------------------------------
# Specify the needed LOT_ID and HOUSE_ID
HOUSE_ID=13
LOT_ID=16
HEN_ID=3
#-------------------------------------------------------------------------

# Common variables
DB_SERVER="${DB_SERVER:-139.180.190.106}"
DB_PORT="${DB_PORT:-35432}"
DB_NAME="${DB_NAME:-og2_stag}"
DB_USER="${DB_USER:-postgres}"
DB_PASSWORD="${DB_PASSWORD:-postgres}"
LOG_FILE="${LOG_FILE:-./add_lot.log}"


# The lastest value of PK columns
LOT_PK_START=$LOT_ID
HENGRP_PK_START=0
BATCH_PK_START=0
EC_PK_START=0
FEEDER_PK_START=0
FW_PK_START=0
THERMO_PK_START=0


# Function to log messages
log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $1" >> "$LOG_FILE"
}

# Query the latest value of PK in tables
export PGPASSWORD="$DB_PASSWORD"

HENGRP_PK_START=$(psql -h "$DB_SERVER" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" -t -c \
"SELECT COALESCE(MAX(hengrp_id), 0) + 1 FROM public.dbt_hengrp;" | tr -d '[:space:]')

if [[ -z "$HENGRP_PK_START" ]]; then
    log "Error: Failed to retrieve the latest hengrp_id."
    exit 1
fi
log "Latest hengrp_id is $((HENGRP_PK_START - 1)). Starting inserts from $HENGRP_PK_START."

#-----------------------
BATCH_PK_START=$(psql -h "$DB_SERVER" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" -t -c \
"SELECT COALESCE(MAX(batch_id), 0) + 1 FROM public.dbt_batch;" | tr -d '[:space:]')

if [[ -z "$BATCH_PK_START" ]]; then
    log "Error: Failed to retrieve the latest batch_id."
    exit 1
fi
log "Latest batch_id is $((BATCH_PK_START - 1)). Starting inserts from $BATCH_PK_START."

#-----------------------
EC_PK_START=$(psql -h "$DB_SERVER" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" -t -c \
"SELECT COALESCE(MAX(megg_id), 0) + 1 FROM public.dbm_egg;" | tr -d '[:space:]')

if [[ -z "$EC_PK_START" ]]; then
    log "Error: Failed to retrieve the latest megg_id."
    exit 1
fi
log "Latest megg_id is $((EC_PK_START - 1)). Starting inserts from $EC_PK_START."

#-----------------------
FEEDER_PK_START=$(psql -h "$DB_SERVER" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" -t -c \
"SELECT COALESCE(MAX(mfooder_id), 0) + 1 FROM public.dbm_fooder;" | tr -d '[:space:]')

if [[ -z "$FEEDER_PK_START" ]]; then
    log "Error: Failed to retrieve the latest mfooder_id."
    exit 1
fi
log "Latest mfooder_id is $((FEEDER_PK_START - 1)). Starting inserts from $FEEDER_PK_START."

#-----------------------
FW_PK_START=$(psql -h "$DB_SERVER" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" -t -c \
"SELECT COALESCE(MAX(mfw_id), 0) + 1 FROM public.dbm_fw;" | tr -d '[:space:]')

if [[ -z "$FW_PK_START" ]]; then
    log "Error: Failed to retrieve the latest mfw_id."
    exit 1
fi
log "Latest mfw_id is $((FW_PK_START - 1)). Starting inserts from $FW_PK_START."

#-----------------------
THERMO_PK_START=$(psql -h "$DB_SERVER" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" -t -c \
"SELECT COALESCE(MAX(mthermo_id), 0) + 1 FROM public.dbm_thermo;" | tr -d '[:space:]')

if [[ -z "$THERMO_PK_START" ]]; then
    log "Error: Failed to retrieve the latest mthermo_id."
    exit 1
fi
log "Latest mthermo_id is $((THERMO_PK_START - 1)). Starting inserts from $THERMO_PK_START."


#-----------------------
# Check for required variables
if [[ -z "$LOT_PK_START" || -z "$THERMO_PK_START" ]]; then
    log "Error: Primary key start values are not initialized."
    exit 1
fi

# Execute SQL commands
if ! ( export PGPASSWORD="$DB_PASSWORD"; psql -h "$DB_SERVER" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" <<EOF
  BEGIN;
INSERT INTO public.dbm_lot(lot_id, lot_name, cage_num, equip_type, comm_type, ip_address, food_input, eggco, stop_count, created_tdate, house_id)
	VALUES ($LOT_ID, 'Test Lot $LOT_ID', 4000, 0, 0, '192.168.24.122', 0, 24, 0, '2014-01-01 00:00:00+00', $HOUSE_ID);

INSERT INTO public.dbt_hengrp(hengrp_id, hengrp_name, created_tdate)
	VALUES ($HENGRP_PK_START, 'Test HenGroup $HENGRP_PK_START', '2014-01-01 00:00:00+00');

INSERT INTO public.dbt_batch(batch_id, growth_kind, start_date, end_date, initial_count, feeding_date, eggcnt_stime, created_tdate, eggcnt_etime, hen_id, lot_id, hengrp_id)
	VALUES ($BATCH_PK_START, 3, '2024-11-22', '9999-12-31', 29999, 	'2024-11-20', 8, '2024-11-22 00:00:00+00', $HEN_ID, $LOT_ID, $HENGRP_PK_START);

INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file)
        VALUES ($EC_PK_START+0, 1, 1, 1, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_101');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+1, 2, 1, 2, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_102');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+2, 3, 1, 3, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_103');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+3, 4, 2, 1, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_104');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+4, 5, 2, 2, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_105');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+5, 6, 2, 3, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_106');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+6, 7, 3, 1, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_107');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+7, 8, 3, 2, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_108');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+8, 9, 3, 3, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_109');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+9, 10, 4, 1, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_110');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+10, 11, 4, 2, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_111');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+11, 12, 4, 3, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_112');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+12, 13, 5, 1, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_113');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+13, 14, 5, 2, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_114');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+14, 15, 5, 3, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_115');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+15, 16, 6, 1, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_116');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+16, 17, 6, 2, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_117');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+17, 18, 6, 3, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'EC_118');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+18, 19, 7, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_119');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+19, 20, 7, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_120');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+20, 21, 7, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_121');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+21, 22, 8, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_122');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+22, 23, 8, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_123');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+23, 24, 8, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_124');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+24, 25, 9, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_201');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+25, 26, 9, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_202');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+26, 27, 9, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_203');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+27, 28, 10, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_204');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+28, 29, 10, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_205');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+29, 30, 10, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_206');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+30, 31, 11, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_207');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+31, 32, 11, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_208');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+32, 33, 11, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_209');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+33, 34, 12, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_210');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+34, 35, 12, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_211');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+35, 36, 12, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_212');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+36, 37, 13, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_213');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+37, 38, 13, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_214');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+38, 39, 13, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_215');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+39, 40, 14, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_216');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+40, 41, 14, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_217');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+41, 42, 14, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_218');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+42, 43, 15, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_219');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+43, 44, 15, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_220');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+44, 45, 15, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_221');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+45, 46, 16, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_222');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+46, 47, 16, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_223');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+47, 48, 16, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_224');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+48, 49, 17, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_301');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+49, 50, 17, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_302');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+50, 51, 17, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_303');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+51, 52, 18, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_304');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+52, 53, 18, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_305');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+53, 54, 18, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_306');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+54, 55, 19, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_307');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+55, 56, 19, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_308');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+56, 57, 19, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_309');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+57, 58, 20, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_310');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+58, 59, 20, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_311');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+59, 60, 20, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_312');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+60, 61, 21, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_313');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+61, 62, 21, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_314');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+62, 63, 21, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_315');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+63, 64, 22, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_316');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+64, 65, 22, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_317');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+65, 66, 22, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_318');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+66, 67, 23, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_319');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+67, 68, 23, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_320');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+68, 69, 23, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_321');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+69, 70, 24, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_322');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+70, 71, 24, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_323');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+71, 72, 24, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_324');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+72, 73, 25, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_401');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+73, 74, 25, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_402');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+74, 75, 25, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_403');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+75, 76, 26, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_404');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+76, 77, 26, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_405');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+77, 78, 26, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_406');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+78, 79, 27, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_407');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+79, 80, 27, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_408');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+80, 81, 27, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_409');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+81, 82, 28, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_410');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+82, 83, 28, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_411');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+83, 84, 28, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_412');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+84, 85, 29, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_413');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+85, 86, 29, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_414');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+86, 87, 29, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_415');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+87, 88, 30, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_416');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+88, 89, 30, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_417');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+89, 90, 30, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_418');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+90, 91, 31, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_419');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+91, 92, 31, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_420');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+92, 93, 31, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_421');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+93, 94, 32, 1, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_422');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+94, 95, 32, 2, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_423');
INSERT INTO public.dbm_egg(megg_id, eggctr_no, cage_row, cage_tier, reg_egg, created_tdate, lot_id, json_file) VALUES ($EC_PK_START+95, 96, 32, 3, 0, '2024-11-07 00:00:00+00', $LOT_ID, 'EC_424');


INSERT INTO public.dbm_fooder(mfooder_id, fooder_no, fooder_type, reg_dep, reg_arv, created_tdate, lot_id, json_file)
	VALUES ($FEEDER_PK_START+0, 1, 1, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'FE_101');
INSERT INTO public.dbm_fooder(mfooder_id, fooder_no, fooder_type, reg_dep, reg_arv, created_tdate, lot_id, json_file) VALUES ($FEEDER_PK_START+1, 2, 1, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'FE_102');
INSERT INTO public.dbm_fooder(mfooder_id, fooder_no, fooder_type, reg_dep, reg_arv, created_tdate, lot_id, json_file) VALUES ($FEEDER_PK_START+2, 3, 1, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'FE_201');
INSERT INTO public.dbm_fooder(mfooder_id, fooder_no, fooder_type, reg_dep, reg_arv, created_tdate, lot_id, json_file) VALUES ($FEEDER_PK_START+3, 4, 1, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'FE_202');
INSERT INTO public.dbm_fooder(mfooder_id, fooder_no, fooder_type, reg_dep, reg_arv, created_tdate, lot_id, json_file) VALUES ($FEEDER_PK_START+4, 5, 1, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'FE_301');
INSERT INTO public.dbm_fooder(mfooder_id, fooder_no, fooder_type, reg_dep, reg_arv, created_tdate, lot_id, json_file) VALUES ($FEEDER_PK_START+5, 6, 1, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'FE_302');
INSERT INTO public.dbm_fooder(mfooder_id, fooder_no, fooder_type, reg_dep, reg_arv, created_tdate, lot_id, json_file) VALUES ($FEEDER_PK_START+6, 7, 1, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'FE_401');
INSERT INTO public.dbm_fooder(mfooder_id, fooder_no, fooder_type, reg_dep, reg_arv, created_tdate, lot_id, json_file) VALUES ($FEEDER_PK_START+7, 8, 1, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'FE_402');

INSERT INTO public.dbm_fw(mfw_id, fw_no, reg_food_up, reg_food_lo, reg_water, created_tdate, lot_id, json_file)
	VALUES ($FW_PK_START+0, 1, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'F_001');
INSERT INTO public.dbm_fw(mfw_id, fw_no, reg_food_up, reg_food_lo, reg_water, created_tdate, lot_id, json_file) VALUES ($FW_PK_START+1, 2, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'F_002');

INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)
        VALUES ($THERMO_PK_START+0, 1, 1, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'TEMP_101');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+1, 1, 2, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'TEMP_102');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+2, 1, 3, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'TEMP_103');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+3, 1, 4, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'TEMP_201');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+4, 1, 5, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'TEMP_202');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+5, 1, 6, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'TEMP_203');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+6, 1, 7, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'TEMP_301');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+7, 1, 8, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'TEMP_302');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+8, 1, 9, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'TEMP_303');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+9, 1, 10, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'TEMP_401');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+10, 1, 11, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'TEMP_402');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+11, 1, 12, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'TEMP_403');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+12, 2, 1, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'OUTER_TEMP');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+13, 3, 1, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'HUMIDITY_101');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+14, 3, 2, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'HUMIDITY_102');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+15, 3, 3, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'HUMIDITY_103');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+16, 3, 4, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'HUMIDITY_201');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+17, 3, 5, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'HUMIDITY_202');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+18, 3, 6, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'HUMIDITY_203');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+19, 3, 7, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'HUMIDITY_301');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+20, 3, 8, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'HUMIDITY_302');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+21, 3, 9, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'HUMIDITY_303');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+22, 3, 10, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'HUMIDITY_401');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+23, 3, 11, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'HUMIDITY_402');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+24, 3, 12, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'HUMIDITY_403');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+25, 4, 1, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'NH3_101');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+26, 4, 2, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'NH3_102');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+27, 4, 3, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'NH3_103');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+28, 4, 4, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'NH3_201');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+29, 4, 5, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'NH3_202');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+30, 4, 6, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'NH3_203');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+31, 4, 7, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'NH3_301');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+32, 4, 8, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'NH3_302');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+33, 4, 9, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'NH3_303');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+34, 4, 10, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'NH3_401');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+35, 4, 11, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'NH3_402');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+36, 4, 12, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'NH3_403');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+37, 5, 1, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'CO2_101');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+38, 5, 2, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'CO2_102');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+39, 5, 3, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'CO2_103');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+40, 5, 4, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'CO2_201');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+41, 5, 5, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'CO2_202');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+42, 5, 6, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'CO2_203');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+43, 5, 7, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'CO2_301');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+44, 5, 8, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'CO2_302');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+45, 5, 9, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'CO2_303');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+46, 5, 10, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'CO2_401');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+47, 5, 11, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'CO2_402');
INSERT INTO public.dbm_thermo(mthermo_id, mthermo_type, mthermo_no, reg_cur, reg_high, reg_low, created_tdate, lot_id, json_file)  VALUES ($THERMO_PK_START+48, 5, 12, 0, 0, 0, '2014-01-01 00:00:00+00', $LOT_ID, 'CO2_403');

  COMMIT;
EOF
); then
    log "Error: Failed to populate Lot data. Rolling back."
    exit 1
else
    log "Successfully inserted data into dbm_thermo for LOT_ID=$LOT_ID."
fi
