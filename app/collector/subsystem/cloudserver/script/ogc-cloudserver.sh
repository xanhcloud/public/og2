#!/bin/bash

# Enable strict error handling
set -euo pipefail

# Load values for variables from the environment file
readonly SCRIPT_DIR=$(dirname "$(realpath "$0")")
source "${SCRIPT_DIR}/.env"

# Define paths
readonly TMP_DIR="${TMP_DIR:-$APP_DIR/log/tmp}"

# Validate critical environment variables
if [[ -z "${TMP_DIR}" ]]; then
  echo "Error: Required environment variables are not set."
  exit 1
fi

# Create TMP_DIR if it doesn't exist
mkdir -p "${TMP_DIR}"

# All interval times are defined in seconds
readonly INTERVAL_PROCESS_FILES="${INTERVAL_PROCESS_FILES:-30}"
readonly INTERVAL_INSERT_DATA="${INTERVAL_INSERT_DATA:-30}"
readonly INTERVAL_REMOVE_ARCHIVES="${INTERVAL_REMOVE_ARCHIVES:-84600}"

# Log file
readonly LOG_FILE="${CS_LOG_FILE:-ogc-cloudserver.log}"

# Notification command
readonly APPRISE_CMD="${CS_APPRISE_CMD:-apprise -t 'OG-Collector CloudServer Failure' -b}"

# Function to log messages
log() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1" >> "${TMP_DIR}/${LOG_FILE}"
}

# Function to send failure notifications
send_failure_notification() {
    local message="$1"
#    ${APPRISE_CMD} "$message"
}

# Function to handle SIGTERM
cleanup() {
  log "Stopping wrapper script gracefully."
  exit 0
}

# Trap SIGTERM
trap cleanup SIGTERM

# Initialize a counter
counter=1

while true; do
  # Run this first script every INTERVAL_PROCESS_FILES seconds
  if (( counter % ${INTERVAL_PROCESS_FILES} == 0 )); then
    log "Running process_files.sh..."
    "${SCRIPT_DIR}/process_files.sh"
    if [ $? -ne 0 ]; then
      log "Script Process_files.sh failed with exit code $?."
      send_failure_notification "Script process_files.sh failed with exit code $?."
      exit 1
    fi
  fi

  # Run this script every INTERVAL_INSERT_DATA seconds
#  if (( counter % ${INTERVAL_INSERT_DATA} == 0 )); then
#    log "Running insert_data.sh..."
#    "${SCRIPT_DIR}/insert_data.sh"
#    if [ $? -ne 0 ]; then
#      log "Script insert_data.sh failed with exit code $?."
#      send_failure_notification "Script inseart_data.sh failed with exit code $?."
#      exit 1
#    fi
#  fi

  # Run this script every INTERVAL_REMOVE_ARCHIVES seconds
  if (( counter % ${INTERVAL_REMOVE_ARCHIVES} == 0 )); then
    log "Running remove_archives.sh..."
    "${SCRIPT_DIR}/remove_archives.sh"
    if [ $? -ne 0 ]; then
      log "Script remove_archives.sh failed with exit code $?."
      send_failure_notification "Script remove_archives.sh failed with exit code $?."
      exit 1
    fi
  fi

  # Increment the counter and sleep for 1 second
  ((counter++))
  sleep 1

  # Reset the counter after 1 day (86400 seconds) or LCM of intervals
  if (( counter > 86400 )); then
    counter=1
  fi
done
