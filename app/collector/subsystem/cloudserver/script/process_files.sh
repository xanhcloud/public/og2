#!/bin/bash

# Load values for variables from the environment file
SCRIPT_DIR=$(dirname "$(realpath "$0")")
source "$SCRIPT_DIR/.env"

APP_DIR="${APP_DIR:-~/apps/ovalgrowth/runtime/og-collector/cloudserver}"
INBOX_DIR="${INBOX_DIR:-$APP_DIR/file/inbox}"
ARCHIVE_DIR="${ARCHIVE_DIR:-$APP_DIR/file/archive}"
ERROR_DIR="${ERROR_DIR:-$APP_DIR/file/error}"
TMP_DIR="${TMP_DIR:-$APP_DIR/log/tmp}"

DB_SERVER="${DB_SERVER:-127.0.0.1}"
DB_PORT="${DB_PORT:-35432}"
DB_NAME="${DB_NAME:-og2_stag}"
DB_USER="${DB_USER:-postgres}"
DB_PASSWORD="${DB_PASSWORD:-postgres}"
DB_TABLE_JSON="${DB_TABLE_JSON:-dbt_json}"
DB_TABLE_ERROR="${DB_TABLE_ERROR:-dbt_error}"

LOG_FILE="${FP_LOG_FILE:-process_files.log}"
APPRISE_CMD="${FP_APPRISE_CMD:-apprise -t 'OGC-CloudServer File Processing Failure' -b}"


# Function to log messages
log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $1" >> ${TMP_DIR}/${LOG_FILE}
}

# Function to send failure notifications
send_notification() {
    local message="$1"
#    $APPRISE_CMD "$message"
}

cleanup() {
    log "Cleanup completed."
}


# Function to import NDJSON file into PostgreSQL using \COPY
import_ndjson() {
    local file=$1

    log "Starting import for $file."

    # Start a transaction, create a temp table, load data, and insert into the main table in one session
    if [[ "${file##*.}" == "ndjson" ]]; then
        if ! ( export PGPASSWORD="$DB_PASSWORD"; psql -h "$DB_SERVER" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" <<EOF
        BEGIN;
        CREATE TEMP TABLE temp_dbt_json (DATA JSONB);
        \COPY temp_dbt_json (DATA) FROM $file WITH (FORMAT 'text');
        INSERT INTO $DB_TABLE_JSON (data, created_at, used)
        SELECT data, CURRENT_TIMESTAMP, false FROM temp_dbt_json;
        COMMIT;
EOF
        ) ; then
            log "Error during import process for $file. Rolling back."
            send_notification "Error during import process for $file. Transaction rolled back."
            mv "$file" "$ERROR_DIR/$file"
            return 1
        fi
    else
        if ! ( export PGPASSWORD="$DB_PASSWORD"; psql -h "$DB_SERVER" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" <<EOF
            BEGIN;
            CREATE TEMP TABLE temp_dbt_error (DATA JSONB);
            \COPY temp_dbt_error (DATA) FROM $file WITH (FORMAT 'text');
            INSERT INTO $DB_TABLE_ERROR (data, created_at, used)
            SELECT data, CURRENT_TIMESTAMP, false FROM temp_dbt_error;
            COMMIT;
EOF
        ) ; then
            log "Error during import process for $file. Rolling back."
            send_notification "Error during import process for $file. Transaction rolled back."
            mv "$file" "$ERROR_DIR/$file"
            return 1
        fi
    fi

    log "Imported successfully."

    # Get current date and hour for archive folder structure (e.g., YYYY-MM-DD/HH)
    local current_date
    current_date=$(date +"%Y-%m-%d")
    local current_hour
    current_hour=$(date +"%H")

    # Ensure the archive directory with the date and hour exists before moving the file
    local archive_dir_with_date_and_hour="${ARCHIVE_DIR}/${current_date}/${current_hour}"
    mkdir -p "$archive_dir_with_date_and_hour"

    # Move the processed file to the archive folder with the current date and hour
    if ! mv "$file" "$archive_dir_with_date_and_hour"/$(basename "$file"); then
        log "Error moving $file to archive"
        return 1
    fi

    log "Archived successfully."
    return 0
}


# Handle cleanup on exit and error
trap cleanup EXIT SIGHUP SIGINT SIGTERM ERR

## Notes: Use a .pif file and flock to make sure there is only one instance of this script running
mkdir -p "$TMP_DIR"
PIDFILE="$TMP_DIR/$(basename "$0").pid"
LOCK_FILE="$TMP_DIR/$(basename "$0").lock"

# Create a lock using flock
(
    flock -n 200 || { log "Another transfer is in progress, exiting"; exit 0; }

    if [ -e "$PIDFILE" ] && kill -0 "$(cat "$PIDFILE")"; then
        log "Another instance of the script is running, exiting."
        exit 1
    fi

    # Create the PID file
    echo $$ > "$PIDFILE"

    # Ensure the PID file is removed when the script exits
    trap 'rm -f "$PIDFILE"' EXIT

    log "Starting import for $file."

    # Create a temporary file to hold the current list of data files
    TEMP_FILE=$(mktemp)

    # Capture the list of current NDJSON & NDERR files into the temp file
    find "$INBOX_DIR" \( -name "*.ndjson" -o -name "*.nderr" \) | sort > "$TEMP_FILE"

    # If there are files in the temp file
    if [ -s "$TEMP_FILE" ]; then
        # Read through the temp file and process the data files
        while read -r ndjson_file; do
            if [ -f "$ndjson_file" ]; then
                if ! import_ndjson "$ndjson_file"; then
                    log "Failed to import $ndjson_file."
                fi
            else
                log "No NDJSON/NDERR files found in $INBOX_DIR."
            fi
        done < "$TEMP_FILE"
    fi

    # Remove the temporary file
    rm "$TEMP_FILE"

) 200>"$LOCK_FILE"

