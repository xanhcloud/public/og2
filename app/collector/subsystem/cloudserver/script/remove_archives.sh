#!/bin/bash

# Load values for variables from the environment file
SCRIPT_DIR=$(dirname "$(realpath "$0")")
source "$SCRIPT_DIR/.env"

# Set environment variables for archive directory, retention period, and log file
APP_DIR="${APP_DIR:-~/apps/ovalgrowth/runtime/og-collector/cloudserver}"
ARCHIVE_DIR="${ARCHIVE_DIR:-$APP_DIR/file/archive}"

LOG_FILE="${AR_LOG_FILE:-remove_archives.log}"
APPRISE_CMD="${AR_APPRISE_CMD:-apprise -t 'OGC-CloudServer Archives Removal Failure' -b}"
FILE_RETENTION_DAYS="${FILE_RETENTION_DAYS:-1}"

# Function to log messages
log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $1" >> ${TMP_DIR}/${LOG_FILE}
}

# Function to send failure notifications
send_notification() {
    local message="$1"
#    $APPRISE_CMD "$message"
}

# Cleanup function to remove lock file and send notifications on exit
cleanup() {
    rm -f "$LOCK_FILE"
}

# Trap signals and call cleanup function on script termination
trap 'cleanup; exit' INT TERM EXIT

mkdir -p "$TMP_DIR"
LOCK_FILE="$TMP_DIR/$(basename "$0").lock"

# Check if the lock file exists
if [ -f "$LOCK_FILE" ]; then
    log "ERROR: Script is already running."
    exit 0
fi

# Create lock file
touch "$LOCK_FILE"

# Start logging
log "Starting archive cleanup process."

# Get the current date in seconds since the epoch
current_date=$(date +%s)

# Error handling: Wrap file cleanup process in a subshell
(
    # Iterate over each file in the archive directory (including subdirectories)
    find "$ARCHIVE_DIR" -type f | while read -r file; do
        # Get the file's modification time in seconds since the epoch
        file_modification_date=$(stat -c %Y "$file")

        # Calculate the file's age in days
        file_age=$(( (current_date - file_modification_date) / 86400 ))

        # Remove the file if it's older than the retention period
        if [ "$file_age" -gt "$FILE_RETENTION_DAYS" ]; then
            log "Removing file: $file (Age: $file_age days)"
            rm -f "$file" || log "Error removing file: $file"
        fi
    done

    # Next, remove empty directories
    find "$ARCHIVE_DIR" -type d -empty -depth | while read -r dir; do
        log "Removing empty directory: $dir"
        rmdir "$dir" || log "Error removing directory: $dir"
    done
) || {
    # Error during cleanup process
    log "Error during archive cleanup process."
    send_notification "Archive cleanup process encountered an error. Check logs for details."
    rm -f "$LOCK_FILE"  # Remove lock file
    exit 1
}

# Successful completion
log "Archive cleanup process completed successfully."
