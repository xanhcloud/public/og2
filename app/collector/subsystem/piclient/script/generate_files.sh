#!/bin/bash

# Load values for variables from the environment file
SCRIPT_DIR=$(dirname "$(realpath "$0")")
source "$SCRIPT_DIR/.env"

# Define paths
APP_DIR="${APP_DIR:-~/apps/ovalgrowth/runtime/og-collector/piclient}"
INBOX_DIR="${INBOX_DIR:-$APP_DIR/file/inbox}"
OUTBOX_DIR="${OUTBOX_DIR:-$APP_DIR/file/outbox}"
TMP_DIR="${TMP_DIR:-$APP_DIR/log/tmp}"

LOG_FILE="${FG_LOG_FILE:-generate_files.log}"
LOCK_FILE=${TMP_DIR}/generate_files.lock

# The Lot configuration file
CONFIG_FILE="${CONFIG_FILE:-00_lot_all.conf}" # Use default if not provided

# Global variables for number of actual sensors
# Those variables will be calculated later based on the Lot config file
NUMBER_EC=0
NUMBER_TEMP=0
NUMBER_HUMIDITY=0
NUMBER_NH3=0
NUMBER_CO2=0
NUMBER_FEEDER=0

##
## Utilities
##

# Function to log messages
log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $1" >> "$TMP_DIR/$LOG_FILE"
}

cleanup() {
    log "Cleanup completed."
}


# Function to calculate the actual number of sensors from range
calculate_number_sensors() {
  # Function to count items for a given range
  count_items_in_range() {
    local range=$1
    local count=0

    for item in $range; do
      if [[ $item == *-* ]]; then
        # Extract start and end values
        start=${item%-*}
        end=${item#*-}
        count=$((count + end - start + 1))
      else
        # Single item
        count=$((count + 1))
      fi
    done

    echo $count
  }

  # Calculate counts for each category
  NUMBER_EC=$(count_items_in_range "$EC_RANGE")
  NUMBER_TEMP=$(count_items_in_range "$TEMP_RANGE")
  NUMBER_HUMIDITY=$(count_items_in_range "$HUMIDITY_RANGE")
  NUMBER_NH3=$(count_items_in_range "$NH3_RANGE")
  NUMBER_CO2=$(count_items_in_range "$CO2_RANGE")
  NUMBER_FEEDER=$(count_items_in_range "$FEEDER_RANGE")
}

# Function to generate random values for EC, TEMP, NH3, CO2, FS, FE, F, and W
generate_random_data() {
  local min=$1
  local max=$2
  echo $((RANDOM % (max - min + 1) + min))
}

generate_float_data() {
  local min=$1
  local max=$2
  echo "$(awk "BEGIN { printf \"%.2f\", $min + ($RANDOM / 32767) * ($max - $min) }")"
}

generate_temperature_data() {
  local min=$1
  local max=$2
  echo "$(awk "BEGIN { printf \"%.1f\", $min + ($RANDOM / 32767) * ($max - $min) }")"
}

generate_time() {
  # Generate a random time
  echo "$(date -d "@$((RANDOM % 86400))" +"%H:%M")"
}

generate_end_time() {
  local start_time=$1
  local start_seconds=$(date -d "$start_time" +"%s")
  local random_minutes=$((30 + RANDOM % 31))  # Random minutes between 30 and 60
  local end_seconds=$((start_seconds + random_minutes * 60))
  echo "$(date -d "@$end_seconds" +"%H:%M")"
}

## Functions to generate file content

# Generate random data - EC section
generate_ec_section() {
  local filename=$1
  local no_error=$2

  # Generate the full list of EC sensors
  items=()
  for range in $EC_RANGE; do
    start=${range%-*}
    end=${range#*-}
    for i in $(seq "$start" "$end"); do
      items+=("EC_$i")
    done
  done

  # Ensure NUMBER_EC is valid
  if (( NUMBER_EC > ${#items[@]} )); then
    echo "Error: NUMBER_EC cannot be greater than the total number of items (${#items[@]})"
    exit 1
  fi

  # Select NUMBER_EC random active items
  active_items=($(shuf -e "${items[@]}" | head -n "$NUMBER_EC"))

  # Declare an associative array to store results
  declare -A results

  # Initialize all active items in results array with random values
  for item in "${active_items[@]}"; do
    results["$item"]=$(generate_random_data 300 600)  # Default random value
  done

  # If this file contains error sensors (no_error != 0)
  if [ "$no_error" -ne 0 ]; then
    # Determine the number of items to assign -9999 (1/4 of active items)
    count_fixed=$((NUMBER_EC / 4))

    # Shuffle and pick the first `count_fixed` items to assign -9999
    fixed_items=($(shuf -e "${active_items[@]}" | head -n "$count_fixed"))
    for item in "${fixed_items[@]}"; do
      results["$item"]=-9999  # Assign -9999 for error items
    done
  fi

  # Open the file for appending and write results
  {
    for item in "${items[@]}"; do
      # Sort the EC items by order
      if [[ -n "${results[$item]}" ]]; then
        echo "    \"$item\": ${results[$item]},"
      fi
    done
  } >> "$filename"
}

# Generate random data - TEMP section
generate_temp_section() {
  local filename=$1
  local no_error=$2

  broken_sensors=()
  if [ "$no_error" -ne 0 ]; then
    # Generate random unique indices for the error value -9999 (up to 1/4 of actual sensors)
    for _ in $(seq 1 $((RANDOM % 2 + ${NUMBER_TEMP} / 4))); do
      while true; do
        random_group=$((RANDOM % 3 + 1))0$((RANDOM % 3 + 1))  # Generate a random group, e.g., 101, 102, etc.
        if [[ ! " ${broken_sensors[@]} " =~ " ${random_group} " ]]; then
          broken_sensors+=($random_group)
          break
        fi
      done
    done
  fi

  # Open the file for appending
  {
    # Define the sensor sets you want to generate (e.g., 101-103, 201-203, etc.)
    for group in $TEMP_RANGE; do
      if [[ " ${broken_sensors[@]} " =~ " $group " ]]; then
        # Assign fixed value -9999 for broken sensor groups
        echo "    \"TEMP_$group\": -9999,"
        echo "    \"TEMP_HI_$group\": -9999,"
        echo "    \"TEMP_LO_$group\": -9999,"
      else
        # Use the generate_temperature_data function for normal sensor groups
        echo "    \"TEMP_$group\": $(generate_temperature_data 30 33),"
        echo "    \"TEMP_HI_$group\": $(generate_temperature_data 34 38),"
        echo "    \"TEMP_LO_$group\": $(generate_temperature_data 25 29),"
      fi
    done
  } >> "$filename"
}

# Generate random data - HUMIDITY section
generate_humidity_section() {
  local filename=$1
  local no_error=$2

  broken_sensors=()

  if [ "$no_error" -ne 0 ]; then
    # Generate random unique indices for the error value -9999 (up to 1/4 of actual sensors)
    for _ in $(seq 1 $((RANDOM % 2 + ${NUMBER_HUMIDITY} / 4))); do
      while true; do
        random_group=$((RANDOM % 3 + 1))0$((RANDOM % 3 + 1))  # Generate a random group, e.g., 101, 102, etc.
        if [[ ! " ${broken_sensors[@]} " =~ " ${random_group} " ]]; then
          broken_sensors+=($random_group)
          break
        fi
      done
    done
  fi

  # Open the file for appending
  {
    # Define the sensor sets you want to generate (e.g., 101-103, 201-203, etc.)
    for group in $HUMIDITY_RANGE; do
      if [[ " ${broken_sensors[@]} " =~ " $group " ]]; then
        # Assign fixed value -9999 for broken sensor groups
        echo "    \"HUMIDITY_$group\": -9999,"
        echo "    \"HUMIDITY_HI_$group\": -9999,"
        echo "    \"HUMIDITY_LO_$group\": -9999,"
      else
        # Use the random data for normal sensor groups
        echo "    \"HUMIDITY_$group\": $(generate_float_data 60 69),"
        echo "    \"HUMIDITY_HI_$group\": $(generate_float_data 70 99),"
        echo "    \"HUMIDITY_LO_$group\": $(generate_float_data 50 59),"
      fi
    done
  } >> "$filename"
}

# Generate random data - OUTER TEMP section
generate_outer_temp_section() {
  local filename=$1
  local no_error=$2

  # Open the file for appending
  {
  if [ "$no_error" -ne 0 ]; then
    # Use the error value
      echo "    \"OUTER_TEMP\": -9999,"
      echo "    \"OUTER_TEMP_HI\": -9999,"
      echo "    \"OUTER_TEMP_LO\": -9999,"
  else
    # Use the generate_random_data function for each OUTER_TEMP_XXX entry
      echo "    \"OUTER_TEMP\": $(generate_temperature_data 30 33),"
      echo "    \"OUTER_TEMP_HI\": $(generate_temperature_data 34 38),"
      echo "    \"OUTER_TEMP_LO\": $(generate_temperature_data 25 29),"
  fi
  } >> "$filename"
}

# Generate random data - NH3 section
generate_nh3_section() {
  local filename=$1
  local no_error=$2

  broken_sensors=()
  if [ "$no_error" -ne 0 ]; then
    # Generate random unique indices for the error value -9999 (up to 1/4 of actual sensors)
    for _ in $(seq 1 $((RANDOM % 2 + ${NUMBER_NH3} / 4))); do
      while true; do
        random_group=$((RANDOM % 3 + 1))0$((RANDOM % 3 + 1))  # Generate a random group, e.g., 101, 102, etc.
        if [[ ! " ${broken_sensors[@]} " =~ " ${random_group} " ]]; then
          broken_sensors+=($random_group)
          break
        fi
      done
    done
  fi

  # Open the file for appending
  {
    for group in $NH3_RANGE; do
      if [[ " ${broken_sensors[@]} " =~ " $group " ]]; then
        # Assign fixed value -9999 for broken sensor groups
        echo "    \"NH3_$group\": -9999,"
        echo "    \"NH3_HI_$group\": -9999,"
        echo "    \"NH3_LO_$group\": -9999,"
      else
        # Use the random data for normal sensor groups
        echo "    \"NH3_$group\": $(generate_random_data 210 240),"
        echo "    \"NH3_HI_$group\": $(generate_random_data 250 400),"
        echo "    \"NH3_LO_$group\": $(generate_random_data 20 200),"
      fi
    done
  } >> "$filename"
}

# Generate random data - CO2 section
generate_co2_section() {
  local filename=$1
  local no_error=$2

  broken_sensors=()
  if [ "$no_error" -ne 0 ]; then
    # Generate random unique indices for the error value -9999 (up to 1/4 of actual sensors)
    for _ in $(seq 1 $((RANDOM % 2 + ${NUMBER_CO2} / 4))); do
      while true; do
        random_group=$((RANDOM % 3 + 1))0$((RANDOM % 3 + 1))  # Generate a random group, e.g., 101, 102, etc.
        if [[ ! " ${broken_sensors[@]} " =~ " ${random_group} " ]]; then
          broken_sensors+=($random_group)
          break
        fi
      done
    done
  fi

  # Open the file for appending
  {
    # Define the sensor sets want to generate (e.g., 101-103, 201-203, etc.)
    for group in $CO2_RANGE; do
      if [[ " ${broken_sensors[@]} " =~ " $group " ]]; then
        # Assign fixed value -9999 for broken sensor groups
        echo "    \"CO2_$group\": -9999,"
        echo "    \"CO2_HI_$group\": -9999,"
        echo "    \"CO2_LO_$group\": -9999,"
      else
        # Use the random data for normal sensor groups
        echo "    \"CO2_$group\": $(generate_random_data 210 240),"
        echo "    \"CO2_HI_$group\": $(generate_random_data 250 400),"
        echo "    \"CO2_LO_$group\": $(generate_random_data 20 200),"
      fi
    done
  } >> "$filename"
}

# Generate random data - Feeder section
generate_feeder_section() {
  local filename=$1
  local no_error=$2

  local start
  local end

  broken_sensors=()
  if [ "$no_error" -ne 0 ]; then
    # Generate random unique indices for the error value -9999 (up to 1/4 of actual sensors)
    for _ in $(seq 1 $((RANDOM % 2 + ${NUMBER_FEEDER} / 4))); do
      while true; do
        random_group=$((RANDOM % 3 + 1))0$((RANDOM % 3 + 1))  # Generate a random group, e.g., 101, 102, etc.
        if [[ ! " ${broken_sensors[@]} " =~ " ${random_group} " ]]; then
          broken_sensors+=($random_group)
          break
        fi
      done
    done
  fi

  # Open the file for appending
  {
    for group in $FEEDER_RANGE; do
      if [[ " ${broken_sensors[@]} " =~ " $group " ]]; then
        # Assign fixed value -9999 for broken sensor groups
        echo "    \"FS_$group\": -9999,"
        echo "    \"FE_$group\": -9999,"
      else
        # Use the random data for normal sensor groups
        start=$(generate_time)
        end=$(generate_end_time "$start")
        echo "    \"FS_$group\": \"$start\","
        echo "    \"FE_$group\": \"$end\","
      fi
    done
  } >> "$filename"
}

# Generate random data - Food & Water section
generate_food_water_section() {
  local filename=$1
  local no_error=$2

  # Open the file for appending
  {
  if [ "$no_error" -ne 0 ]; then
    # Use the error value
    echo "    \"F_001\": $(generate_float_data 200 400),"
    echo "    \"F_002\": -9999,"
    echo "    \"W_001\": $(generate_random_data 10 30),"
    echo "    \"W_002\": -9999"
  else
    # Use the generate_random_data function for each CO2_XXX entry
    echo "    \"F_001\": $(generate_float_data 200 400),"
    echo "    \"F_002\": $(generate_float_data 200 400),"
    echo "    \"W_001\": $(generate_random_data 10 30),"
    echo "    \"W_002\": $(generate_random_data 10 30)"
  fi
  } >> "$filename"
}

## Generate file
# Get current date and time
current_date=$(date +"%Y-%m-%d")

# Generate an error file (.err)
generate_error_file() {
  local input_file=$1

  # Filename
  output_file="${input_file%.tmp}.err.tmp"

  # Extract metadata and format it correctly in the output file
  metadata=$(grep -E '{|"PF_CODE":|"H_CODE":|"LOT_ID":|"DATE":|"TIME":' "$input_file")
  echo "$metadata" > "$output_file"

  # Process keys with value -9999 and assign error values based on patterns
  grep '": -9999' "$input_file" | sed -E 's/^ *"([^"]+)": -9999.*/\1/' | while read -r key; do
      # Determine the error value based on the key pattern
      case "$key" in
          EC_*) value="\"EGG_CNT_ERR\"" ;;
          TEMP*|OUTER_TEMP*) value="\"TEMP_ERR\"" ;;
          HUMIDITY_*) value="\"HUMIDITY_ERR\"" ;;
          NH3_*) value="\"AMMONIA_ERR\"" ;;
          CO2_*) value="\"CO2_ERROR\"" ;;
          FS_*|FE_*) value="\"FEEDER_ERR\"" ;;
          F_*) value="\"FOODER_ERR\"" ;;
          W_*) value="\"WATER_ERR\"" ;;
          *) value="\"UNKNOWN_ERR\"" ;;  # Default for unhandled patterns
      esac

      # Write the key-value pair to the output file in reversed order
      echo "    $value: \"$key\"," >> "$output_file"
  done

  # Remove the trailing comma from the last key-value entry
  sed -i '$ s/,$//' "$output_file"

  # Close the JSON object
  echo "}" >> "$output_file"
}

# Generate JSON file with different House and Lot IDs
generate_file() {
  local house=$1
  local lot=$2
  local has_error=$3
  local start_house

  current_time=$(date +"%H:%M:%S")
  timestamp=$(date +%s)

  start_house=$((house + START_HOUSE))

  # Pad house and lot numbers with zeros
  padded_lot=$(printf "%03d" "$lot")
  padded_house=$(printf "%03d" "$start_house")

  # Generate a single data file in the background
  (
  # Filename
  filename="$INBOX_DIR/${timestamp}_${padded_house}_${padded_lot}.tmp"

  # Open the file for adding
  cat > "$filename" << EOF
{
    "PF_CODE": "PF001",
    "H_CODE": $house,
    "LOT_ID": $lot,
    "DATE": "$current_date",
    "TIME": "$current_time",
EOF


  # Add data sections
  generate_ec_section "$filename" "$has_error"
  generate_temp_section "$filename" "$has_error"
  generate_humidity_section "$filename" "$has_error"
  generate_outer_temp_section "$filename" "$has_error"
  generate_nh3_section "$filename" "$has_error"
  generate_co2_section "$filename" "$has_error"
  generate_feeder_section "$filename" "$has_error"
  generate_food_water_section "$filename" "$has_error"

  # Remove the trailing comma from the last key-value entry
  sed -i '$ s/,$//' "$filename"

  # Close the JSON object
  echo "}" >> "$filename"

  if [ "$has_error" -ne 0 ]; then
    # Generate a single error file
    generate_error_file "$filename"
  fi

  ) &
}


##
## The main program
##

# Handle cleanup on exit and error
trap cleanup EXIT SIGHUP SIGINT SIGTERM ERR

## Notes: Use a .pif file and flock to make sure there is only one instance of this script running
PIDFILE="$TMP_DIR/$(basename "$0").pid"
LOCK_FILE="$TMP_DIR/$(basename "$0").lock"

# Create a lock using flock
(
    flock -n 200 || { log "Another transfer is in progress, exiting"; exit 0; }

    if [ -e "$PIDFILE" ] && kill -0 "$(cat "$PIDFILE")"; then
        log "Another instance of the script is running, exiting."
        exit 1
    fi

    # Create the PID file
    echo $$ > "$PIDFILE"

    # Ensure the PID file is removed when the script exits
    trap 'rm -f "$PIDFILE"' EXIT

    log "Starting file generation process."

# Check if the Lot config file exists
if [[ -f "$SCRIPT_DIR/$CONFIG_FILE" ]]; then
  source "$SCRIPT_DIR/$CONFIG_FILE"
else
  echo "Configuration file $CONFIG_FILE not found. Exiting."
  exit 1
fi


## Calculate the actual number of sensors
calculate_number_sensors

# Convert the LOT_HOUSE string into an array
IFS=',' read -r -a lot_house_array <<< "$LOT_HOUSE"

# Count the number of elements in the array
NUMBER_LOT=${#lot_house_array[@]}

# Check if variables are properly set
if [ -z "$NUMBER_LOT" ] || [ "$NUMBER_LOT" -eq 0 ]; then
  echo "Error: NUMBER_LOT must be set and non-zero."
  exit 1
fi

##
## Create all files
##

# Calculate total number of iterations
TOTAL_ITERATIONS="$NUMBER_LOT"

# Generate unique random error positions
error_positions=()
while [ ${#error_positions[@]} -lt "$NUMBER_ERROR" ]; do
  random_position=$((RANDOM % TOTAL_ITERATIONS + 1))
  if [[ ! " ${error_positions[@]} " =~ " $random_position " ]]; then
    error_positions+=("$random_position")
  fi
done

# Helper function to check if current iteration is an error position
is_error_position() {
  local pos=$1
  for err_pos in "${error_positions[@]}"; do
    if [ "$err_pos" -eq "$pos" ]; then
      return 0  # Found a match
    fi
  done
  return 1  # No match
}

# Counter for current iteration
current_iteration=0

# Nested loops to generate files
for n_lot in $(seq 1 "$NUMBER_LOT"); do
  # Get the appropriate n_house from LOT_HOUSE array
  house_id=$(echo "${lot_house_array[$((n_lot-1))]}" | cut -d'-' -f2)
  lot_id=$(echo "${lot_house_array[$((n_lot-1))]}" | cut -d'-' -f1)

  current_iteration=$((current_iteration + 1))

  # Check if the current iteration is one of the error positions
  if is_error_position "$current_iteration"; then
    # Call generate_file with 1 for error cases
    generate_file "$house_id" "$lot_id" 1
  else
    # Call generate_file with 0 for normal cases
    generate_file "$house_id" "$lot_id" 0
  fi
done


# Wait for all background tasks to finish
wait

## Move files to Outbox, ready for transferring
# Check if there are any files to move
data_files=$(find "$INBOX_DIR" -maxdepth 1 -type f -name "*.tmp")
if [[ -z "$data_files" ]]; then
   echo "No .tmp files found in $INBOX_DIR"
else
   # Move .tmp files to OUTBOX
   if ! find "$INBOX_DIR" -maxdepth 1 -type f -name "*.tmp" -exec mv {} "$OUTBOX_DIR" \;; then
       log "Error while moving files from $INBOX_DIR to $OUTBOX_DIR"
   fi
fi

# Rename error files from .tmp to .err in OUTBOX
if ! find "$OUTBOX_DIR" -maxdepth 1 -type f -name "*.err.tmp" -exec bash -c 'mv "$1" "${1%.err.tmp}.err"' _ {} \;; then
   log "Error renaming error files in $OUTBOX_DIR"
fi

# Rename data files from .tmp to .json in OUTBOX
if ! find "$OUTBOX_DIR" -maxdepth 1 -type f -name "*.tmp" -exec bash -c 'mv "$1" "${1%.tmp}.json"' _ {} \;; then
   log "Error renaming data files in $OUTBOX_DIR"
fi


) 200>"$LOCK_FILE"
