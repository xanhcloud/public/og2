#!/bin/bash

# Enable strict error handling
set -euo pipefail

# Load values for variables from the environment file
readonly SCRIPT_DIR=$(dirname "$(realpath "$0")")
source "${SCRIPT_DIR}/.env"

# Define paths
readonly TMP_DIR="${TMP_DIR:-$APP_DIR/log/tmp}"

# Validate critical environment variables
if [[ -z "${TMP_DIR}" ]]; then
  echo "Error: Required environment variables are not set."
  exit 1
fi

# Create TMP_DIR if it doesn't exist
mkdir -p "${TMP_DIR}"

# All interval times are defined in seconds
readonly INTERVAL_GENERATE_FILES="${INTERVAL_GENERATE_FILES:-5}"
readonly INTERVAL_TRANFER_FILES="${INTERVAL_TRANFER_FILES:-30}"

# Log file
readonly LOG_FILE="${PC_LOG_FILE:-ogc-piclient.log}"

# Notification command
readonly APPRISE_CMD="${PC_APPRISE_CMD:-apprise -t 'OGC-PiClient Failure' -b}"

# Function to log messages
log() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1" >> ${TMP_DIR}/${LOG_FILE}
}

# Function to send failure notifications
send_failure_notification() {
    local message="$1"
#    ${APPRISE_CMD} "$message"
}

# Function to handle SIGTERM
cleanup() {
  log "Stopping wrapper script gracefully."
  exit 0
}

# Trap SIGTERM
trap cleanup SIGTERM

# Initialize a counter
counter=1

while true; do
  # Run this first script every INTERVAL_GENERATE_FILES seconds
  if (( counter % ${INTERVAL_GENERATE_FILES} == 0 )); then
    log "Running generate_files.sh..."
    "${SCRIPT_DIR}/generate_files.sh"
    if [ $? -ne 0 ]; then
      log "Script generate_files.sh failed with exit code $?."
      send_failure_notification "Script generate_files.sh failed with exit code $?."
      exit 1
    fi
  fi

  # Run this script every INTERVAL_TRANFER_FILES seconds
  if (( counter % ${INTERVAL_TRANFER_FILES} == 0 )); then
    log "Running transfer_files.sh..."
     "${SCRIPT_DIR}/transfer_files.sh"
    if [ $? -ne 0 ]; then
      log "Script transfer_files.sh failed with exit code $?."
      send_failure_notification "Script transfer_files.sh failed with exit code $?."
      exit 1
    fi
  fi

  # Increment the counter and sleep for 1 second
  ((counter++))
  sleep 1

  # Reset the counter after 1 day (86400 seconds) or LCM of intervals
  if (( counter > 86400 )); then
    counter=1
  fi
done
