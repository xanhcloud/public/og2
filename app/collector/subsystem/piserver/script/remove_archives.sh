#!/bin/bash

# Load values for variables from the environment file
SCRIPT_DIR=$(dirname "$(realpath "$0")")
source "$SCRIPT_DIR/.env"

# Environment variables for configuration
APP_DIR="${APP_DIR:-~/apps/ovalgrowth/runtime/og-collector/piserver}"
ARCHIVE_DIR="${ARCHIVE_DIR:-$APP_DIR/file/archive}"
TMP_DIR="${TMP_DIR:-$APP_DIR/log/tmp}"

FILE_RETENTION_DAYS="${FILE_RETENTION_DAYS:-1}"      # Default to 2 days if not set

LOG_FILE="${AR_LOG_FILE:-archive_cleanup.log}"
APPRISE_URL="${AR_APPRISE_URL:-https://apprise.saigondeveloper.com}"  # Apprise server URL

# Lock file to prevent multiple script instances
mkdir -p "$TMP_DIR"
LOCK_FILE="$TMP_DIR/$(basename "$0").lock"

# Function to log messages with timestamps
log_message() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1" >> ${TMP_DIR}/${LOG_FILE}
}

# Function to send notifications via Apprise
send_notification() {
    local message="$1"
#    curl -s -X POST "$APPRISE_URL" -d "message=$message" -d "title=Archive Cleanup Notification" || log_message "Failed to send notification"
}

# Cleanup function to remove lock file and send notifications on exit
cleanup() {
    rm -f "$LOCK_FILE"
}

# Trap signals and call cleanup function on script termination
trap 'cleanup; exit' INT TERM EXIT

# Check if the lock file exists
if [ -f "$LOCK_FILE" ]; then
    log_message "ERROR: Script is already running."
    exit 0
fi

# Create the lock file
touch "$LOCK_FILE"

# Start the cleanup process in a subshell for error handling
(
    # Get the current date in seconds since epoch
    current_date=$(date +%s)

    # Check if the archive directory exists
    if [ ! -d "$ARCHIVE_DIR" ]; then
        log_message "ERROR: Archive directory '$ARCHIVE_DIR' does not exist."
        send_notification "ERROR: Archive directory '$ARCHIVE_DIR' does not exist."
        exit 1
    fi

    # Find and iterate over each subdirectory (YYYYMMDD format)
    for date_dir in "$ARCHIVE_DIR"/*; do
        # Check if the item is a directory
        if [ -d "$date_dir" ]; then
            # Extract the date from the directory name
            dir_date=$(basename "$date_dir")

            # Validate directory name format (YYYYMMDD)
            if ! [[ "$dir_date" =~ ^[0-9]{8}$ ]]; then
                log_message "WARNING: Skipping '$date_dir' - invalid date format."
                continue
            fi

            # Convert the directory date to seconds since epoch
            dir_timestamp=$(date -d "$dir_date" +%s 2>/dev/null)

            # Check for date conversion errors
            if [ $? -ne 0 ]; then
                log_message "ERROR: Failed to convert directory name '$dir_date' to timestamp."
                continue
            fi

            # Calculate the age of the directory (in days)
            age=$(( (current_date - dir_timestamp) / 86400 ))

            # Check if the age is greater than the retention days
            if [ "$age" -gt "$FILE_RETENTION_DAYS" ]; then
                log_message "Removing directory: $date_dir"
                rm -rf "$date_dir"  # Remove the entire directory and its contents
                if [ $? -eq 0 ]; then
                    log_message "Successfully removed directory: $date_dir"
                else
                    log_message "ERROR: Failed to remove directory: $date_dir"
                    send_notification "ERROR: Failed to remove directory: $date_dir"
                fi
            fi
        else
            log_message "WARNING: '$date_dir' is not a directory, skipping."
        fi
    done

    log_message "Cleanup completed."

) || {
    log_message "ERROR: An error occurred during the cleanup process."
    send_notification "ERROR: An error occurred during the archive cleanup process."
    exit 1
}

# The script will automatically remove the lock file when it exits
