#!/bin/bash

# Exit on error
set -e

# Load values for variables from the environment file
SCRIPT_DIR=$(dirname "$(realpath "$0")")
source "$SCRIPT_DIR/.env/app_upgrade.env"

# Check if required variables are set
if [ -z "$GITLAB_TOKEN" ] || [ -z "$GITLAB_REGISTRY" ] || [ -z "$SERVER_LIST" ] || [ -z "$SERVER_USER" ]; then
  echo "Error: Required environment variables are missing."
  exit 1
fi

# Check if SSH key exists
SSH_KEY="$SCRIPT_DIR/.ssh/ssh_key.pem"
if [ ! -f "$SSH_KEY" ]; then
  echo "Error: SSH key file not found at $SSH_KEY."
  exit 1
fi

# Usage function to display help
usage() {
  echo "Usage: $0 {og-app|og-backend|og-frontend|og-collector}"
  exit 1
}

# Check if a parameter was provided
if [ -z "$1" ]; then
  echo "Error: No service/component specified."
  usage
fi

# Define service based on input parameter
service=$1

# Define the compose file path
COMPOSE_FILE="~/apps/ovalgrowth/app/og-app/compose/ovalgrowth.yml"

# Processing blocks for each service/component
case "$service" in
  og-app)
    echo "Upgrading the entire app stack."

    for server in $(echo "$SERVER_LIST" | tr ',' ' '); do
      echo "Upgrading the entire app stack on $server..."

      # Log in to the GitLab Container Registry
      ssh -i "$SSH_KEY" -o StrictHostKeyChecking=no "$SERVER_USER@$server" \
        "echo -n '$GITLAB_TOKEN' | podman login -u gitlab+deploy-token-7010228 --password-stdin $GITLAB_REGISTRY"

      # Pull the latest images and restart the stack using podman-compose
      ssh -i "$SSH_KEY" -o StrictHostKeyChecking=no "$SERVER_USER@$server" \
        "/usr/bin/podman-compose -f $COMPOSE_FILE up -d --always-pull"

      echo "Upgrade completed for the entire app stack on $server."
    done

    echo "All servers have been upgraded for the entire app stack."
    exit 0
    ;;

  og-frontend)
    IMAGE="$FRONTEND_IMAGE"
    SERVICE_NAME="ovalgrowth-frontend"
    ;;

  og-backend)
    IMAGE="$BACKEND_IMAGE"
    SERVICE_NAME="ovalgrowth-backend"
    ;;

  og-collector)
    IMAGE="$COLLECTOR_IMAGE"
    SERVICE_NAME="ovalgrowth-collector"
    ;;

  *)
    echo "Error: Invalid service '$service'."
    usage
    ;;
esac

# Upgrade the specified service on all servers
for server in $(echo "$SERVER_LIST" | tr ',' ' '); do
  echo "Upgrading $service on $server..."

  # Log in to the GitLab Container Registry
  ssh -i "$SSH_KEY" -o StrictHostKeyChecking=no "$SERVER_USER@$server" \
    "echo -n '$GITLAB_TOKEN' | podman login -u gitlab+deploy-token-7010228 --password-stdin $GITLAB_REGISTRY"

  # Pull the latest image
  ssh -i "$SSH_KEY" -o StrictHostKeyChecking=no "$SERVER_USER@$server" \
    "podman pull $GITLAB_REGISTRY/$IMAGE"

  # Restart the service using podman-compose
  ssh -i "$SSH_KEY" -o StrictHostKeyChecking=no "$SERVER_USER@$server" \
    "/usr/bin/podman-compose -f $COMPOSE_FILE up -d --no-deps $SERVICE_NAME"

  echo "Upgrade completed for $service on $server."
done

echo "All servers have been upgraded for $service."
