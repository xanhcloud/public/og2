#!/bin/bash

# Exit on error
set -e

# Usage function to display help
usage() {
  echo "Usage: $0 {provision|config|install|upgrade} {piclient|piserver|cloudserver|aws} [component]"
  echo "Examples:"
  echo "  $0 provision aws"
  echo "  $0 provision piclient"
  echo "  $0 config piserver"
  echo "  $0 install cloudserver"
  echo "  $0 upgrade piclient og-backend"
  exit 1
}

# Check if the minimum number of arguments is provided
if [ $# -lt 2 ]; then
  echo "Error: Insufficient arguments."
  usage
fi

# Parse arguments
TASK=$1
SUBSYSTEM=$2
COMPONENT=$3

# Define the base directory for scripts
SCRIPT_DIR=$(dirname "$(realpath "$0")")

# Function to call the appropriate script
call_script() {
  local script_name=$1
  local script_path="$SCRIPT_DIR/$script_name"

  if [ ! -f "$script_path" ]; then
    echo "Error: Script '$script_name' not found."
    exit 1
  fi

  echo "Running $script_name..."
  bash "$script_path"
}

# Function to call the component-specific upgrade script
call_upgrade_script() {
  local subsystem=$1
  local component=$2
  local script_name="app_upgrade_${subsystem}.sh"
  local script_path="$SCRIPT_DIR/$script_name"

  if [ ! -f "$script_path" ]; then
    echo "Error: Script '$script_name' not found."
    exit 1
  fi

  echo "Running $script_name for component $component..."
  bash "$script_path" "$component"
}

# Main logic
case "$TASK" in
  provision)
    case "$SUBSYSTEM" in
      piclient)
        call_script "system_provision_vps_piclient.sh"
        ;;
      piserver)
        call_script "system_provision_vps_piserver.sh"
        ;;
      cloudserver)
        call_script "system_provision_vps_cloudserver.sh"
        ;;
      aws)
        call_script "system_provision_aws_cloudserver.sh"
        ;;
      *)
        echo "Error: Invalid subsystem '$SUBSYSTEM' for task 'provision'."
        usage
        ;;
    esac
    ;;

  config)
    case "$SUBSYSTEM" in
      piclient)
        call_script "system_config_vps_piclient.sh"
        ;;
      piserver)
        call_script "system_config_vps_piserver.sh"
        ;;
      cloudserver)
        call_script "system_config_vps_cloudserver.sh"
        ;;
      aws)
        call_script "system_config_aws_cloudserver.sh"
        ;;
      *)
        echo "Error: Invalid subsystem '$SUBSYSTEM' for task 'config'."
        usage
        ;;
    esac
    ;;

  install)
    case "$SUBSYSTEM" in
      piclient)
        call_script "app_install_piclient.sh"
        ;;
      piserver)
        call_script "app_install_piserver.sh"
        ;;
      cloudserver)
        call_script "app_install_cloudserver.sh"
        ;;
      *)
        echo "Error: Invalid subsystem '$SUBSYSTEM' for task 'install'."
        usage
        ;;
    esac
    ;;

  upgrade)
    if [ -z "$COMPONENT" ]; then
      echo "Error: Component not specified for task 'upgrade'."
      usage
    fi

    case "$SUBSYSTEM" in
      piclient)
        call_upgrade_script "piclient" "$COMPONENT"
        ;;
      piserver)
        call_upgrade_script "piserver" "$COMPONENT"
        ;;
      cloudserver)
        call_upgrade_script "cloudserver" "$COMPONENT"
        ;;
      *)
        echo "Error: Invalid subsystem '$SUBSYSTEM' for task 'upgrade'."
        usage
        ;;
    esac
    ;;

  *)
    echo "Error: Invalid task '$TASK'."
    usage
    ;;
esac

echo "Task '$TASK' completed for subsystem '$SUBSYSTEM'."
