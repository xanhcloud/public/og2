# OvalGrowth Frontend

## Getting started

```
git clone https://gitlab.com/kohshin/ovalgrowth-frontend.git
npm install
```

## Config

```
run backend

copy src/config/app.config.ts.sample to src/config/app.config.ts

config backend url to app.config.ts
config websocket url to app.config.ts
```

## run

```
npm run dev
```
