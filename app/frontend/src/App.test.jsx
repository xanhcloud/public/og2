import { useEffect, useState } from 'react';
import { Route, Routes, useLocation } from 'react-router-dom';

import Loader from './common/Loader';
import PageTitle from './components/PageTitle';
import SignIn from './pages/Authentication/SignIn';
import SignUp from './pages/Authentication/SignUp';
import Calendar from './pages/Calendar';
import Chart from './pages/Chart';
import ECommerce from './pages/Dashboard/ECommerce';
import FormElements from './pages/Form/FormElements';
import FormLayout from './pages/Form/FormLayout';
import Profile from './pages/Profile';
import Settings from './pages/Settings';
import Tables from './pages/Tables';
import Alerts from './pages/UiElements/Alerts';
import Buttons from './pages/UiElements/Buttons';
import UserList from './pages/User/UserList';
import DailyList from './pages/Daily/List.tsx';
import DefaultLayout from './layout/DefaultLayout.tsx';
import WeeklyList from './pages/Weekly/List.tsx';
import DailyComparison from './pages/Daily/Comparison.tsx';
import DailyDatePeriod from './pages/Daily/DateRange.tsx';
import WeeklyDatePeriod from './pages/Weekly/DateRange.tsx';
import WeeklyComparison from './pages/Weekly/Comparison.tsx';

function App() {
  const [loading, setLoading] = useState<boolean>(true);
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  useEffect(() => {
    setTimeout(() => setLoading(false), 1000);
  }, []);

  return loading ? (
    <Loader />
  ) : (
    <>
      <Routes>
        <Route
          index
          element={
            <>
              <PageTitle title="Dashboard" />
              <ECommerce />
            </>
          }
        />
        <Route
          path="/daily/list"
          element={
            <>
              <PageTitle title="Daily" />
              <DailyList />
            </>
          }
        />
        <Route
          path="/daily/comparison"
          element={
            <>
              <PageTitle title="Past Data Comparison" />
              <DailyComparison/>
            </>
          }
        />
        <Route
          path="/daily/date-range"
          element={
            <>
              <PageTitle title="Custom Date Range" />
              <DailyDatePeriod/>
            </>
          }
        />
        <Route
          path="/daily/setting"
          element={
            <>
              <PageTitle title="Daily Display Setting" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/weekly/list"
          element={
            <>
              <PageTitle title="Weekly" />
              <WeeklyList/>
            </>
          }
        />
        <Route
          path="/weekly/comparison"
          element={
            <>
              <PageTitle title="Past Data Comparison" />
              <WeeklyComparison/>
            </>
          }
        />
        <Route
          path="/weekly/date-range"
          element={
            <>
              <PageTitle title="Custom Date Range" />
              <WeeklyDatePeriod/>
            </>
          }
        />
        <Route
          path="/weekly/history"
          element={
            <>
              <PageTitle title="Weekly Display Setting" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/weekly/setting"
          element={
            <>
              <PageTitle title="Weekly Display Setting" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/egg-table/day"
          element={
            <>
              <PageTitle title="Show Egg of The Day" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/egg-table/list"
          element={
            <>
              <PageTitle title="Show Egg table" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/egg-table/setting"
          element={
            <>
              <PageTitle title="Egg Display Setting" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/graphs/list"
          element={
            <>
              <PageTitle title="Daily Graphs" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/graphs/3-days"
          element={
            <>
              <PageTitle title="3 Day Graphs" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/graphs/weekly"
          element={
            <>
              <PageTitle title="Weekly Graphs" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/graphs/date-range"
          element={
            <>
              <PageTitle title="Custom Range Graphs" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/graphs/comparison"
          element={
            <>
              <PageTitle title="Past Data Comparison Graphs" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/feeder/start-time"
          element={
            <>
              <PageTitle title="Register Feeder Start Time" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/group/history"
          element={
            <>
              <PageTitle title="Group History" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/group/batch"
          element={
            <>
              <PageTitle title="Register Batch" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/calendar"
          element={
            <>
              <PageTitle title="Calendar" />
              <Calendar />
            </>
          }
        />
        <Route
          path="/profile"
          element={
            <>
              <PageTitle title="Profile" />
              <Profile />
            </>
          }
        />
        <Route
          path="/forms/form-elements"
          element={
            <>
              <PageTitle title="Form Elements" />
              <FormElements />
            </>
          }
        />
        <Route
          path="/forms/form-layout"
          element={
            <>
              <PageTitle title="Form Layout" />
              <FormLayout />
            </>
          }
        />
        <Route
          path="/tables"
          element={
            <>
              <PageTitle title="Tables" />
              <Tables />
            </>
          }
        />
        <Route
          path="/settings"
          element={
            <>
              <PageTitle title="Settings" />
              <Settings />
            </>
          }
        />
        <Route
          path="/chart"
          element={
            <>
              <PageTitle title="Basic Chart" />
              <Chart />
            </>
          }
        />
        <Route
          path="/ui/alerts"
          element={
            <>
              <PageTitle title="Alerts" />
              <Alerts />
            </>
          }
        />
        <Route
          path="/ui/buttons"
          element={
            <>
              <PageTitle title="Buttons" />
              <Buttons />
            </>
          }
        />
        <Route
          path="/auth/signin"
          element={
            <>
              <PageTitle title="Signin" />
              <SignIn />
            </>
          }
        />
        <Route
          path="/auth/signup"
          element={
            <>
              <PageTitle title="Signup" />
              <SignUp />
            </>
          }
        />
        <Route
          path="/users"
          element={
            <>
              <PageTitle title="User List" />
              <UserList />
            </>
          }
        />
      </Routes>
    </>
  );
}

export default App;
