import { useEffect, useState } from 'react';
import { Route, Routes, useLocation } from 'react-router-dom';

import Loader from './common/Loader';
import PageTitle from './components/PageTitle';
import SignIn from './pages/Authentication/SignIn';
import SignUp from './pages/Authentication/SignUp';
import Calendar from './pages/Calendar';
import Chart from './pages/Chart';
import ECommerce from './pages/Dashboard/ECommerce';
import FormElements from './pages/Form/FormElements';
import FormLayout from './pages/Form/FormLayout';
import Profile from './pages/Profile';
import Settings from './pages/Settings';
import Tables from './pages/Tables';
import Alerts from './pages/UiElements/Alerts';
import Buttons from './pages/UiElements/Buttons';
import UserList from './pages/User/UserList';
import DailyList from './pages/Daily/List.tsx';
import DefaultLayout from './layout/DefaultLayout.tsx';
import WeeklyList from './pages/Weekly/List.tsx';
import DailyComparison from './pages/Daily/Comparison.tsx';
import DailyDatePeriod from './pages/Daily/DateRange.tsx';
import DailyDisplaySetting from './pages/Daily/DisplaySetting.tsx';
import WeeklyDatePeriod from './pages/Weekly/DateRange.tsx';
import WeeklyComparison from './pages/Weekly/Comparison.tsx';
import WeeklyDisplaySetting from './pages/Weekly/DisplaySetting.tsx';
import WeeklyHistory from './pages/Weekly/History.tsx';
import EggsOfTheDay from './pages/Egg/Day.tsx';
import EggList from './pages/Egg/List.tsx';
import EggSetting from './pages/Egg/Setting.tsx';
import DailyGraphs from './pages/Graphs/Daily.tsx';
import Day3Graphs from './pages/Graphs/Day3.tsx';
import RegisterFeeder from './pages/Feeder/Register.tsx';
import History from './pages/Group/History.tsx';
import RegisterBatch from './pages/Group/RegisterBatch.tsx';
import ListBatch from './pages/Group/ListBatch.tsx';
import EditBatch from './pages/Group/EditBatch.tsx';
import { useTranslation } from 'react-i18next';
import DeadChickenRegistrationList from './pages/DeadChickenRegistration/List.tsx';
import WeeklyGraphs from './pages/Graphs/Weekly.tsx';
import DatePeriodGraphs from './pages/Graphs/DateRange.tsx';

function App() {
  const {t} = useTranslation();
  const [loading, setLoading] = useState<boolean>(true);
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  useEffect(() => {
    setTimeout(() => setLoading(false), 1000);
  }, []);

  return loading ? (
    <Loader />
  ) : (
    <>
      <Routes>
        <Route
          index
          element={
            <>
              <PageTitle title={t('main')} />
              <ECommerce />
            </>
          }
        />
        <Route
          path="/daily/list"
          element={
            <>
              <PageTitle title={t('daily_table')} />
              <DailyList />
            </>
          }
        />
        <Route
          path="/daily/comparison"
          element={
            <>
              <PageTitle title={t('past_data_comparison')} />
              <DailyComparison/>
            </>
          }
        />
        <Route
          path="/daily/date-range"
          element={
            <>
              <PageTitle title={t('show_duration_order')} />
              <DailyDatePeriod/>
            </>
          }
        />
        <Route
          path="/daily/setting"
          element={
            <>
              <PageTitle title={t('daily_display_settings')} />
              <DailyDisplaySetting/>
            </>
          }
        />
        <Route
          path="/weekly/list"
          element={
            <>
              <PageTitle title={t('weekly_table')} />
              <WeeklyList/>
            </>
          }
        />
        <Route
          path="/weekly/comparison"
          element={
            <>
              <PageTitle title={t('past_data_comparison')} />
              <WeeklyComparison/>
            </>
          }
        />
        <Route
          path="/weekly/date-range"
          element={
            <>
              <PageTitle title={t('show_duration_order')} />
              <WeeklyDatePeriod/>
            </>
          }
        />
        <Route
          path="/weekly/history"
          element={
            <>
              <PageTitle title={t('weekly_history')} />
              <WeeklyHistory/>
            </>
          }
        />
        <Route
          path="/weekly/setting"
          element={
            <>
              <PageTitle title={t('weekly_display_settings')} />
              <WeeklyDisplaySetting/>
            </>
          }
        />
        <Route
          path="/egg-table/day"
          element={
            <>
              <PageTitle title={t('show_eggs_of_the_day')} />
              <EggsOfTheDay/>
            </>
          }
        />
        <Route
          path="/egg-table/list"
          element={
            <>
              <PageTitle title={t('show_egg_table')} />
              <EggList/>
            </>
          }
        />
        <Route
          path="/egg-table/setting"
          element={
            <>
              <PageTitle title={t('egg_display_settings')} />
              <EggSetting/>
            </>
          }
        />
        <Route
          path="/graphs/list"
          element={
            <>
              <PageTitle title={t('graphs')} />
              <DailyGraphs/>
            </>
          }
        />
        <Route
          path="/graphs/3-days"
          element={
            <>
              <PageTitle title={t('graphs')} />
              <Day3Graphs/>
            </>
          }
        />
        <Route
          path="/graphs/weekly"
          element={
            <>
              <PageTitle title="Weekly Graphs" />
              <WeeklyGraphs/>
            </>
          }
        />
        <Route
          path="/graphs/date-range"
          element={
            <>
              <PageTitle title="Custom Range Graphs" />
              <DatePeriodGraphs/>
            </>
          }
        />
        <Route
          path="/graphs/comparison"
          element={
            <>
              <PageTitle title="Past Data Comparison Graphs" />
              <DefaultLayout><></></DefaultLayout>
            </>
          }
        />
        <Route
          path="/feeder/start-time"
          element={
            <>
              <PageTitle title={t('register_feeder_starting_time')} />
              <RegisterFeeder/>
            </>
          }
        />
        <Route
          path="/group/history"
          element={
            <>
              <PageTitle title={t('group_history')} />
              <History/>
            </>
          }
        />
        <Route
          path="/group/batch"
          element={
            <>
              <PageTitle title={t('group_register_batch')} />
              <ListBatch/>
            </>
          }
        />
        <Route
          path="/group/batch/create"
          element={
            <>
              <PageTitle title="Register Batch" />
              <RegisterBatch/>
            </>
          }
        />
        <Route
          path="/group/batch/edit/:id"
          element={
            <>
              <PageTitle title="Edit Batch" />
              <EditBatch/>
            </>
          }
        />
        <Route
          path="/dead-chicken-registration"
          element={
            <>
              <PageTitle title={t('dead_chicken_registration')} />
              <DeadChickenRegistrationList/>
            </>
          }
        />
        <Route
          path="/calendar"
          element={
            <>
              <PageTitle title="Calendar" />
              <Calendar />
            </>
          }
        />
        <Route
          path="/profile"
          element={
            <>
              <PageTitle title="Profile" />
              <Profile />
            </>
          }
        />
        <Route
          path="/forms/form-elements"
          element={
            <>
              <PageTitle title="Form Elements" />
              <FormElements />
            </>
          }
        />
        <Route
          path="/forms/form-layout"
          element={
            <>
              <PageTitle title="Form Layout" />
              <FormLayout />
            </>
          }
        />
        <Route
          path="/tables"
          element={
            <>
              <PageTitle title="Tables" />
              <Tables />
            </>
          }
        />
        <Route
          path="/settings"
          element={
            <>
              <PageTitle title="Settings" />
              <Settings />
            </>
          }
        />
        <Route
          path="/chart"
          element={
            <>
              <PageTitle title="Basic Chart" />
              <Chart />
            </>
          }
        />
        <Route
          path="/ui/alerts"
          element={
            <>
              <PageTitle title="Alerts" />
              <Alerts />
            </>
          }
        />
        <Route
          path="/ui/buttons"
          element={
            <>
              <PageTitle title="Buttons" />
              <Buttons />
            </>
          }
        />
        <Route
          path="/auth/signin"
          element={
            <>
              <PageTitle title="Signin" />
              <SignIn />
            </>
          }
        />
        <Route
          path="/auth/signup"
          element={
            <>
              <PageTitle title="Signup" />
              <SignUp />
            </>
          }
        />
        <Route
          path="/users"
          element={
            <>
              <PageTitle title="User List" />
              <UserList />
            </>
          }
        />
      </Routes>
    </>
  );
}

export default App;
