import { useState, useMemo } from 'react';

interface Config {
  key: string;
  direction: string
}

export const useSortableData = (items: any, config?: Config) => {
  const [sortConfig, setSortConfig] = useState(config);

  const sortedItems = useMemo(() => {
    let sortableItems = [...items];
    if (sortConfig && sortConfig.key) {
      sortableItems.sort((a, b) => {
        if (a[sortConfig.key] < b[sortConfig.key]) {
          return sortConfig.direction === 'ascending' ? -1 : 1;
        }
        if (a[sortConfig.key] > b[sortConfig.key]) {
          return sortConfig.direction === 'ascending' ? 1 : -1;
        }
        return 0;
      });
    }
    return sortableItems;
  }, [items, sortConfig]);

  const requestSort = (key: string) => {
    let sc = {key, direction: 'ascending'};

    if (sortConfig && sortConfig.key === key && sortConfig.direction === 'ascending') {
      sc.direction = 'descending';
    }
    if (sortConfig && sortConfig.key === key && sortConfig.direction === 'descending') {
      sc.key = '';
      sc.direction = '';
    }

    setSortConfig(sc);
  };

  return { items: sortedItems, requestSort, sortConfig };
};
