import React from 'react';
import { render, screen } from '@testing-library/react';
import Breadcrumb from './Breadcrumb';

describe('Breadcrumb', () => {
  test('renders Breadcrumb correctly', () => {
    render(<Breadcrumb />);
    const element = screen.getByText(/Breadcrumb/i);
    expect(element).toBeInTheDocument();
  });

  // Additional tests...
});
