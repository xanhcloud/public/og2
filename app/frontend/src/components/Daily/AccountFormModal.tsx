import { FC, useRef, useEffect } from 'react';
import { SubmitHandler, useForm, Controller} from 'react-hook-form';
import { requestToken } from '../../services/request.ts';
import Datepicker, { DateValueType } from 'react-tailwindcss-datepicker';
import { useTranslation } from 'react-i18next';
import { getLanguageObject } from '../../services/Helpers.ts';

const AccountFormModal: FC<{ open: boolean, onOpenChange: Function, tab: any }> = ({open, onOpenChange, tab}) => {
  const { t } = useTranslation();
  const ref = useRef<any>(null);
  const { control, register, handleSubmit, reset, formState: { errors }, } = useForm({
    defaultValues: {},
  });
  const locale = getLanguageObject();

  const fields: any = [
    {key: 'EGG_COUNT', label: 'Eggs', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'ROOM_TEMP', label: 'House Temp °C', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'VENTILATION', label: 'Ventilation', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'FOOD_AMOUNT', label: 'Used Food Amount', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'OUTER_TEMP', label: 'Outside Temp °C', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'FOOD_INTERVAL', label: 'Food Interval', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'WATER_AMOUNT', label: 'Used Water Amount', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'HUMIDITY', label: 'Humidity', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'HEATING', label: 'Heating', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'DEAD_COUNT', label: 'Dead Count', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'HUMIDITY_UPL', label: 'Humidity Upper Limit', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'WATER_POS', label: 'Drinking Water Position', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'HEN_WEIGHT', label: 'Bird Weight', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'HUMIDITY_LOL', label: 'Min Humidity (%)', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'AIRCON_SYSTEM', label: 'Air Conditioning System', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'EGG_WEIGHT', label: 'Egg Weight', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'MIN_VENTILATION', label: 'Minimum Ventilation Value', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'LIGHTING_TIME', label: 'Light Time', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'COEFF', label: 'Coeficient Of Variation', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'BMIN_VENTILATION', label: 'Minimum Ventilation Reference Value', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'FOOD', label: 'Food', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'DEFECTION', label: 'Standard Deviation', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'COMMENTS', label: 'Remarks', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'DAYLIGHT_HOURS', label: 'Sunshie Duration', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'REMV_EXCR', label: 'Excrement Removal', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'PRESET_TEMP', label: 'Preset Temp', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
    {key: 'VENTILATION_SETUP', label: 'Ventilation Settings', type: 'string', required: false, className: 'col-span-12 md:col-span-6 xl:col-span-4'},
  ];

  // Close modal if clicked outside
  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (ref.current && !ref.current.contains(event.target)) {
        onOpenChange(false);
      }
    };

    // Attach event listener when the modal is open
    if (open) {
      document.addEventListener('mousedown', handleClickOutside);
    }

    // Clean up the event listener when the modal is closed
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [open]);

  // close if the esc key is pressed
  useEffect(() => {
    const keyHandler = ({ keyCode }: KeyboardEvent) => {
      if (!open || keyCode !== 27) return;
      onOpenChange(false);
    };
    document.addEventListener('keydown', keyHandler);
    return () => document.removeEventListener('keydown', keyHandler);
  });

  const onSubmit: SubmitHandler<any> = async (data: any) => {
    try {
      const result = await requestToken({
        method: 'POST',
        url: '/api/daily-input',
        data: { ...data, lot_id: tab.id}
      });
      if (result && result.message) {
        onOpenChange(false);
      }
    } catch (e) {
      console.log('Error', e);
    }
  }

  const handleDateSubmit: SubmitHandler<any> = async (data: any) => {
    try {
      await requestToken({
        method: 'GET',
        url: '/api/daily-input-date',
        data: {
          lot_id: tab.id,
          rnd: Math.random(),
          upd_date: data.upd_date
        }
      });
    } catch (e) {
      console.log('Error', e);
    }
  }

  return (
    <div className={`fixed left-0 top-0 z-999999 flex h-full min-h-screen w-full items-center justify-center bg-black/90 px-4 py-5 ${open ? 'block' : 'hidden'}`}>
      <div
        ref={ref}
        onFocus={() => onOpenChange(true)}
        className="w-full max-w-[1000px] max-h-[100%] rounded-lg bg-white dark:bg-boxdark p-4 lg:p-8 overflow-y-auto"
      >
        <div className="text-center mb-4">
          <h3 className="text-xl font-bold text-black dark:text-white sm:text-2xl">
            {t('daily_item_input')}
          </h3>
          <span className="mx-auto inline-block h-1 w-22.5 rounded bg-primary"></span>
        </div>

        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="grid grid-cols-12 gap-4 mb-4">
            <div className="col-span-12 md:col-span-6 xl:col-span-4">
              <Controller
                name="upd_date"
                control={control}
                rules={{ required: false }}
                render={({ field }) => (
                  <Datepicker
                    i18n={locale.code}
                    asSingle={true}
                    useRange={false}
                    primaryColor={'blue'}
                    value={{
                      startDate: field.value || null,
                      endDate: field.value || null,
                    }}
                    onChange={(newValue: DateValueType) => {
                      field.onChange(newValue.startDate);
                    }}
                    // showShortcuts={true}
                    inputClassName={`w-full rounded border-[1.5px] bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary ${errors['upd_date']?.type === 'required' ? 'border-danger' : 'border-stroke'}`}
                  />
                )}
              />
            </div>
            <div className="col-span-12 md:col-span-2">
              <button
                type="button"
                onClick={handleSubmit(handleDateSubmit)}
                className="block w-full rounded bg-primary py-1 px-3 text-center font-medium text-white transition hover:bg-opacity-90"
              >
                {t('submit')}
              </button>
            </div>
          </div>

          <div className="grid grid-cols-12 gap-4">
            {fields.map((item: any) => (
              <div key={item.key} className={item.className}>
                <label className="mb-2 block text-black dark:text-white">
                  {t(item.key.toLowerCase())}
                </label>
                <input
                  type="text"
                  {...register(item.key, { required: item.required })}
                  // placeholder={`Enter ${item.label}`}
                  className={`w-full rounded border-[1.5px] bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary 
                  ${errors[item.key]?.type === 'required' ? 'border-danger' : 'border-stroke'}`}
                />
              </div>
            ))}
          </div>

          <div className="-mx-3 flex flex-wrap gap-y-4 mt-6">
            <div className="2xsm:w-1/3 w-full px-3">
              <button
                type="submit"
                className="block w-full rounded border border-primary bg-primary py-2 px-4 text-center font-medium text-white transition hover:bg-opacity-90"
              >
                {t('save_changes')}
              </button>
            </div>
            <div className="2xsm:w-1/3 w-full px-3">
              <button
                type="button"
                onClick={() => reset()}
                className="block w-full rounded border border-stroke bg-gray py-2 px-4 text-center font-medium text-black transition hover:border-primary hover:bg-primary hover:text-white dark:border-strokedark dark:bg-meta-4 dark:text-white dark:hover:border-primary dark:hover:bg-primary"
              >
                {t('abort_changes')}
              </button>
            </div>
            <div className="2xsm:w-1/3 w-full px-3">
              <button
                type="button"
                onClick={() => onOpenChange(false)}
                className="block w-full rounded border border-stroke bg-gray py-2 px-4 text-center font-medium text-black transition hover:border-meta-1 hover:bg-meta-1 hover:text-white dark:border-strokedark dark:bg-meta-4 dark:text-white dark:hover:border-meta-1 dark:hover:bg-meta-1"
              >
                {t('close_window')}
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AccountFormModal;
