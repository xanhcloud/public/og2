import { useRequest } from 'ahooks';
import { useTranslation } from 'react-i18next';
import { FC, useEffect, useState } from 'react';
import { requestToken } from '../../services/request';
import moment from 'moment/moment';
import DailyHistoryModal from "./HistoryModal.tsx";

const DailyComparisonTable: FC<{ tab: any, settings: any }> = ({ tab, settings }) => {
  const { t } = useTranslation();
  const [fieldColumns, setFieldColumns] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [params, setParams] = useState({batch_id: null});

  const { data: comparisonTable, loading } = useRequest(() => {
    return requestToken({
      url: `/api/d-comparison/${tab.id}`,
      method: 'GET',
      data: params
    });
  }, {refreshDeps: [params]});

  // Get field columns
  useEffect(() => {
    if (settings && settings.items && Object.keys(settings.items).length > 0) {
      let columns: any = [];

      Object.entries(settings.items).map(([key, value]: any)  => {
        if (value && value.toUpperCase() === 'ON') {
          columns.push({ key });
        }
      });

      setFieldColumns(columns);
    }
  }, [settings]);

  return (
    <>
      {!loading && (
        <div className={'flex gap-4'}>
          <div className={'flex self-start gap-2 bg-white dark:bg-meta-4 border border-[#eee] dark:border-strokedark rounded text-black dark:text-white px-4 py-2 mb-4'}>
            <div>{t('from_date')}: <span className="font-medium">{comparisonTable?.from_date_p}〜{comparisonTable?.to_date_p}</span></div>
            <div>{t('to_date')}: <span className="font-medium">{comparisonTable?.from_date_c}〜{comparisonTable?.to_date_c}</span></div>
          </div>

          <div>
            <button onClick={() => setModalOpen(!modalOpen)}
                    className="rounded-md bg-primary py-2 px-4 text-center font-medium text-white hover:bg-opacity-90">
              {t('history')}
            </button>
          </div>

          <DailyHistoryModal
            open={modalOpen}
            onOpenChange={setModalOpen}
            setComparisonParams={setParams}
            tab={tab}
          />
        </div>
      )}

      <div className="max-w-full overflow-x-auto rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white">
        <table className="w-full table-auto" style={{ borderCollapse: 'separate', borderSpacing: 0 }}>
          <thead className="sticky top-[0px]">
          <tr className="bg-gray-2 text-left dark:bg-meta-4">
            <th className="min-w-[100px] bg-[#dfe5e7] dark:bg-meta-4 whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#ccc] dark:border-strokedark">
              {t('date')}
            </th>
            <th className="min-w-[100px] bg-[#f9f9f2] dark:bg-meta-4 whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#ccc] dark:border-strokedark">
              {t('date')}
            </th>
            <th className="bg-[#dfe5e7] dark:bg-meta-4 whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#ccc] dark:border-strokedark">
              {t('days_old')}
            </th>
            <th className="bg-[#f9f9f2] dark:bg-meta-4 whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#ccc] dark:border-strokedark">
              {t('days_old')}
            </th>
            <th className="bg-[#dfe5e7] dark:bg-meta-4 whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#ccc] dark:border-strokedark">
              {t('number_of_eggs')}
            </th>
            <th className="bg-[#f9f9f2] dark:bg-meta-4 whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#ccc] dark:border-strokedark">
              {t('number_of_eggs')}
            </th>
            {fieldColumns.length > 0 && fieldColumns.map((field: any) => (
              <>
                <th key={field.key + '_0'}
                    className="bg-[#dfe5e7] dark:bg-meta-4 whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#ccc] dark:border-strokedark">
                  {t(field.key)}
                </th>
                <th key={field.key + '_1'}
                    className="bg-[#f9f9f2] dark:bg-meta-4 whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#ccc] dark:border-strokedark">
                  {t(field.key)}
                </th>
              </>
            ))}
          </tr>
          </thead>
          <tbody>
          {loading && (
            <tr key={'loading'}>
              <td colSpan={fieldColumns.length * 2 + 6} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <h5 className="text-black dark:text-white text-center text-sm">Loading...</h5>
              </td>
            </tr>
          )}

          {!loading && comparisonTable?.nrows?.length === 0 && (
            <tr key={'loading'}>
              <td colSpan={fieldColumns.length * 2 + 6} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <h5 className="text-black dark:text-white text-center text-sm">No Data</h5>
              </td>
            </tr>
          )}

          {!loading && comparisonTable?.nrows?.length > 0 && comparisonTable?.nrows?.map((item: any, index: number) => (
            <tr key={index}>
              <td className="bg-[#e3f1f4] dark:bg-meta-4 border-b border-r border-[#ccc] dark:border-strokedark py-2 px-2">
                {comparisonTable?.from_date_p && item[1] && (
                  <h5 className="text-black dark:text-white">{item[1].DAILY_DATE && moment(item[1].DAILY_DATE).format('MM-DD')}</h5>
                )}
              </td>
              <td className="border-b border-r border-[#ccc] dark:bg-meta-4 dark:border-strokedark py-2 px-2">
                <h5 className="text-black dark:text-white">{item[0].DAILY_DATE && moment(item[0].DAILY_DATE).format('MM-DD')}</h5>
              </td>
              <td className="bg-[#e3f1f4] dark:bg-meta-4 border-b border-r border-[#ccc] dark:border-strokedark py-2 px-2">
                {comparisonTable?.from_date_p && item[1] && (
                  <p className="text-black dark:text-white text-center text-sm">{item[1].DAY_PERIODS}</p>
                )}
              </td>
              <td className="border-b border-r border-[#ccc] dark:bg-meta-4 dark:border-strokedark py-2 px-2">
                <p className="text-black dark:text-white text-center text-sm">
                  {item[0].DAY_PERIODS}
                </p>
              </td>
              <td className="bg-[#e3f1f4] dark:bg-meta-4 border-b border-r border-[#ccc] dark:border-strokedark py-2 px-2">
                {comparisonTable?.from_date_p && item[1] && (
                  <p className="text-black dark:text-white text-center text-sm">{item[1].EGG_COUNT}</p>
                )}
              </td>
              <td className="border-b border-r border-[#ccc] dark:bg-meta-4 dark:border-strokedark py-2 px-2">
                <p className="text-black dark:text-white text-center text-sm">
                  {item[0].EGG_COUNT}
                </p>
              </td>
              {fieldColumns.length > 0 && fieldColumns.map((field: any) => (
                <>
                  <td key={field.key + '_0'} className="bg-[#e3f1f4] dark:bg-meta-4 border-b border-r border-[#ccc] dark:border-strokedark py-2 px-2">
                    {comparisonTable?.from_date_p && item[1] && (
                      <p className="text-black dark:text-white text-center text-sm">{item[1][field.key.toUpperCase()]}</p>
                    )}
                  </td>
                  <td key={field.key + '_1'} className="border-b border-r border-[#ccc] dark:bg-meta-4 dark:border-strokedark py-2 px-2">
                    <p className="text-black dark:text-white text-center text-sm">
                      {item[0][field.key.toUpperCase()]}
                    </p>
                  </td>
                </>
              ))}
            </tr>
          ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default DailyComparisonTable;
