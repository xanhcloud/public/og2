import { FC, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FiEdit } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import { BatchList } from '../../types/batch.ts';
import { growthSegments } from '../../services/Helpers.ts';

interface List {
  data: BatchList[];
  loading?: boolean;
  isEdit?: boolean;
  rowSelectEnable?: boolean;
  onRowSelected?: (val: any) => void;
  handleEditAction?: (row: any) => void;
  setComparisonParams?: (params: any) => void;
  setOpenModal?: (open: boolean) => void;
}

const DailyHistoryListTable: FC<List> =
  ({
     data,
     loading,
     rowSelectEnable = true,
     onRowSelected,
     isEdit = false,
     handleEditAction,
     setComparisonParams,
     setOpenModal
   }) => {
    const { t } = useTranslation();
    const [row, setRow] = useState<any>(null);

    return (
      <>
        <div className="max-w-full overflow-x-auto rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white">
          <table className="w-full table-auto">
            <thead className="sticky top-[0px]">
            <tr className="bg-gray-2 text-left dark:bg-meta-4">
              {rowSelectEnable && (
                <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-sm cursor-pointer"></th>
              )}
              <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
                <div className="flex gap-1 flex-nowrap items-center justify-center">
                  <div>{t('group')}</div>
                </div>
              </th>
              <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
                <div className="flex gap-1 flex-nowrap items-center justify-center">
                  <div>{t('chicken_house')}</div>
                </div>
              </th>
              <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
                <div className="flex gap-1 flex-nowrap items-center justify-center">
                  <div>{t('chicken')}</div>
                </div>
              </th>
              <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
                <div className="flex gap-1 flex-nowrap items-center justify-center">
                  <div>{t('feeding_date')}</div>
                </div>
              </th>
              <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
                <div className="flex gap-1 flex-nowrap items-center justify-center">
                  <div>{t('growth_segment')}</div>
                </div>
              </th>
              <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
                <div className="flex gap-1 flex-nowrap items-center justify-center">
                  <div>{t('batch_initial_count')}</div>
                </div>
              </th>
              <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
                <div className="flex gap-1 flex-nowrap items-center justify-center">
                  <div>{t('revenue')}</div>
                </div>
              </th>
              <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
                <div className="flex gap-1 flex-nowrap items-center justify-center">
                  <div>{t('batch_start_date')}</div>
                </div>
              </th>
              <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
                <div className="flex gap-1 flex-nowrap items-center justify-center">
                  <div>{t('batch_end_date')}</div>
                </div>
              </th>
              <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
                <div className="flex gap-1 flex-nowrap items-center justify-center">
                  <div></div>
                </div>
              </th>
              {isEdit && (
                <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer"></th>
              )}
            </tr>
            </thead>
            <tbody>

            {loading && (
              <tr key={'loading'}>
                <td colSpan={10} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <h5 className="text-black dark:text-white text-center text-sm">Loading...</h5>
                </td>
              </tr>
            )}

            {!loading && data?.length === 0 && (
              <tr key={'loading'}>
                <td colSpan={10} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <h5 className="text-black dark:text-white text-center text-sm">No Data</h5>
                </td>
              </tr>
            )}

            {!loading && data?.length > 0 && data?.map((item: any, index: number) => (
              <tr key={index}>
                {rowSelectEnable && (
                  <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark text-sm">
                    <div className="flex justify-center items-center">
                      <label htmlFor={item.id} className="flex cursor-pointer select-none items-center text-black">
                        <div className="relative">
                          <input
                            type="checkbox"
                            id={item.id}
                            className="sr-only"
                            onChange={(event: any) => {
                              const getItem = event.target.checked ? item : null;
                              setRow(getItem);
                              onRowSelected && onRowSelected(getItem);
                            }}
                          />
                          <div
                            className={`mr-4 flex h-5 w-5 items-center justify-center rounded border ${
                              row && row.BATCH_ID === item.BATCH_ID && 'border-primary bg-gray dark:bg-transparent'
                            }`}
                          >
                        <span
                          className={`h-2.5 w-2.5 rounded-sm ${row && row.BATCH_ID === item.BATCH_ID && 'bg-primary'}`}></span>
                          </div>
                        </div>
                      </label>
                    </div>
                  </td>
                )}
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark text-sm">
                  <p className="text-black dark:text-white text-center text-sm">
                    {item.HENGRP_NAME}
                  </p>
                </td>
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <p className="text-black dark:text-white text-center text-sm">
                    {item.HOUSE_NAME}
                  </p>
                </td>
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <p className="text-black dark:text-white text-center text-sm">
                    {item.HEN_NAME}
                  </p>
                </td>
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <p className="text-black dark:text-white text-center text-sm">
                    {item.FEEDING_DATE}
                  </p>
                </td>
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <p className="text-black dark:text-white text-center text-sm">
                    {t(growthSegments[item.GROWTH_KIND || 0])}
                  </p>
                </td>
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <p className="text-black dark:text-white text-center text-sm">
                    {item.INITIAL_COUNT}
                  </p>
                </td>
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark"></td>
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <p className="text-black dark:text-white text-center text-sm">
                    {item.START_DATE}
                  </p>
                </td>
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <p className="text-black dark:text-white text-center text-sm">
                    {item.END_DATE}
                  </p>
                </td>
                {isEdit && (
                  <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                    <Link to={!handleEditAction ? `/group/batch/edit/${item.BATCH_ID}` : '#'}
                          onClick={() => handleEditAction && handleEditAction(item) || null}>
                      <FiEdit title="Edit" />
                    </Link>
                  </td>
                )}
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <button
                    onClick={() => {
                      setComparisonParams && setComparisonParams((prev: any) => ({...prev, batch_id: item.BATCH_ID}));
                      setOpenModal && setOpenModal(false);
                    }}
                    className="rounded-md bg-primary py-1 px-4 text-center font-medium text-white hover:bg-opacity-90"
                  >
                    {t('comparison_select')}
                  </button>
                </td>
              </tr>
            ))}
            </tbody>
          </table>
        </div>
      </>
    );
  };

export default DailyHistoryListTable;
