import { useTranslation } from 'react-i18next';

const DataStats = () => {
  const { t } = useTranslation();

  return (
    <div className="col-span-12 rounded-sm border border-stroke bg-white p-7.5 shadow-default dark:border-strokedark dark:bg-boxdark">
      <div className="grid grid-cols-1 gap-5 sm:grid-cols-2 xl:grid-cols-4 xl:gap-0">
        <div className="flex items-center justify-center gap-2 border-b border-stroke pb-5 dark:border-strokedark xl:border-b-0 xl:border-r xl:pb-0">
          <div className="text-center">
            <h4 className="mb-0.5 text-xl font-semibold text-black dark:text-white md:text-title-lg">
              24.8°C
            </h4>
            <p className="text-sm font-medium">{t('indoor_temperature')}</p>
          </div>
        </div>
        <div className="flex items-center justify-center gap-2 border-b border-stroke pb-5 dark:border-strokedark xl:border-b-0 xl:border-r xl:pb-0">
          <div className="text-center">
            <h4 className="mb-0.5 text-xl font-semibold text-black dark:text-white md:text-title-lg">
              65%
            </h4>
            <p className="text-sm font-medium">{t('indoor_humidity')}</p>
          </div>
        </div>
        <div className="flex items-center justify-center gap-2 border-b border-stroke pb-5 dark:border-strokedark sm:border-b-0 sm:pb-0 xl:border-r">
          <div className="text-center">
            <h4 className="mb-0.5 text-xl font-semibold text-black dark:text-white md:text-title-lg">
              25 ppm
            </h4>
            <p className="text-sm font-medium">{t('ammonia')}</p>
          </div>
        </div>
        <div className="flex items-center justify-center gap-2">
          <div className="text-center">
            <h4 className="mb-0.5 text-xl font-semibold text-black dark:text-white md:text-title-lg">
              600 ppm
            </h4>
            <p className="text-sm font-medium">{t('co2')}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DataStats;
