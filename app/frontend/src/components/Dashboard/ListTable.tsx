import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

const DashboardListTable: FC<{ data: any, loading: boolean }> = ({ data, loading }) => {
  const { t } = useTranslation();

  return (
    <>
      <div className="max-w-full overflow-x-auto rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white">
        <table className="w-full table-auto">
          <thead className="sticky top-[0px]">
          <tr className="bg-gray-2 text-left dark:bg-meta-4">
            <th className="py-2 px-2"></th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('born_date')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('rearing_period')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('days_alive')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('last_week')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('last_day')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('cage_number')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('number_of_birds_introduced')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('number_of_birds_at_present')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('no_of_dead_birds')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('egg_weight')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('bird_weight')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('used_food_amount')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('used_water_ammount')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              {t('preset_temperature')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('house_temperature')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('house_max_temperature')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('house_min_temperature')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('outside_temperature')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('outside_max_temperature')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('outside_min_temperature')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('sunshine_duration')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('coeficient_of_variation')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('weight_change_rate_percentage')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('min_vent')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('standard_min_vent')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('max_humidity_percentage')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('current_humidity_setting_percentage')}
            </th>
            <th className="py-2 px-2 font-medium text-black dark:text-white text-center text-sm">
              { t('min_humidity_percentage')}
            </th>
          </tr>
          </thead>
          <tbody>
          {loading && (
            <tr key={'loading'}>
              <td colSpan={28} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <h5 className="text-black dark:text-white text-center text-sm">Loading...</h5>
              </td>
            </tr>
          )}

          {!loading && data?.rows?.length === 0 && (
            <tr key={'loading'}>
              <td colSpan={28} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <h5 className="text-black dark:text-white text-center text-sm">No Data</h5>
              </td>
            </tr>
          )}

          {!loading && data?.rows?.length > 0 && data?.rows?.map((row: any, index: number) => (
            <tr key={index}>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark text-sm">
                <h5 className="text-black dark:text-white font-medium">{row.HOUSE_NAME}:{row.LOT_ID}</h5>
              </td>
              <td className="whitespace-nowrap border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.FEEDING_DATE && moment(row.FEEDING_DATE).format('MM-DD')}
                </p>
              </td>
              <td className="whitespace-nowrap border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.START_DATE && moment(row.START_DATE).format('MM-DD')}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.STTDAY_PERIODS}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.WEEK_PERIODS}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.DAY_PERIODS}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.CAGE_NUM}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.INITIAL_COUNT}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.CURR_COUNT}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.DEAD_COUNT}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.EGG_WEIGHT}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.HEN_WEIGHT}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                {row.ALARM_FOOD ? (
                  <p className="text-danger dark:text-white text-center text-sm">{row.ALARM_FOOD}</p>
                ) : (
                  <p className="text-black dark:text-white text-center text-sm">{row.FOOD_AMOUNT}</p>
                )}
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                {row.ALARM_WATER ? (
                  <p className="text-danger dark:text-white text-center text-sm">{row.ALARM_WATER}</p>
                ) : (
                  <p className="text-black dark:text-white text-center text-sm">{row.WATER_AMOUNT}</p>
                )}
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.PRESET_TEMP}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                {row.ROOM_TEMP >= data?.alarmTempUpper || row.ROOM_TEMP <= data?.alarmTempLower ? (
                  <p className="text-danger dark:text-white text-center text-sm">{row.ROOM_TEMP}</p>
                ) : (
                  <p className="text-black dark:text-white text-center text-sm">{row.ROOM_TEMP}</p>
                )}
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.ROOM_TEMP_HI}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.ROOM_TEMP_LO}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.OUTER_TEMP}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.OUTER_TEMP_HI}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.OUTER_TEMP_LO}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.DAYLIGHT_HOURS}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.DEFECTION}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.WEIGHTCHG_RATE}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.MIN_VENTILATION}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.BMIN_VENTILATION}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.HUMIDITY_UPL}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.HUMIDITY}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {row.HUMIDITY_LOL}
                </p>
              </td>
            </tr>
          ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default DashboardListTable;
