import { CurrCountByHouses, House } from '../../types/batch.ts';
import { FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

interface Props {
  items: CurrCountByHouses[];
  tabs: House[]
}

const TotalEggs: FC<Props> = ({items, tabs}) => {
  const { t } = useTranslation();
  const [totalEgg, setTotalEgg] = useState(0);

  useEffect(() => {
    let total = 0;

    if (items && items.length > 0) {
      items.map((item: CurrCountByHouses) => {
        const getTab = tabs.find((tab: House) => tab.HOUSE_ID === item.HOUSE_ID);
        if (getTab) {
          getTab.CURR_COUNT = item.CURR_COUNT || 0;
        }
        total += item.CURR_COUNT;
      })
    }

    setTotalEgg(total);
  }, [items]);

  if (tabs && tabs.length === 0) {
    return;
  }

  return (
    <div className="col-span-12 rounded-sm border border-stroke bg-white p-7.5 shadow-default dark:border-strokedark dark:bg-boxdark">
      <div className="grid grid-cols-1 gap-4">
        <div className="flex items-center justify-center gap-2 border-stroke pb-5 dark:border-strokedark">
          <div className="text-center">
            <h4 className="mb-0.5 text-xl font-semibold text-black dark:text-white md:text-title-lg">{totalEgg}</h4>
            <p className="text-sm font-medium">{t('total_egg_count')}</p>
          </div>
        </div>
      </div>

      <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 xl:grid-cols-4">
        {tabs.map((item) => (
          <div className="flex items-center justify-center gap-2 border-b border-stroke pb-5 dark:border-strokedark xl:border-b-0 xl:border-r xl:pb-0">
            <div className="text-center">
              <h4 className="mb-0.5 text-xl font-semibold text-black dark:text-white md:text-title-lg">{item.CURR_COUNT || 0}</h4>
              <p className="text-sm font-medium">{item.HOUSE_NAME}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default TotalEggs;
