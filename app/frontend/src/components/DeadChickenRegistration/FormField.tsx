import { requestToken } from '../../services/request';
import { FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { SubmitHandler, useForm, FormProvider } from 'react-hook-form';
import { getTimeSlots } from '../../services/Helpers.ts';
import moment from 'moment';
import { Lot } from '../../types/batch.ts';
import { DBTDead } from '../../types/dbd-dead.ts';
import { FieldItem } from '../../types/field.ts';
import FormLayout from '../FormLayout.tsx';

interface List {
  record?: DBTDead | null;
  tab?: Lot | null;
  onFished: (res: DBTDead) => void;
  className?: string | undefined;
  isModalClosed?: boolean;
  modalOpenChange?: (open: boolean) => void;
  openModal?: boolean;
}

const DeadChickenFormField: FC<List> = ({ record, tab, onFished, className, isModalClosed, modalOpenChange, openModal }) => {
  const { t } = useTranslation();
  const dateFormat = 'YYYY-MM-DD';
  const [timeSlots, setTimeSlots] = useState<string[]>([]);
  const [recordData, setRecordData] = useState<any>(null);
  const formMethods = useForm();

  // Form fields
  const fields: FieldItem[] = [
    {
      key: 'house',
      label: 'chicken_house',
      type: 'enum',
      required: true,
      className: 'col-span-12',
      options: [...[{HOUSE_ID: 0, FULL_NAME: ''}], ...[tab] || []],
      valueKey: 'HOUSE_ID',
      labelKey: 'FULL_NAME',
      disabled: true,
      suffix: <></>
    },
    {
      key: 'lot',
      label: 'lot',
      type: 'enum',
      required: true,
      className: 'col-span-12 hidden',
      options: [...[{LOT_ID: 0, LOT_NAME: ''}], ...[tab] || []],
      valueKey: 'LOT_ID',
      labelKey: 'LOT_NAME',
      disabled: true
    },
    {
      key: 'check_date',
      label: 'date',
      type: 'date',
      required: true,
      className: 'col-span-12',
    },
    {
      key: 'check_time',
      label: 'time',
      type: 'enum',
      required: true,
      className: 'col-span-12',
      options: [...[""], ...timeSlots],
      valueKey: '',
      labelKey: '',
      indexValue: false
    },
    {
      key: 'dead_count',
      label: 'dead_count',
      type: 'number',
      required: true,
      className: 'col-span-12',
    },
  ];

  // Get timeslots
  useEffect(() => {
    // const d = new Date();
    // const m = (Math.round(d.getMinutes() / 15) * 15) % 60;
    setTimeSlots(getTimeSlots('00:00', '23:59', 30));
  }, []);

  // Form initial value
  useEffect(() => {
    let item = {
      house: tab?.HOUSE_ID,
      lot: tab?.LOT_ID,
      check_date: moment().format(dateFormat),
      check_time: moment().startOf('hour').format('HH:mm'),
    };

    if (record && record.tdead_id) {
      record.check_time = record.check_time && moment(record.check_time, 'HH:mm:ss').format('HH:mm');
      item = record;
    }

    setRecordData(item);
  }, [tab, record, timeSlots, openModal]);

  // Handle submit
  const handleOnSubmit: SubmitHandler<any> = async (data: any) => {
    try {
      if (!record || (record && !record.tdead_id)) {
        data.created_tdate = moment().format(dateFormat);
      }

      let options = {
        method: 'POST',
        url: '/api/dbt-deads/',
        data
      }

      if (record && record.tdead_id) {
        options.method = 'PUT';
        options.url = `/api/dbt-deads/${record.tdead_id}/`
      }

      const result = await requestToken(options);

      if (result) {
        formMethods.reset();
        setRecordData(null);
        onFished && onFished(result);
      }
    } catch (e: any) {
      onFished && onFished(e);
    }
  }

  return (
    <>
      <div className={className || 'max-w-full rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white p-4'}>
        <FormProvider {...formMethods}>
          <FormLayout fields={fields} record={recordData}>
            <div className="-mx-3 flex flex-wrap gap-y-4 mt-6 justify-center">
              <div className="2xsm:w-1/3 w-full px-3">
                <button
                  type="button"
                  onClick={formMethods.handleSubmit(handleOnSubmit)}
                  className="block w-full rounded border border-primary bg-primary py-2 px-4 text-center font-medium text-white transition hover:bg-opacity-90"
                >
                  {t('save_changes')}
                </button>
              </div>
              {isModalClosed && (
                <div className="2xsm:w-1/3 w-full px-3">
                  <button
                    type="button"
                    onClick={() => modalOpenChange && modalOpenChange(false)}
                    className="block w-full rounded border border-stroke bg-gray py-2 px-4 text-center font-medium text-black transition hover:border-meta-1 hover:bg-meta-1 hover:text-white dark:border-strokedark dark:bg-meta-4 dark:text-white dark:hover:border-meta-1 dark:hover:bg-meta-1"
                  >
                    {t('close_window')}
                  </button>
                </div>
              )}
            </div>
          </FormLayout>
        </FormProvider>
      </div>
    </>
  );
};

export default DeadChickenFormField;
