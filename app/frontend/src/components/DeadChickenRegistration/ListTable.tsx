import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useSortableData } from '../../common/sortableData.ts';
import { FiArrowUp, FiArrowDown, FiEdit, FiTrash } from 'react-icons/fi';
import { DBTDead } from '../../types/dbd-dead.ts';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { Lot } from '../../types/batch.ts';

interface List {
  tab: Lot | null;
  refresh?: number;
  handleEditAction?: (row: DBTDead) => void;
  handleRemoveAction?: (row: DBTDead) => void;
}

const DeadChickenListTable: FC<List> = ({ tab, refresh,handleEditAction, handleRemoveAction }) => {
  const { t } = useTranslation();

  const { data, loading } = useRequest(() => {
    return requestToken({
      url: '/api/dbt-deads',
      method: 'GET',
      data: {lot_id: tab?.LOT_ID}
    });
  }, {refreshDeps: [refresh]});

  // Sortable data
  const { items, requestSort, sortConfig } = useSortableData(data?.results || []);

  // Check field sorting
  const fieldHasSorted = (name: string) => {
    if (!sortConfig) {
      return;
    }
    return sortConfig.key === name ? sortConfig.direction : undefined;
  };

  return (
    <>
      <div className="max-w-full overflow-x-auto rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white">
        <table className="w-full table-auto">
          <thead className="sticky top-[0px]">
          <tr className="bg-gray-2 text-left dark:bg-meta-4">
            <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-sm cursor-pointer" onClick={() => requestSort('check_date')}>
              <div className="flex gap-1 flex-nowrap items-center">
                <div>{t('date')}</div>
                {fieldHasSorted('check_date') === 'ascending' && (<FiArrowUp />)}
                {fieldHasSorted('check_date') === 'descending' && (<FiArrowDown />)}
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer" onClick={() => requestSort('check_time')}>
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('time')}</div>
                {fieldHasSorted('check_time') === 'ascending' && (<FiArrowUp />)}
                {fieldHasSorted('check_time') === 'descending' && (<FiArrowDown/>)}
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer" onClick={() => requestSort('dead_count')}>
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('dead_count')}</div>
                {fieldHasSorted('dead_count') === 'ascending' && (<FiArrowUp />)}
                {fieldHasSorted('dead_count') === 'descending' && (<FiArrowDown />)}
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer"></th>
          </tr>
          </thead>
          <tbody>
          {loading && (
            <tr key={'loading'}>
            <td colSpan={4} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <h5 className="text-black dark:text-white text-center text-sm">Loading...</h5>
              </td>
            </tr>
          )}

          {!loading && items?.length === 0 && (
            <tr key={'loading'}>
              <td colSpan={4} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <h5 className="text-black dark:text-white text-center text-sm">No Data</h5>
              </td>
            </tr>
          )}

          {!loading && items?.length > 0 && items?.map((item: DBTDead, index: number) => (
            <tr key={index}>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark text-sm">
                <h5 className="text-black dark:text-white">{item.check_date}</h5>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {item.check_time && moment(item.check_time, 'HH:mm:ss').format('HH:mm')}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {item.dead_count}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <div className="flex items-center gap-2">
                  {handleEditAction && (
                    <Link to={'#'} onClick={() => handleEditAction(item)}>
                      <FiEdit title="Edit" />
                    </Link>
                  )}

                  {handleRemoveAction && (
                    <Link to={'#'} onClick={() => handleRemoveAction(item)}>
                      <FiTrash title="Delete" />
                    </Link>
                  )}
                </div>
              </td>
            </tr>
          ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default DeadChickenListTable;
