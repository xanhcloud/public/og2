import React, {FC, ForwardedRef, forwardRef, useEffect, useState} from 'react';
import {useTranslation} from 'react-i18next';
import {Controller, SubmitHandler, useForm} from "react-hook-form";
import {useRequest} from "ahooks";
import {requestToken} from "../../services/request.ts";
import Datepicker, {DateValueType} from "react-tailwindcss-datepicker";

const EggCorrectionModal: FC<{
    open: boolean,
    onOpenChange: Function,
    tab: any
}> =
    forwardRef(({open, onOpenChange, tab}, ref: ForwardedRef<any>) => {
        const {t} = useTranslation();
        const {control, register, handleSubmit, reset, formState: {errors},} = useForm({
            defaultValues: {},
        });

        const [eggInputData, setEggInputData] = useState({});
        const [errMsg, setErrMsg] = useState('');

        const {data: eggTable} = useRequest(() => {
            return requestToken({
                url: `/api/egg-input`,
                method: 'GET',
                data: {
                    id: tab.id
                }
            });
        });

        useEffect(() => {
            if (eggTable && Object.keys(eggTable).length > 0) {
                setEggInputData(eggTable);
            }
        }, [eggTable]);

        const handleDateSubmit: SubmitHandler<any> = async (data: any) => {
            try {
                if (data && data['egg_date']) {
                    requestToken({
                        url: '/api/egg-input',
                        method: 'GET',
                        data: {
                            id: tab.id,
                            get_egg: true,
                            egg_date: data['egg_date'],
                        }
                    }).then((res: any) => {
                        if (res) {
                            setEggInputData(res);
                            setErrMsg('');

                            if (res['msg']) {
                                onOpenChange(true);
                                setErrMsg(res['msg']);
                            }
                        }
                    });
                }
            } catch (e) {
                console.log('Error', e);
            }
        }

        const onSubmit: SubmitHandler<any> = async (data: any) => {
            try {
                if (data && data['egg_date']) {
                    requestToken({
                        url: '/api/egg-input',
                        method: 'GET',
                        data: {
                            id: tab.id,
                            cage_row: data.cage_row,
                            cage_tier: data.cage_tier,
                            egg_count: data.egg_count,
                            update_egg: 'Save Changes',
                            egg_date: data.egg_date,
                        }
                    }).then((res: any) => {
                        if (res) {
                            if (res['msg']) {
                                onOpenChange(true);
                                setErrMsg(res['msg']);
                            }
                        }
                    });
                }
            } catch (e) {
                console.log('Error', e);
            }
        }

        return (
            <div
                className={`fixed left-0 top-0 z-999999 flex h-full min-h-screen w-full items-center justify-center bg-black/90 px-4 py-5 ${open ? 'block' : 'hidden'}`}>
                <div
                    ref={ref}
                    onFocus={() => onOpenChange(true)}
                    className="w-full max-w-[1000px] max-h-[100%] rounded-lg bg-white dark:bg-boxdark p-4 lg:p-8 overflow-y-auto"
                >
                    <div className="text-center mb-4">
                        <h3 className="text-xl font-bold text-black dark:text-white sm:text-2xl">
                            {t('egg_table_modification')}
                        </h3>
                        <span className="mx-auto inline-block h-1 w-22.5 rounded bg-primary"></span>
                    </div>

                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="grid grid-cols-12 gap-4 mb-4">
                            <div className="col-span-12 md:col-span-6 xl:col-span-4">
                                <Controller
                                    name="egg_date"
                                    control={control}
                                    rules={{required: false}}
                                    render={({field}) => (
                                        <Datepicker
                                            asSingle={true}
                                            useRange={false}
                                            primaryColor={'blue'}
                                            value={{
                                                startDate: field.value || null,
                                                endDate: field.value || null,
                                            }}
                                            onChange={(newValue: DateValueType) => {
                                                field.onChange(newValue.startDate);
                                            }}
                                            inputClassName={`w-full rounded border-[1.5px] bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary ${errors['upd_date']?.type === 'required' ? 'border-danger' : 'border-stroke'}`}
                                        />
                                    )}
                                />
                            </div>

                            <div className="col-span-12 md:col-span-2">
                                <button
                                    type="button"
                                    name="get_egg"
                                    onClick={handleSubmit(handleDateSubmit)}
                                    className="block w-full rounded bg-primary py-1 px-3 text-center font-medium text-white transition hover:bg-opacity-90"
                                >
                                    {t('submit')}
                                </button>
                            </div>
                        </div>

                        <div className='mb-8'>
                            <table className="w-full table-auto">
                                <thead className="sticky top-[0px]">
                                <tr className="bg-gray-2 text-center dark:bg-meta-4">
                                    <th></th>
                                    {eggInputData && eggInputData['r_infos'] && eggInputData['r_infos'].map((value: any) => (
                                        <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-sm cursor-pointer">
                                            {value['CAGE_ROW']} {t('row')}
                                        </th>
                                    ))}
                                </tr>
                                </thead>

                                <tbody>
                                {eggInputData && eggInputData['tnum'] && (() => {
                                    const arr = [];
                                    for (let i = 0; i < eggInputData['tnum']; i++) {
                                        arr.push(
                                            <>
                                                <tr>
                                                    <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                                                        <p className="text-black dark:text-white text-center text-sm font-semibold">
                                                            {eggInputData['t_infos'][i]['CAGE_TIER']} {t('tier')}
                                                        </p>
                                                    </td>

                                                    {eggInputData && eggInputData['rnum'] && (() => {
                                                        const arr2 = [];
                                                        for (let j = 0; j < eggInputData['rnum']; j++) {
                                                            arr2.push(
                                                                <>
                                                                    <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark hover:bg-[#fdfddf] hover:dark:bg-strokedark">
                                                                        <p className="text-black dark:text-white text-center text-sm">
                                                                            {eggInputData['teggs'][j][i]}
                                                                        </p>
                                                                    </td>
                                                                </>
                                                            )
                                                        }
                                                        return arr2;
                                                    })()}
                                                </tr>
                                            </>
                                        );
                                    }
                                    return arr;
                                })()}
                                </tbody>
                            </table>
                        </div>

                        <div className='mb-8'>
                            <div className="grid grid-cols-12 gap-4">
                                <div className='col-span-12 md:col-span-6 xl:col-span-4 flex gap-4 items-center'>
                                    <label className="mb-2 text-black dark:text-white">
                                        {t('row')}
                                    </label>
                                    <input
                                        type="text"
                                        {...register('cage_row', {required: false})}
                                        className={`rounded border-[1.5px] border-stroke bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary`}
                                    />
                                </div>

                                <div className='col-span-12 md:col-span-6 xl:col-span-4 flex gap-4 items-center'>
                                    <label className="mb-2 text-black dark:text-white">
                                        {t('tier')}
                                    </label>
                                    <input
                                        type="text"
                                        {...register('cage_tier', {required: false})}
                                        className={`rounded border-[1.5px] border-stroke bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary`}
                                    />
                                </div>

                                <div className='col-span-12 md:col-span-6 xl:col-span-4 flex gap-4 items-center'>
                                    <label className="mb-2 text-black dark:text-white">
                                        {t('number_of_eggs')}
                                    </label>
                                    <input
                                        type="text"
                                        {...register('egg_count', {required: false})}
                                        className={`rounded border-[1.5px] border-stroke bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary`}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="-mx-3 flex flex-wrap gap-y-4">
                            <div className="2xsm:w-1/3 w-full px-3">
                                <button
                                    type="submit"
                                    name="update_egg"
                                    className="block w-full rounded border border-primary bg-primary py-2 px-4 text-center font-medium text-white transition hover:bg-opacity-90"
                                >
                                    {t('save_changes')}
                                </button>
                            </div>

                            <div className="2xsm:w-1/3 w-full px-3">
                                <button
                                    type="button"
                                    onClick={() => reset()}
                                    className="block w-full rounded border border-stroke bg-gray py-2 px-4 text-center font-medium text-black transition hover:border-primary hover:bg-primary hover:text-white dark:border-strokedark dark:bg-meta-4 dark:text-white dark:hover:border-primary dark:hover:bg-primary"
                                >
                                    {t('abort_changes')}
                                </button>
                            </div>

                            <div className="2xsm:w-1/3 w-full px-3">
                                <button
                                    type="button"
                                    onClick={() => onOpenChange(false)}
                                    className="block w-full rounded border border-stroke bg-gray py-2 px-4 text-center font-medium text-black transition hover:border-meta-1 hover:bg-meta-1 hover:text-white dark:border-strokedark dark:bg-meta-4 dark:text-white dark:hover:border-meta-1 dark:hover:bg-meta-1"
                                >
                                    {t('close_window')}
                                </button>
                            </div>
                        </div>

                        {errMsg && (
                            <div
                                className="flex w-full border-[#F87171] bg-[#F87171] bg-opacity-[15%] p-4 mt-6 dark:bg-[#1B1B24] dark:bg-opacity-30">
                                <div className="w-full font-medium text-center text-[#B45454]">{errMsg}</div>
                            </div>
                        )}
                    </form>
                </div>
            </div>
        );
    });

export default EggCorrectionModal;
