import {useRequest} from 'ahooks';
import {useTranslation} from 'react-i18next';
import React, {FC, useEffect, useRef, useState} from 'react';
import {requestToken} from '../../services/request';
import CorrectionModal from "./CorrectionModal.tsx";

const EggListTable: FC<{ tab: any }> = ({tab}) => {
    const {t} = useTranslation();
    const [id, set_id] = useState(null);
    const [deggs, set_deggs] = useState(null);
    const [r_infos, set_r_infos] = useState([]);
    const [t_infos, set_t_infos] = useState([]);
    const [teggs, set_teggs] = useState([]);
    const [rnum, set_rnum] = useState(null);
    const [tnum, set_tnum] = useState(null);
    const [today, set_today] = useState(null);
    const [modalOpen, setModalOpen] = useState(false);

    const trigger = useRef<any>(null);
    const modalRef = useRef<any>(null);

    const {data: eggTable, loading} = useRequest(() => {
        return requestToken({
            url: `/api/egg-d-table/${tab.id}`,
            method: 'GET',
        });
    });

    // close on click outside
    useEffect(() => {
        const clickHandler = ({target}: MouseEvent) => {
            if (!modalRef.current) return;
            if (
                !modalOpen ||
                modalRef.current.contains(target) ||
                trigger.current.contains(target)
            )
                return;
            setModalOpen(false);
        };
        document.addEventListener('click', clickHandler);
        return () => document.removeEventListener('click', clickHandler);
    });

    // close if the esc key is pressed
    useEffect(() => {
        const keyHandler = ({keyCode}: KeyboardEvent) => {
            if (!modalOpen || keyCode !== 27) return;
            setModalOpen(false);
        };
        document.addEventListener('keydown', keyHandler);
        return () => document.removeEventListener('keydown', keyHandler);
    });

    useEffect(() => {
        if (eggTable && Object.keys(eggTable).length > 0) {
            let data: any = {};

            Object.entries(eggTable).forEach(([key, value]) => {
                data[key] = value;
            });

            set_id(data['id']);
            set_deggs(data['deggs']);
            set_r_infos(data['r_infos']);
            set_t_infos(data['t_infos']);
            set_teggs(data['teggs']);
            set_rnum(data['rnum']);
            set_tnum(data['tnum']);
            set_today(data['today']);
        }
    }, [eggTable]);

    return (
        <>
            {loading && (
                <div className="text-center flex flex-1 items-center justify-center py-4">Loading...</div>
            )}

            {!loading && id && (
                <>
                    <div className={'mb-4 flex justify-end gap-2'}>
                        <div className={'flex gap-2'}>
                            <button
                                ref={trigger}
                                onClick={() => setModalOpen(!modalOpen)}
                                className="rounded-md bg-primary py-2 px-4 text-center font-medium text-white hover:bg-opacity-90">
                                {t('correction')}
                            </button>

                            <button onClick={() => window.location.reload()}
                                    className="rounded-md bg-primary py-2 px-4 text-center font-medium text-white hover:bg-opacity-90">
                                {t('refresh')}
                            </button>
                        </div>

                        <CorrectionModal ref={modalRef}  open={modalOpen} onOpenChange={setModalOpen} tab={tab}/>
                    </div>

                    <div
                        className="max-w-full overflow-x-auto rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white">
                        <table className="w-full table-auto" style={{borderCollapse: 'separate', borderSpacing: 0}}>
                            <thead className="sticky top-[0px]">

                            <tr className="bg-gray-2 text-left dark:bg-meta-4">
                                <th className="min-w-[100px] bg-[#eeeee0] whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark">
                                    {today}
                                </th>

                                {(() => {
                                    const arr = [];
                                    for (let i = 0; i < rnum; i++) {
                                        for (let j = 0; j < tnum; j++) {
                                            arr.push(
                                                <>
                                                    <th className="min-w-[100px] bg-white whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark">
                                                        {teggs[i][j]}
                                                    </th>

                                                    <th className="min-w-[100px] bg-white whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark">
                                                        -
                                                    </th>
                                                </>
                                            );
                                        }
                                    }
                                    return arr;
                                })()}
                            </tr>

                            <tr className="bg-gray-2 text-left dark:bg-meta-4">
                                <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark"></th>

                                {(() => {
                                    const arr = [];
                                    for (let r_info of r_infos) {
                                        for (let t_info of t_infos) {
                                            arr.push(
                                                <>
                                                    <th colSpan="2"
                                                        className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark">
                                                        {r_info['CAGE_ROW']} {t('row')} {tnum - t_info['CAGE_TIER'] + 1} {t('tier')}
                                                    </th>
                                                </>
                                            );
                                        }
                                    }
                                    return arr;
                                })()}
                            </tr>

                            <tr className="bg-gray-2 text-left dark:bg-meta-4">
                                <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark"></th>
                                {(() => {
                                    const arr = [];
                                    for (let r_info of r_infos) {
                                        for (let t_info of t_infos) {
                                            arr.push(
                                                <>
                                                    <th className="min-w-[100px] bg-white whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark">
                                                        Eggs
                                                    </th>

                                                    <th className="min-w-[100px] bg-white whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark">
                                                        {t('egg_laying_rate')}
                                                    </th>
                                                </>
                                            );
                                        }
                                    }
                                    return arr;
                                })()}
                            </tr>
                            </thead>

                            <tbody>
                            {deggs[0].map((value: any, index) => {
                                return (
                                    <tr className={index % 2 === 0 ? "bg-[#effcf0]" : "bg-white"}>
                                        <td className="bg-white border-b border-r border-[#eee] dark:border-strokedark py-2 px-2 dark:border-strokedark text-center">
                                            <p className="text-black dark:text-white text-center text-sm">
                                                {deggs[0][index]}
                                            </p>
                                        </td>

                                        {(() => {
                                            const arr = [];
                                            for (let r = 0; r < rnum; r++) {
                                                for (let t = 0; t < tnum; t++) {
                                                    arr.push(
                                                        <>
                                                            <td className="border-b border-r border-[#eee] dark:border-strokedark py-2 px-2 dark:border-strokedark text-center">
                                                                <p className="text-black dark:text-white text-center text-sm">
                                                                    {deggs[(r * tnum + t) * 2 + 1][index]}
                                                                </p>
                                                            </td>

                                                            <td className="border-b border-r border-[#eee] dark:border-strokedark py-2 px-2 dark:border-strokedark text-center">
                                                                <p className="text-black dark:text-white text-center text-sm">
                                                                    {deggs[(r * tnum + t) * 2 + 2][index]}
                                                                </p>
                                                            </td>
                                                        </>
                                                    );
                                                }
                                            }
                                            return arr;
                                        })()}
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                    </div>
                </>
            )}
        </>
    );
};

export default EggListTable;
