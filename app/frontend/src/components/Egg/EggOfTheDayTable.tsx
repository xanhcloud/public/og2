// import { useRequest } from 'ahooks';
import { useTranslation } from 'react-i18next';
import { FC, useEffect, useState } from 'react';

const EggOfTheDayTable: FC<{ listData: any }> = ({ listData }) => {
  const { t } = useTranslation();
  const [id, set_id] = useState(null);
  const [lot_ecount, set_lot_ecount] = useState(null);
  const [norm_min, set_norm_mint] = useState(null);
  const [r_eggs, set_r_eggs] = useState([]);
  const [r_infos, set_r_infos] = useState([]);
  const [t_eggs, set_t_eggs] = useState([]);
  const [t_infos, set_t_infos] = useState([]);
  const [teggs, set_teggs] = useState([]);
  const [layingRates, setLayingRates] = useState([]);
  const [rLayingRates, setRLayingRates] = useState([]);
  const [tLayingRates, setTLayingRates] = useState([]);
  const [lotLayingRate, setLotLayingRate] = useState(0);
  const [showRate, setShowRate] = useState(false);

  // const [tnum, set_tnum] = useState(null);
  // const [total_ecount, set_total_ecount] = useState(null);

  // const { data: eggTable, loading } = useRequest(() => {
  //   return requestToken({
  //     url: `/api/eggtable/${tab.id}`,
  //     method: 'GET',
  //     data: params
  //   });
  // }, {refreshDeps: [params]});

  useEffect(() => {
    if (listData && Object.keys(listData).length > 0) {
      let data: any = {};

      Object.entries(listData).forEach(([key, value]) => {
        data[key] = value;
      });

      set_id(data['id']);
      set_lot_ecount(data['lot_ecount']);
      set_norm_mint(data['norm_min']);
      set_r_eggs(data['r_eggs']);
      set_r_infos(data['r_infos']);
      set_t_eggs(data['t_eggs']);
      set_t_infos(data['t_infos']);
      set_teggs(data['teggs']);
      setLayingRates(data['laying_rates']);
      setRLayingRates(data['r_laying_rates']);
      setTLayingRates(data['t_laying_rates']);
      setLotLayingRate(data['lot_laying_rate']);
      // set_tnum(data['tnum']);
      // set_total_ecount(data['total_ecount']);
    }
  }, [listData]);

  return (
    <>
      <div className={'mb-4 flex justify-between gap-2'}>
        <div className={'flex gap-2'}>
          <button
            onClick={() => setShowRate(false)}
            className="rounded-md bg-primary py-2 px-4 text-center font-medium text-white hover:bg-opacity-90">
            {t('number_of_eggs')}
          </button>

          <button
            onClick={() => setShowRate(true)}
            className="rounded-md bg-primary py-2 px-4 text-center font-medium text-white hover:bg-opacity-90">
            {t('egg_laying_rate')}
          </button>
        </div>
      </div>

      <div className="max-w-full overflow-x-auto rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white">
        {id && (
          <>
            <table className="w-full table-auto">
              <thead className="sticky top-[0px]">
              <tr className="bg-gray-2 text-left dark:bg-meta-4">
                <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-sm"></th>
                {r_infos.map((value: any, key) => {
                  return (
                    <th key={key} className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm">
                      {value['CAGE_ROW']} {t('cage_row_egg_count')}
                    </th>
                  );
                })}
                <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm">
                  {t('tier_row_egg_count')}
                </th>
              </tr>
              </thead>

              <tbody>
              {t_infos.map((value: any, key) => {
                return (
                  <tr key={key}>
                    <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                      <p className="font-medium text-black dark:text-white text-sm whitespace-nowrap">
                        {t_infos[key]['CAGE_TIER']} {t('egg_cage_tier')}
                      </p>
                    </td>

                    {r_infos.map((value2: any, key2) => {
                      return (
                        <td key={key2} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                          <p className={`text-center text-sm ${teggs[key2][key] < norm_min ? 'text-red dark:text-red' : 'text-black dark:text-white'}`}>
                            {!showRate ? teggs[key2][key] : layingRates[key2][key]}
                          </p>
                        </td>
                      );
                    })}

                    <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                      <p className="text-black dark:text-white text-center text-sm">
                        {!showRate ? t_eggs[key] : tLayingRates?.length > 0 && tLayingRates[key]}
                      </p>
                    </td>
                  </tr>
                );
              })}

              <tr>
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <p className="font-medium text-black dark:text-white text-sm whitespace-nowrap">
                    {t('stage_egg_count')}
                  </p>
                </td>

                {r_infos.map((value: any, key) => {
                  return (
                    <td key={key} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                      <p className="text-black dark:text-white text-center text-sm">
                        {!showRate ? r_eggs[key] : rLayingRates?.length > 0 && rLayingRates[key]}
                      </p>
                    </td>
                  );
                })}

                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <p className="text-center text-black dark:text-white text-sm">
                    {lot_ecount ? (!showRate ? lot_ecount : lotLayingRate) : 'None'}
                  </p>
                </td>
              </tr>

              <tr>
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark" colSpan={r_infos.length + 1}>
                  <p className="text-right text-black dark:text-white">
                    {t('lot_egg_count')}:
                  </p>
                </td>

                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <p className="text-center text-black dark:text-white">
                    {lot_ecount ? (!showRate ? lot_ecount : lotLayingRate) : 'None'}
                  </p>
                </td>
              </tr>
              </tbody>
            </table>
          </>
        )}
      </div>
    </>
  );
};

export default EggOfTheDayTable;
