import { useRequest } from 'ahooks';
import { useTranslation } from 'react-i18next';
import { FC, useEffect, useState } from 'react';
import { requestToken } from '../../services/request';

interface List {
  tab: any;
  params: any;
}

const EggSettingTable: FC<List> = ({ tab, params }) => {
  const { t } = useTranslation();
  const [id, set_id] = useState(null);
  const [days, set_days] = useState(null);
  const [deggs, set_deggs] = useState(null);
  const [r_infos, set_r_infos] = useState([]);
  const [t_infos, set_t_infos] = useState([]);
  const [tnum, set_tnum] = useState(null);
  const [rnum, set_rnum] = useState(null);

  const { data: eggTable, loading } = useRequest(() => {
    return requestToken({
      url: `/api/egg-spec-after/${tab.id}`,
      method: 'GET',
      data: params
    });
  }, {refreshDeps: [params]});

  useEffect(() => {
    if (eggTable && Object.keys(eggTable).length > 0) {
      let data: any = {};

      Object.entries(eggTable).forEach(([key, value]) => {
        data[key] = value;
      });

      set_id(data['id']);
      set_days(data['days']);
      set_deggs(data['deggs']);
      set_r_infos(data['r_infos']);
      set_t_infos(data['t_infos']);
      set_rnum(data['r_infos'].length);
      set_tnum(data['t_infos'].length);
    }
  }, [eggTable]);

  return (
    <div className="max-w-full overflow-x-auto rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white">
      {loading && (
        <div className="text-center flex flex-1 items-center justify-center py-4">Loading...</div>
      )}

      {!loading && id && (
        <>
          <table className="w-full table-auto" style={{ borderCollapse: 'separate', borderSpacing: 0 }}>
            <thead className="sticky top-[0px]">

            <tr className="bg-gray-2 text-left dark:bg-meta-4">
              <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark"></th>

              {(() => {
                const arr = [];
                for (let r_info of r_infos) {
                  for (let t_info of t_infos) {
                    arr.push(
                      <>
                        <th colSpan="2" className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark">
                          {r_info['CAGE_ROW']} {t('row')} {tnum - t_info['CAGE_TIER'] + 1} {t('tier')}
                        </th>
                      </>
                    );
                  }
                }
                return arr;
              })()}
            </tr>

            <tr className="bg-gray-2 text-left dark:bg-meta-4">
              <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark"></th>
              {(() => {
                const arr = [];
                for (let r_info of r_infos) {
                  for (let t_info of t_infos) {
                    arr.push(
                      <>
                        <th className="min-w-[100px] bg-white whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark">
                          Eggs
                        </th>

                        <th className="min-w-[100px] bg-white whitespace-nowrap py-2 px-2 font-medium text-center text-black dark:text-white text-sm border-r border-b border-[#eee] dark:border-strokedark">
                          {t('egg_laying_rate')}
                        </th>
                      </>
                    );
                  }
                }
                return arr;
              })()}
            </tr>
            </thead>

            <tbody>
            {deggs[0].map((value: any, index) => {
              return (
                <tr className={index % 2 === 0 ? 'bg-[#effcf0]' : 'bg-white'}>
                  <td className="bg-white border-b border-r border-[#eee] dark:border-strokedark py-2 px-2text-center">
                    <p className="text-black dark:text-white text-center text-sm">
                      {deggs[0][index]}
                    </p>
                  </td>

                  {(() => {
                    const arr = [];
                    for (let r = 0; r < rnum; r++) {
                      for (let t = 0; t < tnum; t++) {
                        arr.push(
                          <>
                            <td className="border-b border-r border-[#eee] dark:border-strokedark py-2 px-2 text-center">
                              <p className="text-black dark:text-white text-center text-sm">
                                {deggs[(r * tnum + t) * 2 + 1][index]}
                              </p>
                            </td>

                            <td className="border-b border-r border-[#eee] dark:border-strokedark py-2 px-2 text-center">
                              <p className="text-black dark:text-white text-center text-sm">
                                {deggs[(r * tnum + t) * 2 + 2][index]}
                              </p>
                            </td>
                          </>
                        );
                      }
                    }
                    return arr;
                  })()}
                </tr>
              );
            })}
            </tbody>
          </table>
        </>
      )}
    </div>
  );
};

export default EggSettingTable;
