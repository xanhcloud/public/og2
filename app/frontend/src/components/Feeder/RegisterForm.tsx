import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request';
import { FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { SubmitHandler, useForm, Controller } from 'react-hook-form';
import Datepicker, { DateValueType } from 'react-tailwindcss-datepicker';
import { getLanguageObject } from '../../services/Helpers.ts';

interface List {
  tab: any;
}

const RegisterForm: FC<List> = ({ tab }) => {
  const { t } = useTranslation();
  const [message, setMessage] = useState(null);
  const locale = getLanguageObject();

  const { control, register, handleSubmit, reset } = useForm({
    defaultValues: {},
  });

  const { data, loading } = useRequest(() => {
    return requestToken({
      url: `/api/fooder-reg/${tab.id}`,
      method: 'GET',
    });
  }, {refreshDeps: [tab.id]});

  // Show alert timeout
  useEffect(() => {
    if (message) {
      const timeout = setTimeout(() => setMessage(null), 2000);
      return () => {
        clearTimeout(timeout);
      };
    }
  }, [message]);

  // Handle submit
  const handleOnSubmit: SubmitHandler<any> = async (data: any) => {
    try {
      const result = await requestToken({
        method: 'POST',
        url: `/api/fooder-reg/${tab.id}`,
        data
      });
      if (result && result.r_msg) {
        setMessage(result.r_msg);
      }
    } catch (e) {
      console.log('Error', e);
    }
  }

  return (
    <>
      {message && (
        <div className="flex items-center w-full border-l-6 border-[#34D399] bg-[#34D399] bg-opacity-[15%] shadow-md dark:bg-[#1B1B24] dark:bg-opacity-30 p-4 mb-4">
          <div className="mr-5 flex h-9 w-full max-w-[36px] items-center justify-center rounded-lg bg-[#34D399]">
            <svg
              width="16"
              height="12"
              viewBox="0 0 16 12"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M15.2984 0.826822L15.2868 0.811827L15.2741 0.797751C14.9173 0.401867 14.3238 0.400754 13.9657 0.794406L5.91888 9.45376L2.05667 5.2868C1.69856 4.89287 1.10487 4.89389 0.747996 5.28987C0.417335 5.65675 0.417335 6.22337 0.747996 6.59026L0.747959 6.59029L0.752701 6.59541L4.86742 11.0348C5.14445 11.3405 5.52858 11.5 5.89581 11.5C6.29242 11.5 6.65178 11.3355 6.92401 11.035L15.2162 2.11161C15.5833 1.74452 15.576 1.18615 15.2984 0.826822Z"
                fill="white"
                stroke="white"
              ></path>
            </svg>
          </div>
          <div className="w-full">
            <p className="text-base leading-relaxed text-black">
              {message}
            </p>
          </div>
        </div>
      )}

      <div className="max-w-full rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white p-4">
        {loading && (
          <div className="text-center flex flex-1 items-center justify-center py-4">Loading...</div>
        )}

        {!loading && (
          <form>
            <div className="grid grid-cols-12 gap-4 mb-4">
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('flock')}</p>
                  </div>

                  <div className="col-span-12 md:col-span-4 xl:col-span-3 relative z-20">
                    <select {...register('fooder_id')}
                            className="relative z-20 w-full text-black appearance-none rounded border border-stroke bg-transparent py-1 px-3 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input">
                      <option value="" className="text-body dark:text-bodydark"></option>
                      {data && data.fooders && data.fooders.length > 0 && data.fooders.map((fooder: any) => (
                        <option key={fooder.MFOODER_ID} value={fooder.MFOODER_ID}>{fooder.FOODER_NO}</option>
                      ))}
                    </select>
                    <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g opacity="0.8">
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                            fill="#637381"
                          ></path>
                        </g>
                      </svg>
                    </span>
                  </div>

                  <div className="col-span-12 md:col-span-4 xl:col-span-2">
                    <button
                      onClick={handleSubmit(async (data) => handleOnSubmit({ ...data, select: t('confirm_feeders') }))}
                      type="button"
                      className="block w-full rounded bg-primary py-1 px-3 text-center font-medium text-white transition hover:bg-opacity-90"
                    >
                      {t('confirm_feeders')}
                    </button>
                  </div>
                </div>
              </div>

              {/*col*/}
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('start_time')}</p>
                  </div>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <input
                      {...register('start_date')}
                      type="time"
                      className="w-full rounded border-[1.5px] border-stroke bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary"/>
                  </div>
                </div>
              </div>

              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('effective_date')}</p>
                  </div>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <Controller
                      name="apply_date"
                      control={control}
                      rules={{ required: false }}
                      render={({ field }) => (
                        <Datepicker
                          i18n={locale.code}
                          asSingle={true}
                          useRange={false}
                          primaryColor={'blue'}
                          placeholder={' '}
                          value={{
                            startDate: field.value || null,
                            endDate: field.value || null
                          }}
                          onChange={(newValue: DateValueType) => {
                            field.onChange(newValue.startDate);
                          }}
                          inputClassName={`w-full rounded border-[1.5px] border-stroke bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary`}
                        />
                      )}
                    />
                  </div>
                </div>
              </div>
            </div>

            <div className="-mx-3 flex flex-wrap gap-y-4 mt-6">
              <div className="2xsm:w-1/2 md:w-1/3 xl:w-1/4 2xl:w-1/5 px-3">
                <button
                  type="button"
                  onClick={handleSubmit(async (data) => await handleOnSubmit({ ...data, register: t('save_changes') }))}
                  className="block w-full rounded border border-primary bg-primary py-2 px-4 text-center font-medium text-white transition hover:bg-opacity-90"
                >
                  {t('save_changes')}
                </button>
              </div>
              <div className="2xsm:w-1/2 md:w-1/3 xl:w-1/4 2xl:w-1/5 px-3">
                <button
                  type="button"
                  onClick={() => reset()}
                  className="block w-full rounded border border-stroke bg-gray py-2 px-4 text-center font-medium text-black transition hover:border-primary hover:bg-primary hover:text-white dark:border-strokedark dark:bg-meta-4 dark:text-white dark:hover:border-primary dark:hover:bg-primary"
                >
                  {t('abort_changes')}
                </button>
              </div>
            </div>
          </form>
        )}
      </div>
    </>
  );
};

export default RegisterForm;
