import { FC, FormEventHandler, ReactNode, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Controller, useFormContext } from 'react-hook-form';
import Datepicker, { DateValueType } from 'react-tailwindcss-datepicker';
import moment from 'moment';
import { getLanguageObject, objKeyToLower } from '../services/Helpers.ts';
import { FieldItem } from '../types/field.ts';

interface Form {
  fields: FieldItem[];
  record?: any;
  children?: ReactNode;
  onSubmit?: FormEventHandler<HTMLFormElement>;
}

const FormLayout: FC<Form> = ({ fields, record, children, onSubmit }) => {
  const { t } = useTranslation();
  const dateFormat = 'YYYY-MM-DD';
  const locale = getLanguageObject();

  const { control, register, getValues, setValue, reset, formState: { errors } } = useFormContext();

  // Form initial value
  useEffect(() => {
    if (record && Object.keys(record).length > 0) {
      const defaultValues = getValues();
      const values = objKeyToLower(record);

      Object.keys(defaultValues).forEach((key: string) => {
        setValue(key, values[key.toLowerCase()] || null);
      });
    } else {
      reset();
    }
  }, [record]);

  const DropdownSuffix = ({className = ''}) => (
    <span className={className || 'absolute top-1/2 right-4 z-10 -translate-y-1/2'}>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g opacity="0.8">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
            fill="#637381"
          ></path>
        </g>
      </svg>
    </span>
  );

  return (
    <form onSubmit={onSubmit}>
      <div className="grid grid-cols-12 gap-4 mb-4">
        {fields && fields.length > 0 && fields.map((field: FieldItem) => {
          switch (field.type) {
            case 'enum':
              return (
                <div key={field.key} className={field.className}>
                  <label className="mb-2 block text-black dark:text-white">
                    {t(field.label)}
                  </label>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3 relative z-5">
                    {field?.prefix || <></>}
                    <select
                      {...register(field.key, { required: field.required })}
                      className={`relative z-5 w-full text-black appearance-none rounded border bg-transparent py-1 px-3 outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input 
                              ${errors[field.key]?.type === 'required' ? 'border-danger' : 'border-stroke'}`}
                      disabled={!!field?.disabled}
                    >
                      {field.options && field.options.length > 0 && field.options.map((item: any, index: number) => (
                        <option
                          key={field?.indexValue ? index : (field?.valueKey ? item[field.valueKey] : item)}
                          value={field?.indexValue ? index : (field?.valueKey ? item[field.valueKey] : item)}
                        >
                          {field?.labelKey ? t(item[field.labelKey]) : item}
                        </option>
                      ))}
                    </select>
                    {field?.suffix || <DropdownSuffix />}
                  </div>
                </div>
              )
            case 'date':
              return (
                <div key={field.key} className={field.className}>
                  <label className="mb-2 block text-black dark:text-white">
                    {t(field.label)}
                  </label>
                  <Controller
                    name={field.key}
                    control={control}
                    rules={{ required: field.required }}
                    render={({ field: fieldRender }) => (
                      <Datepicker
                        i18n={locale.code}
                        asSingle={true}
                        useRange={false}
                        primaryColor={'blue'}
                        placeholder={field.placeholder || ' '}
                        value={{
                          startDate: moment(fieldRender.value, dateFormat).isValid() ? fieldRender.value : null,
                          endDate: moment(fieldRender.value, dateFormat).isValid() ? fieldRender.value : null,
                        }}
                        onChange={(newValue: DateValueType) => {
                          fieldRender.onChange(newValue.startDate);
                        }}
                        inputClassName={`w-full rounded border-[1.5px] bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary 
                                ${errors[field.key]?.type === 'required' ? 'border-danger' : 'border-stroke'}`}
                        disabled={!!field?.disabled}
                        readOnly={!!field?.readonly}
                      />
                    )}
                  />
                </div>
              )
            case 'time_range':
              return (
                <div key={field.key} className={field.className}>
                  <label className="mb-2 block text-black dark:text-white">
                    {t(field.label)}
                  </label>
                  <div className="grid grid-cols-12 gap-4">
                    <div className="col-span-12 md:col-span-6 relative z-5">
                      <select
                        {...register(field.startKey, { required: field.required })}
                        className={`relative z-5 w-full text-black appearance-none rounded border bg-transparent py-1 px-3 outline-none transition focus:border-primary disabled:cursor-default disabled:bg-whiter active:border-primary dark:border-form-strokedark dark:bg-form-input 
                              ${errors[field.startKey]?.type === 'required' ? 'border-danger' : 'border-stroke'}`}
                        disabled={!!field?.disabled}
                      >
                        {field.options && field.options.length > 0 && field.options.map((item: any, index: number) => (
                          <option
                            key={field?.indexValue ? index : (field?.valueKey ? item[field.valueKey] : item)}
                            value={field?.indexValue ? index : (field?.valueKey ? item[field.valueKey] : item)}
                          >
                            {field?.labelKey ? t(item[field.labelKey]) : item}
                          </option>
                        ))}
                      </select>
                      <DropdownSuffix />
                    </div>
                    <div className="col-span-12 md:col-span-6 relative z-5">
                      <select
                        {...register(field.endKey, { required: field.required })}
                        className={`relative z-5 w-full text-black appearance-none rounded border bg-transparent py-1 px-3 outline-none transition focus:border-primary disabled:cursor-default disabled:bg-whiter active:border-primary dark:border-form-strokedark dark:bg-form-input 
                              ${errors[field.endKey]?.type === 'required' ? 'border-danger' : 'border-stroke'}`}
                        disabled={!!field?.disabled}
                      >
                        {field.options && field.options.length > 0 && field.options.map((item: any, index: number) => (
                          <option
                            key={field?.indexValue ? index : (field?.valueKey ? item[field.valueKey] : item)}
                            value={field?.indexValue ? index : (field?.valueKey ? item[field.valueKey] : item)}
                          >
                            {field?.labelKey ? t(item[field.labelKey]) : item}
                          </option>
                        ))}
                      </select>
                      <DropdownSuffix />
                    </div>
                  </div>
                </div>
              )
            case 'number':
              return (
                <div key={field.key} className={field.className}>
                  <label className="mb-2 block text-black dark:text-white">
                    {t(field.label)}
                  </label>
                  <input
                    type="number"
                    {...register(field.key, { required: field.required })}
                    placeholder={field?.placeholder || ''}
                    className={`w-full rounded border-[1.5px] bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary [appearance:textfield] [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:appearance-none
                            ${errors[field.key]?.type === 'required' ? 'border-danger' : 'border-stroke'}`}
                    disabled={!!field?.disabled}
                    readOnly={!!field?.readonly}
                  />
                </div>
              );
            default:
              return (
                <div key={field.key} className={field.className}>
                  <label className="mb-2 block text-black dark:text-white">
                    {t(field.label)}
                  </label>
                  <input
                    type="text"
                    {...register(field.key, { required: field.required })}
                    placeholder={field?.placeholder || ''}
                    className={`w-full rounded border-[1.5px] bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary
                            ${errors[field.key]?.type === 'required' ? 'border-danger' : 'border-stroke'}`}
                    disabled={!!field?.disabled}
                    readOnly={!!field?.readonly}
                  />
                </div>
              );
          }
        })}
      </div>

      {children}
    </form>
  );
};

export default FormLayout;
