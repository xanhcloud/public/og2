import { FC, useEffect, useState } from 'react';
import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request.ts';
import { humanize, listColor } from '../../services/Helpers.ts';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  TimeScale,
} from 'chart.js';
import zoomPlugin from 'chartjs-plugin-zoom';
import { Line } from 'react-chartjs-2';
import 'chartjs-adapter-moment';

ChartJS.register(zoomPlugin);
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  TimeScale,
);

const Day3Chart: FC<{tab: any, params: any}> = ({tab, params}) => {
  const [chartData, setChartData] = useState({
    labels: [],
    datasets: []
  });

  const { data: graphs, loading } = useRequest(() => {
    return requestToken({
      url: `/api/graph-3days/${tab.id}`,
      method: 'GET',
      data: params
    });
  });

  // Get chart data
  useEffect(() => {
    const getData = async () => {
      // X data
      if (graphs && graphs.graphs_data_x && graphs.graphs_data_x.length > 0) {
        setChartData((prev) => ({...prev, labels: graphs.graphs_data_x}))
      }

      // Left Y data
      let leftData: any = [];
      if (graphs && graphs.left_data_y && Object.keys(graphs.left_data_y).length > 0) {
        await Promise.all(Object.entries(graphs.left_data_y).map(([key, value]) => {
          leftData.push({
            label: humanize(key),
            data: value,
            yAxisID: 'y',
            borderWidth: 1
          })
        }));
      }

      // Left Y data
      let rightData: any = [];
      if (graphs && graphs.right_data_y && Object.keys(graphs.right_data_y).length > 0) {
        await Promise.all(Object.entries(graphs.right_data_y).map(([key, value]) => {
          rightData.push({
            label: humanize(key),
            data: value,
            yAxisID: 'y1',
            borderWidth: 1
          })
        }));
      }

      // Set colors
      const seriesData: any = [...leftData, ...rightData];
      seriesData.map((yList: any, index: number) => {
        yList.borderColor = listColor[index];
        yList.backgroundColor = listColor[index];
      });

      // Set chart
      setChartData((prev) => ({...prev, datasets: seriesData}));
    }
    getData();
  }, [loading]);

  // Chart options
  const config: any = {
    responsive: true,
    interaction: {
      mode: 'index',
      intersect: false,
    },
    stacked: false,
    plugins: {
      title: {
        display: false,
        text: ''
      },
    },
    scales: {
      y: {
        type: 'linear',
        display: true,
        position: 'left',
      },
      y1: {
        type: 'linear',
        display: true,
        position: 'right',
        grid: {
          drawOnChartArea: false, // only want the grid lines for one axis to show up
        }
      },
      x: {
        type: 'time',
        time: {
          unit: 'minute',
          displayFormats: {
            'minute': 'HH:mm',
          }
        },
      },
    },
    maintainAspectRatio: false
  };

  return (
    <Line options={config} data={chartData}/>
  );
};

export default Day3Chart;
