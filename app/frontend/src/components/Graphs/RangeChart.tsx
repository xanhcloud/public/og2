import { FC, useEffect, useState } from 'react';
import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request.ts';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import { ChartDataOption } from '../../types/chart.ts';
import { humanize, listColor } from '../../services/Helpers.ts';
import zoomPlugin from 'chartjs-plugin-zoom';

ChartJS.register(zoomPlugin);
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

const GraphRangeChart: FC<{tab: any}> = ({tab}) => {
  const [chartData, setChartData] = useState({
    labels: [],
    datasets: []
  });

  const { data: graphs, loading } = useRequest(() => {
    return requestToken({
      url: `/api/graph-spec-after/${tab.id}`,
      method: 'GET',
    });
  });

  // Get chart data
  useEffect(() => {
    const getData = async () => {
      if (graphs && graphs.graph1) {
        const graph1: ChartDataOption = graphs.graph1;

        // X data
        if (graph1.graphs_data_x && graph1.graphs_data_x.length > 0) {
          setChartData((prev) => ({...prev, labels: graph1.graphs_data_x}))
        }

        // Left Y data
        let leftData: any = [];
        if (graph1.left_data_y && Object.keys(graph1.left_data_y).length > 0) {
          await Promise.all(Object.entries(graph1.left_data_y).map(([key, value]) => {
            leftData.push({
              label: humanize(key),
              data: value,
              yAxisID: 'y',
              borderWidth: 1
            })
          }));
        }

        // Left Y data
        let rightData: any = [];
        if (graph1.right_data_y && Object.keys(graph1.right_data_y).length > 0) {
          await Promise.all(Object.entries(graph1.right_data_y).map(([key, value]) => {
            rightData.push({
              label: humanize(key),
              data: value,
              yAxisID: 'y1',
              borderWidth: 1
            })
          }));
        }

        // Set colors
        const seriesData: any = [...leftData, ...rightData];
        seriesData.map((yList: any, index: number) => {
          yList.borderColor = listColor[index];
          yList.backgroundColor = listColor[index];
        });

        // Set chart
        setChartData((prev) => ({...prev, datasets: seriesData}));
      }
    }
    getData();
  }, [loading]);

  const config: any = {
    responsive: true,
    interaction: {
      mode: 'index',
      intersect: false,
    },
    stacked: false,
    plugins: {
      title: {
        display: false,
        text: ''
      },
      // zoom: {
      //   zoom: {
      //     wheel: {
      //       enabled: true,
      //     },
      //     pinch: {
      //       enabled: true
      //     },
      //     mode: 'xy',
      //   }
      // }
    },
    scales: {
      y: {
        type: 'linear',
        display: true,
        position: 'left',
      },
      y1: {
        type: 'linear',
        display: true,
        position: 'right',
        grid: {
          drawOnChartArea: false, // only want the grid lines for one axis to show up
        },
      },
    },
    maintainAspectRatio: false
  };

  return (
    <Line options={config} data={chartData} />
  );
};

export default GraphRangeChart;
