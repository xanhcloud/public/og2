import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request';
import { FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FormProvider, SubmitHandler, useForm } from 'react-hook-form';
import { getTimeSlots } from '../../services/Helpers.ts';
import { HengrpList, House } from '../../types/batch.ts';
import FormLayout from '../FormLayout.tsx';
import { FieldItem } from '../../types/field.ts';

interface List {
  id: number;
  record: HengrpList;
  houses: House[];
  onFished: (res: HengrpList) => void;
  className?: string | undefined;
  isModalClosed?: boolean;
  modalOpenChange?: (open: boolean) => void;
}

const BatchFormCreate: FC<List> = ({ id, record, houses, onFished, className, isModalClosed, modalOpenChange }) => {
  const { t } = useTranslation();
  const [timeSlots, setTimeSlots] = useState<string[]>([]);
  const [recordData, setRecordData] = useState<any>(null);
  const formMethods = useForm();

  // Watch house field
  const houseId = formMethods.watch('house_id');
  const recordHouseId = record && record.batch && record.batch.length > 0 && record.batch[0].HOUSE_ID || 0;

  const { data: hengrpData, loading: hengrpLoading } = useRequest(() => {
    return requestToken({
      url: '/api/hengrp-reg',
      method: 'GET',
    });
  });

  const { data: lotData } = useRequest(() => {
    if (parseInt(houseId) > 0 || recordHouseId > 0) {
      return requestToken({
        url: '/api/lots',
        method: 'GET',
        data: {
          house_id: recordHouseId || parseInt(houseId),
          not_used: id < 1 ? 1 : null
        }
      });
    }
  }, {refreshDeps: [houseId, recordHouseId]});

  // Form fields
  const fields: FieldItem[] = [
    {
      key: 'hengrp_id',
      label: 'flock',
      type: 'enum',
      required: id > 0,
      className: `col-span-12 md:col-span-6 ${id < 1 ? 'hidden' : ''}`,
      options: [...[{HENGRP_ID: 0, HENGRP_NAME: ''}], ...hengrpData?.hengrps || []],
      valueKey: 'HENGRP_ID',
      labelKey: 'HENGRP_NAME',
      disabled: id < 1
    },
    {
      key: 'hengrp_name',
      label: 'new_group',
      type: 'text',
      required: id < 1,
      className: `col-span-12 md:col-span-6 ${id > 0 && 'hidden'}`,
      disabled: id > 0
    },
    {
      key: 'house_id',
      label: 'chicken_house',
      type: 'enum',
      required: true,
      className: 'col-span-12 md:col-span-6',
      options: [...[{HOUSE_ID: 0, HOUSE_NAME: ''}], ...houses || []],
      valueKey: 'HOUSE_ID',
      labelKey: 'HOUSE_NAME'
    },
    {
      key: 'lot_id',
      label: 'lot',
      type: 'enum',
      required: true,
      className: 'col-span-12 md:col-span-6',
      options: lotData?.lots || [],
      valueKey: 'LOT_ID',
      labelKey: 'LOT_NAME'
    },
    {
      key: 'hen_id',
      label: 'chicken',
      type: 'enum',
      required: true,
      className: 'col-span-12 md:col-span-6',
      options: [...[{HEN_ID: 0, HEN_NAME: ''}], ...hengrpData?.hens || []],
      valueKey: 'HEN_ID',
      labelKey: 'HEN_NAME'
    },
    {
      key: 'growth_kind',
      label: 'growth_segment',
      type: 'enum',
      required: true,
      className: 'col-span-12 md:col-span-6',
      options: [
        {value: 0, label: ''},
        {value: 1, label: 'brooder'},
        {value: 2, label: 'pullet'},
        {value: 3, label: 'layer'}
      ],
      valueKey: 'value',
      labelKey: 'label'
    },
    {
      key: 'start_date',
      label: 'batch_start_date',
      type: 'date',
      required: true,
      className: 'col-span-12 md:col-span-6',
    },
    {
      key: 'feeding_date',
      label: 'feeding_period',
      type: 'date',
      required: true,
      className: 'col-span-12 md:col-span-6',
    },
    {
      key: 'initial_count',
      label: 'batch_initial_count',
      type: 'number',
      required: true,
      className: 'col-span-12 md:col-span-6',
    },
    {
      key: 'end_date',
      label: 'batch_end_date',
      type: 'date',
      required: false,
      className: `col-span-12 md:col-span-6 hidden`,
      disabled: true
    },
    {
      key: 'egg_time',
      label: 'egg_count_by_hour',
      type: 'time_range',
      required: true,
      className: 'col-span-12 md:col-span-6',
      options: [...[""], ...timeSlots],
      startKey: 'eggcnt_stime',
      endKey: 'eggcnt_etime',
      indexValue: true
    },
  ];

  // Form initial value
  useEffect(() => {
    if (!hengrpLoading && fields.length > 0) {
      let item = {};
      if (record && record.batch && record.batch.length > 0) {
        item = record.batch[0];
      }
      setRecordData(item);
    }
  }, [record, hengrpData]);

  // Get timeslots
  useEffect(() => {
    setTimeSlots(getTimeSlots());
  }, []);

  // Handle submit
  const handleOnSubmit: SubmitHandler<any> = async (data: any) => {
    try {
      const result = await requestToken({
        method: 'POST',
        url: '/api/hengrp-reg',
        data
      });

      if (result) {
        onFished && onFished(result);
      }
    } catch (e: any) {
      onFished && onFished(e);
    }
  }

  return (
    <>
      <div className={className || 'max-w-full rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white p-4'}>
        <FormProvider {...formMethods}>
          <FormLayout fields={fields} record={recordData}>
            <div className="-mx-3 flex flex-wrap gap-y-4 mt-6 justify-center">
              <div className="2xsm:w-1/3 w-full px-3">
                <button
                  type="button"
                  onClick={formMethods.handleSubmit(handleOnSubmit)}
                  className="block w-full rounded border border-primary bg-primary py-2 px-4 text-center font-medium text-white transition hover:bg-opacity-90"
                >
                  {t('save_changes')}
                </button>
              </div>
              {/*<div className="2xsm:w-1/3 w-full px-3">*/}
              {/*  <button*/}
              {/*    type="button"*/}
              {/*    onClick={() => formMethods.reset()}*/}
              {/*    className="block w-full rounded border border-stroke bg-gray py-2 px-4 text-center font-medium text-black transition hover:border-primary hover:bg-primary hover:text-white dark:border-strokedark dark:bg-meta-4 dark:text-white dark:hover:border-primary dark:hover:bg-primary"*/}
              {/*  >*/}
              {/*    {t('abort_changes')}*/}
              {/*  </button>*/}
              {/*</div>*/}
              {isModalClosed && (
                <div className="2xsm:w-1/3 w-full px-3">
                  <button
                    type="button"
                    onClick={() => modalOpenChange && modalOpenChange(false)}
                    className="block w-full rounded border border-stroke bg-gray py-2 px-4 text-center font-medium text-black transition hover:border-meta-1 hover:bg-meta-1 hover:text-white dark:border-strokedark dark:bg-meta-4 dark:text-white dark:hover:border-meta-1 dark:hover:bg-meta-1"
                  >
                    {t('close_window')}
                  </button>
                </div>
              )}
            </div>
          </FormLayout>
        </FormProvider>
      </div>
    </>
  );
};

export default BatchFormCreate;
