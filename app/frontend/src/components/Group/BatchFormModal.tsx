import { FC, useEffect, useRef, useState } from 'react';
import BatchFormEdit from './BatchFormEdit.tsx';
import BatchFormCreate from './BatchFormCreate.tsx';
import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request.ts';
import { HengrpList } from '../../types/batch.ts';

interface BatchFormModalProps {
  id: number;
  open: boolean;
  isStartBatch: boolean;
  isEndBatch: boolean;
  onOpenChange: (open: boolean) => void;
  onRefresh: (refresh: any) => void;
}

const BatchFormModal: FC<BatchFormModalProps> = ({id, open, isStartBatch, isEndBatch,onOpenChange, onRefresh}) => {
  const ref = useRef<any>(null);
  const [message, setMessage] = useState<string>('');

  const { data: recordData } = useRequest(() => {
    if (!open || !id || id < 1) return;

    return requestToken({
      url: '/api/hengrp-upd',
      method: 'GET',
      data: {sel_batch: id}
    });
  }, {refreshDeps: [id]});

  const { data: housesData } = useRequest(() => {
    return requestToken({
      url: '/api/houses',
      method: 'GET',
    });
  });

  // Close modal if clicked outside
  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (ref.current && !ref.current.contains(event.target)) {
        onOpenChange(false);
      }
    };

    // Attach event listener when the modal is open
    if (open) {
      document.addEventListener('mousedown', handleClickOutside);
    }

    // Clean up the event listener when the modal is closed
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [open]);

  // close if the esc key is pressed
  useEffect(() => {
    const keyHandler = ({ keyCode }: KeyboardEvent) => {
      if (!open || keyCode !== 27) return;
      onOpenChange(false);
    };
    document.addEventListener('keydown', keyHandler);
    return () => document.removeEventListener('keydown', keyHandler);
  });

  // Show alert timeout
  useEffect(() => {
    if (message) {
      const timeout = setTimeout(() => setMessage(''), 2000);
      return () => {
        clearTimeout(timeout);
      };
    }
  }, [message]);

  return (
    <div className={`fixed left-0 top-0 z-999999 flex h-full min-h-screen w-full items-center justify-center bg-black/90 px-4 py-5 ${open ? 'block' : 'hidden'}`}>
      <div
        ref={ref}
        onFocus={() => onOpenChange(true)}
        className="w-full max-w-[1000px] max-h-[100%] rounded-lg bg-white dark:bg-boxdark overflow-y-auto"
      >
        {message && (
          <div className={`flex items-center border-l-6 bg-opacity-[15%] shadow-md dark:bg-[#1B1B24] dark:bg-opacity-30 p-4 m-4 ${message === 'Error' ? 'border-[#F87171] bg-[#F87171]' : 'border-[#34D399] bg-[#34D399]'}`}>
            <div className={`mr-5 flex h-9 w-full max-w-[36px] items-center justify-center rounded-lg ${message === 'Error' ? 'bg-[#F87171]' : 'bg-[#34D399]'}`}>
              {message === 'Error' ? (
                <svg
                  width="13"
                  height="13"
                  viewBox="0 0 13 13"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M6.4917 7.65579L11.106 12.2645C11.2545 12.4128 11.4715 12.5 11.6738 12.5C11.8762 12.5 12.0931 12.4128 12.2416 12.2645C12.5621 11.9445 12.5623 11.4317 12.2423 11.1114C12.2422 11.1113 12.2422 11.1113 12.2422 11.1113C12.242 11.1111 12.2418 11.1109 12.2416 11.1107L7.64539 6.50351L12.2589 1.91221L12.2595 1.91158C12.5802 1.59132 12.5802 1.07805 12.2595 0.757793C11.9393 0.437994 11.4268 0.437869 11.1064 0.757418C11.1063 0.757543 11.1062 0.757668 11.106 0.757793L6.49234 5.34931L1.89459 0.740581L1.89396 0.739942C1.57364 0.420019 1.0608 0.420019 0.740487 0.739944C0.42005 1.05999 0.419837 1.57279 0.73985 1.89309L6.4917 7.65579ZM6.4917 7.65579L1.89459 12.2639L1.89395 12.2645C1.74546 12.4128 1.52854 12.5 1.32616 12.5C1.12377 12.5 0.906853 12.4128 0.758361 12.2645L1.1117 11.9108L0.758358 12.2645C0.437984 11.9445 0.437708 11.4319 0.757539 11.1116C0.757812 11.1113 0.758086 11.111 0.75836 11.1107L5.33864 6.50287L0.740487 1.89373L6.4917 7.65579Z"
                    fill="#ffffff"
                    stroke="#ffffff"
                  ></path>
                </svg>
              ) : (
                <svg
                  width="16"
                  height="12"
                  viewBox="0 0 16 12"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M15.2984 0.826822L15.2868 0.811827L15.2741 0.797751C14.9173 0.401867 14.3238 0.400754 13.9657 0.794406L5.91888 9.45376L2.05667 5.2868C1.69856 4.89287 1.10487 4.89389 0.747996 5.28987C0.417335 5.65675 0.417335 6.22337 0.747996 6.59026L0.747959 6.59029L0.752701 6.59541L4.86742 11.0348C5.14445 11.3405 5.52858 11.5 5.89581 11.5C6.29242 11.5 6.65178 11.3355 6.92401 11.035L15.2162 2.11161C15.5833 1.74452 15.576 1.18615 15.2984 0.826822Z"
                    fill="white"
                    stroke="white"
                  ></path>
                </svg>
              )}
            </div>
            <div className="w-full">
              <p className="text-base leading-relaxed text-black">
                {message}
              </p>
            </div>
          </div>
        )}

        {open && (
          <>
            {isStartBatch ? (
              <BatchFormCreate
                id={id}
                record={recordData}
                houses={housesData?.houses || []}
                onFished={(res: HengrpList) => {
                  if (res && res.hengrps) {
                    setMessage('Successfully');
                    onOpenChange(false);
                    onRefresh((r: number) => r + 1);
                  } else {
                    setMessage('Error');
                  }
                }}
                isModalClosed={true}
                modalOpenChange={onOpenChange}
              />
            ) : (
              <BatchFormEdit
                id={id}
                record={recordData}
                houses={housesData?.houses || []}
                onFished={(res: HengrpList) => {
                  if (res && res.batch) {
                    setMessage('Successfully');
                    onOpenChange(false);
                    onRefresh((r: number) => r + 1);
                  } else {
                    setMessage('Error');
                  }
                }}
                isModalClosed={true}
                modalOpenChange={onOpenChange}
                isEndBatch={isEndBatch}
              />
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default BatchFormModal;
