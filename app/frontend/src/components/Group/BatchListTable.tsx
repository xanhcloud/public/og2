import { FC, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { BatchList } from '../../types/batch.ts';
import { growthSegments } from '../../services/Helpers.ts';
import moment from 'moment';
import { FiEdit, FiTrash } from 'react-icons/fi';
import { Link } from 'react-router-dom';

interface List {
  data: BatchList[];
  loading?: boolean;
  rowSelectEnable?: boolean;
  onRowSelected?: (val: any) => void;
  handleEditAction?: (row: BatchList) => void;
  handleRemoveAction?: (row: BatchList) => void;
  handleStartBatch?: (row: BatchList) => void;
  handleEndBatch?: (row: BatchList) => void;
}

const BatchListTable: FC<List> = ({ data, loading, rowSelectEnable = true, onRowSelected, handleEditAction, handleRemoveAction, handleStartBatch, handleEndBatch }) => {
  const { t } = useTranslation();
  const [row, setRow] = useState<any>(null);

  return (
    <>
      <div className="max-w-full overflow-x-auto rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white">
        <table className="w-full table-auto">
          <thead className="sticky top-[0px]">
          <tr className="bg-gray-2 text-left dark:bg-meta-4">
            {rowSelectEnable && (
              <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-sm cursor-pointer"></th>
            )}
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('chicken_house')}</div>
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('lot')}</div>
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('group')}</div>
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('feeding_date')}</div>
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('chicken')}</div>
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('growth_segment')}</div>
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('revenue')}</div>
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('batch_initial_count')}</div>
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('batch_start_date')}</div>
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer">
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('batch_end_date')}</div>
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer"></th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer"></th>
          </tr>
          </thead>
          <tbody>

          {loading && (
            <tr key={'loading'}>
              <td colSpan={11} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <h5 className="text-black dark:text-white text-center text-sm">Loading...</h5>
              </td>
            </tr>
          )}

          {!loading && data?.length === 0 && (
            <tr key={'loading'}>
              <td colSpan={11} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <h5 className="text-black dark:text-white text-center text-sm">No Data</h5>
              </td>
            </tr>
          )}

          {!loading && data?.length > 0 && data?.map((item: any, index: number) => (
            <tr key={index}>
              {rowSelectEnable && (
                <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark text-sm">
                  <div className="flex justify-center items-center">
                    <label htmlFor={item.id} className="flex cursor-pointer select-none items-center text-black">
                      <div className="relative">
                        <input
                          type="checkbox"
                          id={item.id}
                          className="sr-only"
                          onChange={(event: any) => {
                            const getItem = event.target.checked ? item : null;
                            setRow(getItem);
                            onRowSelected && onRowSelected(getItem);
                          }}
                        />
                        <div className={`mr-4 flex h-5 w-5 items-center justify-center rounded border ${
                          row && row.BATCH_ID === item.BATCH_ID && 'border-primary bg-gray dark:bg-transparent'
                        }`}
                        >
                        <span
                          className={`h-2.5 w-2.5 rounded-sm ${row && row.BATCH_ID === item.BATCH_ID && 'bg-primary'}`}></span>
                        </div>
                      </div>
                    </label>
                  </div>
                </td>
              )}
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {item.HOUSE_NAME}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                {item.LOT_NAME}
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark text-sm">
                <p className="text-black dark:text-white text-center text-sm">
                  {item.HENGRP_NAME}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {item.FEEDING_DATE}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {item.HEN_NAME}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {t(growthSegments[item.GROWTH_KIND || 0])}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark"></td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {item.INITIAL_COUNT}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {(item.START_DATE && moment(item.START_DATE).isValid()) && item.START_DATE || ''}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {(item.END_DATE && moment(item.END_DATE).isValid()) && item.END_DATE || ''}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <div className="flex items-center justify-center gap-2">
                  {(item.END_DATE && moment(item.END_DATE).isValid()) && (
                    <button
                      type="button"
                      className="rounded-md bg-meta-3 py-1 px-2 text-center font-medium text-white hover:bg-opacity-90 text-xs"
                      onClick={() => handleStartBatch && handleStartBatch(item)}
                    >
                      {t('start_batch')}
                    </button>
                  )}

                  {(!item.END_DATE || !moment(item.END_DATE).isValid()) && (
                    <button
                      type="button"
                      className="rounded-md bg-primary py-1 px-2 text-center font-medium text-white hover:bg-opacity-90 text-xs"
                      onClick={() => handleEndBatch && handleEndBatch(item)}
                    >
                      {t('end_batch')}
                    </button>
                  )}
                </div>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <div className="flex items-center justify-center gap-2">
                  {handleEditAction && (
                    <Link to={'#'} onClick={() => handleEditAction(item)}>
                      <FiEdit title="Edit" />
                    </Link>
                  )}

                  {handleRemoveAction && (
                    <Link to={'#'} onClick={() => handleRemoveAction(item)}>
                      <FiTrash title="Delete" />
                    </Link>
                  )}
                </div>
              </td>
            </tr>
          ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default BatchListTable;
