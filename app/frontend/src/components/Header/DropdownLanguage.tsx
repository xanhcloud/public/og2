import { useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import { LANGUAGES } from '../../config/language';
import { useTranslation } from 'react-i18next';
import { getLanguageObject } from '../../services/Helpers.ts';

export default function DropdownLanguage({wrapClassName=  '', containerClassName = '', contentClassName = ''}) {
  const { i18n, t } = useTranslation();

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [language, setLanguage] = useState<any>({});

  const trigger = useRef<any>(null);
  const dropdown = useRef<any>(null);

  const onChangeLanguage = (language: any) => {
    i18n.changeLanguage(language.code);
    localStorage.setItem('language', JSON.stringify(language));
    setLanguage(language);
    setDropdownOpen(false);
  };

  useEffect(() => {
    const clickHandler = ({ target }: MouseEvent) => {
      if (!dropdown.current) return;
      if (
        !dropdownOpen ||
        dropdown.current.contains(target) ||
        trigger.current.contains(target)
      )
        return;
      setDropdownOpen(false);
    };
    document.addEventListener('click', clickHandler);
    return () => document.removeEventListener('click', clickHandler);
  });

  useEffect(() => {
    const keyHandler = ({ keyCode }: KeyboardEvent) => {
      if (!dropdownOpen || keyCode !== 27) return;
      setDropdownOpen(false);
    };
    document.addEventListener('keydown', keyHandler);
    return () => document.removeEventListener('keydown', keyHandler);
  });

  useEffect(() => {
    const langObject = getLanguageObject();

    setTimeout(() => {
      onChangeLanguage(langObject);
      setLanguage(langObject);
    });
  }, []);

  return (
    <li className={`relative ${wrapClassName}`}>
      <Link
        ref={trigger}
        onClick={() => {
          setDropdownOpen(!dropdownOpen);
        }}
        to="#"
        className={containerClassName || 'relative flex h-8.5 w-8.5 items-center justify-center rounded-full border-[0.5px] border-stroke bg-gray hover:text-primary dark:border-strokedark dark:bg-meta-4 dark:text-white'}
        title={language?.label}
      >
        <span style={{ textTransform: 'uppercase' }}>{language?.code}</span>
      </Link>

      <div
        ref={dropdown}
        onFocus={() => setDropdownOpen(true)}
        onBlur={() => setDropdownOpen(false)}
        className={`absolute -right-27 mt-2.5 flex h-auto w-75 flex-col rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark sm:right-0 sm:w-80 ${
          dropdownOpen ? 'block' : 'hidden'
        } ${contentClassName}`}
      >
        <div className="px-4.5 py-3">
          <h5 className="text-sm font-medium text-bodydark2">
            {t('language')}
          </h5>
        </div>

        <ul className="flex h-auto flex-col overflow-y-auto">
          {LANGUAGES.map((row: any) => (
            <li key={row.code}>
              <Link
                className="flex flex-col gap-2.5 border-t border-stroke px-4.5 py-3 hover:bg-gray-2 dark:border-strokedark dark:hover:bg-meta-4"
                to="#"
                onClick={() => onChangeLanguage(row)}
              >
                <span>{row.label}</span>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </li>
  );
}
