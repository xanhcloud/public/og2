import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

interface PageTitleProps {
  title: string;
}

const PageTitle: React.FC<PageTitleProps> = ({ title }) => {
  const location = useLocation();
  const app_name = 'Oval Growth';

  useEffect(() => {
    document.title = title + ' | ' + app_name;
  }, [location, title]);

  return null; // This component doesn't render anything
};

export default PageTitle;
