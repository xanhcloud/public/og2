import {useRequest} from 'ahooks';
import {useTranslation} from 'react-i18next';
import React, {FC, useEffect, useState} from 'react';
import {requestToken} from '../../services/request';

const WeeklyHistoryTable: FC<{ tab: any }> = ({tab}) => {
    const {t} = useTranslation();
    const [id, set_id] = useState(null);
    const [batchs, setBatchs] = useState([]);
    const [batchId, setBatchId] = useState(-1);
    const [flags, set_flags] = useState([]);
    const [items, set_items] = useState([]);
    const [rows, set_rows] = useState([]);
    const [showTable, setShowTable] = useState(false);

    const {data: historyTable, loading} = useRequest(() => {
        return requestToken({
            url: `/api/w-history/${tab}`,
            method: 'GET',
        })
    });

    useEffect(() => {
        if (historyTable && Object.keys(historyTable).length > 0) {
            let data: any = {};

            Object.entries(historyTable).forEach(([key, value]) => {
                data[key] = value;
            });

            set_id(data['id']);
            setBatchs(data['batchs']);
            setShowTable(false);
        }
    }, [historyTable]);

    const handleChange = (event: any) => {
        if (event.target.value) {
            setBatchId(event.target.value);
        } else {
            setBatchId(-1);
        }
    };

    const handleSubmit = () => {
        if (batchId > -1) {
            requestToken({
                url: `/api/w-his-after/${tab}`,
                method: 'POST',
                data: {
                    batch: batchId,
                    history: 'Submit',
                }
            }).then((res: any) => {
                if (res) {
                    set_flags(res['flags']);
                    set_items(res['items']);
                    set_rows(res['rows']);

                    setShowTable(true);
                }
            });
        }
    };

    return (
        <>
            <div>
                {loading && (
                    <div
                        className="max-w-full overflow-x-auto rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white">
                        <div className="text-center flex flex-1 items-center justify-center py-4">Loading...</div>
                    </div>
                )}

                {!loading && id && (
                    <>
                        {batchs.length > 0 && (
                            <>
                                <div className="flex gap-4 items-center mb-6">
                                    <div>{t('feeding_period')}:</div>

                                    <select name="batch" onChange={handleChange}
                                            className="relative z-20 w-[150px] appearance-none rounded border border-stroke bg-white px-6 py-2 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input">
                                        <option value=""></option>
                                        {(() => {
                                            const arr = [];
                                            for (let i = 0; i < batchs.length; i++) {
                                                arr.push(
                                                    <>
                                                        <option value={batchs[i]['BATCH_ID']}>
                                                            {batchs[i]['FEEDING_DATE']}
                                                        </option>
                                                    </>
                                                );
                                            }
                                            return arr;
                                        })()}
                                    </select>

                                    <button type="button" onClick={handleSubmit}
                                            className="rounded bg-primary py-2 px-6 font-medium text-gray hover:bg-opacity-90">
                                        {t('submit')}
                                    </button>
                                </div>

                                {showTable && (
                                    <div
                                        className="max-w-full overflow-x-auto rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white">

                                        <table className="w-full table-auto"
                                               style={{borderCollapse: 'separate', borderSpacing: 0}}>
                                            <thead className="sticky top-[0px]">
                                            <tr className="bg-gray-2 text-left dark:bg-meta-4">
                                                <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium border-b border-[#eee] dark:border-strokedark text-black dark:text-white text-sm cursor-pointer">{t('date')}</th>
                                                <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium border-b border-[#eee] dark:border-strokedark text-black dark:text-white text-sm text-center cursor-pointer">{t('days_old')}</th>
                                                <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium border-b border-[#eee] dark:border-strokedark text-black dark:text-white text-sm text-center cursor-pointer">{t('number_of_eggs')}</th>
                                                {(() => {
                                                    const arr = [];
                                                    for (let i = 0; i < flags.length; i++) {
                                                        if (flags[i] == 1) {
                                                            arr.push(
                                                                <>
                                                                    <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium border-b border-[#eee] dark:border-strokedark text-black dark:text-white text-sm text-center cursor-pointer">
                                                                        {items[i]}
                                                                    </th>
                                                                </>
                                                            );
                                                        }
                                                    }
                                                    return arr;
                                                })()}
                                            </tr>
                                            </thead>

                                            <tbody>
                                            {rows.map((row: any) => {
                                                return (
                                                    <tr>
                                                        <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                                                            <p className="text-black dark:text-white text-sm">
                                                                {row['WEEKLY_DATE']}
                                                            </p>
                                                        </td>

                                                        <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                                                            <p className="text-black dark:text-white text-center text-sm">
                                                                {row['WEEK_PERIODS']}
                                                            </p>
                                                        </td>

                                                        <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                                                            <p className="text-black dark:text-white text-center text-sm">
                                                                {row['EGG_COUNT']}
                                                            </p>
                                                        </td>

                                                        {(() => {
                                                            const arr = [];
                                                            let list: any = [
                                                                "CURR_COUNT",
                                                                "EGG_WEIGHT",
                                                                "EGG_VOL",
                                                                "INITIAL_COUNT",
                                                                "HEN_WEIGHT",
                                                                "FOOD_AMOUNT",
                                                                "FOOD_AMOUNT_U",
                                                                "SURVIVAL_RATE",
                                                                "EGG_RATE_CUR",
                                                                "EGG_RATE_INI",
                                                                "FOOD_EFFICEINCY",
                                                                "FOOD_DEMAND",
                                                                "WATER_AMOUNT",
                                                                "WATER_AMOUNT_U",
                                                                "PRESET_TEMP",
                                                                "ROOM_TEMP",
                                                                "ROOM_TEMP_HI",
                                                                "ROOM_TEMP_LO",
                                                                "OUTER_TEMP",
                                                                "OUTER_TEMP_HI",
                                                                "OUTER_TEMP_LO",
                                                                "DAYLIGHT_HOURS",
                                                                "DEFECTION",
                                                                "COEFF",
                                                                "TOTAL_DEAD",
                                                                "HUMIDITY",
                                                                "HUMIDITY_UPL",
                                                                "HUMIDITY_LOL",
                                                                "MIN_VENTILATION",
                                                                "BMIN_VENTILATION",
                                                                "VENTILATION_SETUP",
                                                            ];
                                                            for (let i = 0; i < flags.length; i++) {
                                                                if (flags[i] == 1) {
                                                                    arr.push(
                                                                        <>
                                                                            <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                                                                                <p className="text-black dark:text-white text-center text-sm">
                                                                                    {row[list[i]]}
                                                                                </p>
                                                                            </td>
                                                                        </>
                                                                    );
                                                                }
                                                            }
                                                            return arr;
                                                        })()}
                                                    </tr>
                                                )
                                            })}
                                            </tbody>
                                        </table>
                                    </div>
                                )}
                            </>
                        )}

                        {batchs.length < 1 && (
                            <div
                                className="max-w-full overflow-x-auto rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white">
                                <div className="text-center flex flex-1 items-center justify-center py-4">
                                    No History
                                </div>
                            </div>
                        )}
                    </>
                )}
            </div>
        </>
    );
};

export default WeeklyHistoryTable;
