import { FC, useEffect, useState } from 'react';
import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request.ts';
import { useTranslation } from 'react-i18next';
import { useSortableData } from '../../common/sortableData.ts';
import { FiArrowUp, FiArrowDown } from "react-icons/fi";
import moment from 'moment';

const WeeklyListTable: FC<{ tab: any, settings: any }> = ({ tab, settings }) => {
  const { t } = useTranslation();
  const [fieldColumns, setFieldColumns] = useState([]);

  const { data: weeklyTable, loading } = useRequest(() => {
    return requestToken({
      url: `/api/weeklytable/${tab.id}`,
      method: 'GET'
    });
  });

  // Sortable data
  const { items, requestSort, sortConfig } = useSortableData(weeklyTable?.rows || []);

  // Check field sorting
  const fieldHasSorted = (name: string) => {
    if (!sortConfig) {
      return;
    }
    return sortConfig.key === name ? sortConfig.direction : undefined;
  };

  // Get field columns
  useEffect(() => {
    if (settings && settings.items && Object.keys(settings.items).length > 0) {
      let columns: any = [];

      Object.entries(settings.items).map(([key, value]: any) => {
        if (value && value.toUpperCase() === 'ON') {
          columns.push({ key });
        }
      });

      setFieldColumns(columns);
    }
  }, [settings]);

  return (
    <>
      {!loading && (
        <div className={'mb-4 flex gap-2'}>
          <button
            className="inline-flex items-center justify-center rounded-md bg-primary py-2 px-8 text-center font-medium text-white hover:bg-opacity-90"
            style={{ opacity: !weeklyTable?.growth1 ? 0.4 : 1 }}>
            {t('brooder')}
          </button>

          <button
            className="inline-flex items-center justify-center rounded-md bg-primary py-2 px-8 text-center font-medium text-white hover:bg-opacity-90"
            style={{ opacity: !weeklyTable?.growth2 ? 0.4 : 1 }}>
            {t('pullet')}
          </button>

          <button
            className="inline-flex items-center justify-center rounded-md bg-primary py-2 px-8 text-center font-medium text-white hover:bg-opacity-90"
            style={{ opacity: !weeklyTable?.growth3 ? 0.4 : 1 }}>
            {t('layer')}
          </button>
        </div>
      )}

      <div className="max-w-full overflow-x-auto rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white">
        <table className="w-full table-auto">
          <thead className="sticky top-[0px]">
          <tr className="bg-gray-2 text-left dark:bg-meta-4">
            <th className="min-w-[100px] whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-sm cursor-pointer" onClick={() => requestSort('WEEKLY_DATE')}>
              <div className="flex gap-1 flex-nowrap items-center">
                <div>{t('date')}</div>
                {fieldHasSorted('WEEKLY_DATE') === 'ascending' && (<FiArrowUp />)}
                {fieldHasSorted('WEEKLY_DATE') === 'descending' && (<FiArrowDown />)}
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer" onClick={() => requestSort('WEEK_PERIODS')}>
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('last_week')}</div>
                {fieldHasSorted('WEEK_PERIODS') === 'ascending' && (<FiArrowUp />)}
                {fieldHasSorted('WEEK_PERIODS') === 'descending' && (<FiArrowDown />)}
              </div>
            </th>
            <th className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer" onClick={() => requestSort('EGG_COUNT')}>
              <div className="flex gap-1 flex-nowrap items-center justify-center">
                <div>{t('number_of_eggs')}</div>
                {fieldHasSorted('EGG_COUNT') === 'ascending' && (<FiArrowUp />)}
                {fieldHasSorted('EGG_COUNT') === 'descending' && (<FiArrowDown />)}
              </div>
            </th>
            {fieldColumns.length > 0 && fieldColumns.map((field: any) => (
              <th
                key={field.key}
                className="whitespace-nowrap py-2 px-2 font-medium text-black dark:text-white text-center text-sm cursor-pointer"
                onClick={() => requestSort(field.key.toUpperCase())}
              >
                <div className="flex gap-1 flex-nowrap items-center justify-center">
                  <div>{t(field.key)}</div>
                  {fieldHasSorted(field.key.toUpperCase()) === 'ascending' && (<FiArrowUp />)}
                  {fieldHasSorted(field.key.toUpperCase()) === 'descending' && (<FiArrowDown />)}
                </div>
              </th>
            ))}
          </tr>
          </thead>
          <tbody>
          {loading && (
            <tr key={'loading'}>
              <td colSpan={fieldColumns.length + 3} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <h5 className="text-black dark:text-white text-center text-sm">Loading...</h5>
              </td>
            </tr>
          )}

          {!loading && items?.length === 0 && (
            <tr key={'loading'}>
              <td colSpan={fieldColumns.length + 3} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <h5 className="text-black dark:text-white text-center text-sm">No Data</h5>
              </td>
            </tr>
          )}

          {!loading && items?.length > 0 && items?.map((item: any, index: number) => (
            <tr key={index}>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark text-sm">
                <h5 className="text-black dark:text-white">{item.WEEKLY_DATE && moment(item.WEEKLY_DATE).format('MM-DD')}</h5>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {item.WEEK_PERIODS}
                </p>
              </td>
              <td className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                <p className="text-black dark:text-white text-center text-sm">
                  {item.EGG_COUNT}
                </p>
              </td>
              {fieldColumns.length > 0 && fieldColumns.map((field: any) => (
                <td key={field.key} className="border-b border-[#eee] py-2 px-2 dark:border-strokedark">
                  <p className="text-black dark:text-white text-center text-sm">
                    {item[field.key.toUpperCase()]}
                  </p>
                </td>
              ))}
            </tr>
          ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default WeeklyListTable;
