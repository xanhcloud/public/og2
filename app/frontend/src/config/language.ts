export const LANGUAGES: any = [
  { label: 'English', code: 'en' },
  { label: 'Japanese', code: 'ja' },
  { label: 'Spanish', code: 'es' },
];
