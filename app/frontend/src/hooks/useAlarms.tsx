import { useEffect } from 'react';
import useLocalStorage from './useLocalStorage';

const useAlarms = () => {
  const [alarmEnabled, setAlarmEnabled] = useLocalStorage('alarm-enabled', false);

  useEffect(() => {
    console.log('Alarm enabled', alarmEnabled);
  }, [alarmEnabled]);

  return [alarmEnabled, setAlarmEnabled];
};

export default useAlarms;
