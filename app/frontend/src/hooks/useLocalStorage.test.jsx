import { renderHook, act } from '@testing-library/react-hooks';
import useLocalStorage from './useLocalStorage';

describe('useLocalStorage', () => {
  test('should do something', () => {
    const { result } = renderHook(() => useLocalStorage());
    act(() => {
      result.current.someFunction();
    });
    expect(result.current.someValue).toBe(true);
  });

  // Additional tests...
});
