import { useRequest } from 'ahooks';
import React, { ReactNode, useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Header from '../components/Header/index';
import Sidebar from '../components/Sidebar/index';
import AuthService from '../services/AuthService';
import useLocalStorage from '../hooks/useLocalStorage.tsx';

interface DefaultLayoutProps {
  children: ReactNode;
}

const DefaultLayout: React.FC<DefaultLayoutProps> = ({ children }) => {
  const [currentUser, setCurrentUser] = useLocalStorage('current-user', null);
  const {
    data: user,
    loading,
    error,
  } = useRequest(() => {
    return AuthService.currentUser();
  });

  const navigate = useNavigate();
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const headerRef = useRef<any>(null);

  // Check login
  useEffect(() => {
    if (!loading && error !== undefined) {
      navigate('/auth/signin');
    }
  }, [user, loading, error]);

  // Save user to store
  useEffect(() => {
    setTimeout(() => {
      setCurrentUser(user || null);
    })
  }, [user]);

  return (
    <>
      <div className="dark:bg-boxdark-2 dark:text-bodydark">
        {/* <!-- ===== Page Wrapper Start ===== --> */}
        <div className="flex h-screen overflow-hidden">
          {/* <!-- ===== Sidebar Start ===== --> */}
          <Sidebar
            sidebarOpen={sidebarOpen}
            setSidebarOpen={setSidebarOpen}
          />
          {/* <!-- ===== Sidebar End ===== --> */}

          {/* <!-- ===== Content Area Start ===== --> */}
          <div className="relative flex flex-1 flex-col overflow-y-auto overflow-x-hidden">
            {/* <!-- ===== Header Start ===== --> */}
            <div ref={headerRef}>
              <Header
                sidebarOpen={sidebarOpen}
                setSidebarOpen={setSidebarOpen}
              />
            </div>
            {/* <!-- ===== Header End ===== --> */}

            {/*{loading && <div className="text-center flex flex-1 items-center justify-center">loading...</div>}*/}

            {/* <!-- ===== Main Content Start ===== --> */}
            <main>
              <div className="mx-auto max-w-screen-2xl p-4 lg:p-8">
                {children}
              </div>
            </main>
            {/* <!-- ===== Main Content End ===== --> */}
          </div>
          {/* <!-- ===== Content Area End ===== --> */}
        </div>
        {/* <!-- ===== Page Wrapper End ===== --> */}
      </div>
    </>
  );
};

export default DefaultLayout;
