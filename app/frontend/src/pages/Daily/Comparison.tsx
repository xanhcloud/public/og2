import DefaultLayout from '../../layout/DefaultLayout.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import DailyComparisonTable from '../../components/Daily/ComparisonTable.tsx';
import { useEffect, useRef, useState } from 'react';
import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request.ts';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import AuthService from '../../services/AuthService.ts';
import useLocalStorage from '../../hooks/useLocalStorage.tsx';

const DailyComparison = () => {
  const { t } = useTranslation();
  const ref = useRef<any>(null);
  const tabNavRef = useRef<any>(null);
  const [breadCrumbHeight, setBreadCrumbHeight] = useState(0);
  const [tabNavHeight, setTabNavHeight] = useState(0);
  const [openTab, setOpenTab] = useState(1);
  const activeClasses = 'text-primary border-primary';
  const inactiveClasses = 'border-transparent';
  const [user, setUser] = useLocalStorage('current-user', null);
  const [activeTab, setActiveTab] = useLocalStorage('active-house-tab-daily-past-data-comparison', null);

  const { data: comparisonData, loading } = useRequest(() => {
    return requestToken({
      url: '/api/d-comparison/1',
      method: 'GET'
    });
  });

  const { data: settings } = useRequest(() => {
    return requestToken({
      url: '/api/setup-dailytable',
      method: 'GET',
      data: {user_id: user?.id}
    });
  }, {refreshDeps: [user]});

  const onSetOpenTab = (tabId: any) => {
    setOpenTab(tabId);
    setActiveTab(tabId);
  }

  useEffect(() => {
    if (comparisonData && Object.keys(comparisonData).length > 0) {
      if (comparisonData.tabs.length > 0) {
        comparisonData.tabs = comparisonData.tabs.filter((el: any) => {
          return el.visibility;
        });
        if (activeTab && comparisonData.tabs.some((value: any) => { return value.id == activeTab })) {
          setOpenTab(activeTab);
        } else {
          setOpenTab(comparisonData.tabs[0].id);
        }
      }
    }
  }, [comparisonData]);

  useEffect(() => {
    if (ref && ref.current) {
      setBreadCrumbHeight(ref.current.clientHeight);
    }
  }, [ref]);

  useEffect(() => {
    if (tabNavRef && tabNavRef.current) {
      setTabNavHeight(tabNavRef.current.clientHeight);
    }
  }, [tabNavRef, loading]);

  return (
    <DefaultLayout>
      <div ref={ref}>
        <Breadcrumb pageName={t('past_data_comparison')} className="mb-6 flex flex-col-reverse gap-3" />
      </div>

      {!loading && comparisonData?.tabs?.length === 0 && (
        <div className="text-center flex flex-1 items-center justify-center h-[50vh]">No Data Found</div>
      )}

      {!loading && comparisonData?.tabs?.length > 0 && (
        <>
          <div ref={tabNavRef} className="mb-6 flex gap-4 sm:gap-8 border-b border-stroke dark:border-strokedark overflow-x-auto pr-4">
            {comparisonData.tabs.map((tab: any) => (
              <Link
                key={tab.id}
                to="#"
                className={`border-b-2 py-4 text-sm font-medium hover:text-primary md:text-base min-w-[80px] ${openTab === tab.id ? activeClasses : inactiveClasses}`}
                onClick={() => onSetOpenTab(tab.id)}
              >
                {tab.name}: {tab.id}
              </Link>
            ))}
          </div>

          {comparisonData.tabs.map((tab: any) => (
            <div key={tab.id}>
              {openTab === tab.id && (
                <div
                  className={`flex flex-col leading-relaxed ${openTab === tab.id ? 'block' : 'hidden'}`}
                  style={{ height: `calc(100vh - ${breadCrumbHeight}px - ${tabNavHeight}px - 7.5rem - 80px` }}>
                  <DailyComparisonTable tab={tab} settings={settings}/>
                </div>
              )}
            </div>
          ))}
        </>
      )}
    </DefaultLayout>
  );
};

export default DailyComparison;
