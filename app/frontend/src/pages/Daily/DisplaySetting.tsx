import React, {useEffect, useState} from 'react';
import Drag from '../../js/drag.ts';
import { FiEdit, FiMove } from 'react-icons/fi';
import { useTranslation } from 'react-i18next';
import useRequest from 'ahooks/lib/useRequest';
import { requestToken } from '../../services/request';
import DefaultLayout from '../../layout/DefaultLayout.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import useLocalStorage from '../../hooks/useLocalStorage.tsx';
import { Link, useNavigate } from 'react-router-dom';

const DailyDisplaySetting = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const [user, setUser] = useLocalStorage('current-user', null);
  const [items, setItems] = useState({});
  const [submitting, setSubmitting] = useState(false);
  const itemData: any = {
      "curr_count": "OFF",
      "egg_weight": "OFF",
      "egg_vol": "OFF",
      "initial_count": "OFF",
      "hen_weight": "OFF",
      "food_amount": "OFF",
      "food_amount_u": "OFF",
      "survival_rate": "OFF",
      "egg_rate_cur": "OFF",
      "egg_rate_ini": "OFF",
      "food_efficeincy": "OFF",
      "food_demand": "OFF",
      "water_amount": "OFF",
      "water_amount_u": "OFF",
      "preset_temp": "OFF",
      "room_temp": "OFF",
      "room_temp_hi": "OFF",
      "room_temp_lo": "OFF",
      "outer_temp": "OFF",
      "outer_temp_hi": "OFF",
      "outer_temp_lo": "OFF",
      "daylight_hours": "OFF",
      "defection": "OFF",
      "coeff": "OFF",
      "total_dead": "OFF",
      "dead_count": "OFF",
      "humidity": "OFF",
      "humidity_upl": "OFF",
      "humidity_lol": "OFF",
      "min_ventilation": "OFF",
      "bmin_ventilation": "OFF",
      "ventilation_setup": "OFF",
      "remv_excr": "OFF",
      "heating": "OFF",
      "water_pos": "OFF",
      "aircon_system": "OFF",
      "lighting_time": "OFF",
      "food": "OFF",
      "comments": "OFF",
  }

  const { data: settings, loading } = useRequest(() => {
    return requestToken({
      url: `/api/setup-dailytable`,
      method: 'GET',
      data: {user_id: user?.id}
    });
  }, {refreshDeps: [user]});

  useEffect(() => {
    if (settings?.items && Object.keys(settings?.items).length > 0) {
      setItems(structuredClone(settings.items));
    } else {
      setItems(structuredClone(itemData));
    }
  }, [settings, loading]);

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    let data: any = {};

    Array.prototype.forEach.call(event.target, (item) => {
      if (item?.value && item.type == 'checkbox' && item.name == 'sel_item') {
        data[item.value] = item.checked ? 'ON' : 'OFF';
      }
    });

    try {
      setSubmitting(true);
      const result = await requestToken({
        url: '/api/save-setup-dailytable',
        method: 'POST',
        data: { user_id: user?.id, items: data }
      });

      if (result) {
        navigate('/daily/list')
      }
    } catch (error) {
      console.log(error);
    } finally {
      setSubmitting(false);
    }
  };

  useEffect(() => {
    Drag();
  });

  return (
    <DefaultLayout>
      <Breadcrumb pageName={t('daily_display_settings')} className="mb-6 flex flex-col-reverse gap-3" />

      <div className="dark:border-strokedark dark:bg-boxdark bg-white rounded-md p-4">
        {(loading || submitting) && (
          <div className="text-center flex flex-1 items-center justify-center py-4 h-[100vh]">
            <div className="animate-spin">
              <svg
                width="24"
                height="25"
                viewBox="0 0 49 50"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <mask id="path-1-inside-1_1881_16179" fill="white">
                  <path
                    d="M18.5503 49.3989C23.2997 50.4597 28.2554 50.1113 32.8097 48.3965C37.364 46.6816 41.3187 43.6748 44.1889 39.7449C47.0591 35.815 48.7199 31.1328 48.9676 26.2727C49.2153 21.4125 48.0392 16.5858 45.5834 12.3844C43.1277 8.18304 39.4991 4.78974 35.1428 2.62071C30.7865 0.451685 25.8918 -0.398759 21.0592 0.173693C16.2265 0.746144 11.666 2.7166 7.93703 5.84338C4.20803 8.97015 1.47267 13.1173 0.0664691 17.7761L5.29059 19.353C6.38986 15.711 8.52815 12.4691 11.4432 10.0248C14.3582 7.58057 17.9233 6.04021 21.7011 5.59272C25.4789 5.14522 29.3052 5.81003 32.7106 7.50561C36.116 9.20119 38.9526 11.8538 40.8723 15.1381C42.792 18.4224 43.7114 22.1956 43.5178 25.9949C43.3241 29.7942 42.0258 33.4543 39.7822 36.5264C37.5385 39.5986 34.4469 41.949 30.8868 43.2896C27.3266 44.6302 23.4525 44.9025 19.7398 44.0732L18.5503 49.3989Z" />
                </mask>
                <path
                  d="M18.5503 49.3989C23.2997 50.4597 28.2554 50.1113 32.8097 48.3965C37.364 46.6816 41.3187 43.6748 44.1889 39.7449C47.0591 35.815 48.7199 31.1328 48.9676 26.2727C49.2153 21.4125 48.0392 16.5858 45.5834 12.3844C43.1277 8.18304 39.4991 4.78974 35.1428 2.62071C30.7865 0.451685 25.8918 -0.398759 21.0592 0.173693C16.2265 0.746144 11.666 2.7166 7.93703 5.84338C4.20803 8.97015 1.47267 13.1173 0.0664691 17.7761L5.29059 19.353C6.38986 15.711 8.52815 12.4691 11.4432 10.0248C14.3582 7.58057 17.9233 6.04021 21.7011 5.59272C25.4789 5.14522 29.3052 5.81003 32.7106 7.50561C36.116 9.20119 38.9526 11.8538 40.8723 15.1381C42.792 18.4224 43.7114 22.1956 43.5178 25.9949C43.3241 29.7942 42.0258 33.4543 39.7822 36.5264C37.5385 39.5986 34.4469 41.949 30.8868 43.2896C27.3266 44.6302 23.4525 44.9025 19.7398 44.0732L18.5503 49.3989Z"
                  stroke="#3C50E0"
                  strokeWidth="14"
                  mask="url(#path-1-inside-1_1881_16179)"
                />
              </svg>
            </div>
          </div>
        )}

        {(!loading && !submitting) && Object.keys(items).length > 0 && (
          <form onSubmit={handleSubmit} method="POST">
            <div className="min-w-[370px] max-w-max rounded-md border border-stroke py-1 dark:border-strokedark">
              <ul className="swim-lane flex flex-col">
                {Object.entries(items).map(([key, value], index) => (
                  <li key={index + '_' + key + '_' + value} draggable="true"
                      className="task cursor-move flex justify-between items-center gap-3 border-b border-stroke px-5 py-3 last:border-b-0 dark:border-strokedark">
                    <label className="flex cursor-pointer select-none items-center">
                      <div className="relative">
                        <input type="checkbox" className="taskCheckbox sr-only"
                               name="sel_item" value={key} data-label={t(key)}
                               defaultChecked={value == 'ON'} />
                        <div
                          className="box mr-3 flex h-5 w-5 items-center justify-center rounded border">
                          <span className="text-white opacity-0">
                            <svg className="fill-current" width="10" height="7"
                                 viewBox="0 0 10 7"
                                 fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path fillRule="evenodd" clipRule="evenodd" fill=""
                                    d="M9.70685 0.292804C9.89455 0.480344 10 0.734667 10 0.999847C10 1.26503 9.89455 1.51935 9.70685 1.70689L4.70059 6.7072C4.51283 6.89468 4.2582 7 3.9927 7C3.72721 7 3.47258 6.89468 3.28482 6.7072L0.281063 3.70701C0.0986771 3.5184 -0.00224342 3.26578 3.785e-05 3.00357C0.00231912 2.74136 0.10762 2.49053 0.29326 2.30511C0.4789 2.11969 0.730026 2.01451 0.992551 2.01224C1.25508 2.00996 1.50799 2.11076 1.69683 2.29293L3.9927 4.58607L8.29108 0.292804C8.47884 0.105322 8.73347 0 8.99896 0C9.26446 0 9.51908 0.105322 9.70685 0.292804Z" />
                            </svg>
                          </span>
                        </div>
                      </div>
                      <p className="text-black dark:text-white">{t(key)}</p>
                    </label>
                    <FiMove />
                  </li>
                ))}
              </ul>
            </div>

            <div className="flex justify-center gap-4 mt-8">
              <button className="rounded bg-primary py-2 px-6 font-medium text-gray hover:bg-opacity-90" type="submit" disabled={submitting}>
                {t('save_changes')}
              </button>

              <Link to="/daily/list" className="rounded bg-gray py-2 px-6 font-medium text-black hover:shadow-1">
                {t('abort_changes')}
              </Link>
            </div>
          </form>
        )}
      </div>
    </DefaultLayout>
  );
};

export default DailyDisplaySetting;
