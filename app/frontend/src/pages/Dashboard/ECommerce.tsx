import React, { useEffect, useState } from 'react';
import DefaultLayout from '../../layout/DefaultLayout';
import { useRequest, useWebSocket } from 'ahooks';
import DashboardService from '../../services/DashboardService';
import { AppConfig } from '../../config/app.config';
import { useTranslation } from 'react-i18next';
import DashboardListTable from '../../components/Dashboard/ListTable.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import { Controller, useForm } from 'react-hook-form';
import Datepicker, { DateValueType } from 'react-tailwindcss-datepicker';
import moment from 'moment';
import useAlarms from '../../hooks/useAlarms.tsx';
import { getLanguageObject } from '../../services/Helpers.ts';
import DataStats from '../../components/Dashboard/DataStats.tsx';
import { requestToken } from '../../services/request.ts';
import { House, Lot } from '../../types/batch.ts';
import { Link } from 'react-router-dom';
import GraphDailyChart from '../../components/Graphs/DailyChart.tsx';
import TotalEgg from '../../components/Dashboard/TotalEgg.tsx';
import useLocalStorage from '../../hooks/useLocalStorage.tsx';

type Params = {
  date: string | null | Date,
}

const ECommerce: React.FC = () => {
  const { t } = useTranslation();
  const dateFormat = 'YYYY-MM-DD';
  const [refresh, setRefresh] = useState(0);
  const [filterDate, setFilterDate] = useLocalStorage('filterDate', moment().format(dateFormat));
  const [params, setParams] = useState<Params>({
    date: filterDate,
  });
  const [alarmEnabled, setAlarmEnabled] = useAlarms();
  const locale = getLanguageObject();
  const [tabs, setTabs] = useState([]);
  const [openTab, setOpenTab] = useState<House | null>(null);
  const activeClasses = 'text-primary border-primary';
  const inactiveClasses = 'border-transparent';

  const { control } = useForm({
    defaultValues: {
      date: params.date
    },
  });

  const { data: dashboard, loading } = useRequest(
    () => DashboardService.getDashboard(params),
    {
      refreshDeps: [refresh],
    },
  );

  const { latestMessage: dashboardChanged } = useWebSocket(
    `${AppConfig.ws}/database-activity`,
  );

  const { data: housesData } = useRequest(() => {
    return requestToken({
      url: '/api/houses',
      method: 'GET',
    });
  });

  useEffect(() => {
    if (dashboardChanged) {
      if (dashboardChanged?.data) {
        // const message: any = JSON.parse(dashboardChanged.data);
        setRefresh(refresh + 1);
      }
    }
  }, [dashboardChanged]);

  // Get tabs
  useEffect(() => {
    let getTabs: any = [];

    if (housesData && housesData.houses && housesData.houses.length > 0) {
      housesData.houses.map((tab: House) => {
        getTabs.push({...tab, id: tab.HOUSE_ID});
      });

      setTabs(getTabs);
    }

    if (getTabs.length > 0) {
      setOpenTab(getTabs[0]);
    }
  }, [housesData]);

  return (
    <DefaultLayout>
      <div className={'flex flex-col gap-3 sm:flex-row sm:items-center sm:justify-between mb-6'}>
        <Breadcrumb pageName={t('main')} className="flex flex-col-reverse gap-3" />

        <div className={'flex gap-2'}>
          <Controller
            name="date"
            control={control}
            rules={{ required: false }}
            render={({ field }) => (
              <Datepicker
                i18n={locale.code}
                asSingle={true}
                useRange={false}
                readOnly={true}
                primaryColor={'blue'}
                placeholder={' '}
                value={{
                  startDate: field.value || null,
                  endDate: field.value || null
                }}
                onChange={(newValue: DateValueType) => {
                  if (newValue && newValue.startDate) {
                    field.onChange(newValue.startDate);
                    setFilterDate(newValue.startDate.toString());
                    setParams((prev) => ({ ...prev, date: newValue.startDate }));
                    setRefresh(refresh + 1);
                  }
                }}
                inputClassName={`w-full rounded border-[1.5px] border-stroke py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary`}
              />
            )}
          />
        </div>
      </div>

      <div className="w-full mb-4 p-4 border rounded-sm border-stroke bg-white dark:border-strokedark dark:bg-boxdark">
        <div className="flex justify-between items-center">
          <h2 className="text-2xl font-bold">{t('recorded_alarms')}</h2>

          <div className="flex items-center gap-2">
            <h5 className="font-normal text-black dark:text-white">
              {alarmEnabled ? 'ON' : 'OFF'}
            </h5>
            <div>
              <label htmlFor="alarmEnable" className="flex cursor-pointer select-none items-center">
                <div className="relative">
                  <input
                    type="checkbox"
                    id="alarmEnable"
                    className="sr-only"
                    onChange={() => {
                      if (typeof setAlarmEnabled === 'function') {
                        setAlarmEnabled(!alarmEnabled);
                      }
                    }}
                  />
                  <div className={`block h-8 w-14 rounded-full ${alarmEnabled ? 'bg-primary' : 'bg-stroke dark:bg-[#5A616B]'}`}></div>
                  <div
                    className={`absolute left-1 top-1 flex h-6 w-6 items-center justify-center rounded-full bg-white transition ${
                      alarmEnabled && '!right-1 !translate-x-full'
                    }`}
                  ></div>
                </div>
              </label>
            </div>
          </div>
        </div>

        <div className="mt-4 border rounded-sm border-stroke bg-amber-50 dark:border-strokedark dark:bg-boxdark">
          <div className="p-4">
            <ul>
              {dashboard?.alarms?.map((alarm: string, key: number) => (
                <li key={key}>{alarm}</li>
              ))}
            </ul>
          </div>
        </div>
      </div>

      {/*<div className="grid grid-cols-1 gap-4 md:grid-cols-2 xl:grid-cols-4 mb-4">*/}
      {/*  <CardDataStats*/}
      {/*    title={t('total_birds')}*/}
      {/*    total={dashboard?.tot_chiken || 0}*/}
      {/*  />*/}
      {/*  <CardDataStats*/}
      {/*    title={t('total_birds_layers')}*/}
      {/*    total={dashboard?.tot_adult_chiken || 0}*/}
      {/*  />*/}
      {/*</div>*/}

      {!loading && tabs.length > 0 && (
        <div className="mb-4">
          <div className="mb-6 flex gap-4 sm:gap-8 border-b border-stroke dark:border-strokedark overflow-x-auto pr-4">
            {tabs.map((tab: House) => (
              <Link
                key={tab.HOUSE_ID}
                to="#"
                className={`border-b-2 py-4 text-sm font-medium hover:text-primary md:text-base min-w-[80px] ${
                  openTab?.HOUSE_ID === tab.HOUSE_ID ? activeClasses : inactiveClasses
                }`}
                onClick={() => setOpenTab(tab)}
              >
                {tab.HOUSE_NAME}: {tab.HOUSE_ID}
              </Link>
            ))}
          </div>

          {tabs.map((tab: Lot) => (
            <div key={tab.HOUSE_ID}>
              {openTab?.HOUSE_ID === tab.HOUSE_ID && (
                <div className={`grid grid-cols-12 gap-4 ${openTab?.HOUSE_ID === tab.HOUSE_ID ? 'block' : 'hidden'}`}>
                  <div className="col-span-12">
                    <DataStats />
                  </div>

                  <div className="col-span-12">
                    <GraphDailyChart tab={tab} params={params} height={500}/>
                  </div>
                </div>
              )}
            </div>
          ))}
        </div>
      )}

      <div className="grid grid-cols-12 gap-4">
        {!loading && tabs.length > 0 && (
          <div className="col-span-12">
            <TotalEgg items={dashboard?.curr_count_by_houses || []} tabs={tabs} />
          </div>
        )}

        <div className="col-span-12">
          <DashboardListTable data={dashboard} loading={loading} />
        </div>
      </div>
    </DefaultLayout>
  );
};

export default ECommerce;
