import DefaultLayout from '../../layout/DefaultLayout.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import { useEffect, useRef, useState } from 'react';
import 'react-tabs/style/react-tabs.css';
import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request.ts';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import DeadChickenListTable from '../../components/DeadChickenRegistration/ListTable.tsx';
import DeadChickenFormModal from '../../components/DeadChickenRegistration/FormModal.tsx';
import { Lot } from '../../types/batch.ts';
import { DBTDead } from '../../types/dbd-dead.ts';
import ConfirmRemoveModal from '../../components/ConfirmRemoveModal.tsx';

const DeadChickenRegistrationList = () => {
  const [refresh, setRefresh] = useState(0);
  const {t} = useTranslation();
  const ref = useRef<any>(null);
  const tabNavRef = useRef<any>(null);
  const [tabs, setTabs] = useState([]);
  const [breadCrumbHeight, setBreadCrumbHeight] = useState(0);
  const [tabNavHeight, setTabNavHeight] = useState(0);
  const [openTab, setOpenTab] = useState<Lot | null>(null);
  const activeClasses = 'text-primary border-primary';
  const inactiveClasses = 'border-transparent';
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [modalRemoveOpen, setModalRemoveOpen] = useState<boolean>(false);
  const [recordData, setRecordData] = useState<DBTDead | null>(null);

  // const { data: housesData, loading } = useRequest(() => {
  //   return requestToken({
  //     url: '/api/houses',
  //     method: 'GET',
  //   });
  // });

  const { data: lotData, loading } = useRequest(() => {
    return requestToken({
      url: '/api/lots',
      method: 'GET',
    });
  });

  useEffect(() => {
    if (ref && ref.current) {
      setBreadCrumbHeight(ref.current.clientHeight);
    }
  }, [ref]);

  useEffect(() => {
    if (tabNavRef && tabNavRef.current) {
      setTabNavHeight(tabNavRef.current.clientHeight);
    }
  }, [tabNavRef, loading]);

  // Get tabs
  useEffect(() => {
    let getTabs: any = [];

    if (lotData && lotData.lots && lotData.lots.length > 0) {
      lotData.lots.map((tab: Lot) => {
        getTabs.push({...tab, FULL_NAME: `${tab.HOUSE_NAME}: ${tab.LOT_ID}`});
      });

      setTabs(getTabs);
    }

    if (getTabs.length > 0) {
      setOpenTab(getTabs[0]);
    }
  }, [loading]);

  // Handle on delete
  const handleOnDelete = async () => {
    if (!recordData || (recordData && !recordData.tdead_id)) return;
    try {
      await requestToken({
        method: 'DELETE',
        url: `/api/dbt-deads/${recordData?.tdead_id}`,
      });

      setRecordData(null);
      setModalRemoveOpen(false);
      setRefresh((r) => r + 1);
    } catch (e: any) {
      console.log(e);
    }
  }

  return (
    <DefaultLayout>
      <div ref={ref} className={'flex flex-col gap-3 sm:flex-row sm:items-center sm:justify-between mb-6'}>
        <Breadcrumb pageName={t('dead_chicken_registration')} className="flex flex-col-reverse gap-3" />
      </div>

      {!loading && tabs.length === 0 && (
        <div className="text-center flex flex-1 items-center justify-center h-[50vh]">No Data Found</div>
      )}

      {!loading && tabs.length > 0 && (
        <>
          <div ref={tabNavRef} className="mb-6 flex gap-4 sm:gap-8 border-b border-stroke dark:border-strokedark overflow-x-auto pr-4">
            {tabs.map((tab: Lot) => (
              <Link
                key={tab.LOT_ID}
                to="#"
                className={`border-b-2 py-4 text-sm font-medium hover:text-primary md:text-base min-w-[80px] ${
                  openTab?.LOT_ID === tab.LOT_ID ? activeClasses : inactiveClasses
                }`}
                onClick={() => setOpenTab(tab)}
              >
                {tab.HOUSE_NAME}: {tab.LOT_NAME}
              </Link>
            ))}
          </div>

          {tabs.map((tab: Lot) => (
            <div key={tab.LOT_ID}>
              {openTab?.LOT_ID === tab.LOT_ID && (
                <div className={`flex flex-col leading-relaxed ${openTab?.LOT_ID === tab.LOT_ID ? 'block' : 'hidden'}`}
                     style={{ height: `calc(100vh - ${breadCrumbHeight}px - ${tabNavHeight}px - 7.5rem - 80px` }}>
                  <div className={'mb-4'}>
                    <button
                      type="button"
                      className="rounded-md bg-primary py-2 px-4 text-center font-medium text-white hover:bg-opacity-90"
                      onClick={() => {
                        setRecordData(null);
                        setModalOpen(true);
                      }}
                    >
                      {t('dead_chicken_registration_create')}
                    </button>
                  </div>

                  <DeadChickenListTable
                    tab={tab}
                    refresh={refresh}
                    handleEditAction={(row: DBTDead) => {
                      setRecordData(row);
                      setModalOpen(true);
                    }}
                    handleRemoveAction={(row: DBTDead) => {
                      setRecordData(row);
                      setModalRemoveOpen(true);
                    }}
                  />
                </div>
              )}
            </div>
          ))}
        </>
      )}

      <ConfirmRemoveModal
        open={modalRemoveOpen}
        onOpenChange={setModalRemoveOpen}
        onConfirm={handleOnDelete}
      />

      {openTab && (
        <DeadChickenFormModal
          tab={openTab}
          open={modalOpen}
          onOpenChange={setModalOpen}
          onRefresh={setRefresh}
          record={recordData}
        />
      )}
    </DefaultLayout>
  );
};

export default DeadChickenRegistrationList;
