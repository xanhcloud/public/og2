import DefaultLayout from '../../layout/DefaultLayout.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import EggOfTheDayTable from '../../components/Egg/EggOfTheDayTable.tsx';
import { useEffect, useRef, useState } from 'react';
import 'react-tabs/style/react-tabs.css';
import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request.ts';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { getLanguageObject } from '../../services/Helpers.ts';
import moment from 'moment';
import { Controller, useForm } from 'react-hook-form';
import Datepicker, { DateValueType } from 'react-tailwindcss-datepicker';
import useLocalStorage from '../../hooks/useLocalStorage.tsx';

type Params = {
  date: string | null | Date,
}

const EggsOfTheDay = () => {
  const locale = getLanguageObject();
  const { t } = useTranslation();
  const dateFormat = 'YYYY-MM-DD';
  const ref = useRef<any>(null);
  const tabNavRef = useRef<any>(null);
  const [breadCrumbHeight, setBreadCrumbHeight] = useState(0);
  const [tabNavHeight, setTabNavHeight] = useState(0);
  const [tabs, setTabs] = useState([]);
  const [openTab, setOpenTab] = useState(1);
  const activeClasses = 'text-primary border-primary';
  const inactiveClasses = 'border-transparent';
  const [filterDate, setFilterDate] = useLocalStorage('filterDate', moment().format(dateFormat));
  const [params, setParams] = useState<Params>({
    date: filterDate,
  });
  const [activeTab, setActiveTab] = useLocalStorage('egg-table-day-default-active-tab', null);

  const { control } = useForm({
    defaultValues: {
      date: params.date
    },
  });

  const { data: eggData, loading } = useRequest(() => {
    return requestToken({
      url: `/api/eggtable/${openTab}`,
      method: 'GET',
      data: params
    });
  }, {refreshDeps: [params, openTab]});

  useEffect(() => {
    if (ref && ref.current) {
      setBreadCrumbHeight(ref.current.clientHeight);
    }
  }, [ref]);

  useEffect(() => {
    if (tabNavRef && tabNavRef.current) {
      setTabNavHeight(tabNavRef.current.clientHeight);
    }
  }, [tabNavRef, loading]);

  // Get tabs
  useEffect(() => {
    let getTabs: any = [];

    if (eggData && eggData.tabs && eggData.tabs.length > 0) {
      eggData.tabs.map((tab: any) => {
        if (tab.visibility) {
          getTabs.push(tab);
        }
      });

      setTabs(getTabs);
    }

    if (getTabs.length > 0) {
      if (activeTab && getTabs.some((value: any) => { return value.id == activeTab })) {
        setOpenTab(activeTab);
      } else {
        setOpenTab(getTabs[0].id);
      }
    }
  }, [loading]);

  useEffect(() => {

  }, [tabs]);

  return (
    <DefaultLayout>
      <div ref={ref} className={'flex flex-col gap-3 sm:flex-row sm:items-center sm:justify-between mb-6'}>
        <Breadcrumb pageName={t('show_eggs_of_the_day')} className="flex flex-col-reverse gap-3" />

        <div className={'flex gap-2'}>
          <Controller
            name="date"
            control={control}
            rules={{ required: false }}
            render={({ field }) => (
              <Datepicker
                i18n={locale.code}
                asSingle={true}
                useRange={false}
                readOnly={true}
                primaryColor={'blue'}
                placeholder={' '}
                value={{
                  startDate: field.value || null,
                  endDate: field.value || null
                }}
                onChange={(newValue: DateValueType) => {
                  if (newValue && newValue.startDate) {
                    field.onChange(newValue.startDate);
                    setFilterDate(newValue.startDate.toString());
                    setParams((prev) => ({ ...prev, date: newValue.startDate }));
                  }
                }}
                inputClassName={`w-full rounded border-[1.5px] border-stroke py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary`}
              />
            )}
          />
        </div>
      </div>

      {!loading && tabs.length === 0 && (
        <div className="text-center flex flex-1 items-center justify-center h-[50vh]">No Data Found</div>
      )}

      {tabs.length > 0 && (
        <>
          <div className="bg-gray-2 dark:bg-meta-4 border border-[#eee] dark:border-strokedark rounded font-medium text-sm text-black dark:text-white p-2">
            {t('total_egg_count')}: {eggData?.total_ecount || 'None'}
          </div>

          <div ref={tabNavRef} className="mb-6 flex gap-4 sm:gap-8 border-b border-stroke dark:border-strokedark overflow-x-auto pr-4">
            {tabs.map((tab: any) => (
              <Link
                key={tab.id}
                to="#"
                className={`border-b-2 py-4 text-sm font-medium hover:text-primary md:text-base min-w-[80px] ${openTab === tab.id ? activeClasses : inactiveClasses}`}
                onClick={() => {
                  setOpenTab(tab.id);
                  setActiveTab(tab.id);
                }}
              >
                {tab.name}: {tab.id}
              </Link>
            ))}
          </div>

          {tabs.map((tab: any) => (
            <div key={tab.id}>
              {openTab === tab.id && (
                <div className={`flex flex-col leading-relaxed ${openTab === tab.id ? 'block' : 'hidden'}`}
                     style={{ height: `calc(100vh - ${breadCrumbHeight}px - ${tabNavHeight}px - 7.5rem - 118px` }}>
                  {!loading && (
                    <EggOfTheDayTable listData={eggData}/>
                  )}
                </div>
              )}
            </div>
          ))}
        </>
      )}
    </DefaultLayout>
  );
};

export default EggsOfTheDay;
