import DefaultLayout from '../../layout/DefaultLayout.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import EggListTable from '../../components/Egg/EggListTable.tsx';
import {useEffect, useRef, useState} from 'react';
// import {Tab, TabList, TabPanel, Tabs} from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import {useRequest} from 'ahooks';
import {requestToken} from '../../services/request.ts';
import {Link} from 'react-router-dom';
import {useTranslation} from 'react-i18next';

const EggList = () => {
    const {t} = useTranslation();
    const ref = useRef<any>(null);
    const tabNavRef = useRef<any>(null);
    const [breadCrumbHeight, setBreadCrumbHeight] = useState(0);
    const [tabNavHeight, setTabNavHeight] = useState(0);
    const [openTab, setOpenTab] = useState(1);
    const activeClasses = 'text-primary border-primary';
    const inactiveClasses = 'border-transparent';

    const {data: eggData, loading} = useRequest(() => {
        return requestToken({
            url: `/api/egg-d-table/1`,
            method: 'GET',
        });
    });

    useEffect(() => {
        if (eggData && Object.keys(eggData).length > 0) {
            if (eggData.tabs.length > 0) {
                eggData.tabs = eggData.tabs.filter((el: any) => {
                    return el.visibility;
                });
                setOpenTab(eggData.tabs[0].id);
            }
        }
    }, [eggData]);

    useEffect(() => {
        if (ref && ref.current) {
            setBreadCrumbHeight(ref.current.clientHeight);
        }
    }, [ref]);

    useEffect(() => {
        if (tabNavRef && tabNavRef.current) {
            setTabNavHeight(tabNavRef.current.clientHeight);
        }
    }, [tabNavRef, loading]);

    return (
        <DefaultLayout>
            <div ref={ref}>
                <Breadcrumb pageName={t('show_egg_table')} className="mb-6 flex flex-col-reverse gap-3" />
            </div>

            {!loading && eggData?.tabs?.length === 0 && (
                <div className="text-center flex flex-1 items-center justify-center h-[50vh]">No Data Found</div>
            )}

            {!loading && eggData?.tabs?.length > 0 && (
                <>
                    <div
                        className="bg-gray-2 dark:bg-meta-4 border border-[#eee] dark:border-strokedark rounded font-medium text-sm text-black dark:text-white p-2">
                        {t('total_egg_count')}: {eggData?.total_ecount || 'None'}
                    </div>

                    <div ref={tabNavRef}
                         className="mb-6 flex gap-4 sm:gap-8 border-b border-stroke dark:border-strokedark overflow-x-auto pr-4">
                        {eggData.tabs.map((tab: any) => (
                            <Link key={tab.id} to="#"
                                  className={`border-b-2 py-4 text-sm font-medium hover:text-primary md:text-base min-w-[80px] ${openTab === tab.id ? activeClasses : inactiveClasses}`}
                                  onClick={() => setOpenTab(tab.id)}>
                                {tab.name}: {tab.id}
                            </Link>
                        ))}
                    </div>

                    {eggData.tabs.map((tab: any) => (
                        <div key={tab.id}>
                            {openTab === tab.id && (
                                <div
                                    className={`flex flex-col leading-relaxed ${openTab === tab.id ? 'block' : 'hidden'}`}
                                    style={{height: `calc(100vh - ${breadCrumbHeight}px - ${tabNavHeight}px - 7.5rem - 118px`}}>
                                    <EggListTable tab={tab}/>
                                </div>
                            )}
                        </div>
                    ))}
                </>
            )}
        </DefaultLayout>
    );
};

export default EggList;
