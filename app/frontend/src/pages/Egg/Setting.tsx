import DefaultLayout from '../../layout/DefaultLayout.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import EggSettingTable from '../../components/Egg/SettingTable.tsx';
import Datepicker, { DateValueType } from 'react-tailwindcss-datepicker';
import { useEffect, useRef, useState } from 'react';
import 'react-tabs/style/react-tabs.css';
import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request.ts';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import moment from 'moment/moment';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { getLanguageObject } from '../../services/Helpers.ts';

type Params = {
  from_date: string | null | Date,
  to_date: string | null | Date,
}

const EggSetting = () => {
  const { t } = useTranslation();
  const dateFormat = 'YYYY-MM-DD';
  const ref = useRef<any>(null);
  const tabNavRef = useRef<any>(null);
  const [breadCrumbHeight, setBreadCrumbHeight] = useState(0);
  const [tabNavHeight, setTabNavHeight] = useState(0);
  const [openTab, setOpenTab] = useState(1);
  const activeClasses = 'text-primary border-primary';
  const inactiveClasses = 'border-transparent';
  const [params, setParams] = useState<Params>({
    from_date: moment().format(dateFormat),
    to_date: moment().format(dateFormat)
  });
  const locale = getLanguageObject();

  const { control, handleSubmit } = useForm({
    defaultValues: {
      from_date: params.from_date,
      to_date: params.to_date
    },
  });

  const { data: eggData, loading } = useRequest(() => {
    return requestToken({
      url: `/api/egg-d-table/1`,
      method: 'GET',
      data: params
    });
  });

  useEffect(() => {
    if (eggData && Object.keys(eggData).length > 0) {
      if (eggData.tabs.length > 0) {
        eggData.tabs = eggData.tabs.filter((el: any) => {
          return el.visibility;
        });
        setOpenTab(eggData.tabs[0].id);
      }
    }
  }, [eggData]);

  useEffect(() => {
    if (ref && ref.current) {
      setBreadCrumbHeight(ref.current.clientHeight);
    }
  }, [ref]);

  useEffect(() => {
    if (tabNavRef && tabNavRef.current) {
      setTabNavHeight(tabNavRef.current.clientHeight);
    }
  }, [tabNavRef, loading]);

  // Date submit
  const handleDateSubmit: SubmitHandler<any> = async (data: any)=> {
    setParams(data);
  };

  return (
    <DefaultLayout>
      <div ref={ref}>
        <Breadcrumb pageName={t('egg_display_settings')} className="mb-6 flex flex-col-reverse gap-3" />
      </div>

      <form onSubmit={handleSubmit(handleDateSubmit)} className="flex flex-col md:flex-row items-center gap-2 mb-4">
        <p className="font-medium text-black dark:text-white">{t('period')}:</p>

        <div className="flex items-center gap-2 flex-wrap justify-center">
          <div className="w-[45%] md:w-auto">
            <Controller
              name="from_date"
              control={control}
              rules={{ required: false }}
              render={({ field }) => (
                <Datepicker
                  i18n={locale.code}
                  asSingle={true}
                  useRange={false}
                  primaryColor={'blue'}
                  value={{
                    startDate: field.value,
                    endDate: field.value,
                  }}
                  onChange={(newValue: DateValueType) => {
                    field.onChange(newValue.startDate);
                  }}
                  inputClassName={`w-full rounded border-[1.5px] border-stroke bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary`}
                />
              )}
            />
          </div>

          <div>~</div>

          <div className="w-[45%] md:w-auto">
            <Controller
              name="to_date"
              control={control}
              rules={{ required: false }}
              render={({ field }) => (
                <Datepicker
                  i18n={locale.code}
                  asSingle={true}
                  useRange={false}
                  primaryColor={'blue'}
                  value={{
                    startDate: field.value,
                    endDate: field.value,
                  }}
                  onChange={(newValue: DateValueType) => {
                    field.onChange(newValue.startDate);
                  }}
                  inputClassName={`w-full rounded border-[1.5px] border-stroke bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary`}
                />
              )}
            />
          </div>

          <div className="w-2/5 md:w-auto">
            <button type="submit" className="block w-full rounded bg-primary py-1 px-3 text-center font-medium text-white transition hover:bg-opacity-90">
              {t('submit')}
            </button>
          </div>
        </div>
      </form>

      {!loading && eggData?.tabs?.length === 0 && (
        <div className="text-center flex flex-1 items-center justify-center h-[50vh]">No Data Found</div>
      )}

      {!loading && eggData?.tabs?.length > 0 && (
        <>
          <div className="bg-gray-2 dark:bg-meta-4 border border-[#eee] dark:border-strokedark rounded font-medium text-black dark:text-white p-4 mb-4">
            {t('total_egg_count')}: {eggData?.total_ecount || 'None'}
          </div>

          <div ref={tabNavRef} className="mb-6 flex gap-4 sm:gap-8 border-b border-stroke dark:border-strokedark overflow-x-auto pr-4">
            {eggData.tabs.map((tab: any) => (
              <Link key={tab.id} to="#" className={`border-b-2 py-4 text-sm font-medium hover:text-primary md:text-base min-w-[80px] ${openTab === tab.id ? activeClasses : inactiveClasses}`}
                    onClick={() => setOpenTab(tab.id)}>
                {tab.name}: {tab.id}
              </Link>
            ))}
          </div>

          {eggData.tabs.map((tab: any) => (
            <div key={tab.id}>
              {openTab === tab.id && (
                <div className={`flex flex-col leading-relaxed ${openTab === tab.id ? 'block' : 'hidden'}`}
                  style={{ height: `calc(100vh - ${breadCrumbHeight}px - ${tabNavHeight}px - 7.5rem - 118px` }}>
                  <EggSettingTable tab={tab} params={params} />
                </div>
              )}
            </div>
          ))}
        </>
      )}
    </DefaultLayout>
  );
};

export default EggSetting;
