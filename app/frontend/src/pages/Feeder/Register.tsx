import DefaultLayout from '../../layout/DefaultLayout.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import { useEffect, useState } from 'react';
import 'react-tabs/style/react-tabs.css';
import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request.ts';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import RegisterForm from '../../components/Feeder/RegisterForm.tsx';

const RegisterFeeder = () => {
  const {t} = useTranslation();
  const [tabs, setTabs] = useState([]);
  const [openTab, setOpenTab] = useState(1);
  const activeClasses = 'text-primary border-primary';
  const inactiveClasses = 'border-transparent';

  const { data, loading } = useRequest(() => {
    return requestToken({
      url: `/api/fooder-reg/1`,
      method: 'GET'
    });
  });

  // Get tabs
  useEffect(() => {
    let getTabs: any = [];

    if (data && data.tabs && data.tabs.length > 0) {
      data.tabs.map((tab: any) => {
        if (tab.visibility) {
          getTabs.push(tab);
        }
      });

      setTabs(getTabs);
    }

    if (getTabs.length > 0) {
      setOpenTab(getTabs[0].id);
    }
  }, [loading]);

  return (
    <DefaultLayout>
      <Breadcrumb pageName={t('register_feeder_starting_time')} className="mb-6 flex flex-col-reverse gap-3" />

      {!loading && tabs.length === 0 && (
        <div className="text-center flex flex-1 items-center justify-center h-[50vh]">No Data Found</div>
      )}

      {!loading && tabs.length > 0 && (
        <>
          <div className="mb-6 flex gap-4 sm:gap-8 border-b border-stroke dark:border-strokedark overflow-x-auto pr-4">
            {tabs.map((tab: any) => (
              <Link
                key={tab.id}
                to="#"
                className={`border-b-2 py-4 text-sm font-medium hover:text-primary md:text-base min-w-[80px] ${
                  openTab === tab.id ? activeClasses : inactiveClasses
                }`}
                onClick={() => setOpenTab(tab.id)}
              >
                {tab.name}: {tab.id}
              </Link>
            ))}
          </div>

          {tabs.map((tab: any) => (
            <div key={tab.id}>
              {openTab === tab.id && (
                <div className={`flex flex-col leading-relaxed ${openTab === tab.id ? 'block' : 'hidden'}`}>
                  <RegisterForm tab={tab}/>
                </div>
              )}
            </div>
          ))}
        </>
      )}
    </DefaultLayout>
  );
};

export default RegisterFeeder;
