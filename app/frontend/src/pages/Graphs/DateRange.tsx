import DefaultLayout from '../../layout/DefaultLayout.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request.ts';
import { Link } from 'react-router-dom';
import { SubmitHandler, useForm } from 'react-hook-form';
import GraphRangeChart from '../../components/Graphs/RangeChart.tsx';

type Params = {
  from_week: number,
  to_week: number,
}

const DatePeriodGraphs = () => {
  const {t} = useTranslation();
  const ref = useRef<any>(null);
  const tabNavRef = useRef<any>(null);
  const [tabs, setTabs] = useState([]);
  const [openTab, setOpenTab] = useState(1);
  const activeClasses = 'text-primary border-primary';
  const inactiveClasses = 'border-transparent';
  const [params, setParams] = useState<Params>({
    from_week: 1,
    to_week: 10
  });

  const { register, handleSubmit } = useForm({
    defaultValues: {
      from_week: params.from_week,
      to_week: params.to_week
    },
  });

  const { data: dailyData, loading } = useRequest(() => {
    return requestToken({
      url: `/api/graph-spec-after/1`,
      method: 'GET',
      data: params
    });
  }, {refreshDeps: [params]});

  // Get tabs
  useEffect(() => {
    let getTabs: any = [];

    if (dailyData && dailyData.tabs && dailyData.tabs.length > 0) {
      dailyData.tabs.map((tab: any) => {
        if (tab.visibility) {
          getTabs.push(tab);
        }
      });

      setTabs(getTabs);
    }

    if (getTabs.length > 0) {
      setOpenTab(getTabs[0].id);
    }
  }, [loading]);

  // Week submit
  const handleWeekSubmit: SubmitHandler<any> = async (data: any)=> {
    setParams(data);
  };

  return (
    <DefaultLayout>
      <div ref={ref}>
        <Breadcrumb pageName={t('custom_range_graph')} className="mb-6 flex flex-col-reverse gap-3" />
      </div>

      <form onSubmit={handleSubmit(handleWeekSubmit)} className="flex flex-col md:flex-row items-center gap-2 mb-4">
        <p className="font-medium text-black dark:text-white">{t('week_range')}:</p>

        <div className="flex items-center gap-2">
          <div className="w-1/3 md:w-16">
            <input
              {...register("from_week")}
              type="number"
              className="text-center w-full rounded border-[1.5px] border-stroke bg-transparent p-1 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary [appearance:textfield] [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:appearance-none" />
          </div>

          <div>~</div>

          <div className="w-1/3 md:w-16">
            <input
              {...register("to_week")}
              type="number"
              className="text-center w-full rounded border-[1.5px] border-stroke bg-transparent p-1 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary [appearance:textfield] [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:appearance-none" />
          </div>

          <div className="w-1/3 md:w-auto">
            <button type="submit" className="block w-full rounded bg-primary py-1 px-3 text-center font-medium text-white transition hover:bg-opacity-90">
              {t('submit')}
            </button>
          </div>
        </div>
      </form>

      {!loading && tabs.length === 0 && (
        <div className="text-center flex flex-1 items-center justify-center h-[50vh]">No Data Found</div>
      )}

      {!loading && tabs.length > 0 && (
        <>
          <div ref={tabNavRef}
               className="mb-6 flex gap-4 sm:gap-8 border-b border-stroke dark:border-strokedark overflow-x-auto pr-4">
            {tabs.map((tab: any) => (
              <Link
                key={tab.id}
                to="#"
                className={`border-b-2 py-4 text-sm font-medium hover:text-primary md:text-base min-w-[80px] ${
                  openTab === tab.id ? activeClasses : inactiveClasses
                }`}
                onClick={() => setOpenTab(tab.id)}
              >
                {tab.name}: {tab.id}
              </Link>
            ))}
          </div>

          {tabs.map((tab: any) => (
            <div key={tab.id}>
              {openTab === tab.id && (
                <div className={`flex flex-col leading-relaxed ${openTab === tab.id ? 'block' : 'hidden'}`}>
                  <GraphRangeChart tab={tab}/>
                </div>
              )}
            </div>
          ))}
        </>
      )}
    </DefaultLayout>
  );
};

export default DatePeriodGraphs;
