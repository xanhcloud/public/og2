import DefaultLayout from '../../layout/DefaultLayout.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import { useEffect, useRef, useState } from 'react';
import 'react-tabs/style/react-tabs.css';
import { useRequest } from 'ahooks';
import { requestToken } from '../../services/request.ts';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import Day3Chart from '../../components/Graphs/Day3Chart.tsx';
import { getLanguageObject } from '../../services/Helpers.ts';
import { Controller, useForm } from 'react-hook-form';
import Datepicker, { DateValueType } from 'react-tailwindcss-datepicker';
import useLocalStorage from '../../hooks/useLocalStorage.tsx';
import moment from 'moment/moment';

type Params = {
  date: string | null | Date,
}

const Day3Graphs = () => {
  const locale = getLanguageObject();
  const dateFormat = 'YYYY-MM-DD';
  const {t} = useTranslation();
  const ref = useRef<any>(null);
  const tabNavRef = useRef<any>(null);
  const [filterDate, setFilterDate] = useLocalStorage('filterDate', moment().format(dateFormat));
  const [params, setParams] = useState<Params>({
    date: filterDate
  });
  const [tabs, setTabs] = useState([]);
  const [openTab, setOpenTab] = useState(1);
  const activeClasses = 'text-primary border-primary';
  const inactiveClasses = 'border-transparent';

  const { control } = useForm({
    defaultValues: {
      date: params.date
    },
  });

  const { data: graphs, loading } = useRequest(() => {
    return requestToken({
      url: '/api/graph-3days/1',
      method: 'GET',
      data: params
    });
  }, {refreshDeps: [params]});

  // Get tabs
  useEffect(() => {
    let getTabs: any = [];

    if (graphs && graphs.tabs && graphs.tabs.length > 0) {
      graphs.tabs.map((tab: any) => {
        if (tab.visibility) {
          getTabs.push(tab);
        }
      });

      setTabs(getTabs);
    }

    if (getTabs.length > 0) {
      setOpenTab(getTabs[0].id);
    }
  }, [loading]);

  return (
    <DefaultLayout>
      <div ref={ref} className={'flex flex-col gap-3 sm:flex-row sm:items-center sm:justify-between mb-6'}>
        <Breadcrumb pageName={t('3_days_graph')} className="flex flex-col-reverse gap-3" />

        <div className={'flex gap-2'}>
          <Controller
            name="date"
            control={control}
            rules={{ required: false }}
            render={({ field }) => (
              <Datepicker
                i18n={locale.code}
                asSingle={true}
                useRange={false}
                readOnly={true}
                primaryColor={'blue'}
                placeholder={' '}
                value={{
                  startDate: field.value || null,
                  endDate: field.value || null
                }}
                onChange={(newValue: DateValueType) => {
                  if (newValue && newValue.startDate) {
                    field.onChange(newValue.startDate);
                    setFilterDate(newValue.startDate.toString());
                    setParams((prev) => ({ ...prev, date: newValue.startDate }));
                  }
                }}
                inputClassName={`w-full rounded border-[1.5px] border-stroke py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary`}
              />
            )}
          />
        </div>
      </div>

      {!loading && tabs.length === 0 && (
        <div className="text-center flex flex-1 items-center justify-center h-[50vh]">No Data Found</div>
      )}

      {!loading && tabs.length > 0 && (
        <>
          <div ref={tabNavRef}
               className="mb-6 flex gap-4 sm:gap-8 border-b border-stroke dark:border-strokedark overflow-x-auto pr-4">
            {tabs.map((tab: any) => (
              <Link
                key={tab.id}
                to="#"
                className={`border-b-2 py-4 text-sm font-medium hover:text-primary md:text-base min-w-[80px] ${
                  openTab === tab.id ? activeClasses : inactiveClasses
                }`}
                onClick={() => setOpenTab(tab.id)}
              >
                {tab.name}: {tab.id}
              </Link>
            ))}
          </div>

          {tabs.map((tab: any) => (
            <div key={tab.id}>
              {openTab === tab.id && (
                <div className={`flex flex-col leading-relaxed ${openTab === tab.id ? 'block' : 'hidden'}`}>
                  <Day3Chart tab={tab} params={params} />
                </div>
              )}
            </div>
          ))}
        </>
      )}
    </DefaultLayout>
  );
};

export default Day3Graphs;
