import DefaultLayout from '../../layout/DefaultLayout.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import 'react-tabs/style/react-tabs.css';
import { useTranslation } from 'react-i18next';
import { useState, useEffect, useRef } from 'react';
import { requestToken } from '../../services/request.ts';
import { useRequest } from 'ahooks';
import BatchFormModal from '../../components/Group/BatchFormModal.tsx';
import BatchListTable from '../../components/Group/BatchListTable.tsx';
import { BatchList } from '../../types/batch.ts';
import ConfirmRemoveModal from '../../components/ConfirmRemoveModal.tsx';

const ListBatch = () => {
  const [refresh, setRefresh] = useState(0);
  const {t} = useTranslation();
  const ref = useRef<any>(null);
  const [breadCrumbHeight, setBreadCrumbHeight] = useState<number>(0);
  const [batchId, setBatchId] = useState<number>(0);
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [isStartBatch, setIsStartBatch] = useState<boolean>(false);
  const [isEndBatch, setIsEndBatch] = useState<boolean>(false);
  const [modalRemoveOpen, setModalRemoveOpen] = useState<boolean>(false);

  const { data, loading } = useRequest(() => {
    return requestToken({
      url: '/api/batch-reg',
      method: 'GET',
    });
  }, {refreshDeps: [refresh]});

  // Calculator element height
  useEffect(() => {
    if (ref && ref.current) {
      setBreadCrumbHeight(ref.current.clientHeight);
    }
  }, [ref]);

  // Handle on delete
  const handleOnDelete = async () => {
    alert('the feature is under maintenance');
    // if (!batchId || batchId < 1) return;
    // try {
    //   await requestToken({
    //     method: 'DELETE',
    //     url: `/api/batch-reg/${batchId}`,
    //   });
    //
    //   setBatchId(0);
    //   setModalRemoveOpen(false);
    //   setRefresh((r) => r + 1);
    // } catch (e: any) {
    //   console.log(e);
    // }
  }

  return (
    <DefaultLayout>
      <div ref={ref} className={'flex flex-col gap-3 sm:flex-row sm:items-center sm:justify-between mb-6'}>
        <Breadcrumb pageName={t('group_register_batch')} className="flex flex-col-reverse gap-3" />

        <div className={'flex gap-2'}>
          <button
            type="button"
            className="rounded-md bg-primary py-2 px-4 text-center font-medium text-white hover:bg-opacity-90"
            onClick={() => {
              setBatchId(0);
              setIsStartBatch(true);
              setModalOpen(true);
            }}
          >
            {t('new_batch')}
          </button>
        </div>
      </div>

      <div className="flex flex-col leading-relaxed" style={{ height: `calc(100vh - ${breadCrumbHeight}px - 5.5rem - 80px` }}>
        <BatchListTable
          data={data?.batchs || []}
          loading={loading}
          rowSelectEnable={false}
          handleEditAction={(row: BatchList) => {
            setBatchId(row.BATCH_ID);
            setIsStartBatch(false);
            setIsEndBatch(false);
            setModalOpen(true);
          }}
          handleRemoveAction={(row: BatchList) => {
            setBatchId(row.BATCH_ID);
            setIsStartBatch(false);
            setIsEndBatch(false);
            setModalRemoveOpen(true);
          }}
          handleStartBatch={(row: BatchList) => {
            setBatchId(row.BATCH_ID);
            setIsStartBatch(true);
            setIsEndBatch(false);
            setModalOpen(true);
          }}
          handleEndBatch={(row: BatchList) => {
            setBatchId(row.BATCH_ID);
            setIsStartBatch(false);
            setIsEndBatch(true);
            setModalOpen(true);
          }}
        />
      </div>

      <ConfirmRemoveModal
        open={modalRemoveOpen}
        onOpenChange={setModalRemoveOpen}
        onConfirm={handleOnDelete}
      />

      <BatchFormModal
        id={batchId}
        open={modalOpen}
        isStartBatch={isStartBatch}
        onOpenChange={setModalOpen}
        onRefresh={setRefresh}
        isEndBatch={isEndBatch}
      />
    </DefaultLayout>
  );
};

export default ListBatch;
