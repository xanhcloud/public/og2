import DefaultLayout from '../../layout/DefaultLayout.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import 'react-tabs/style/react-tabs.css';
import { useTranslation } from 'react-i18next';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useState, useEffect } from 'react';
import { getTimeSlots } from '../../services/Helpers.ts';
import { requestToken } from '../../services/request.ts';
import Datepicker, { DateValueType } from 'react-tailwindcss-datepicker';
import { useRequest } from 'ahooks';
import { useNavigate } from 'react-router-dom';

const RegisterBatch = () => {
  const navigate = useNavigate();
  const {t} = useTranslation();
  const [timeSlots, setTimeSlots] = useState<string[]>([]);
  const [message, setMessage] = useState<string>('');

  const { control, register, handleSubmit, reset } = useForm({
    defaultValues: {},
  });

  const { data, loading } = useRequest(() => {
    return requestToken({
      url: '/api/hengrp-reg',
      method: 'GET',
    });
  });

  // Get timeslots
  useEffect(() => {
    setTimeSlots(getTimeSlots());
  }, []);

  // Show alert timeout
  useEffect(() => {
    if (message) {
      const timeout = setTimeout(() => setMessage(null), 2000);
      return () => {
        clearTimeout(timeout);
      };
    }
  }, [message]);

  // Handle submit
  const handleOnSubmit: SubmitHandler<any> = async (data: any) => {
    try {
      const result = await requestToken({
        method: 'POST',
        url: '/api/hengrp-reg',
        data
      });

      if (result && (result.r_msg || result.message)) {
        setMessage(result.r_msg || result.message || 'Successfully');
      }

      if (result && result.hengrps) {
        navigate('/group/batch');
      }
    } catch (e: any) {
      console.log('Error', e);
      setMessage('Error');
    }
  }

  return (
    <DefaultLayout>
      <Breadcrumb pageName={t('group_register_batch')} className="mb-6 flex flex-col-reverse gap-3" />

      {message && (
        <div className={`flex items-center w-full border-l-6 bg-opacity-[15%] shadow-md dark:bg-[#1B1B24] dark:bg-opacity-30 p-4 mb-4 ${message === 'Error' ? 'border-[#F87171] bg-[#F87171]' : 'border-[#34D399] bg-[#34D399]'}`}>
          <div className={`mr-5 flex h-9 w-full max-w-[36px] items-center justify-center rounded-lg ${message === 'Error' ? 'bg-[#F87171]' : 'bg-[#34D399]'}`}>
            {message === 'Error' ? (
              <svg
                width="13"
                height="13"
                viewBox="0 0 13 13"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M6.4917 7.65579L11.106 12.2645C11.2545 12.4128 11.4715 12.5 11.6738 12.5C11.8762 12.5 12.0931 12.4128 12.2416 12.2645C12.5621 11.9445 12.5623 11.4317 12.2423 11.1114C12.2422 11.1113 12.2422 11.1113 12.2422 11.1113C12.242 11.1111 12.2418 11.1109 12.2416 11.1107L7.64539 6.50351L12.2589 1.91221L12.2595 1.91158C12.5802 1.59132 12.5802 1.07805 12.2595 0.757793C11.9393 0.437994 11.4268 0.437869 11.1064 0.757418C11.1063 0.757543 11.1062 0.757668 11.106 0.757793L6.49234 5.34931L1.89459 0.740581L1.89396 0.739942C1.57364 0.420019 1.0608 0.420019 0.740487 0.739944C0.42005 1.05999 0.419837 1.57279 0.73985 1.89309L6.4917 7.65579ZM6.4917 7.65579L1.89459 12.2639L1.89395 12.2645C1.74546 12.4128 1.52854 12.5 1.32616 12.5C1.12377 12.5 0.906853 12.4128 0.758361 12.2645L1.1117 11.9108L0.758358 12.2645C0.437984 11.9445 0.437708 11.4319 0.757539 11.1116C0.757812 11.1113 0.758086 11.111 0.75836 11.1107L5.33864 6.50287L0.740487 1.89373L6.4917 7.65579Z"
                  fill="#ffffff"
                  stroke="#ffffff"
                ></path>
              </svg>
            ) : (
              <svg
                width="16"
                height="12"
                viewBox="0 0 16 12"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M15.2984 0.826822L15.2868 0.811827L15.2741 0.797751C14.9173 0.401867 14.3238 0.400754 13.9657 0.794406L5.91888 9.45376L2.05667 5.2868C1.69856 4.89287 1.10487 4.89389 0.747996 5.28987C0.417335 5.65675 0.417335 6.22337 0.747996 6.59026L0.747959 6.59029L0.752701 6.59541L4.86742 11.0348C5.14445 11.3405 5.52858 11.5 5.89581 11.5C6.29242 11.5 6.65178 11.3355 6.92401 11.035L15.2162 2.11161C15.5833 1.74452 15.576 1.18615 15.2984 0.826822Z"
                  fill="white"
                  stroke="white"
                ></path>
              </svg>
            )}
          </div>
          <div className="w-full">
            <p className="text-base leading-relaxed text-black">
              {message}
            </p>
          </div>
        </div>
      )}

      <div className="max-w-full rounded-sm border border-stroke shadow-default dark:border-strokedark dark:bg-boxdark bg-white p-4">
        {loading && (
          <div className="text-center flex flex-1 items-center justify-center py-4">Loading...</div>
        )}

        {!loading && (
          <form>
            <div className="grid grid-cols-12 gap-4 mb-4">
              {/*col*/}
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('flock')}</p>
                  </div>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3 relative z-5">
                    <select {...register('hengrp_id')}
                            className="relative z-5 w-full text-black appearance-none rounded border border-stroke bg-transparent py-1 px-3 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input">
                      <option value={0} className="text-body dark:text-bodydark"></option>
                      {data && data.hengrps && data.hengrps.length > 0 && data.hengrps.map((item: any) => (
                        <option key={item.HENGRP_ID} value={item.HENGRP_ID}>{item.HENGRP_NAME}</option>
                      ))}
                    </select>
                    <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g opacity="0.8">
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                            fill="#637381"
                          ></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                </div>
              </div>

              {/*col*/}
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('new_group')}</p>
                  </div>

                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <input
                      {...register('hengrp_name')}
                      type="text"
                      className="w-full rounded border-[1.5px] border-stroke bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary" />
                  </div>
                </div>
              </div>

              {/*col*/}
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('lot')}</p>
                  </div>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3 relative z-5">
                    <select {...register('lot_id')}
                            className="relative z-5 w-full text-black appearance-none rounded border border-stroke bg-transparent py-1 px-3 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input">
                      <option value={0} className="text-body dark:text-bodydark"></option>
                      {data && data.lots && data.lots.length > 0 && data.lots.map((item: any) => (
                        <option key={item.LOT_ID} value={item.LOT_ID}>{item.LOT_NAME}</option>
                      ))}
                    </select>
                    <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g opacity="0.8">
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                            fill="#637381"
                          ></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                </div>
              </div>

              {/*col*/}
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('chicken')}</p>
                  </div>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3 relative z-5">
                    <select {...register('hen_id')}
                            className="relative z-5 w-full text-black appearance-none rounded border border-stroke bg-transparent py-1 px-3 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input">
                      <option value={0} className="text-body dark:text-bodydark"></option>
                      {data && data.hens && data.hens.length > 0 && data.hens.map((item: any) => (
                        <option key={item.HEN_ID} value={item.HEN_ID}>{item.HEN_NAME}</option>
                      ))}
                    </select>
                    <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g opacity="0.8">
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                            fill="#637381"
                          ></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                </div>
              </div>

              {/*col*/}
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('growth_segment')}</p>
                  </div>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3 relative z-5">
                    <select {...register('growth_kind')}
                            className="relative z-5 w-full text-black appearance-none rounded border border-stroke bg-transparent py-1 px-3 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input">
                      <option value={0} className="text-body dark:text-bodydark"></option>
                      <option value={1}>{t('brooder')}</option>
                      <option value={2}>{t('pullet')}</option>
                      <option value={3}>{t('layer')}</option>
                    </select>
                    <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g opacity="0.8">
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                            fill="#637381"
                          ></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                </div>
              </div>

              {/*col*/}
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('batch_start_date')}</p>
                  </div>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <Controller
                      name="start_date"
                      control={control}
                      rules={{ required: false }}
                      render={({ field }) => (
                        <Datepicker
                          asSingle={true}
                          useRange={false}
                          primaryColor={'blue'}
                          placeholder={' '}
                          value={{
                            startDate: field.value || null,
                            endDate: field.value || null
                          }}
                          onChange={(newValue: DateValueType) => {
                            field.onChange(newValue.startDate);
                          }}
                          inputClassName={`w-full rounded border-[1.5px] border-stroke bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary`}
                        />
                      )}
                    />
                  </div>
                </div>
              </div>

              {/*col*/}
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('feeding_period')}</p>
                  </div>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <Controller
                      name="feeding_date"
                      control={control}
                      rules={{ required: false }}
                      render={({ field }) => (
                        <Datepicker
                          asSingle={true}
                          useRange={false}
                          primaryColor={'blue'}
                          placeholder={' '}
                          value={{
                            startDate: field.value || null,
                            endDate: field.value || null
                          }}
                          onChange={(newValue: DateValueType) => {
                            field.onChange(newValue.startDate);
                          }}
                          inputClassName={`w-full rounded border-[1.5px] border-stroke bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary`}
                        />
                      )}
                    />
                  </div>
                </div>
              </div>

              {/*col*/}
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('batch_initial_count')}</p>
                  </div>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <input
                      {...register('initial_count')}
                      type="number"
                      className="w-full rounded border-[1.5px] border-stroke bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary [appearance:textfield] [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:appearance-none" />
                  </div>
                </div>
              </div>

              {/*col*/}
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('batch_end_date')}</p>
                  </div>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <Controller
                      name="end_date"
                      control={control}
                      rules={{ required: false }}
                      render={({ field }) => (
                        <Datepicker
                          asSingle={true}
                          useRange={false}
                          primaryColor={'blue'}
                          placeholder={' '}
                          value={{
                            startDate: field.value || null,
                            endDate: field.value || null
                          }}
                          onChange={(newValue: DateValueType) => {
                            field.onChange(newValue.startDate);
                          }}
                          inputClassName={`w-full rounded border-[1.5px] border-stroke bg-transparent py-1 px-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary`}
                        />
                      )}
                    />
                  </div>
                </div>
              </div>

              {/*col*/}
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('egg_count_by_hour')}</p>
                  </div>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3 relative z-5">
                    <select {...register('eggcnt_stime')}
                            className="relative z-5 w-full text-black appearance-none rounded border border-stroke bg-transparent py-1 px-3 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input">
                      <option value="" className="text-body dark:text-bodydark"></option>
                      {timeSlots && timeSlots.length > 0 && (
                        timeSlots.map((time: string, index: number) => (
                          <option key={time} value={index}>{time}</option>
                        ))
                      )}
                    </select>
                    <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g opacity="0.8">
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                            fill="#637381"
                          ></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                </div>
              </div>

              {/*col*/}
              <div className="col-span-12">
                <div className="grid grid-cols-12 gap-2">
                  <div className="col-span-12 md:col-span-4 xl:col-span-3">
                    <p className="font-medium text-black dark:text-white">{t('egg_count_by_hour')}</p>
                  </div>
                  <div className="col-span-12 md:col-span-4 xl:col-span-3 relative z-5">
                    <select {...register('eggcnt_etime')}
                            className="relative z-5 w-full text-black appearance-none rounded border border-stroke bg-transparent py-1 px-3 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input">
                      <option value="" className="text-body dark:text-bodydark"></option>
                      {timeSlots && timeSlots.length > 0 && (
                        timeSlots.map((time: string, index: number) => (
                          <option key={time} value={index}>{time}</option>
                        ))
                      )}
                    </select>
                    <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g opacity="0.8">
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                            fill="#637381"
                          ></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            {/*col*/}
            <div className="-mx-3 flex flex-wrap gap-y-4 mt-6">
              <div className="2xsm:w-1/2 md:w-1/3 xl:w-1/4 2xl:w-1/5 px-3">
                <button
                  type="button"
                  onClick={handleSubmit(handleOnSubmit)}
                  className="block w-full rounded border border-primary bg-primary py-2 px-4 text-center font-medium text-white transition hover:bg-opacity-90"
                >
                  {t('save_changes')}
                </button>
              </div>
              <div className="2xsm:w-1/2 md:w-1/3 xl:w-1/4 2xl:w-1/5 px-3">
                <button
                  type="button"
                  onClick={() => reset()}
                  className="block w-full rounded border border-stroke bg-gray py-2 px-4 text-center font-medium text-black transition hover:border-primary hover:bg-primary hover:text-white dark:border-strokedark dark:bg-meta-4 dark:text-white dark:hover:border-primary dark:hover:bg-primary"
                >
                  {t('reset')}
                </button>
              </div>
            </div>
          </form>
        )}
      </div>
    </DefaultLayout>
  );
};

export default RegisterBatch;
