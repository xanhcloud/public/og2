import DefaultLayout from '../../layout/DefaultLayout.tsx';
import Breadcrumb from '../../components/Breadcrumbs/Breadcrumb.tsx';
import HistoryTable from '../../components/Weekly/HistoryTable.tsx';
import React, {useEffect, useRef, useState} from 'react';
// import {Tab, TabList, TabPanel, Tabs} from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import {useRequest} from 'ahooks';
import {requestToken} from '../../services/request.ts';
import {Link} from 'react-router-dom';
import {useTranslation} from "react-i18next";
import useLocalStorage from "../../hooks/useLocalStorage.tsx";

const WeeklyHistory = () => {
    const {t} = useTranslation();
    const ref = useRef<any>(null);
    const tabNavRef = useRef<any>(null);
    const [breadCrumbHeight, setBreadCrumbHeight] = useState(0);
    const [tabNavHeight, setTabNavHeight] = useState(0);
    const [openTab, setOpenTab] = useState(1);
    const activeClasses = 'text-primary border-primary';
    const inactiveClasses = 'border-transparent';
    const [activeTab, setActiveTab] = useLocalStorage('active-house-tab-weekly-history-table', null);

    const {data: historyData, loading} = useRequest(() => {
        return requestToken({
            url: `/api/w-history/1`,
            method: 'GET',
        });
    });

    const onSetOpenTab = (tabId: any) => {
        setOpenTab(tabId);
        setActiveTab(tabId);
    }

    useEffect(() => {
        if (historyData && Object.keys(historyData).length > 0) {
            if (historyData.tabs.length > 0) {
                historyData.tabs = historyData.tabs.filter((el: any) => {
                    return el.visibility;
                });
                if (activeTab && historyData.tabs.some((value: any) => { return value.id == activeTab })) {
                    setOpenTab(activeTab);
                } else {
                    setOpenTab(historyData.tabs[0].id);
                }
            }
        }
    }, [historyData]);

    useEffect(() => {
        if (ref && ref.current) {
            setBreadCrumbHeight(ref.current.clientHeight);
        }
    }, [ref]);

    useEffect(() => {
        if (tabNavRef && tabNavRef.current) {
            setTabNavHeight(tabNavRef.current.clientHeight);
        }
    }, [tabNavRef, loading]);

    return (
        <DefaultLayout>
            <div ref={ref}>
                <Breadcrumb pageName={t('weekly_history')} className="mb-6 flex flex-col-reverse gap-3" />
            </div>

            {!loading && historyData?.tabs?.length === 0 && (
                <div className="text-center flex flex-1 items-center justify-center h-[50vh]">No Data Found</div>
            )}

            {!loading && historyData?.tabs?.length > 0 && (
                <>
                    <div ref={tabNavRef}
                         className="mb-6 flex gap-4 sm:gap-8 border-b border-stroke dark:border-strokedark overflow-x-auto pr-4">
                        {historyData.tabs.map((tab: any) => (
                            <Link key={tab.id} to="#"
                                  className={`border-b-2 py-4 text-sm font-medium hover:text-primary md:text-base min-w-[80px] ${openTab === tab.id ? activeClasses : inactiveClasses}`}
                                  onClick={() => onSetOpenTab(tab.id)}>
                                {tab.name}: {tab.id}
                            </Link>
                        ))}
                    </div>

                    {historyData.tabs.map((tab: any) => (
                        <div key={tab.id}>
                            {openTab === tab.id && (
                                <>
                                    <div
                                        className={`flex flex-col leading-relaxed ${openTab === tab.id ? 'block' : 'hidden'}`}
                                        style={{height: `calc(100vh - ${breadCrumbHeight}px - ${tabNavHeight}px - 7.5rem - 118px`}}>
                                            <HistoryTable tab={openTab}/>
                                    </div>
                                </>
                            )}
                        </div>
                    ))}
                </>
            )}
        </DefaultLayout>
    );
};

export default WeeklyHistory;
