import { requestToken } from './request';

export default class ApiService {
  public api: string | undefined;

  getMany(params: any = {}) {
    if (!this.api) {
      throw new Error('api is undefined');
    }

    return requestToken({
      method: 'GET',
      url: this.api,
      data: params,
    });
  }

  getOne(id: any) {
    if (!this.api) {
      throw new Error('api is undefined');
    }

    return requestToken({
      method: 'GET',
      url: `${this.api}/${id}`,
    });
  }
}
