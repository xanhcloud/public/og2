import { request, requestToken } from './request';

class AuthService {
  async login(username: string, password: string) {
    try {
      const token = await request({
        method: 'POST',
        url: '/api/auth/token',
        data: {
          username,
          password,
        },
      });
      // set token
      this.setToken(token);
      return token;
    } catch (error) {
      throw error;
    }
  }

  setToken(token: any) {
    const hash = btoa(JSON.stringify(token));
    localStorage.setItem('7o23n', hash);
  }

  currentUser() {
    return requestToken({
      method: 'GET',
      url: '/api/auth/user',
    });
  }

  logout() {
    localStorage.removeItem('7o23n');
  }
}

export default new AuthService();
