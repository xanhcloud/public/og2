import { requestToken } from './request';

class DashboardService {
  getDashboard(params: any) {
    return requestToken({
      method: 'GET',
      url: '/api/dashboard',
      data: params
    });
  }
}

export default new DashboardService();
