import moment from 'moment';
import { LANGUAGES } from '../config/language.ts';

export function humanize(str: string) {
  if (str) {
    let i, frags = str.split('_');

    for (i = 0; i < frags.length; i++) {
      frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
    }

    return frags.join(' ');
  }

  return str;
}

export function getTimeSlots(start: string = '00:00', end: string = '23:59', amount: number = 60, timeFormat: string = 'HH:mm'): string[] {
  const startTime = moment(start, timeFormat);
  const endTime = moment(end, timeFormat);

  if (endTime.isBefore(startTime)) {
    endTime.add(1, 'day');
  }

  let timeStops = [];

  while (startTime <= endTime) {
    timeStops.push(moment(startTime).format(timeFormat));
    startTime.add(amount, 'minutes');
  }

  return timeStops;
}

export const growthSegments: any = {
  0: '',
  1: 'brooder',
  2: 'pullet',
  3: 'layer',
}

export const getLanguageObject = () => {
  let langObject: any;
  try {
    const lang = localStorage.getItem('language');
    langObject = lang ? JSON.parse(lang) : LANGUAGES[0];
  } catch (err: any) {
    langObject = LANGUAGES[0];
  }
  return langObject;
}

export const objKeyToLower = (obj: any) => {
  let item: any = {};
  Object.keys(obj).reduce((_acc: any, k: string) => {
    item[k.toLowerCase()] = obj[k];
  }, {});
  return item;
}

export const listColor = ['green', 'blue', 'navy', 'maroon', 'indigo', 'tomato', 'violet', 'gold', 'aqua', 'darkgreen', 'olive', 'mediumaquamarine', 'teal', 'purple', 'red'];

// export const getFilterDate = () => {
//   const format = 'YYYY-MM-DD';
//   let filterDate = localStorage.getItem('filterDate');
//   filterDate = filterDate ? moment(filterDate).format(format) : null;
//
//   if (!filterDate || !moment(filterDate).isValid()) {
//     filterDate = moment().format(format);
//   }
//
//   return filterDate;
// }
