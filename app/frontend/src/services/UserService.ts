import ApiService from './ApiService';

class UserService extends ApiService {
  constructor() {
    super();
    this.api = '/api/users';
  }
}

export default new UserService();
