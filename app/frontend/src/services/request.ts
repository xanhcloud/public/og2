import axios, { AxiosRequestConfig } from 'axios';
import { AppConfig } from '../config/app.config';

type RequestOptions = {
  method?: string;
  url: string;
  data?: any;
  headers?: any;
};

export async function request(options: RequestOptions) {
  const method = options.method || 'GET';

  let url = options.url;
  if (url.indexOf('http://') > 0 || url.indexOf('https://') > 0) {
    // @TODO
  } else {
    url = `${AppConfig.backend_url}${url}`;
  }

  let config: AxiosRequestConfig = {
    method,
    url,
    headers: options.headers || {},
  };

  if (method.toLocaleUpperCase() === 'GET') {
    config.params = options.data;
  } else {
    config.data = options.data;
  }

  try {
    const res = await axios.request(config);
    return res.data;
  } catch (err: any) {
    if (err.response) {
      throw new Error(err.response.data);
    }
    throw new Error(err.message);
  }
}

export async function requestToken(options: RequestOptions) {
  let headers = options.headers || {};
  const tokenEncode = localStorage.getItem('7o23n') || '';
  if (!tokenEncode) {
    throw new Error('token not found');
  }

  let token: any = {};
  if (tokenEncode) {
    token = atob(tokenEncode);
    token = JSON.parse(token);
  }

  if (token?.token) {
    headers = {
      ...headers,
      Authorization: `Token ${token.token}`,
    };
  } else {
    localStorage.removeItem('7o23n');
    throw new Error('invalid token');
  }

  return await request({
    ...options,
    headers,
  });
}
