export type House = {
  HOUSE_ID: number;
  HOUSE_NAME: string;
  CURR_COUNT?: number;
}

export type Lot = {
  LOT_ID: number;
  LOT_NAME: string;
  HOUSE_ID?: number;
  HOUSE_NAME?: string;
  CAGE_NUM?: number;
}

export type Hen = {
  HEN_ID: number;
  HEN_NAME: string;
}

export type Hengrp = {
  HENGRP_ID: number;
  HENGRP_NAME: string;
}

export type Batch = {
  BATCH_ID: number;
  GROWTH_KIND: number;
  START_DATE: string;
  FEEDING_DATE: string;
  INITIAL_COUNT: number;
  END_DATE: string;
  EGGCNT_STIME: number;
  EGGCNT_ETIME: number;
  CREATED_TDATE: Date;
  HEN_ID: number;
  LOT_ID: number;
  HENGRP_ID: number;
  HOUSE_ID: number;
}

export type BatchList = {
  BATCH_ID: number;
  HEN_NAME: string;
  HOUSE_NAME: string;
  LOT_NAME: string;
  GROWTH_KIND: number;
  START_DATE: string;
  FEEDING_DATE: string;
  INITIAL_COUNT: number;
  END_DATE: string;
  EGGCNT_STIME: number;
  EGGCNT_ETIME: number;
}

export type HengrpList = {
  batch?: Batch[];
  batchs?: BatchList;
  hengrps?: Hengrp[];
  hens?: Hen[];
  lots?: Lot[];
  sel_batch?: number
}

export type CurrCountByHouses = {
  HOUSE_ID: number;
  HOUSE_NAME: string;
  CURR_COUNT: number;
}
