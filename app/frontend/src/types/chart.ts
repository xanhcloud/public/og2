export type ChartDataOption = {
  graphs_data_x: any;
  left_data_y: any;
  right_data_y: any;
}
