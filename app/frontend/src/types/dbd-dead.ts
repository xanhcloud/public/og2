export type DBTDead = {
  tdead_id: number;
  dead_count: number;
  check_date: string;
  check_time: string;
  created_tdate: string;
  house: number;
  lot: number;
};
