import { ReactNode } from 'react';

export type FieldItem = {
  key: string;
  label: string;
  type: string;
  required: boolean;
  className: string;
  options?: any;
  valueKey?: string;
  labelKey?: string;
  placeholder?: string;
  indexValue?: boolean;
  startKey?: string;
  endKey?: string;
  readonly?: boolean;
  disabled?: boolean;
  prefix?: ReactNode;
  suffix?: ReactNode;
}
