import { render } from '@testing-library/react';

// Custom render function for tests
const customRender = (ui, options) => render(ui, { wrapper: TestWrapper, ...options });

export * from '@testing-library/react';
export { customRender as render };
