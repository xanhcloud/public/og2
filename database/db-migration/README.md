# OvalGrowth Data migrartion

## Description
OvalGrowth 1.0 uses MySQL 5.6 as the database server. OvalGrowth 2.0 uses Posrgres 16 as the database server.
This is the guidelines to help migrate the OvalGrowth database from a MySQL dump file to Postgres databasde.

## MySQL data dump file and generated files
The sample MySQL dump file is created using mysqldump command:
  - 20240605ovalgrowthdb.sql

The database schema and views are extracted from this file:
	grep -v "^INSERT" 20240605ovalgrowthdb.sql > og1_mysql_schema.sql
	grep -v "ALGORITHM" 20240605ovalgrowthdb.sql > og1_mysql_view.sql

## Postgres database files
The database schema and views are manually converted from MySQL files:
  - og1_postgres_schema.sql
  - og1_postgres_view.sql

Run script to generate the data importing SQL file from the MySQL dump file
	./create_og1_postgres_data.sh

The generated file for data importing:
  - og1_postgres_data.sql


## Migrate OG1 database to Postgres
  - Create the database
	psql -U <username> -c "CREATE DATABASE og1_stag;"

  - Create the Tables
	psql -U <username> -d og1_stag < og1_postgres_schema.sql	

  - Inport data to the Tables
	psql -U <username> -d og1_stag < og1_postgres_insert.sql

  - Inport Views
	psql -U <username> -d og1_stag < og1_postgres_view.sql

## Import OG1 data to new OG2 Postgres database
The OG2 database use the same OG1 database schema, with a few additional tables.
So we can still import data, which is exported from the OG1 database, to the new OG2 database.

  - Export the database schema
	pg_dump --schema-only -U <username> -d og2_stag > og2_postgres_schema.sql

  - Import data to the new database
	psql -U <username> -d og2_stag < og1_postgres_insert.sql

