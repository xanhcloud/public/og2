echo "Extract INSERT commands to a MySQL dump file ..."
grep -e "^INSERT" 20240605ovalgrowthdb.sql > mysql_insert.sql

echo "Make some Postgres-compatible modifications ..."
sed -i -e 's/`/"/g' mysql_insert.sql
sed -i -e 's/0000-00-00 00:00:00/2014-01-01 00:00:00/g' mysql_insert.sql

echo "Generate the Postgres INSERT dump file ..."
rm postgres_insert.sql
for item in dbm_dic dbm_user dbm_house dbm_lot dbm_lot_backup dbm_price dbm_thermo dbm_fooder dbm_fw dbm_hen dbt_hengrp dbt_batch dbt_collection dbt_summary dbm_egg dbt_eggchg dbt_egg_copy dbm_fooderst dbm_manual dbt_egg dbt_fooder dbt_fw dbt_thermo;
do
  grep -e "$item" mysql_insert.sql >> postgres_insert.sql
done

echo "Remove the MySQL dump file ..."
rm mysql_insert.sql

