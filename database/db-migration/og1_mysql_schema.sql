/*
Navicat MySQL Data Transfer

Source Server         : oval
Source Server Version : 50635
Source Host           : localhost:3306
Source Database       : ovalgrowthdb

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2024-06-05 13:44:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dbm_dic
-- ----------------------------
DROP TABLE IF EXISTS `dbm_dic`;
CREATE TABLE `dbm_dic` (
  `DIC_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TERM_NAME` varchar(32) DEFAULT NULL,
  `JP_NAME` varchar(64) DEFAULT NULL,
  `EN_NAME` varchar(64) DEFAULT NULL,
  `PT_NAME` varchar(64) DEFAULT NULL,
  `ES_NAME` varchar(64) DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`DIC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_dic
-- ----------------------------

-- ----------------------------
-- Table structure for dbm_egg
-- ----------------------------
DROP TABLE IF EXISTS `dbm_egg`;
CREATE TABLE `dbm_egg` (
  `MEGG_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `LOT_ID` int(10) unsigned NOT NULL,
  `EGGCTR_NO` smallint(6) NOT NULL,
  `CAGE_ROW` smallint(6) NOT NULL,
  `CAGE_TIER` smallint(6) NOT NULL,
  `REG_EGG` varchar(16) DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MEGG_ID`),
  KEY `egg_lot` (`LOT_ID`),
  CONSTRAINT `egg_lot` FOREIGN KEY (`LOT_ID`) REFERENCES `dbm_lot` (`LOT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_egg
-- ----------------------------

-- ----------------------------
-- Table structure for dbm_fooder
-- ----------------------------
DROP TABLE IF EXISTS `dbm_fooder`;
CREATE TABLE `dbm_fooder` (
  `MFOODER_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LOT_ID` int(11) unsigned NOT NULL,
  `FOODER_NO` smallint(6) NOT NULL,
  `FOODER_TYPE` smallint(6) NOT NULL,
  `REG_DEP` varchar(16) DEFAULT NULL,
  `REG_ARV` varchar(16) DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MFOODER_ID`),
  KEY `lot_footer` (`LOT_ID`),
  CONSTRAINT `lot_footer` FOREIGN KEY (`LOT_ID`) REFERENCES `dbm_lot` (`LOT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_fooder
-- ----------------------------

-- ----------------------------
-- Table structure for dbm_fooderst
-- ----------------------------
DROP TABLE IF EXISTS `dbm_fooderst`;
CREATE TABLE `dbm_fooderst` (
  `MFOODERST_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MFOODER_ID` int(10) unsigned NOT NULL,
  `START_TIME` time NOT NULL,
  `APPLY_DATE` date NOT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MFOODERST_ID`),
  KEY `mfooder_mfooderst` (`MFOODER_ID`),
  CONSTRAINT `mfooder_mfooderst` FOREIGN KEY (`MFOODER_ID`) REFERENCES `dbm_fooder` (`MFOODER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_fooderst
-- ----------------------------

-- ----------------------------
-- Table structure for dbm_fw
-- ----------------------------
DROP TABLE IF EXISTS `dbm_fw`;
CREATE TABLE `dbm_fw` (
  `MFW_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LOT_ID` int(10) unsigned NOT NULL,
  `FW_NO` smallint(6) NOT NULL,
  `REG_FOOD_UP` varchar(16) DEFAULT NULL,
  `REG_FOOD_LO` varchar(16) DEFAULT NULL,
  `REG_WATER` varchar(16) DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MFW_ID`),
  KEY `lot_fw` (`LOT_ID`),
  CONSTRAINT `lot_fw` FOREIGN KEY (`LOT_ID`) REFERENCES `dbm_lot` (`LOT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_fw
-- ----------------------------

-- ----------------------------
-- Table structure for dbm_hen
-- ----------------------------
DROP TABLE IF EXISTS `dbm_hen`;
CREATE TABLE `dbm_hen` (
  `HEN_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `HEN_NAME` varchar(128) DEFAULT NULL,
  `CREATED_TDATE` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`HEN_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_hen
-- ----------------------------

-- ----------------------------
-- Table structure for dbm_house
-- ----------------------------
DROP TABLE IF EXISTS `dbm_house`;
CREATE TABLE `dbm_house` (
  `HOUSE_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `HOUSE_NAME` varchar(64) DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`HOUSE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_house
-- ----------------------------

-- ----------------------------
-- Table structure for dbm_lot
-- ----------------------------
DROP TABLE IF EXISTS `dbm_lot`;
CREATE TABLE `dbm_lot` (
  `LOT_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `LOT_NAME` varchar(32) DEFAULT NULL,
  `HOUSE_ID` int(10) unsigned NOT NULL,
  `CAGE_NUM` int(10) unsigned DEFAULT NULL,
  `EQUIP_TYPE` smallint(6) NOT NULL,
  `COMM_TYPE` smallint(6) NOT NULL,
  `IP_ADDRESS` varchar(32) DEFAULT NULL,
  `FOOD_INPUT` smallint(6) NOT NULL,
  `EGGCO` smallint(6) NOT NULL,
  `STOP_COUNT` smallint(6) NOT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`LOT_ID`),
  KEY `hen_house` (`HOUSE_ID`),
  CONSTRAINT `hen_house` FOREIGN KEY (`HOUSE_ID`) REFERENCES `dbm_house` (`HOUSE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_lot
-- ----------------------------

-- ----------------------------
-- Table structure for dbm_lot_backup
-- ----------------------------
DROP TABLE IF EXISTS `dbm_lot_backup`;
CREATE TABLE `dbm_lot_backup` (
  `LOT_ID` int(11) unsigned NOT NULL DEFAULT '0',
  `LOT_NAME` varchar(32) DEFAULT NULL,
  `HOUSE_ID` int(10) unsigned NOT NULL,
  `CAGE_NUM` int(10) unsigned DEFAULT NULL,
  `EQUIP_TYPE` smallint(6) NOT NULL,
  `COMM_TYPE` smallint(6) NOT NULL,
  `IP_ADDRESS` varchar(32) DEFAULT NULL,
  `FOOD_INPUT` smallint(6) NOT NULL,
  `STOP_COUNT` smallint(6) NOT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_lot_backup
-- ----------------------------

-- ----------------------------
-- Table structure for dbm_manual
-- ----------------------------
DROP TABLE IF EXISTS `dbm_manual`;
CREATE TABLE `dbm_manual` (
  `MANUL_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `HEN_ID` int(10) unsigned NOT NULL,
  `WEEK_PERIODS` smallint(6) NOT NULL,
  `SURVIVAL_RATE` smallint(6) DEFAULT NULL,
  `HEN_WEIGHT` smallint(6) DEFAULT NULL,
  `EGG_WEIGHT` smallint(6) DEFAULT NULL,
  `EGG_RATE` smallint(6) DEFAULT NULL,
  `EGG_VOL` smallint(6) DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MANUL_ID`),
  KEY `hen` (`HEN_ID`),
  CONSTRAINT `hen` FOREIGN KEY (`HEN_ID`) REFERENCES `dbm_hen` (`HEN_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1111 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_manual
-- ----------------------------

-- ----------------------------
-- Table structure for dbm_price
-- ----------------------------
DROP TABLE IF EXISTS `dbm_price`;
CREATE TABLE `dbm_price` (
  `PRICE_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PRICE_DATE` date NOT NULL,
  `FOOD_PRICE` int(10) unsigned DEFAULT NULL,
  `STDEGG_PRICE` int(10) unsigned DEFAULT NULL,
  `OUTEGG_PRICE` int(10) unsigned DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PRICE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_price
-- ----------------------------

-- ----------------------------
-- Table structure for dbm_thermo
-- ----------------------------
DROP TABLE IF EXISTS `dbm_thermo`;
CREATE TABLE `dbm_thermo` (
  `MTHERMO_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LOT_ID` int(11) unsigned NOT NULL,
  `MTHERMO_TYPE` smallint(6) NOT NULL,
  `MTHERMO_NO` smallint(6) NOT NULL,
  `REG_CUR` varchar(16) DEFAULT NULL,
  `REG_HIGH` varchar(16) DEFAULT NULL,
  `REG_LOW` varchar(16) DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MTHERMO_ID`),
  KEY `lot_thermo` (`LOT_ID`),
  CONSTRAINT `lot_thermo` FOREIGN KEY (`LOT_ID`) REFERENCES `dbm_lot` (`LOT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_thermo
-- ----------------------------

-- ----------------------------
-- Table structure for dbm_user
-- ----------------------------
DROP TABLE IF EXISTS `dbm_user`;
CREATE TABLE `dbm_user` (
  `USER_ID` varchar(8) NOT NULL,
  `PASSWORD` varchar(8) NOT NULL,
  `USER_NAME` varchar(64) DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbm_user
-- ----------------------------

-- ----------------------------
-- Table structure for dbt_batch
-- ----------------------------
DROP TABLE IF EXISTS `dbt_batch`;
CREATE TABLE `dbt_batch` (
  `BATCH_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LOT_ID` int(11) unsigned NOT NULL,
  `HEN_ID` int(10) unsigned NOT NULL,
  `HENGRP_ID` int(10) unsigned NOT NULL,
  `GROWTH_KIND` smallint(6) DEFAULT NULL,
  `START_DATE` date DEFAULT NULL,
  `END_DATE` date DEFAULT NULL,
  `INITIAL_COUNT` mediumint(6) DEFAULT NULL,
  `FEEDING_DATE` date DEFAULT NULL,
  `EGGCNT_STIME` smallint(6) DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `EGGCNT_ETIME` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`BATCH_ID`),
  KEY `lot_batch` (`LOT_ID`),
  KEY `hen_batch` (`HEN_ID`),
  KEY `hengrp_batch` (`HENGRP_ID`),
  CONSTRAINT `hen_batch` FOREIGN KEY (`HEN_ID`) REFERENCES `dbm_hen` (`HEN_ID`),
  CONSTRAINT `hengrp_batch` FOREIGN KEY (`HENGRP_ID`) REFERENCES `dbt_hengrp` (`HENGRP_ID`),
  CONSTRAINT `lot_batch` FOREIGN KEY (`LOT_ID`) REFERENCES `dbm_lot` (`LOT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbt_batch
-- ----------------------------

-- ----------------------------
-- Table structure for dbt_collection
-- ----------------------------
DROP TABLE IF EXISTS `dbt_collection`;
CREATE TABLE `dbt_collection` (
  `COLLECT_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int(10) unsigned NOT NULL,
  `COL_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`COLLECT_ID`),
  KEY `batch_collect` (`BATCH_ID`),
  CONSTRAINT `batch_collect` FOREIGN KEY (`BATCH_ID`) REFERENCES `dbt_batch` (`BATCH_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2646991 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbt_collection
-- ----------------------------

-- ----------------------------
-- Table structure for dbt_egg
-- ----------------------------
DROP TABLE IF EXISTS `dbt_egg`;
CREATE TABLE `dbt_egg` (
  `TEGG_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `COLLECT_ID` int(10) unsigned NOT NULL,
  `MEGG_ID` int(10) unsigned NOT NULL,
  `EGG_COUNT` int(10) unsigned DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TEGG_ID`),
  KEY `collect_tegg` (`COLLECT_ID`),
  KEY `megg_tegg` (`MEGG_ID`),
  CONSTRAINT `collect_tegg` FOREIGN KEY (`COLLECT_ID`) REFERENCES `dbt_collection` (`COLLECT_ID`),
  CONSTRAINT `megg_tegg` FOREIGN KEY (`MEGG_ID`) REFERENCES `dbm_egg` (`MEGG_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=29716888 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbt_egg
-- ----------------------------

-- ----------------------------
-- Table structure for dbt_eggchg
-- ----------------------------
DROP TABLE IF EXISTS `dbt_eggchg`;
CREATE TABLE `dbt_eggchg` (
  `EGGCHG_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SUMMARY_ID` int(10) unsigned NOT NULL,
  `MEGG_ID` int(10) unsigned NOT NULL,
  `EGG_COUNT` int(10) unsigned DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`EGGCHG_ID`),
  KEY `summary_eggchg` (`SUMMARY_ID`),
  KEY `megg_eggchg` (`MEGG_ID`),
  CONSTRAINT `megg_eggchg` FOREIGN KEY (`MEGG_ID`) REFERENCES `dbm_egg` (`MEGG_ID`),
  CONSTRAINT `summary_eggchg` FOREIGN KEY (`SUMMARY_ID`) REFERENCES `dbt_summary` (`SUMMARY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbt_eggchg
-- ----------------------------

-- ----------------------------
-- Table structure for dbt_egg_copy
-- ----------------------------
DROP TABLE IF EXISTS `dbt_egg_copy`;
CREATE TABLE `dbt_egg_copy` (
  `TEGG_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `COLLECT_ID` int(10) unsigned NOT NULL,
  `MEGG_ID` int(10) unsigned NOT NULL,
  `EGG_COUNT` int(10) unsigned DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TEGG_ID`),
  KEY `collect_tegg` (`COLLECT_ID`),
  KEY `megg_tegg` (`MEGG_ID`),
  CONSTRAINT `dbt_egg_copy_ibfk_1` FOREIGN KEY (`COLLECT_ID`) REFERENCES `dbt_collection` (`COLLECT_ID`),
  CONSTRAINT `dbt_egg_copy_ibfk_2` FOREIGN KEY (`MEGG_ID`) REFERENCES `dbm_egg` (`MEGG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbt_egg_copy
-- ----------------------------

-- ----------------------------
-- Table structure for dbt_fooder
-- ----------------------------
DROP TABLE IF EXISTS `dbt_fooder`;
CREATE TABLE `dbt_fooder` (
  `TFOODER_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `COLLECT_ID` int(10) unsigned NOT NULL,
  `MFOODER_ID` int(10) unsigned NOT NULL,
  `DEPARTURE_TIME` time DEFAULT NULL,
  `ARRIVAL_TIME` time DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TFOODER_ID`),
  KEY `mfooder_tfooder` (`MFOODER_ID`),
  KEY `collect_tfooder` (`COLLECT_ID`),
  CONSTRAINT `collect_tfooder` FOREIGN KEY (`COLLECT_ID`) REFERENCES `dbt_collection` (`COLLECT_ID`),
  CONSTRAINT `mfooder_tfooder` FOREIGN KEY (`MFOODER_ID`) REFERENCES `dbm_fooder` (`MFOODER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7109869 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbt_fooder
-- ----------------------------

-- ----------------------------
-- Table structure for dbt_fw
-- ----------------------------
DROP TABLE IF EXISTS `dbt_fw`;
CREATE TABLE `dbt_fw` (
  `TFW_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `COLLECT_ID` int(10) unsigned NOT NULL,
  `MFW_ID` int(10) unsigned DEFAULT NULL,
  `FOOD_AMOUNT` int(10) unsigned DEFAULT NULL,
  `WATER_AMOUNT` int(10) unsigned DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TFW_ID`),
  KEY `collect_tfw` (`COLLECT_ID`),
  KEY `mfw_tfw` (`MFW_ID`),
  CONSTRAINT `collect_tfw` FOREIGN KEY (`COLLECT_ID`) REFERENCES `dbt_collection` (`COLLECT_ID`),
  CONSTRAINT `mfw_tfw` FOREIGN KEY (`MFW_ID`) REFERENCES `dbm_fw` (`MFW_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2265409 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbt_fw
-- ----------------------------

-- ----------------------------
-- Table structure for dbt_hengrp
-- ----------------------------
DROP TABLE IF EXISTS `dbt_hengrp`;
CREATE TABLE `dbt_hengrp` (
  `HENGRP_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `HENGRP_NAME` varchar(64) DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`HENGRP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbt_hengrp
-- ----------------------------

-- ----------------------------
-- Table structure for dbt_summary
-- ----------------------------
DROP TABLE IF EXISTS `dbt_summary`;
CREATE TABLE `dbt_summary` (
  `SUMMARY_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `BATCH_ID` int(10) unsigned NOT NULL,
  `SUMMARY_DATE` date NOT NULL,
  `DAY_PERIODS` smallint(6) DEFAULT NULL,
  `WEEK_PERIODS` smallint(6) DEFAULT NULL,
  `COLLECT_ID` int(10) unsigned NOT NULL,
  `EGG_COUNT` int(10) unsigned DEFAULT NULL,
  `FOOD_AMOUNT` int(10) unsigned DEFAULT NULL,
  `WATER_AMOUNT` int(10) unsigned DEFAULT NULL,
  `CURR_COUNT` int(10) unsigned DEFAULT NULL,
  `DEAD_COUNT` int(10) unsigned DEFAULT '0',
  `HEN_WEIGHT` smallint(6) DEFAULT '0',
  `EGG_WEIGHT` smallint(6) DEFAULT '0',
  `DEFECTION` smallint(6) DEFAULT NULL,
  `COEFF` smallint(6) DEFAULT NULL,
  `DAYLIGHT_HOURS` smallint(6) DEFAULT NULL,
  `PRESET_TEMP` smallint(6) DEFAULT NULL,
  `ROOM_TEMP` smallint(6) DEFAULT NULL,
  `ROOM_TEMP_HI` smallint(6) DEFAULT NULL,
  `ROOM_TEMP_LO` smallint(6) DEFAULT NULL,
  `OUTER_TEMP` smallint(6) DEFAULT NULL,
  `OUTER_TEMP_HI` smallint(6) DEFAULT NULL,
  `OUTER_TEMP_LO` smallint(6) DEFAULT NULL,
  `HUMIDITY` smallint(6) DEFAULT NULL,
  `HUMIDITY_UPL` smallint(6) DEFAULT '0',
  `HUMIDITY_LOL` smallint(6) DEFAULT '0',
  `MIN_VENTILATION` smallint(6) DEFAULT '0',
  `BMIN_VENTILATION` smallint(6) DEFAULT '0',
  `COMMENTS` varchar(256) DEFAULT '0',
  `REMV_EXCR` varchar(256) DEFAULT '0',
  `VENTILATION_SETUP` smallint(6) DEFAULT '0',
  `VENTILATION` smallint(6) DEFAULT '0',
  `FOOD_INTERVAL` varchar(256) DEFAULT '0',
  `HEATING` varchar(256) DEFAULT NULL COMMENT '0',
  `WATER_POS` varchar(256) DEFAULT '0',
  `AIRCON_SYSTEM` varchar(256) DEFAULT '0',
  `LIGHTING_TIME` varchar(256) DEFAULT '0',
  `FOOD` varchar(256) DEFAULT '0',
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`SUMMARY_ID`),
  KEY `batch_summary` (`BATCH_ID`),
  KEY `collect_summary` (`COLLECT_ID`),
  CONSTRAINT `batch_summary` FOREIGN KEY (`BATCH_ID`) REFERENCES `dbt_batch` (`BATCH_ID`),
  CONSTRAINT `collect_summary` FOREIGN KEY (`COLLECT_ID`) REFERENCES `dbt_collection` (`COLLECT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15859 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbt_summary
-- ----------------------------

-- ----------------------------
-- Table structure for dbt_thermo
-- ----------------------------
DROP TABLE IF EXISTS `dbt_thermo`;
CREATE TABLE `dbt_thermo` (
  `TTHERMO _ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `COLLECT_ID` int(10) unsigned NOT NULL,
  `MTHERMO_ID` int(10) unsigned NOT NULL,
  `CUR_VAL` smallint(6) DEFAULT NULL,
  `HIGH_VAL` smallint(6) DEFAULT NULL,
  `LOW_VAL` smallint(6) DEFAULT NULL,
  `CREATED_TDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TTHERMO _ID`),
  KEY `collect_tthermo` (`COLLECT_ID`),
  KEY `mthermo_tthermo` (`MTHERMO_ID`),
  CONSTRAINT `collect_tthermo` FOREIGN KEY (`COLLECT_ID`) REFERENCES `dbt_collection` (`COLLECT_ID`),
  CONSTRAINT `mthermo_tthermo` FOREIGN KEY (`MTHERMO_ID`) REFERENCES `dbm_thermo` (`MTHERMO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4531015 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbt_thermo
-- ----------------------------

-- ----------------------------
-- View structure for dbv_daily
-- ----------------------------
DROP VIEW IF EXISTS `dbv_daily`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dbv_daily` AS select `dbt_summary`.`SUMMARY_DATE` AS `DAILY_DATE`,`dbm_lot`.`LOT_ID` AS `LOT_ID`,`dbm_lot`.`LOT_NAME` AS `LOT_NAME`,`dbm_house`.`HOUSE_ID` AS `HOUSE_ID`,`dbm_house`.`HOUSE_NAME` AS `HOUSE_NAME`,`dbt_summary`.`DAY_PERIODS` AS `DAY_PERIODS`,`dbt_summary`.`EGG_COUNT` AS `EGG_COUNT`,`dbt_summary`.`CURR_COUNT` AS `CURR_COUNT`,(`dbt_summary`.`EGG_WEIGHT` * 0.1) AS `EGG_WEIGHT`,`dbt_summary`.`HEN_WEIGHT` AS `HEN_WEIGHT`,round((`dbt_summary`.`FOOD_AMOUNT` / 1000),1) AS `FOOD_AMOUNT`,round(((((`dbt_summary`.`FOOD_AMOUNT` / `dbt_summary`.`CURR_COUNT`) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `FOOD_AMOUNT_U`,round((((((`dbt_summary`.`EGG_WEIGHT` * `dbt_summary`.`EGG_COUNT`) / `dbt_summary`.`CURR_COUNT`) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `EGG_VOL`,`dbt_batch`.`INITIAL_COUNT` AS `INITIAL_COUNT`,round((((((`dbt_summary`.`CURR_COUNT` / `dbt_batch`.`INITIAL_COUNT`) * 100) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `SURVIVAL_RATE`,round((((((`dbt_summary`.`EGG_COUNT` / `dbt_summary`.`CURR_COUNT`) * 100) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `EGG_RATE_CUR`,round((((((`dbt_summary`.`EGG_COUNT` / `dbt_batch`.`INITIAL_COUNT`) * 100) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `EGG_RATE_INI`,round(((((`dbt_summary`.`HEN_WEIGHT` / (`dbt_summary`.`FOOD_AMOUNT` / `dbt_summary`.`CURR_COUNT`)) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `FOOD_EFFICEINCY`,round((((((`dbt_summary`.`FOOD_AMOUNT` / (`dbt_summary`.`EGG_COUNT` * `dbt_summary`.`EGG_WEIGHT`)) * 100) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `FOOD_DEMAND`,`dbt_summary`.`WATER_AMOUNT` AS `WATER_AMOUNT`,round((((((`dbt_summary`.`WATER_AMOUNT` / `dbt_summary`.`CURR_COUNT`) * 1000) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `WATER_AMOUNT_U`,round(((((`dbt_summary`.`PRESET_TEMP` / 10) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `PRESET_TEMP`,round(((((`dbt_summary`.`ROOM_TEMP` / 10) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `ROOM_TEMP`,round(((((`dbt_summary`.`ROOM_TEMP_HI` / 10) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `ROOM_TEMP_HI`,round(((((`dbt_summary`.`ROOM_TEMP_LO` / 10) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `ROOM_TEMP_LO`,round(((((`dbt_summary`.`OUTER_TEMP` / 10) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `OUTER_TEMP`,round(((((`dbt_summary`.`OUTER_TEMP_HI` / 10) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `OUTER_TEMP_HI`,round(((((`dbt_summary`.`OUTER_TEMP_LO` / 10) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `OUTER_TEMP_LO`,`dbt_summary`.`DAYLIGHT_HOURS` AS `DAYLIGHT_HOURS`,`dbt_summary`.`DEFECTION` AS `DEFECTION`,round(((((`dbt_summary`.`COEFF` / 10) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `COEFF`,(`dbt_batch`.`INITIAL_COUNT` - `dbt_summary`.`CURR_COUNT`) AS `TOTAL_DEAD`,`dbt_summary`.`DEAD_COUNT` AS `DEAD_COUNT`,round(((((`dbt_summary`.`HUMIDITY` / 10) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `HUMIDITY`,round(((((`dbt_summary`.`HUMIDITY_UPL` / 10) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `HUMIDITY_UPL`,round(((((`dbt_summary`.`HUMIDITY_LOL` / 10) * pow(10,1)) + 0.5) / pow(10,1)),1) AS `HUMIDITY_LOL`,round(((((`dbt_summary`.`MIN_VENTILATION` / 100) * pow(10,2)) + 0.5) / pow(10,2)),2) AS `MIN_VENTILATION`,round(((((`dbt_summary`.`BMIN_VENTILATION` / 100) * pow(10,2)) + 0.5) / pow(10,2)),2) AS `BMIN_VENTILATION`,round(((((`dbt_summary`.`VENTILATION_SETUP` / 100) * pow(10,2)) + 0.5) / pow(10,2)),2) AS `VENTILATION_SETUP`,`dbt_summary`.`REMV_EXCR` AS `REMV_EXCR`,`dbt_summary`.`HEATING` AS `HEATING`,`dbt_summary`.`WATER_POS` AS `WATER_POS`,`dbt_summary`.`AIRCON_SYSTEM` AS `AIRCON_SYSTEM`,`dbt_summary`.`LIGHTING_TIME` AS `LIGHTING_TIME`,`dbt_summary`.`FOOD` AS `FOOD`,`dbt_summary`.`COMMENTS` AS `COMMENTS` from (((`dbm_lot` join `dbt_batch` on((`dbt_batch`.`LOT_ID` = `dbm_lot`.`LOT_ID`))) join `dbt_summary` on((`dbt_summary`.`BATCH_ID` = `dbt_batch`.`BATCH_ID`))) join `dbm_house` on((`dbm_lot`.`HOUSE_ID` = `dbm_house`.`HOUSE_ID`))) ;

-- ----------------------------
-- View structure for dbv_egg
-- ----------------------------
DROP VIEW IF EXISTS `dbv_egg`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dbv_egg` AS select `dbt_summary`.`SUMMARY_DATE` AS `EGG_DATE`,`dbm_lot`.`LOT_ID` AS `LOT_ID`,`dbm_lot`.`LOT_NAME` AS `LOT_NAME`,`dbm_egg`.`MEGG_ID` AS `MEGG_ID`,`dbm_egg`.`CAGE_ROW` AS `CAGE_ROW`,`dbm_egg`.`CAGE_TIER` AS `CAGE_TIER`,`dbt_egg`.`EGG_COUNT` AS `EGG_COUNT`,`dbt_summary`.`CURR_COUNT` AS `CURR_COUNT`,(floor(((((`dbt_egg`.`EGG_COUNT` / (`dbt_summary`.`CURR_COUNT` / `dbm_lot`.`CAGE_NUM`)) / 100) * pow(10,2)) + 0.5)) / pow(10,2)) AS `EGG_RATE_CUR` from (((((`dbm_lot` join `dbt_batch` on((`dbt_batch`.`LOT_ID` = `dbm_lot`.`LOT_ID`))) join `dbt_summary` on((`dbt_summary`.`BATCH_ID` = `dbt_batch`.`BATCH_ID`))) join `dbm_house` on((`dbm_lot`.`HOUSE_ID` = `dbm_house`.`HOUSE_ID`))) join `dbt_egg` on((`dbt_summary`.`COLLECT_ID` = `dbt_egg`.`COLLECT_ID`))) join `dbm_egg` on((`dbm_egg`.`MEGG_ID` = `dbt_egg`.`MEGG_ID`))) ;

-- ----------------------------
-- View structure for dbv_main
-- ----------------------------
DROP VIEW IF EXISTS `dbv_main`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dbv_main` AS select `dbt_batch`.`BATCH_ID` AS `BATCH_ID`,`dbt_batch`.`LOT_ID` AS `LOT_ID`,`dbm_lot`.`LOT_NAME` AS `LOT_NAME`,`dbm_house`.`HOUSE_ID` AS `HOUSE_ID`,`dbm_house`.`HOUSE_NAME` AS `HOUSE_NAME`,`dbt_summary`.`WEEK_PERIODS` AS `WEEK_PERIODS`,`dbt_summary`.`DAY_PERIODS` AS `DAY_PERIODS`,`dbt_batch`.`FEEDING_DATE` AS `FEEDING_DATE`,`dbt_batch`.`START_DATE` AS `START_DATE`,`dbt_batch`.`INITIAL_COUNT` AS `INITIAL_COUNT`,`dbm_lot`.`CAGE_NUM` AS `CAGE_NUM`,`dbt_summary`.`CURR_COUNT` AS `CURR_COUNT`,`dbt_summary`.`DEAD_COUNT` AS `DEAD_COUNT`,cast((`dbt_summary`.`EGG_WEIGHT` / 10) as decimal(5,1)) AS `EGG_WEIGHT`,`dbt_summary`.`HEN_WEIGHT` AS `HEN_WEIGHT`,cast((`dbt_summary`.`FOOD_AMOUNT` / 1000) as decimal(5,1)) AS `FOOD_AMOUNT`,`dbt_summary`.`WATER_AMOUNT` AS `WATER_AMOUNT`,cast((`dbt_summary`.`PRESET_TEMP` / 10) as decimal(5,1)) AS `PRESET_TEMP`,cast((`dbt_summary`.`ROOM_TEMP` / 10) as decimal(5,1)) AS `ROOM_TEMP`,cast((`dbt_summary`.`ROOM_TEMP_HI` / 10) as decimal(5,1)) AS `ROOM_TEMP_HI`,cast((`dbt_summary`.`ROOM_TEMP_LO` / 10) as decimal(5,1)) AS `ROOM_TEMP_LO`,cast((`dbt_summary`.`OUTER_TEMP` / 10) as decimal(5,1)) AS `OUTER_TEMP`,cast((`dbt_summary`.`OUTER_TEMP_HI` / 10) as decimal(5,1)) AS `OUTER_TEMP_HI`,cast((`dbt_summary`.`OUTER_TEMP_LO` / 10) as decimal(5,1)) AS `OUTER_TEMP_LO`,`dbt_summary`.`DAYLIGHT_HOURS` AS `DAYLIGHT_HOURS`,`dbt_summary`.`DEFECTION` AS `DEFECTION`,round((`dbt_summary`.`MIN_VENTILATION` / 100),2) AS `MIN_VENTILATION`,round((`dbt_summary`.`BMIN_VENTILATION` / 100),2) AS `BMIN_VENTILATION`,round((`dbt_summary`.`HUMIDITY` / 10),1) AS `HUMIDITY`,round((`dbt_summary`.`HUMIDITY_UPL` / 10),1) AS `HUMIDITY_UPL`,round((`dbt_summary`.`HUMIDITY_LOL` / 10),1) AS `HUMIDITY_LOL`,(to_days(`dbt_batch`.`START_DATE`) - to_days(`dbt_batch`.`FEEDING_DATE`)) AS `STTDAY_PERIODS`,round(((`dbt_summary`.`HEN_WEIGHT` / `dbm_manual`.`HEN_WEIGHT`) * 100),1) AS `WEIGHTCHG_RATE` from ((((`dbm_lot` join `dbm_house` on((`dbm_house`.`HOUSE_ID` = `dbm_lot`.`HOUSE_ID`))) join `dbt_batch` on((`dbt_batch`.`LOT_ID` = `dbm_lot`.`LOT_ID`))) join `dbt_summary` on((`dbt_summary`.`BATCH_ID` = `dbt_batch`.`BATCH_ID`))) join `dbm_manual` on(((`dbm_manual`.`HEN_ID` = `dbt_batch`.`HEN_ID`) and (`dbm_manual`.`WEEK_PERIODS` = `dbt_summary`.`WEEK_PERIODS`)))) where (`dbt_summary`.`SUMMARY_DATE` = curdate()) order by `dbm_lot`.`LOT_ID` ;

-- ----------------------------
-- View structure for dbv_weekly
-- ----------------------------
DROP VIEW IF EXISTS `dbv_weekly`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dbv_weekly` AS select `dbt_summary`.`SUMMARY_DATE` AS `WEEKLY_DATE`,`dbt_summary`.`WEEK_PERIODS` AS `WEEK_PERIODS`,`dbm_lot`.`LOT_ID` AS `LOT_ID`,`dbm_lot`.`LOT_NAME` AS `LOT_NAME`,`dbm_house`.`HOUSE_ID` AS `HOUSE_ID`,`dbm_house`.`HOUSE_NAME` AS `HOUSE_NAME`,round(avg(`dbt_summary`.`EGG_COUNT`),0) AS `EGG_COUNT`,round(avg(`dbt_summary`.`CURR_COUNT`),0) AS `CURR_COUNT`,round((avg(`dbt_summary`.`EGG_WEIGHT`) * 0.1),1) AS `EGG_WEIGHT`,round(((avg((`dbt_summary`.`EGG_WEIGHT` * 0.1)) * `dbt_summary`.`EGG_COUNT`) / `dbt_summary`.`CURR_COUNT`),1) AS `EGG_VOL`,`dbt_batch`.`INITIAL_COUNT` AS `INITIAL_COUNT`,round(avg(`dbt_summary`.`HEN_WEIGHT`),0) AS `HEN_WEIGHT`,cast((round(avg(`dbt_summary`.`FOOD_AMOUNT`),0) / 1000) as decimal(5,1)) AS `FOOD_AMOUNT`,round((`dbt_summary`.`FOOD_AMOUNT` / `dbt_summary`.`CURR_COUNT`),1) AS `FOOD_AMOUNT_U`,round(((`dbt_summary`.`CURR_COUNT` / `dbt_batch`.`INITIAL_COUNT`) * 100),1) AS `SURVIVAL_RATE`,round(((`dbt_summary`.`EGG_COUNT` / `dbt_summary`.`CURR_COUNT`) * 100),1) AS `EGG_RATE_CUR`,round(((`dbt_summary`.`EGG_COUNT` / `dbt_batch`.`INITIAL_COUNT`) * 100),1) AS `EGG_RATE_INI`,round((avg(`dbt_summary`.`HEN_WEIGHT`) / ((`dbt_summary`.`FOOD_AMOUNT` * 1000) / `dbt_summary`.`CURR_COUNT`)),1) AS `FOOD_EFFICEINCY`,round((`dbt_summary`.`FOOD_AMOUNT` / (`dbt_summary`.`EGG_COUNT` * avg((`dbt_summary`.`EGG_WEIGHT` * 0.1)))),1) AS `FOOD_DEMAND`,round((avg(`dbt_summary`.`PRESET_TEMP`) / 10),1) AS `PRESET_TEMP`,round(avg(`dbt_summary`.`DAYLIGHT_HOURS`),0) AS `DAYLIGHT_HOURS`,round(avg(`dbt_summary`.`WATER_AMOUNT`),0) AS `WATER_AMOUNT`,round(((`dbt_summary`.`WATER_AMOUNT` * 1000) / `dbt_summary`.`CURR_COUNT`),1) AS `WATER_AMOUNT_U`,round((avg(`dbt_summary`.`ROOM_TEMP`) / 10),1) AS `ROOM_TEMP`,cast((max(`dbt_summary`.`ROOM_TEMP_HI`) / 10) as decimal(5,1)) AS `ROOM_TEMP_HI`,cast((min(`dbt_summary`.`ROOM_TEMP_LO`) / 10) as decimal(5,1)) AS `ROOM_TEMP_LO`,round((((avg(`dbt_summary`.`OUTER_TEMP`) / 10) * pow(10,1)) / pow(10,1)),1) AS `OUTER_TEMP`,cast((max(`dbt_summary`.`OUTER_TEMP_HI`) / 10) as decimal(5,1)) AS `OUTER_TEMP_HI`,cast((min(`dbt_summary`.`OUTER_TEMP_LO`) / 10) as decimal(5,1)) AS `OUTER_TEMP_LO`,round((avg(`dbt_summary`.`DEFECTION`) / 10),1) AS `DEFECTION`,round((avg(`dbt_summary`.`COEFF`) / 10),1) AS `COEFF`,(`dbt_batch`.`INITIAL_COUNT` - min(`dbt_summary`.`CURR_COUNT`)) AS `TOTAL_DEAD`,round((avg(`dbt_summary`.`HUMIDITY`) / 10),1) AS `HUMIDITY`,round((max(`dbt_summary`.`HUMIDITY_UPL`) / 10),1) AS `HUMIDITY_UPL`,round((min(`dbt_summary`.`HUMIDITY_LOL`) / 10),1) AS `HUMIDITY_LOL`,round((avg(`dbt_summary`.`MIN_VENTILATION`) / 100),2) AS `MIN_VENTILATION`,round((avg(`dbt_summary`.`BMIN_VENTILATION`) / 100),2) AS `BMIN_VENTILATION`,round((avg(`dbt_summary`.`VENTILATION_SETUP`) / 100),2) AS `VENTILATION_SETUP`,`dbm_hen`.`HEN_NAME` AS `HEN_NAME`,`dbm_manual`.`HEN_WEIGHT` AS `HEN_WEIGHT_MAN`,round((`dbm_manual`.`EGG_WEIGHT` / 10),1) AS `EGG_WEIGHT_MAN`,round((`dbm_manual`.`EGG_RATE` / 10),1) AS `EGG_RATE_MAN`,round((`dbm_manual`.`EGG_VOL` / 10),1) AS `EGG_VOL_MAN` from (((((`dbm_lot` join `dbt_batch` on((`dbt_batch`.`LOT_ID` = `dbm_lot`.`LOT_ID`))) join `dbt_summary` on((`dbt_summary`.`BATCH_ID` = `dbt_batch`.`BATCH_ID`))) join `dbm_house` on((`dbm_lot`.`HOUSE_ID` = `dbm_house`.`HOUSE_ID`))) join `dbm_manual` on(((`dbt_batch`.`HEN_ID` = `dbm_manual`.`HEN_ID`) and (`dbt_summary`.`WEEK_PERIODS` = `dbm_manual`.`WEEK_PERIODS`)))) join `dbm_hen` on((`dbt_batch`.`HEN_ID` = `dbm_hen`.`HEN_ID`))) group by `dbt_batch`.`LOT_ID`,`dbt_summary`.`WEEK_PERIODS` ;
