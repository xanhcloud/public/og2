-- Disable foreign key checks
-- Not necessary in PostgreSQL, but kept for reference
-- SET session_replication_role = 'replica';

-- ----------------------------
-- Table structure for dbm_dic
-- ----------------------------
DROP TABLE IF EXISTS dbm_dic;
CREATE TABLE dbm_dic (
  dic_id SERIAL PRIMARY KEY,
  term_name VARCHAR(32),
  jp_name VARCHAR(64),
  en_name VARCHAR(64),
  pt_name VARCHAR(64),
  es_name VARCHAR(64),
  created_tdate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for dbm_user
-- ----------------------------
DROP TABLE IF EXISTS dbm_user;
CREATE TABLE dbm_user (
  USER_ID VARCHAR(8) PRIMARY KEY,
  PASSWORD VARCHAR(8) NOT NULL,
  USER_NAME VARCHAR(64),
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for dbm_house
-- ----------------------------
DROP TABLE IF EXISTS dbm_house;
CREATE TABLE dbm_house (
  HOUSE_ID SERIAL PRIMARY KEY,
  HOUSE_NAME VARCHAR(64),
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for dbm_lot
-- ----------------------------
DROP TABLE IF EXISTS dbm_lot;
CREATE TABLE dbm_lot (
  LOT_ID SERIAL PRIMARY KEY,
  LOT_NAME VARCHAR(32),
  HOUSE_ID INT NOT NULL,
  CAGE_NUM INT,
  EQUIP_TYPE SMALLINT NOT NULL,
  COMM_TYPE SMALLINT NOT NULL,
  IP_ADDRESS VARCHAR(32),
  FOOD_INPUT SMALLINT NOT NULL,
  EGGCO SMALLINT NOT NULL,
  STOP_COUNT SMALLINT NOT NULL,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (HOUSE_ID) REFERENCES dbm_house (HOUSE_ID)
);

-- ----------------------------
-- Table structure for dbm_lot_backup
-- ----------------------------
DROP TABLE IF EXISTS dbm_lot_backup;
CREATE TABLE dbm_lot_backup (
  LOT_ID INT NOT NULL,
  LOT_NAME VARCHAR(32),
  HOUSE_ID INT NOT NULL,
  CAGE_NUM INT,
  EQUIP_TYPE SMALLINT NOT NULL,
  COMM_TYPE SMALLINT NOT NULL,
  IP_ADDRESS VARCHAR(32),
  FOOD_INPUT SMALLINT NOT NULL,
  STOP_COUNT SMALLINT NOT NULL,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for dbm_price
-- ----------------------------
DROP TABLE IF EXISTS dbm_price;
CREATE TABLE dbm_price (
  PRICE_ID SERIAL PRIMARY KEY,
  PRICE_DATE DATE NOT NULL,
  FOOD_PRICE INT,
  STDEGG_PRICE INT,
  OUTEGG_PRICE INT,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for dbm_thermo
-- ----------------------------
DROP TABLE IF EXISTS dbm_thermo;
CREATE TABLE dbm_thermo (
  MTHERMO_ID SERIAL PRIMARY KEY,
  LOT_ID INT NOT NULL,
  MTHERMO_TYPE SMALLINT NOT NULL,
  MTHERMO_NO SMALLINT NOT NULL,
  REG_CUR VARCHAR(16),
  REG_HIGH VARCHAR(16),
  REG_LOW VARCHAR(16),
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (LOT_ID) REFERENCES dbm_lot (LOT_ID)
);

-- ----------------------------
-- Table structure for dbm_fooder
-- ----------------------------
DROP TABLE IF EXISTS dbm_fooder;
CREATE TABLE dbm_fooder (
  MFOODER_ID SERIAL PRIMARY KEY,
  LOT_ID INT NOT NULL,
  FOODER_NO SMALLINT NOT NULL,
  FOODER_TYPE SMALLINT NOT NULL,
  REG_DEP VARCHAR(16),
  REG_ARV VARCHAR(16),
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (LOT_ID) REFERENCES dbm_lot (LOT_ID)
);

-- ----------------------------
-- Table structure for dbm_fw
-- ----------------------------
DROP TABLE IF EXISTS dbm_fw;
CREATE TABLE dbm_fw (
  MFW_ID SERIAL PRIMARY KEY,
  LOT_ID INT NOT NULL,
  FW_NO SMALLINT NOT NULL,
  REG_FOOD_UP VARCHAR(16),
  REG_FOOD_LO VARCHAR(16),
  REG_WATER VARCHAR(16),
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (LOT_ID) REFERENCES dbm_lot (LOT_ID)
);

-- ----------------------------
-- Table structure for dbm_hen
-- ----------------------------
DROP TABLE IF EXISTS dbm_hen;
CREATE TABLE dbm_hen (
  HEN_ID SERIAL PRIMARY KEY,
  HEN_NAME VARCHAR(128),
  CREATED_TDATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for dbt_hengrp
-- ----------------------------
DROP TABLE IF EXISTS dbt_hengrp;
CREATE TABLE dbt_hengrp (
  HENGRP_ID SERIAL PRIMARY KEY,
  HENGRP_NAME VARCHAR(64),
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for dbt_batch
-- ----------------------------
DROP TABLE IF EXISTS dbt_batch;
CREATE TABLE dbt_batch (
  BATCH_ID SERIAL PRIMARY KEY,
  LOT_ID INT NOT NULL,
  HEN_ID INT NOT NULL,
  HENGRP_ID INT NOT NULL,
  GROWTH_KIND SMALLINT,
  START_DATE DATE,
  END_DATE DATE,
  INITIAL_COUNT INT,
  FEEDING_DATE DATE,
  EGGCNT_STIME SMALLINT,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  EGGCNT_ETIME SMALLINT,
  FOREIGN KEY (LOT_ID) REFERENCES dbm_lot (LOT_ID),
  FOREIGN KEY (HEN_ID) REFERENCES dbm_hen (HEN_ID),
  FOREIGN KEY (HENGRP_ID) REFERENCES dbt_hengrp (HENGRP_ID)
);

-- ----------------------------
-- Table structure for dbt_collection
-- ----------------------------
DROP TABLE IF EXISTS dbt_collection;
CREATE TABLE dbt_collection (
  COLLECT_ID SERIAL PRIMARY KEY,
  BATCH_ID INT NOT NULL,
  COL_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (BATCH_ID) REFERENCES dbt_batch (BATCH_ID)
);

-- ----------------------------
-- Table structure for dbt_summary
-- ----------------------------
DROP TABLE IF EXISTS dbt_summary;
CREATE TABLE dbt_summary (
  SUMMARY_ID SERIAL PRIMARY KEY,
  BATCH_ID INT NOT NULL,
  SUMMARY_DATE DATE NOT

 NULL,
  EGG_CHG INT,
  EGG_NUM INT,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (BATCH_ID) REFERENCES dbt_batch (BATCH_ID)
);

-- ----------------------------
-- Table structure for dbm_egg
-- ----------------------------
DROP TABLE IF EXISTS dbm_egg;
CREATE TABLE dbm_egg (
  megg_id SERIAL PRIMARY KEY,
  lot_id INT NOT NULL,
  eggctr_no SMALLINT NOT NULL,
  cage_row SMALLINT NOT NULL,
  cage_tier SMALLINT NOT NULL,
  reg_egg VARCHAR(16),
  created_tdate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (lot_id) REFERENCES dbm_lot (lot_id)
);

-- ----------------------------
-- Table structure for dbt_eggchg
-- ----------------------------
DROP TABLE IF EXISTS dbt_eggchg;
CREATE TABLE dbt_eggchg (
  EGGCHG_ID SERIAL PRIMARY KEY,
  SUMMARY_ID INT NOT NULL,
  MEGG_ID INT NOT NULL,
  EGG_COUNT INT,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (MEGG_ID) REFERENCES dbm_egg (MEGG_ID),
  FOREIGN KEY (SUMMARY_ID) REFERENCES dbt_summary (SUMMARY_ID)
);

-- ----------------------------
-- Table structure for dbt_egg_copy
-- ----------------------------
DROP TABLE IF EXISTS dbt_egg_copy;
CREATE TABLE dbt_egg_copy (
  TEGG_ID SERIAL PRIMARY KEY,
  COLLECT_ID INT NOT NULL,
  MEGG_ID INT NOT NULL,
  EGG_COUNT INT,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (COLLECT_ID) REFERENCES dbt_collection (COLLECT_ID),
  FOREIGN KEY (MEGG_ID) REFERENCES dbm_egg (MEGG_ID)
);

-- ----------------------------
-- Table structure for dbm_fooderst
-- ----------------------------
DROP TABLE IF EXISTS dbm_fooderst;
CREATE TABLE dbm_fooderst (
  MFOODERST_ID SERIAL PRIMARY KEY,
  MFOODER_ID INT NOT NULL,
  START_TIME TIME NOT NULL,
  APPLY_DATE DATE NOT NULL,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (MFOODER_ID) REFERENCES dbm_fooder (MFOODER_ID)
);

-- ----------------------------
-- Table structure for dbm_manual
-- ----------------------------
DROP TABLE IF EXISTS dbm_manual;
CREATE TABLE dbm_manual (
  MANUL_ID SERIAL PRIMARY KEY,
  HEN_ID INT NOT NULL,
  WEEK_PERIODS SMALLINT NOT NULL,
  SURVIVAL_RATE SMALLINT,
  HEN_WEIGHT SMALLINT,
  EGG_WEIGHT SMALLINT,
  EGG_RATE SMALLINT,
  EGG_VOL SMALLINT,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (HEN_ID) REFERENCES dbm_hen (HEN_ID)
);

-- ----------------------------
-- Table structure for dbt_egg
-- ----------------------------
DROP TABLE IF EXISTS dbt_egg;
CREATE TABLE dbt_egg (
  TEGG_ID SERIAL PRIMARY KEY,
  COLLECT_ID INT NOT NULL,
  MEGG_ID INT NOT NULL,
  EGG_COUNT INT,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (COLLECT_ID) REFERENCES dbt_collection (COLLECT_ID),
  FOREIGN KEY (MEGG_ID) REFERENCES dbm_egg (MEGG_ID)
);

-- ----------------------------
-- Table structure for dbt_fooder
-- ----------------------------
DROP TABLE IF EXISTS dbt_fooder;
CREATE TABLE dbt_fooder (
  TFOODER_ID SERIAL PRIMARY KEY,
  COLLECT_ID INT NOT NULL,
  MFOODER_ID INT NOT NULL,
  DEPARTURE_TIME TIME,
  ARRIVAL_TIME TIME,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (COLLECT_ID) REFERENCES dbt_collection (COLLECT_ID),
  FOREIGN KEY (MFOODER_ID) REFERENCES dbm_fooder (MFOODER_ID)
);

-- ----------------------------
-- Table structure for dbt_fw
-- ----------------------------
DROP TABLE IF EXISTS dbt_fw;
CREATE TABLE dbt_fw (
  TFW_ID SERIAL PRIMARY KEY,
  COLLECT_ID INT NOT NULL,
  MFW_ID INT,
  FOOD_AMOUNT INT,
  WATER_AMOUNT INT,
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (COLLECT_ID) REFERENCES dbt_collection (COLLECT_ID),
  FOREIGN KEY (MFW_ID) REFERENCES dbm_fw (MFW_ID)
);

-- ----------------------------
-- Table structure for dbt_thermo
-- ----------------------------
DROP TABLE IF EXISTS dbt_thermo;
CREATE TABLE dbt_thermo (
  TTHERMO_ID SERIAL PRIMARY KEY,
  COLLECT_ID INT NOT NULL,
  MTHERMO_ID INT NOT NULL,
  CURRENT_TEMP VARCHAR(16),
  HIGH_TEMP VARCHAR(16),
  LOW_TEMP VARCHAR(16),
  CREATED_TDATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (COLLECT_ID) REFERENCES dbt_collection (COLLECT_ID),
  FOREIGN KEY (MTHERMO_ID) REFERENCES dbm_thermo (MTHERMO_ID)
);

-- Re-enable foreign key checks
-- SET session_replication_role = 'origin';

