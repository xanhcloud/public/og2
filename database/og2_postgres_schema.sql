--
-- PostgreSQL database dump
--

-- Dumped from database version 16.3
-- Dumped by pg_dump version 16.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS auth_group;
CREATE TABLE auth_group (
  id SERIAL PRIMARY KEY,
  name VARCHAR(150) NOT NULL UNIQUE
);

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS auth_group_permissions;
CREATE TABLE auth_group_permissions (
  id SERIAL PRIMARY KEY,
  group_id INT NOT NULL,
  permission_id INT NOT NULL,
  UNIQUE (group_id, permission_id),
  FOREIGN KEY (group_id) REFERENCES auth_group (id),
  FOREIGN KEY (permission_id) REFERENCES auth_permission (id)
);

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS auth_permission;
CREATE TABLE auth_permission (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  content_type_id INT NOT NULL,
  codename VARCHAR(100) NOT NULL,
  UNIQUE (content_type_id, codename),
  FOREIGN KEY (content_type_id) REFERENCES django_content_type (id)
);

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS auth_user;
CREATE TABLE auth_user (
  id SERIAL PRIMARY KEY,
  password VARCHAR(128) NOT NULL,
  last_login TIMESTAMPTZ,
  is_superuser BOOLEAN NOT NULL,
  username VARCHAR(150) NOT NULL UNIQUE,
  first_name VARCHAR(150) NOT NULL,
  last_name VARCHAR(150) NOT NULL,
  email VARCHAR(254) NOT NULL,
  is_staff BOOLEAN NOT NULL,
  is_active BOOLEAN NOT NULL,
  date_joined TIMESTAMPTZ NOT NULL
);

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS auth_user_groups;
CREATE TABLE auth_user_groups (
  id SERIAL PRIMARY KEY,
  user_id INT NOT NULL,
  group_id INT NOT NULL,
  UNIQUE (user_id, group_id),
  FOREIGN KEY (user_id) REFERENCES auth_user (id),
  FOREIGN KEY (group_id) REFERENCES auth_group (id)
);

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS auth_user_user_permissions;
CREATE TABLE auth_user_user_permissions (
  id SERIAL PRIMARY KEY,
  user_id INT NOT NULL,
  permission_id INT NOT NULL,
  UNIQUE (user_id, permission_id),
  FOREIGN KEY (user_id) REFERENCES auth_user (id),
  FOREIGN KEY (permission_id) REFERENCES auth_permission (id)
);

-- ----------------------------
-- Table structure for authtoken_token
-- ----------------------------
DROP TABLE IF EXISTS authtoken_token;
CREATE TABLE authtoken_token (
  key VARCHAR(40) PRIMARY KEY,
  created TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_id INT NOT NULL UNIQUE,
  FOREIGN KEY (user_id) REFERENCES auth_user (id)
);

-- ----------------------------
-- Table structure for dbm_dic
-- ----------------------------
DROP TABLE IF EXISTS dbm_dic;
CREATE TABLE dbm_dic (
  dic_id SERIAL PRIMARY KEY,
  term_name VARCHAR(32),
  jp_name VARCHAR(64),
  en_name VARCHAR(64),
  pt_name VARCHAR(64),
  es_name VARCHAR(64),
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for dbm_egg
-- ----------------------------
DROP TABLE IF EXISTS dbm_egg;
CREATE TABLE dbm_egg (
  megg_id SERIAL PRIMARY KEY,
  eggctr_no SMALLINT NOT NULL,
  cage_row SMALLINT NOT NULL,
  cage_tier SMALLINT NOT NULL,
  reg_egg VARCHAR(16),
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  lot_id INT NOT NULL,
  FOREIGN KEY (lot_id) REFERENCES dbm_lot (lot_id)
);

-- ----------------------------
-- Table structure for dbm_fooder
-- ----------------------------
DROP TABLE IF EXISTS dbm_fooder;
CREATE TABLE dbm_fooder (
  mfooder_id SERIAL PRIMARY KEY,
  fooder_no SMALLINT NOT NULL,
  fooder_type SMALLINT NOT NULL,
  reg_dep VARCHAR(16),
  reg_arv VARCHAR(16),
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  lot_id INT NOT NULL,
  FOREIGN KEY (lot_id) REFERENCES dbm_lot (lot_id)
);

-- ----------------------------
-- Table structure for dbm_fooderst
-- ----------------------------
DROP TABLE IF EXISTS dbm_fooderst;
CREATE TABLE dbm_fooderst (
  mfooderst_id SERIAL PRIMARY KEY,
  start_time TIME NOT NULL,
  apply_date DATE NOT NULL,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  mfooder_id INT NOT NULL,
  FOREIGN KEY (mfooder_id) REFERENCES dbm_fooder (mfooder_id)
);

-- ----------------------------
-- Table structure for dbm_fw
-- ----------------------------
DROP TABLE IF EXISTS dbm_fw;
CREATE TABLE dbm_fw (
  mfw_id SERIAL PRIMARY KEY,
  fw_no SMALLINT NOT NULL,
  reg_food_up VARCHAR(16),
  reg_food_lo VARCHAR(16),
  reg_water VARCHAR(16),
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  lot_id INT NOT NULL,
  FOREIGN KEY (lot_id) REFERENCES dbm_lot (lot_id)
);

-- ----------------------------
-- Table structure for dbm_hen
-- ----------------------------
DROP TABLE IF EXISTS dbm_hen;
CREATE TABLE dbm_hen (
  hen_id SERIAL PRIMARY KEY,
  hen_name VARCHAR(128),
  created_tdate TIMESTAMPTZ
);

-- ----------------------------
-- Table structure for dbm_house
-- ----------------------------
DROP TABLE IF EXISTS dbm_house;
CREATE TABLE dbm_house (
  house_id SERIAL PRIMARY KEY,
  house_name VARCHAR(64),
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for dbm_lot
-- ----------------------------
DROP TABLE IF EXISTS dbm_lot;
CREATE TABLE dbm_lot (
  lot_id SERIAL PRIMARY KEY,
  lot_name VARCHAR(32),
  cage_num INT,
  equip_type SMALLINT NOT NULL,
  comm_type SMALLINT NOT NULL,
  ip_address VARCHAR(32),
  food_input SMALLINT NOT NULL,
  eggco SMALLINT NOT NULL,
  stop_count SMALLINT NOT NULL,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  house_id INT NOT NULL,
  FOREIGN KEY (house_id) REFERENCES dbm_house (house_id)
);

-- ----------------------------
-- Table structure for dbm_lot_backup
-- ----------------------------
DROP TABLE IF EXISTS dbm_lot_backup;
CREATE TABLE dbm_lot_backup (
  id SERIAL PRIMARY KEY,
  lot_id INT NOT NULL,
  lot_name VARCHAR(32),
  house_id INT NOT NULL,
  cage_num INT,
  equip_type SMALLINT NOT NULL,
  comm_type SMALLINT NOT NULL,
  ip_address VARCHAR(32),
  food_input SMALLINT NOT NULL,
  stop_count SMALLINT NOT NULL,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (lot_id) REFERENCES dbm_lot (lot_id),
  FOREIGN KEY (house_id) REFERENCES dbm_house (house_id)
);

-- ----------------------------
-- Table structure for dbm_manual
-- ----------------------------
DROP TABLE IF EXISTS dbm_manual;
CREATE TABLE dbm_manual (
  manul_id SERIAL PRIMARY KEY,
  week_periods SMALLINT NOT NULL,
  survival_rate SMALLINT,
  hen_weight SMALLINT,
  egg_weight SMALLINT,
  egg_rate SMALLINT,
  egg_vol SMALLINT,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  hen_id INT NOT NULL,
  FOREIGN KEY (hen_id) REFERENCES dbm_hen (hen_id)
);

-- ----------------------------
-- Table structure for dbm_price
-- ----------------------------
DROP TABLE IF EXISTS dbm_price;
CREATE TABLE dbm_price (
  price_id SERIAL PRIMARY KEY,
  price_date DATE NOT NULL,
  food_price INT,
  stdegg_price INT,
  outegg_price INT,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for dbm_thermo
-- ----------------------------
DROP TABLE IF EXISTS dbm_thermo;
CREATE TABLE dbm_thermo (
  mthermo_id SERIAL PRIMARY KEY,
  mthermo_type SMALLINT NOT NULL,
  mthermo_no SMALLINT NOT NULL,
  reg_cur VARCHAR(16),
  reg_high VARCHAR(16),
  reg_low VARCHAR(16),
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  lot_id INT NOT NULL,
  FOREIGN KEY (lot_id) REFERENCES dbm_lot (lot_id)
);

-- ----------------------------
-- Table structure for dbm_user
-- ----------------------------
DROP TABLE IF EXISTS dbm_user;
CREATE TABLE dbm_user (
  user_id VARCHAR(8) PRIMARY KEY,
  password VARCHAR(8) NOT NULL,
  user_name VARCHAR(64),
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for dbt_batch
-- ----------------------------
DROP TABLE IF EXISTS dbt_batch;
CREATE TABLE dbt_batch (
  batch_id SERIAL PRIMARY KEY,
  growth_kind SMALLINT,
  start_date DATE,
  end_date DATE,
  initial_count INT,
  feeding_date DATE,
  eggcnt_stime SMALLINT,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  eggcnt_etime SMALLINT,
  hen_id INT NOT NULL,
  lot_id INT NOT NULL,
  hengrp_id INT

 NOT NULL,
  FOREIGN KEY (hen_id) REFERENCES dbm_hen (hen_id),
  FOREIGN KEY (lot_id) REFERENCES dbm_lot (lot_id),
  FOREIGN KEY (hengrp_id) REFERENCES dbt_hengrp (hengrp_id)
);

-- ----------------------------
-- Table structure for dbt_collection
-- ----------------------------
DROP TABLE IF EXISTS dbt_collection;
CREATE TABLE dbt_collection (
  collect_id SERIAL PRIMARY KEY,
  col_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  batch_id INT NOT NULL,
  FOREIGN KEY (batch_id) REFERENCES dbt_batch (batch_id)
);

-- ----------------------------
-- Table structure for dbt_egg
-- ----------------------------
DROP TABLE IF EXISTS dbt_egg;
CREATE TABLE dbt_egg (
  tegg_id SERIAL PRIMARY KEY,
  egg_count INT,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  collect_id INT NOT NULL,
  megg_id INT NOT NULL,
  FOREIGN KEY (collect_id) REFERENCES dbt_collection (collect_id),
  FOREIGN KEY (megg_id) REFERENCES dbm_egg (megg_id)
);

-- ----------------------------
-- Table structure for dbt_egg_copy
-- ----------------------------
DROP TABLE IF EXISTS dbt_egg_copy;
CREATE TABLE dbt_egg_copy (
  tegg_id SERIAL PRIMARY KEY,
  egg_count INT,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  collect_id INT NOT NULL,
  megg_id INT NOT NULL,
  FOREIGN KEY (collect_id) REFERENCES dbt_collection (collect_id),
  FOREIGN KEY (megg_id) REFERENCES dbm_egg (megg_id)
);

-- ----------------------------
-- Table structure for dbt_eggchg
-- ----------------------------
DROP TABLE IF EXISTS dbt_eggchg;
CREATE TABLE dbt_eggchg (
  eggchg_id SERIAL PRIMARY KEY,
  egg_count INT,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  megg_id INT NOT NULL,
  summary_id INT NOT NULL,
  FOREIGN KEY (megg_id) REFERENCES dbm_egg (megg_id),
  FOREIGN KEY (summary_id) REFERENCES dbt_summary (summary_id)
);

-- ----------------------------
-- Table structure for dbt_fooder
-- ----------------------------
DROP TABLE IF EXISTS dbt_fooder;
CREATE TABLE dbt_fooder (
  tfooder_id SERIAL PRIMARY KEY,
  departure_time TIME,
  arrival_time TIME,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  collect_id INT NOT NULL,
  mfooder_id INT NOT NULL,
  FOREIGN KEY (collect_id) REFERENCES dbt_collection (collect_id),
  FOREIGN KEY (mfooder_id) REFERENCES dbm_fooder (mfooder_id)
);

-- ----------------------------
-- Table structure for dbt_fw
-- ----------------------------
DROP TABLE IF EXISTS dbt_fw;
CREATE TABLE dbt_fw (
  tfw_id SERIAL PRIMARY KEY,
  food_amount INT,
  water_amount INT,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  collect_id INT NOT NULL,
  mfw_id INT,
  FOREIGN KEY (collect_id) REFERENCES dbt_collection (collect_id),
  FOREIGN KEY (mfw_id) REFERENCES dbm_fw (mfw_id)
);

-- ----------------------------
-- Table structure for dbt_hengrp
-- ----------------------------
DROP TABLE IF EXISTS dbt_hengrp;
CREATE TABLE dbt_hengrp (
  hengrp_id SERIAL PRIMARY KEY,
  hengrp_name VARCHAR(64),
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for dbt_summary
-- ----------------------------
DROP TABLE IF EXISTS dbt_summary;
CREATE TABLE dbt_summary (
  summary_id SERIAL PRIMARY KEY,
  batch_id INT NOT NULL,
  summary_date DATE NOT NULL,
  egg_chg INT,
  egg_num INT,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (batch_id) REFERENCES dbt_batch (batch_id)
);

-- ----------------------------
-- Table structure for dbt_thermo
-- ----------------------------
DROP TABLE IF EXISTS dbt_thermo;
CREATE TABLE dbt_thermo (
  tthermo_id SERIAL PRIMARY KEY,
  cur_val SMALLINT,
  high_val SMALLINT,
  low_val SMALLINT,
  created_tdate TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  collect_id INT NOT NULL,
  mthermo_id INT NOT NULL,
  FOREIGN KEY (collect_id) REFERENCES dbt_collection (collect_id),
  FOREIGN KEY (mthermo_id) REFERENCES dbm_thermo (mthermo_id)
);

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS django_admin_log;
CREATE TABLE django_admin_log (
  id SERIAL PRIMARY KEY,
  action_time TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  object_id TEXT,
  object_repr VARCHAR(200) NOT NULL,
  action_flag SMALLINT NOT NULL CHECK (action_flag >= 0),
  change_message TEXT NOT NULL,
  content_type_id INT,
  user_id INT NOT NULL,
  FOREIGN KEY (content_type_id) REFERENCES django_content_type (id),
  FOREIGN KEY (user_id) REFERENCES auth_user (id)
);

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS django_content_type;
CREATE TABLE django_content_type (
  id SERIAL PRIMARY KEY,
  app_label VARCHAR(100) NOT NULL,
  model VARCHAR(100) NOT NULL,
  UNIQUE (app_label, model)
);

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS django_migrations;
CREATE TABLE django_migrations (
  id SERIAL PRIMARY KEY,
  app VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  applied TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS django_session;
CREATE TABLE django_session (
  session_key VARCHAR(40) PRIMARY KEY,
  session_data TEXT NOT NULL,
  expire_date TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

