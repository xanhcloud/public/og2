-- ----------------------------
-- View structure for dbv_main
-- ----------------------------
CREATE OR REPLACE VIEW public.dbv_main
AS
SELECT
  dbt_batch.batch_id,
  dbt_batch.lot_id,
  dbm_lot.lot_name,
  dbm_house.house_id,
  dbm_house.house_name,
  dbt_summary.week_periods,
  dbt_summary.day_periods,
  dbt_batch.feeding_date,
  dbt_batch.start_date,
  dbt_batch.initial_count,
  dbm_lot.cage_num,
  dbt_summary.curr_count,
  dbt_summary.dead_count,
  (dbt_summary.egg_weight / 10)::numeric(5,1) AS egg_weight,
  dbt_summary.hen_weight,
  (dbt_summary.food_amount / 1000)::numeric(5,1) AS food_amount,
  dbt_summary.water_amount,
  (dbt_summary.preset_temp / 10)::numeric(5,1) AS preset_temp,
  (dbt_summary.room_temp / 10)::numeric(5,1) AS room_temp,
  (dbt_summary.room_temp_hi / 10)::numeric(5,1) AS room_temp_hi,
  (dbt_summary.room_temp_lo / 10)::numeric(5,1) AS room_temp_lo,
  (dbt_summary.outer_temp / 10)::numeric(5,1) AS outer_temp,
  (dbt_summary.outer_temp_hi / 10)::numeric(5,1) AS outer_temp_hi,
  (dbt_summary.outer_temp_lo / 10)::numeric(5,1) AS outer_temp_lo,
  dbt_summary.daylight_hours,
  dbt_summary.defection,
  round((dbt_summary.min_ventilation / 100)::numeric, 2) AS min_ventilation,
  round((dbt_summary.bmin_ventilation / 100)::numeric, 2) AS bmin_ventilation,
  round((dbt_summary.humidity / 10)::numeric, 1) AS humidity,
  round((dbt_summary.humidity_upl / 10)::numeric, 1) AS humidity_upl,
  round((dbt_summary.humidity_lol / 10)::numeric, 1) AS humidity_lol,
  EXTRACT(day FROM dbt_batch.start_date) - EXTRACT(day FROM dbt_batch.feeding_date) AS sttday_periods,
  CASE WHEN dbm_manual.hen_weight > 0 THEN round((dbt_summary.hen_weight / dbm_manual.hen_weight * 100)::numeric, 1) ELSE 0 END AS weightchg_rate
FROM dbm_lot
  JOIN dbm_house ON dbm_house.house_id = dbm_lot.house_id
  JOIN dbt_batch ON dbt_batch.lot_id = dbm_lot.lot_id
  JOIN dbt_summary ON dbt_summary.batch_id = dbt_batch.batch_id
  JOIN dbm_manual ON dbm_manual.hen_id = dbt_batch.hen_id AND dbm_manual.week_periods = dbt_summary.week_periods
WHERE dbt_summary.summary_date = CURRENT_DATE
ORDER BY dbm_lot.lot_id;

-- ----------------------------
-- View structure for dbv_daily
-- ----------------------------
CREATE OR REPLACE VIEW public.dbv_daily
AS
SELECT
  dbt_summary.summary_date AS daily_date,
  dbm_lot.lot_id,
  dbm_lot.lot_name,
  dbm_house.house_id,
  dbm_house.house_name,
  dbt_summary.day_periods,
  dbt_summary.egg_count,
  dbt_summary.curr_count,
  ((dbt_summary.egg_weight)::numeric * 0.1) AS egg_weight,
  dbt_summary.hen_weight,
  round(((dbt_summary.food_amount / 1000))::numeric, 1) AS food_amount,
  CASE WHEN dbt_summary.curr_count > 0 THEN round((((((dbt_summary.food_amount / dbt_summary.curr_count) * 10))::numeric + 0.5) / (10)::numeric), 1) ELSE 0 END AS food_amount_u,
  CASE WHEN dbt_summary.curr_count > 0 THEN round(((((((dbt_summary.egg_weight * dbt_summary.egg_count) / dbt_summary.curr_count) * 10))::numeric + 0.5) / (10)::numeric), 1) ELSE 0 END AS egg_vol,
  dbt_batch.initial_count,
  CASE WHEN dbt_batch.initial_count > 0 THEN round(((((((dbt_summary.curr_count / dbt_batch.initial_count) * 100) * 10))::numeric + 0.5) / (10)::numeric), 1) ELSE 0 END AS survival_rate,
  CASE WHEN dbt_summary.curr_count > 0 THEN round(((((((dbt_summary.egg_count / dbt_summary.curr_count) * 100) * 10))::numeric + 0.5) / (10)::numeric), 1) ELSE 0 END AS egg_rate_cur,
  CASE WHEN dbt_batch.initial_count > 0 THEN round(((((((dbt_summary.egg_count / dbt_batch.initial_count) * 100) * 10))::numeric + 0.5) / (10)::numeric), 1) ELSE 0 END AS egg_rate_ini,
  CASE WHEN dbt_summary.curr_count > 0 AND dbt_summary.food_amount > 0 THEN round((((((dbt_summary.hen_weight / NULLIF(dbt_summary.food_amount / dbt_summary.curr_count,0)) * 10))::numeric + 0.5) / (10)::numeric), 1) ELSE 0 END AS food_efficeincy,
  CASE WHEN dbt_summary.egg_count > 0  AND dbt_summary.egg_weight > 0 THEN round(((((((dbt_summary.food_amount / (dbt_summary.egg_count * dbt_summary.egg_weight)) * 100) * 10))::numeric + 0.5) / (10)::numeric), 1) ELSE 0 END AS food_demand,
  dbt_summary.water_amount,
  CASE WHEN dbt_summary.curr_count > 0 THEN round(((((((dbt_summary.water_amount / dbt_summary.curr_count) * 1000) * 10))::numeric + 0.5) / (10)::numeric), 1) ELSE 0 END AS water_amount_u,
  round((((((dbt_summary.preset_temp / 10) * 10))::numeric + 0.5) / (10)::numeric), 1) AS preset_temp,
  round((((((dbt_summary.room_temp / 10) * 10))::numeric + 0.5) / (10)::numeric), 1) AS room_temp,
  round((((((dbt_summary.room_temp_hi / 10) * 10))::numeric + 0.5) / (10)::numeric), 1) AS room_temp_hi,
  round((((((dbt_summary.room_temp_lo / 10) * 10))::numeric + 0.5) / (10)::numeric), 1) AS room_temp_lo,
  round((((((dbt_summary.outer_temp / 10) * 10))::numeric + 0.5) / (10)::numeric), 1) AS outer_temp,
  round((((((dbt_summary.outer_temp_hi / 10) * 10))::numeric + 0.5) / (10)::numeric), 1) AS outer_temp_hi,
  round((((((dbt_summary.outer_temp_lo / 10) * 10))::numeric + 0.5) / (10)::numeric), 1) AS outer_temp_lo,
  dbt_summary.daylight_hours,
  dbt_summary.defection,
  round((((((dbt_summary.coeff / 10) * 10))::numeric + 0.5) / (10)::numeric), 1) AS coeff,
  (dbt_batch.initial_count - dbt_summary.curr_count) AS total_dead,
  dbt_summary.dead_count,
  round((((((dbt_summary.humidity / 10) * 10))::numeric + 0.5) / (10)::numeric), 1) AS humidity,
  round((((((dbt_summary.humidity_upl / 10) * 10))::numeric + 0.5) / (10)::numeric), 1) AS humidity_upl,
  round((((((dbt_summary.humidity_lol / 10) * 10))::numeric + 0.5) / (10)::numeric), 1) AS humidity_lol,
  round((((((dbt_summary.min_ventilation / 100) * 100))::numeric + 0.5) / (100)::numeric), 2) AS min_ventilation,
  round((((((dbt_summary.bmin_ventilation / 100) * 100))::numeric + 0.5) / (100)::numeric), 2) AS bmin_ventilation,
  round((((((dbt_summary.ventilation_setup / 100) * 100))::numeric + 0.5) / (100)::numeric), 2) AS ventilation_setup,
  dbt_summary.remv_excr,
  dbt_summary.heating,
  dbt_summary.water_pos,
  dbt_summary.aircon_system,
  dbt_summary.lighting_time,
  dbt_summary.food,
  dbt_summary.comments
FROM (((dbm_lot
  JOIN dbt_batch ON ((dbt_batch.lot_id = dbm_lot.lot_id)))
  JOIN dbt_summary ON ((dbt_summary.batch_id = dbt_batch.batch_id)))
  JOIN dbm_house ON ((dbm_lot.house_id = dbm_house.house_id)));

-- ----------------------------
-- View structure for dbv_weekly
-- ----------------------------
CREATE OR REPLACE VIEW public.dbv_weekly
AS
SELECT
  dbt_summary.summary_date AS weekly_date,
  dbt_summary.week_periods,
  dbm_lot.lot_id,
  dbm_lot.lot_name,
  dbm_house.house_id,
  dbm_house.house_name,
  round(avg(dbt_summary.egg_count), 0) AS egg_count,
  round(avg(dbt_summary.curr_count), 0) AS curr_count,
  round(avg(dbt_summary.egg_weight) * 0.1, 1) AS egg_weight,
  round(avg(dbt_summary.egg_weight::numeric * 0.1) * avg(dbt_summary.egg_count) / avg(dbt_summary.curr_count), 1) AS egg_vol,
  dbt_batch.initial_count,
  round(avg(dbt_summary.hen_weight), 0) AS hen_weight,
  CASE WHEN avg(dbt_summary.food_amount) < 10000000 THEN (round(avg(dbt_summary.food_amount), 0) / 1000::numeric)::numeric(5,1) ELSE 0 END AS food_amount,
  CASE WHEN avg(dbt_summary.curr_count) > 0 THEN round(avg(dbt_summary.food_amount) / avg(dbt_summary.curr_count), 1) ELSE 0 END AS food_amount_u,
  CASE WHEN dbt_batch.initial_count > 0 THEN round(avg(dbt_summary.curr_count) / dbt_batch.initial_count::numeric * 100::numeric, 1) ELSE 0 END AS survival_rate,
  CASE WHEN avg(dbt_summary.curr_count) > 0 THEN round(avg(dbt_summary.egg_count) / avg(dbt_summary.curr_count) * 100::numeric, 1) ELSE 0 END AS egg_rate_cur,
  CASE WHEN dbt_batch.initial_count > 0 THEN round(avg(dbt_summary.egg_count) / dbt_batch.initial_count::numeric * 100::numeric, 1) ELSE 0 END AS egg_rate_ini,
  CASE WHEN avg(dbt_summary.curr_count) > 0 AND avg(dbt_summary.food_amount) > 0 THEN round(avg(dbt_summary.hen_weight) / (avg(dbt_summary.food_amount) * 1000::numeric / avg(dbt_summary.curr_count)), 1) ELSE 0 END AS food_efficiency,
  CASE WHEN avg(dbt_summary.egg_weight) > 0 AND avg(dbt_summary.egg_count) > 0 THEN round(avg(dbt_summary.food_amount) / (avg(dbt_summary.egg_count) * avg(dbt_summary.egg_weight::numeric * 0.1)), 1) ELSE 0 END AS food_demand,
  round(avg(dbt_summary.preset_temp) / 10::numeric, 1) AS preset_temp,
  round(avg(dbt_summary.daylight_hours), 0) AS daylight_hours,
  round(avg(dbt_summary.water_amount), 0) AS water_amount,
  CASE WHEN avg(dbt_summary.curr_count) > 0 THEN round(avg(dbt_summary.water_amount) * 1000::numeric / avg(dbt_summary.curr_count), 1) ELSE 0 END AS water_amount_u,
  round(avg(dbt_summary.room_temp) / 10::numeric, 1) AS room_temp,
  (max(dbt_summary.room_temp_hi) / 10)::numeric(5,1) AS room_temp_hi,
  (min(dbt_summary.room_temp_lo) / 10)::numeric(5,1) AS room_temp_lo,
  round(avg(dbt_summary.outer_temp) / 10::numeric * 10::numeric / 10::numeric, 1) AS outer_temp,
  (max(dbt_summary.outer_temp_hi) / 10)::numeric(5,1) AS outer_temp_hi,
  (min(dbt_summary.outer_temp_lo) / 10)::numeric(5,1) AS outer_temp_lo,
  round(avg(dbt_summary.defection) / 10::numeric, 1) AS defection,
  round(avg(dbt_summary.coeff) / 10::numeric, 1) AS coeff,
  dbt_batch.initial_count - min(dbt_summary.curr_count) AS total_dead,
  round(avg(dbt_summary.humidity) / 10::numeric, 1) AS humidity,
  round((max(dbt_summary.humidity_upl) / 10)::numeric, 1) AS humidity_upl,
  round((min(dbt_summary.humidity_lol) / 10)::numeric, 1) AS humidity_lol,
  round(avg(dbt_summary.min_ventilation) / 100::numeric, 2) AS min_ventilation,
  round(avg(dbt_summary.bmin_ventilation) / 100::numeric, 2) AS bmin_ventilation,
  round(avg(dbt_summary.ventilation_setup) / 100::numeric, 2) AS ventilation_setup,
  dbm_hen.hen_name,
  dbm_manual.hen_weight AS hen_weight_man,
  round((dbm_manual.egg_weight / 10)::numeric, 1) AS egg_weight_man,
  round((dbm_manual.egg_rate / 10)::numeric, 1) AS egg_rate_man,
  round((dbm_manual.egg_vol / 10)::numeric, 1) AS egg_vol_man
FROM dbm_lot
    JOIN dbt_batch ON dbt_batch.lot_id = dbm_lot.lot_id
    JOIN dbt_summary ON dbt_summary.batch_id = dbt_batch.batch_id
    JOIN dbm_house ON dbm_lot.house_id = dbm_house.house_id
    JOIN dbm_manual ON dbt_batch.hen_id = dbm_manual.hen_id AND dbt_summary.week_periods = dbm_manual.week_periods
    JOIN dbm_hen ON dbt_batch.hen_id = dbm_hen.hen_id
GROUP BY dbt_summary.summary_date, dbt_summary.week_periods, dbm_lot.lot_id, dbm_lot.lot_name, dbm_house.house_id, dbm_house.house_name, dbt_batch.initial_count, dbm_hen.hen_name, dbm_manual.hen_weight, dbm_manual.egg_weight, dbm_manual.egg_rate, dbm_manual.egg_vol;

-- ----------------------------
-- View structure for dbv_egg
-- ----------------------------
CREATE OR REPLACE VIEW public.dbv_egg
AS
SELECT
  dbt_summary.summary_date AS egg_date,
  dbm_lot.lot_id,
  dbm_lot.lot_name,
  dbm_egg.megg_id,
  dbm_egg.cage_row,
  dbm_egg.cage_tier,
  dbt_egg.egg_count,
  dbt_summary.curr_count,
  CASE WHEN dbt_summary.curr_count > 0 AND dbm_lot.cage_num > 0 THEN floor((dbt_egg.egg_count / (dbt_summary.curr_count / dbm_lot.cage_num) / 100)::double precision * (10::double precision ^ 2::double precision) + 0.5::double precision) / (10::double precision ^ 2::double precision) ELSE 0 END AS egg_rate_cur
FROM dbm_lot
  JOIN dbt_batch ON dbt_batch.lot_id = dbm_lot.lot_id
  JOIN dbt_summary ON dbt_summary.batch_id = dbt_batch.batch_id
  JOIN dbm_house ON dbm_lot.house_id = dbm_house.house_id
  JOIN dbt_egg ON dbt_summary.collect_id = dbt_egg.collect_id
  JOIN dbm_egg ON dbm_egg.megg_id = dbt_egg.megg_id;
