#!/bin/bash

# Load values for variables from the environment file
SCRIPT_DIR=$(dirname "$(realpath "$0")")
source "$SCRIPT_DIR/.env"

# Environment variables for database connection, log file, etc.
APP_DIR="${APP_DIR:-~/apps/ovalgrowth/runtime/og-collector/cloudserver}"
TMP_DIR="${TMP_DIR:-$APP_DIR/log/tmp}"

DB_SERVER="${DB_SERVER:-127.0.0.1}"
DB_PORT="${DB_PORT:-35432}"
DB_NAME="${DB_NAME:-og2_stag}"
DB_USER="${DB_USER:-postgres}"
DB_PASSWORD="${DB_PASSWORD:-postgres}"
DB_TABLE_DATA="${DB_TABLE:-dbt_json}"
DB_TABLE_ERROR="${DB_TABLE:-dbt_error}"

LOG_FILE="${DC_LOG_FILE:-cleanup_data.log}"
APPRISE_CMD="${DC_APPRISE_CMD:-apprise -t 'OGC-CloudServer Data Cleanup Failure' -b}"
DATA_RETENTION_DAYS="${DATA_RETENTION_DAYS:-5}"

# Function to log messages
log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $1" >> ${TMP_DIR}/${LOG_FILE}
}

# Function to send failure notifications
send_notification() {
    local message="$1"
#    $APPRISE_CMD "$message"
}

# Function to clean up expired data in the database
cleanup_jsonb() {
    if ! ( export PGPASSWORD="$DB_PASSWORD"; psql -h "$DB_SERVER" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" <<EOF
    BEGIN;
    DELETE FROM $DB_TABLE_DATA WHERE created_at < NOW() - INTERVAL '"$DATA_RETENTION_DAYS" days';
    DELETE FROM $DB_TABLE_ERROR WHERE created_at < NOW() - INTERVAL '"$DATA_RETENTION_DAYS" days';
    COMMIT;
    VACUUM FULL $DB_TABLE_DATA;
    VACUUM FULL $DB_TABLE_ERROR;
EOF
    ); then
        log "Error during cleanup data. Rolling back."
        send_notification "Error during cleanup data process. Transaction rolled back."
        return 1
    fi

    log "Expired data cleaned up successfully."
}

# Function to clean up lock file and handle script exit
cleanup() {
    if [ -f "$LOCK_FILE" ]; then
        rm -f "$LOCK_FILE"
        log "Lock file removed."
    fi
}

# Set up traps for handling script interruptions
trap 'cleanup; exit 1' INT TERM ERR  # Handle signals and errors
trap 'cleanup; exit 0' EXIT          # Ensure cleanup on normal exit

mkdir -p "$TMP_DIR"
LOCK_FILE="$TMP_DIR/$(basename "$0").lock"

# Acquire lock to prevent multiple instances (using flock)
exec 200>"$LOCK_FILE"
flock -n 200 || {
    log "Script is already running. Exiting."
    exit 0
}

# Log the start of the script
log "Starting cleanup process."

# Execute the cleanup operation
if ! cleanup_jsonb; then
    log "Cleanup failed."
    send_notification "Cleanup process failed. Check logs for details."
    exit 1
fi

# Log the completion of the script
log "Cleanup process completed successfully."

# Release lock (handled by flock when the script exits)
exit 0
