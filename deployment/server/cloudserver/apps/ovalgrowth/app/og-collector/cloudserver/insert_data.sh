#!/bin/bash

# Load values for variables from the environment file
SCRIPT_DIR=$(dirname "$(realpath "$0")")
source "$SCRIPT_DIR/.env"

TMP_DIR="${TMP_DIR:-$APP_DIR/log/tmp}"
LOG_FILE="${DI_LOG_FILE:-insert_data.log}"
APPRISE_CMD="${DI_APPRISE_CMD:-apprise -t 'OGC-CloudServer Data Inserting Failure' -b}"


# Function to log messages
log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $1" >> ${TMP_DIR}/${LOG_FILE}
}

# Function to send failure notifications
send_notification() {
    local message="$1"
#    $APPRISE_CMD "$message"
}

log "Start inserting data."

podman exec og-backend sh -c ". /env/bin/activate && python3 manage.py collect"

log "End inserting data."
#send_notification "Error during inserting data process."

