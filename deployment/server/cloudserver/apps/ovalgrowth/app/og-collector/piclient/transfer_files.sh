#!/bin/bash

# Load values for variables from the environment file
SCRIPT_DIR=$(dirname "$(realpath "$0")")
source "$SCRIPT_DIR/.env"

set -e  # Exit immediately on error

# Environment variables or set defaults
AIO="${AIO:-true}"
APP_DIR="${APP_DIR:-~/apps/ovalgrowth/runtime/og-collector/piclient}"
INBOX_DIR="${INBOX_DIR:-$APP_DIR/file/inbox}"
OUTBOX_DIR="${OUTBOX_DIR:-$APP_DIR/file/outbox}"
ERROR_DIR="${ERROR_DIR:-$APP_DIR/file/error}"
TMP_DIR="${TMP_DIR:-$APP_DIR/log/tmp}"

LOCAL_PISERVER_DIR="${LOCAL_PISERVER_DIR:-~/apps/ovalgrowth/runtime/og-collector/piserver/file/inbox}"

LOG_FILE="${FT_LOG_FILE:-transfer_files.log}"
APPRISE_CMD="${FT_APPRISE_CMD:-apprise -t 'OGC-PiClient File Transferring Failure' -b}"

# FTP credentials
FTP_SERVER="${FTP_SERVER:-piserver}"
FTP_CREDENTIAL="${FTP_CREDENTIAL:-transporter,1q2w3e4r}"


# Function to log messages
log() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1" >> ${TMP_DIR}/${LOG_FILE}
}

send_failure_notification() {
    local message="$1"
#    $APPRISE_CMD "$message"
}

cleanup() {
    log "Cleanup completed."
}

# Handle cleanup on exit and error
trap cleanup EXIT SIGHUP SIGINT SIGTERM ERR

## Notes: Use a .pif file and flock to make sure there is only one instance of this script running
mkdir -p "$TMP_DIR"
PIDFILE="$TMP_DIR/$(basename "$0").pid"
LOCK_FILE="$TMP_DIR/$(basename "$0").lock"

# Create a lock using flock
(
    flock -n 200 || { log "Another transfer is in progress, exiting"; exit 0; }

    if [ -e "$PIDFILE" ] && kill -0 "$(cat "$PIDFILE")"; then
        log "Another instance of the script is running, exiting."
        exit 1
    fi

    # Create the PID file
    echo $$ > "$PIDFILE"

    # Ensure the PID file is removed when the script exits
    trap 'rm -f "$PIDFILE"' EXIT

    log "START - Starting file transfer process."

    # Check if there are files to compress
    if [ -z "$(ls -A "$OUTBOX_DIR")" ]; then
        log "No files to transfer."
        exit 0
    fi

    # Compress the moved files into a single tar.gz archive
    mkdir -p "$TMP_DIR"
    TAR_FILE="$TMP_DIR/$(date +'%Y%m%d_%H%M%S').tar.gz"

    cd "$OUTBOX_DIR"
    # Create the tar.gz archive including only .json and .err files
    if tar -czf "$TAR_FILE" --exclude='*.tmp' -C "$OUTBOX_DIR" * 2>> "$LOG_FILE"; then
        log "Tar.gz file created successfully."
    else
        rm -f "$TAR_FILE"
        log "Failed to create tar.gz file. Check error.log for details."
        exit 1  # Exit if tar creation fails
    fi

    # Check if using All-in-One (AIO) configuration
    if [ "${AIO}" == "true" ]; then
        # Check if destination directory exists
        if [ ! -d "${LOCAL_PISERVER_DIR}" ]; then
            log "Destination directory ${LOCAL_PISERVER_DIR} does not exist."
            exit 1
        fi

        # Attempt to copy the file
        if ! cp "${TAR_FILE}" "${LOCAL_PISERVER_DIR}"; then
            rm ${TMP_DIR}/*.tar.gz
            log "File transfer failed."
            send_failure_notification "File transfer failed."
            exit 1
        fi
        rm ${TMP_DIR}/*.tar.gz
    else
        # Upload the tar.gz file using lftp
        if ! lftp -u "$FTP_CREDENTIAL" "$FTP_SERVER" <<EOF
lcd "$TMP_DIR"
mput -e *.gz
bye
EOF
        then
            rm ${TMP_DIR}/*.tar.gz
            log "FTP transfer failed."
            send_failure_notification "FTP transfer failed."
            exit 1
        fi
        rm ${TMP_DIR}/*.tar.gz
    fi

    # Cleanup the Outbox directory only after successful transfer
    if ! find "${OUTBOX_DIR}/" -type f -delete; then
        log "Failed to clean up Outbox directory."
        exit 1
    else
        log "Files successfully transferred and Outbox directory cleaned up."
    fi

    log "END - File transfer process completed."

) 200>"$LOCK_FILE"
