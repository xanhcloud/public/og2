#!/bin/bash

# Enable strict error handling
set -euo pipefail

# Load values for variables from the environment file
readonly SCRIPT_DIR=$(dirname "$(realpath "$0")")
source "${SCRIPT_DIR}/.env"

# Define paths
readonly TMP_DIR="${TMP_DIR:-$APP_DIR/log/tmp}"

# Validate critical environment variables
if [[ -z "${TMP_DIR}" ]]; then
  echo "Error: Required environment variables are not set."
  exit 1
fi

# Create TMP_DIR if it doesn't exist
mkdir -p "${TMP_DIR}"

# All interval times are defined in seconds
readonly INTERVAL_TRANFER_FILES="${INTERVAL_TRANFER_FILES:-30}"
readonly INTERVAL_REMOVE_ARCHIVES="${INTERVAL_REMOVE_ARCHIVES:-84600}"

# Log file
readonly LOG_FILE="${PS_LOG_FILE:-ogc-piserver.log}"

# Notification command
readonly APPRISE_CMD="${PS_APPRISE_CMD:-apprise -t 'OGC-PiServer Failure' -b}"

# Function to log messages
log() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1" >> "${TMP_DIR}/${LOG_FILE}"
}

# Function to send failure notifications
send_failure_notification() {
    local message="$1"
#    ${APPRISE_CMD} "$message"
}

# Function to handle SIGTERM
cleanup() {
  log "Stopping wrapper script gracefully."
  exit 0
}

# Trap SIGTERM
trap cleanup SIGTERM

# Initialize a counter
counter=1

while true; do
  # Run this script every INTERVAL_TRANFER_FILES seconds
  if (( counter % ${INTERVAL_TRANFER_FILES} == 0 )); then
    log "Running transfer_files.sh..."
    "${SCRIPT_DIR}/transfer_files.sh"
    if [ $? -ne 0 ]; then
      log "Script transfer_files.sh failed with exit code $?."
      send_failure_notification "Script transfer_files.sh failed with exit code $?."
      exit 1
    fi
  fi

  # Run this script every INTERVAL_REMOVE_ARCHIVES seconds
  if (( counter % ${INTERVAL_REMOVE_ARCHIVES} == 0 )); then
    log "Running remove_archives.sh..."
    "${SCRIPT_DIR}/remove_archives.sh"
    if [ $? -ne 0 ]; then
      log "Script remove_archives.sh failed with exit code $?."
      send_failure_notification "Script remove_archives.sh failed with exit code $?."
      exit 1
    fi
  fi

  # Increment the counter and sleep for 1 second
  ((counter++))
  sleep 1

  # Reset the counter after 1 day (86400 seconds) or LCM of intervals
  if (( counter > 86400 )); then
    counter=1
  fi
done
