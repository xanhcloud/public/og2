#!/bin/bash

# Load values for variables from the environment file
SCRIPT_DIR=$(dirname "$(realpath "$0")")
source "$SCRIPT_DIR/.env"

set -e  # Exit immediately on error

# Directories
AIO="${AIO:-true}"
APP_DIR="${APP_DIR:-~/apps/ovalgrowth/runtime/og-collector/piserver}"
INBOX_DIR="${INBOX_DIR:-$APP_DIR/file/inbox}"
OUTBOX_DIR="${OUTBOX_DIR:-$APP_DIR/file/outbox}"
ARCHIVE_DIR="${ARCHIVE_DIR:-$APP_DIR/file/archive}"
ERROR_DIR="${ERROR_DIR:-$APP_DIR/file/error}"
TMP_DIR="${TMP_DIR:-$APP_DIR/log/tmp}"

LOCAL_CLOUDSERVER_DIR="${LOCAL_CLOUDSERVER_DIR:-~/apps/ovalgrowth/runtime/og-collector/cloudserver/file/inbox}"

# CloudServer: server, user and path
CLOUDSERVER_HOST="${CLOUDSERVER_HOST:-up5dev-1}"
CLOUDSERVER_USER="${CLOUDSERVER_USER:-transporter}"
CLOUDSERVER_FOLDER="${CLOUDSERVER_USER:-~/apps/ovalgrowth/runtime/og-collector/cloudserver/file/inbox}"

REMOTE_SERVER=${CLOUDSERVER_USER}@${CLOUDSERVER_HOST}:${CLOUDSERVER_FOLDER}

LOG_FILE="${FT_LOG_FILE:-transfer_files.log}"
APPRISE_CMD="${FT_APPRISE_CMD:-apprise -t 'OGC-PiServer File Transferring Failure' -b}"

BATCH_SIZE=500  # number of files per combined output file

log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $1" >> ${TMP_DIR}/${LOG_FILE}
}

send_failure_notification() {
    local message="$1"
#    $APPRISE_CMD "$message"
}

cleanup() {
    log "Cleanup completed."
}

# Handle cleanup on exit and error
trap cleanup EXIT SIGHUP SIGINT SIGTERM ERR

## Notes: Use a .pif file and flock to make sure there is only one instance of this script running
mkdir -p "$TMP_DIR"
PIDFILE="$TMP_DIR/$(basename "$0").pid"
LOCK_FILE="$TMP_DIR/$(basename "$0").lock"

# Create a lock using flock
(
    flock -n 200 || { log "Another transfer is in progress, exiting"; exit 0; }

    if [ -e "$PIDFILE" ] && kill -0 "$(cat "$PIDFILE")"; then
        log "Another instance of the script is running, exiting."
        exit 0
    fi

    # Create the PID file
    echo $$ > "$PIDFILE"

    # Ensure the PID file is removed when the script exits
    trap 'rm -f "$PIDFILE"' EXIT

    log "START - Starting file transfer process."

    ## Process the TAR files, if any
    ## -------------------------------
    # Create a temporary file to hold the current list of TAR files
    TEMP_FILE=$(mktemp)

    # Capture the list of current tar.gz files into the temp file
    find "$INBOX_DIR" -name "*.tar.gz" | sort > "$TEMP_FILE"

    if [ -s "$TEMP_FILE" ]; then
        # Read through the temp file and decompress those tar.gz files
        while read -r file; do
          if tar -xzf "$file" -C "$INBOX_DIR" 2>> ${TMP_DIR}/${LOG_FILE}; then
              log "Tar.gz file '$file' extracted successfully."
              rm "$file"  # Remove the tar.gz file after successful extraction
          else
              log "Failed to extract tar.gz file '$file'. Check log for details."
              # Optionally move the failed tar.gz file to an error directory
              mv "$file" "$ERROR_DIR/"
              continue  # Continue to the next file instead of exiting
          fi
        done < "$TEMP_FILE"
    fi

    # Remove the temporary file
    rm -f "$TEMP_FILE"

    ## Process JSON files
    ## -------------------------------
    # Create a temporary file to hold the current list of JSON files
    TEMP_FILE=$(mktemp)

    # Capture the list of current JSON files into the temp file
    find "$INBOX_DIR" \( -name "*.json" -o -name "*.err" \) | sort > "$TEMP_FILE"

    # If there are files in the temp file
    if [ -s "$TEMP_FILE" ]; then
        # Get the current epoch time for file naming
        current_timestamp=$(date +%s)

        # Process files in batches
        split -l "$BATCH_SIZE" -d -a 3 --additional-suffix=.list "$TEMP_FILE" "$TMP_DIR/$current_timestamp"_

        # Initialize a counter for output file naming
        batch_number=1

        for batch_file in "$TMP_DIR/$current_timestamp"_*.list; do
            # Create a batch output file starting with the epoch timestamp
            output_json_file="${current_timestamp}_batch_$(printf "%03d" $batch_number).ndjson"
            output_err_file="${current_timestamp}_batch_$(printf "%03d" $batch_number).nderr"

            # Read the batch file and concatenate unique files
            while IFS= read -r data_file; do
                if [[ "${data_file##*.}" == "json" ]]; then
                    cat "$data_file" >> "$output_json_file"
                else
                    cat "$data_file" >> "$output_err_file"
                fi
            done < "$batch_file"

            # Formalize the output files to NDJSON format
            if [[ -s "$output_json_file" ]]; then
                cat "$output_json_file" | tr -d '\n' | sed -e 's/ //g' -e 's/}{/}\n{/g' > "$TMP_DIR/$output_json_file"
            fi
            rm -f "$output_json_file"

            if [[ -s "$output_err_file" ]]; then
                cat "$output_err_file" | tr -d '\n' | sed -e 's/ //g' -e 's/}{/}\n{/g' > "$TMP_DIR/$output_err_file"
            fi
            rm -f "$output_err_file"

            log "Combined batch $batch_file into $output_json_file and $output_err_file."

            # Increment the batch counter
            batch_number=$((batch_number + 1))
        done

        # Cleanup temporary batch files
        rm "$TMP_DIR"/*.list

        # Move the NDJSON files to the designated output directory
        # Check for .ndjson files and move them if they exist
        if ls "$TMP_DIR"/*.ndjson 1>/dev/null 2>&1; then
            mv "$TMP_DIR"/*.ndjson "$OUTBOX_DIR/"
        fi

        # Check for .nderr files and move them if they exist
        if ls "$TMP_DIR"/*.nderr 1>/dev/null 2>&1; then
            mv "$TMP_DIR"/*.nderr "$OUTBOX_DIR/"
        fi

        ## Move the original data files to the Archive directory for backup
        # Read through the temp file and move the files to the archive
        while read -r file; do
            # Extract the timestamp from the filename
            timestamp=$(basename "$file" | cut -d'_' -f1)

            # Convert timestamp to date and hour for the folder structure
            date=$(date -d "@$timestamp" +"%Y%m%d")  # Format YYYYMMDD
            hour=$(date -d "@$timestamp" +"%H")      # Format HH
            minute=$(date -d "@$timestamp" +"%M")    # Format MM

            # Create the directory for the date, hour, and minute
            archive_subdir="$ARCHIVE_DIR/$date/$hour/$minute"
            mkdir -p "$archive_subdir"

            # Move the file to the appropriate date/hour folder
            mv "$file" "$archive_subdir/" 2>/dev/null
        done < "$TEMP_FILE"

        log "Processed and combined files into NDJSON."

        # Remove the temporary file
        rm "$TEMP_FILE"
    else
        # Remove the temporary file
        rm "$TEMP_FILE"

        log "END - No files to process."
        exit 0
    fi


    ## Transfer NDJSON and error files to the CloudServer
    ## -------------------------------------------------------------
    # Check if using All-in-One (AIO) configuration
    if [ "${AIO}" == "true" ]; then
        # Transfer files to other location using mv
        # Check if destination directory exists
        if [ ! -d "${LOCAL_CLOUDSERVER_DIR}" ]; then
            log "Destination directory ${LOCAL_CLOUDSERVER_DIR} does not exist."
            exit 1
        fi

        # Attempt to copy the file, if any
        if ! mv $OUTBOX_DIR/* ${LOCAL_CLOUDSERVER_DIR}; then
            log "Failed to transfer files to other location."
            exit 1
        else
            log "Transferred files to other location successfully"
        fi
    else

        # Transfer files to remote server using rsync
        # Remove local files after successful transfer
        /usr/bin/rsync -avz --remove-source-files --omit-dir-times --no-perms \
           -e "/usr/bin/ssh -i ~/.ssh/piserver2cloud -o UserKnownHostsFile=~/.ssh/known_hosts" \
           "$OUTBOX_DIR/" "$REMOTE_SERVER" 1>/dev/null 2>> ${TMP_DIR}/${LOG_FILE}

        # Check if rsync was successful
        if [ $? -eq 0 ]; then
            log "Transferred files to remote server successfully."
        else
            log "Failed to transfer files to remote server."
            send_failure_notification  "Failed to transfer files to remote server."
            exit 1
        fi
    fi

    log "END - File transfer process completed."

) 200>"$LOCK_FILE"

