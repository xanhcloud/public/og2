#!/bin/bash

SCRIPT_DIR=$(dirname "$(realpath "$0")")
STATUS_FILE=${SCRIPT_DIR}/.ovalgrowth_status

# Usage function to display help
usage() {
  echo "Usage: $0 {init|start|stop|status}"
  exit 1
}

# Check if a parameter was provided
if [ -z "$1" ]; then
  echo "Error: No action specified."
  usage
fi

# Define action based on input parameter
action=$1

# Processing blocks for each action
case "$action" in
  init)
    echo "Initializing ovalgrowth service..."

    ## Initialize Podman environment
    # Network
    if ! ( podman network ls --format "{{.Name}}" | grep -qw npm_proxy ); then
      podman network create npm_proxy
    fi

    # Secret
    podman secret rm og_postgres_password og_redis_password

    echo `(cat ${SCRIPT_DIR}/og-app/secret/og_postgres_password)` | podman secret create og_postgres_password -
    echo `(cat ${SCRIPT_DIR}/og-app/secret/og_redis_password)` | podman secret create og_redis_password -

    # Read the GitLab Deploy Token from the secret file
    GITLAB_TOKEN=$(cat ${SCRIPT_DIR}/og-app/secret/gitlab_token)

    # Login to GitLab container registry using the token
    echo -n "$GITLAB_TOKEN" | podman login -u "gitlab+deploy-token-7010228" --password-stdin registry.gitlab.com

    # Enable the OG-Collector timers (scripts)
    systemctl --user enable process_files.timer
    systemctl --user enable insert_data.timer
    systemctl --user enable remove_archives.timer

    echo "Initialization completed."

    # Create the ovalgrowth_status file
    echo "stopped" > "$STATUS_FILE"

    # Start the OvalGrowth system
    systemctl --user enable ovalgrowth.service
    systemctl --user start ovalgrowth.service
    ;;

  start)
#    if [[ -f "$STATUS_FILE" && $(cat "$STATUS_FILE") == "started" ]]; then
#      echo "ovalgrowth service is already started"
#      exit 0
#    fi

    if podman network ls --format "{{.Name}}" | grep -qw npm_proxy; then
      echo "Starting ovalgrowth service..."
    else
      echo "Initilization required, run init instead."
      usage
    fi


    # Start the OG-App services (containers)
    podman-compose -f ${SCRIPT_DIR}/og-app/compose/ovalgrowth.yml up -d --pull-always || exit 1
    podman-compose -f ${SCRIPT_DIR}/og-app/compose/og-support.yml up -d --pull-always || exit 1

    # Start the OG-Collector timers (scripts)
    systemctl --user start process_files.timer
    systemctl --user start insert_data.timer
    systemctl --user start remove_archives.timer
    echo "started" > "$STATUS_FILE"

    echo "Service ovalgrowth started successfully."
    ;;

  stop)
 #   if [[ -f "$STATUS_FILE" && $(cat "$STATUS_FILE") == "stopped" ]]; then
 #     echo "ovalgrowth service is already stopped"
 #     exit 0
 #   fi

    echo "Stopping ovalgrowth service..."

    # Stop the OG-App services (containers)
    podman-compose -f ${SCRIPT_DIR}/og-app/compose/ovalgrowth.yml down || exit 1
    podman-compose -f ${SCRIPT_DIR}/og-app/compose/og-support.yml down || exit 1

    # Stop the OG-Collector services (scripts)
    systemctl --user stop process_files.timer
    systemctl --user stop insert_data.timer
    systemctl --user stop remove_archives.timer
    echo "stopped" > "$STATUS_FILE"

    echo "Service ovalgrowth stopped."
    ;;

  status)
    if [[ -f "$STATUS_FILE" ]]; then
      cat "$STATUS_FILE"
    else
      echo "unknown"
    fi
    ;;

  *)
    echo "Error: Invalid action '$action'."
    usage
    ;;
esac

