#!/bin/bash

SCRIPT_DIR=$(dirname "$(realpath "$0")")

# Usage function to display help
usage() {
  echo "Usage: $0 {init|status|start|restart|stop}"
  exit 1
}

# Check if a parameter was provided
if [ -z "$1" ]; then
  echo "Error: No action specified."
  usage
fi

# Define action based on input parameter
action=$1

# Define component based on input parameter
component=$2

# Processing blocks for each action
case "$action" in
  init)
    $SCRIPT_DIR/app/og-services.sh init
    ;;

  status)
    $SCRIPT_DIR/app/og-services.sh status
    ;;

  start)
    if ! (podman network ls --format "{{.Name}}" | grep -qw npm_proxy ); then
      echo "Need initilization first. Run init."
      usage
    fi

    # Start the OvalGrowth system
    if systemctl --user is-active --quiet ovalgrowth.service; then
      echo "OvalGrowth service is already running."
    else
      systemctl --user start ovalgrowth.service
    fi
    ;;

  stop)
    # Stop the OvalGrowth system
    if systemctl --user is-active --quiet ovalgrowth.service; then
      systemctl --user stop ovalgrowth.service
    else
      echo "OvalGrowth service is not running."
    fi
    ;;

  restart)
    $SCRIPT_DIR/app/og-services.sh stop
    $SCRIPT_DIR/app/og-services.sh start
    ;;

  *)
    echo "Error: Invalid action '$action'."
    usage
    ;;
esac
