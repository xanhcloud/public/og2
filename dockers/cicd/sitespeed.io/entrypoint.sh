#!/bin/bash
set -e

# Start Xvfb
Xvfb :99 -screen 0 1280x1024x24 &
export DISPLAY=:99

# Run Sitespeed.io with the provided arguments
sitespeed.io "$@"
