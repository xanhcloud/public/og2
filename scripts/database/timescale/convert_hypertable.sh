#!/bin/bash

# Load environment variables from .env file
export $(grep -v '^#' .env | xargs)

# Convert TABLES string to an array
IFS=',' read -r -a TABLES_ARRAY <<< "$TABLES"

LOG_FILE="$BACKUP_DIR/migration_$(date +'%Y%m%d_%H%M%S').log"

# Enable error handling
set -e

# Function to log messages
log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') $1" | tee -a "$LOG_FILE"
}

# Trap errors and exit gracefully
trap 'log "❌ Error occurred at line $LINENO. Check log file: $LOG_FILE"; exit 1' ERR

log "🔹 Starting TimescaleDB migration..."

# 1. Export views and constraints
log "🔹 Exporting views and constraints..."
pg_dump -h "$DB_HOST" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" --schema-only --table="${TABLES_ARRAY[@]}" --section=pre-data > "$BACKUP_DIR/views_and_constraints.sql"

# Verify backup success
if [[ ! -s "$BACKUP_DIR/views_and_constraints.sql" ]]; then
    log "❌ Failed to export views and constraints!"
    exit 1
fi
log "✅ Views and constraints exported successfully."

# 2. Start Migration
log "🔹 Starting migration process..."

psql -h "$DB_HOST" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" <<EOF | tee -a "$LOG_FILE"
DO \$\$
DECLARE
    view_name TEXT;
    constraint_name TEXT;
    table_name TEXT;
    dependent_table_name TEXT;
    migrated_tables TEXT[] := ARRAY['${TABLES_ARRAY[@]}'];
BEGIN
    -- Step 1: Drop dependent views
    RAISE NOTICE '🔹 Dropping dependent views...';
    FOR view_name IN
        SELECT relname
        FROM pg_class c
        JOIN pg_namespace n ON n.oid = c.relnamespace
        WHERE c.relkind = 'v' -- View type
          AND n.nspname = 'public'
          AND EXISTS (
              SELECT 1
              FROM pg_depend d
              WHERE d.refobjid = ANY (ARRAY(SELECT oid FROM pg_class WHERE relname = ANY (migrated_tables)))
              AND d.objid = c.oid
          )
    LOOP
        EXECUTE format('DROP VIEW IF EXISTS %I CASCADE;', view_name);
        RAISE NOTICE '✅ Dropped view: %', view_name;
    END LOOP;

    -- Step 2: Drop dependent foreign key constraints
    RAISE NOTICE '🔹 Dropping dependent foreign keys...';
    FOR constraint_name, dependent_table_name IN
        SELECT con.conname, rel_t.relname
        FROM pg_constraint con
        JOIN pg_class rel_t ON con.conrelid = rel_t.oid
        JOIN pg_namespace nsp ON nsp.oid = rel_t.relnamespace
        WHERE nsp.nspname = 'public'
          AND con.contype = 'f'
          AND con.confrelid = ANY (ARRAY(SELECT oid FROM pg_class WHERE relname = ANY (migrated_tables)))
    LOOP
        EXECUTE format('ALTER TABLE %I DROP CONSTRAINT IF EXISTS %I;', dependent_table_name, constraint_name);
        RAISE NOTICE '✅ Dropped foreign key: % on table %', constraint_name, dependent_table_name;
    END LOOP;

    -- Step 3: Drop target tables
    RAISE NOTICE '🔹 Dropping target tables...';
    FOR table_name IN
        SELECT relname
        FROM pg_class c
        JOIN pg_namespace n ON n.oid = c.relnamespace
        WHERE n.nspname = 'public'
          AND c.relname = ANY (migrated_tables)
    LOOP
        EXECUTE format('DROP TABLE IF EXISTS %I CASCADE;', table_name);
        RAISE NOTICE '✅ Dropped table: %', table_name;
    END LOOP;

    -- Step 4: Create and convert new tables to hypertables
    RAISE NOTICE '🔹 Creating new hypertables...';
    FOR table_name IN SELECT unnest(migrated_tables)
    LOOP
        EXECUTE format('CREATE TABLE %I_new (LIKE %I INCLUDING ALL);', table_name, table_name);
        EXECUTE format('SELECT create_hypertable(''%I_new'', ''created_tdate'');', table_name);
    END LOOP;

    -- Step 5: Copy data to new tables
    RAISE NOTICE '🔹 Copying data to new tables...';
    FOR table_name IN SELECT unnest(migrated_tables)
    LOOP
        EXECUTE format('INSERT INTO %I_new SELECT * FROM %I;', table_name, table_name);
    END LOOP;

    -- Step 6: Rename new tables to original names
    RAISE NOTICE '🔹 Renaming new tables...';
    FOR table_name IN SELECT unnest(migrated_tables)
    LOOP
        EXECUTE format('ALTER TABLE %I_new RENAME TO %I;', table_name, table_name);
    END LOOP;

    -- Step 7: Generate Triggers to Simulate Foreign Keys
    RAISE NOTICE '🔹 Creating triggers to simulate foreign keys...';
    FOR constraint_name, table_name, column_name, referenced_table, referenced_column IN
        SELECT con.conname, conrelid::regclass, a.attname, confrelid::regclass, af.attname
        FROM pg_constraint con
        JOIN pg_attribute a ON a.attnum = ANY(conkey) AND a.attrelid = conrelid
        JOIN pg_attribute af ON af.attnum = ANY(confkey) AND af.attrelid = confrelid
        WHERE contype = 'f'
          AND conrelid::regclass::text = ANY (migrated_tables)
    LOOP
        EXECUTE format(
            'CREATE OR REPLACE FUNCTION enforce_fk_%I() RETURNS TRIGGER AS $$
            BEGIN
                IF NOT EXISTS (SELECT 1 FROM %I WHERE %I = NEW.%I) THEN
                    RAISE EXCEPTION ''Invalid %I: %s'', NEW.%I;
                END IF;
                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;',
            table_name || '_' || column_name || '_fk',
            referenced_table,
            referenced_column,
            column_name,
            column_name, '%', column_name
        );

        EXECUTE format(
            'CREATE TRIGGER trg_enforce_fk_%I BEFORE INSERT OR UPDATE ON %I 
            FOR EACH ROW EXECUTE FUNCTION enforce_fk_%I();',
            table_name || '_' || column_name || '_fk',
            table_name,
            table_name || '_' || column_name || '_fk'
        );
    END LOOP;

    RAISE NOTICE '✅ Migration complete.';
END $$;
EOF

log "🔹 Re-importing views and constraints..."
psql -h "$DB_HOST" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" < "$BACKUP_DIR/views_and_constraints.sql"

log "✅ Migration process completed successfully!"
