# OvalGrowth End-to-End Testing

## Description
This is the End-to-End automation testing for OvalGrowth using:
- Playwright
	- Headless browsers: Chrome, Edge, WebKit (Safari) 

## Folder structure
This folder contains the files for testing: fixture, pages, scripts

