// loginPage.ts

import { Page, expect } from '@playwright/test';

export class LoginPage {
  readonly page: Page;
  readonly usernameInput = 'input[placeholder="Enter your username"]';
  readonly passwordInput = 'input[placeholder="6+ Characters, 1 Capital letter"]';
  readonly loginButton = 'input[type="submit"][value="Sign In"]';

  constructor(page: Page) {
    this.page = page;
  }

  async navigate(url: string) {
    await this.page.goto(url);
  }

  async login(username: string, password: string) {
    await this.page.fill(this.usernameInput, username);
    await this.page.fill(this.passwordInput, password);
    await this.page.click(this.loginButton);
  }

  async handleAlert() {
    const dialog = await this.page.waitForEvent('dialog');
    const message = dialog.message();
    await dialog.accept();
    return message;
  }

  async assertErrorMessage(expectedMessage: string) {
    const message = await this.handleAlert();
    expect(message).toBe(expectedMessage);
  }
}
