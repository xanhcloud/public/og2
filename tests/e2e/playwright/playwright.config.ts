// playwright.config.ts
import { defineConfig, devices } from '@playwright/test';

export default defineConfig({
  testDir: './scripts',
  timeout: 60000,
  retries: 2,
  use: {
    headless: true,
    trace: 'on-first-retry',
  },
  projects: [
    {
      name: 'Desktop Chrome',
      use: { ...devices['Desktop Chrome'] },
    },
    {
      name: 'Desktop Firefox',
      use: { ...devices['Desktop Firefox'] },
    },
    {
      name: 'Desktop Safari',
      use: { ...devices['Desktop Safari'] },
    },
    {
      name: 'Tablet Landscape',
      use: {
        browserName: 'chromium',
        viewport: { width: 1024, height: 768 },
        userAgent: 'Mozilla/5.0 (Linux; Android 9; SM-T865 Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.136 Safari/537.36',
      },
    },
    {
      name: 'Tablet Portrait',
      use: {
        browserName: 'chromium',
        viewport: { width: 768, height: 1024 },
        userAgent: 'Mozilla/5.0 (Linux; Android 9; SM-T865 Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.136 Safari/537.36',
      },
    },
    {
      name: 'Smartphone Chrome',
      use: {
        browserName: 'chromium',
        viewport: { width: 375, height: 667 },
        userAgent: 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15A5341f Safari/604.1',
      },
    },
    {
      name: 'Smartphone Firefox',
      use: {
        browserName: 'firefox',
        viewport: { width: 375, height: 667 },
        userAgent: 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X; rv:12.0) Gecko/12.0 Firefox/68.0',
      },
    },
    {
      name: 'Smartphone Safari',
      use: {
        browserName: 'webkit',
        viewport: { width: 375, height: 667 },
        userAgent: 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15A5341f Safari/604.1',
      },
    },
  ],
  reporter: [
    ['list'],
    ['html', { open: 'never' }]
  ],
});

