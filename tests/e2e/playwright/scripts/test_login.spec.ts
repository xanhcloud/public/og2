import { test, expect, chromium, Browser, Page } from '@playwright/test';
import { LoginPage } from '../pages/loginPage';

test.describe('Login tests', () => {
  let browser: Browser;
  let page: Page;
  let loginPage: LoginPage;

  test.beforeAll(async () => {
    browser = await chromium.launch();
    console.log('Browser launched');
  });

  test.afterAll(async () => {
    await browser.close();
    console.log('Browser closed');
  });

  test.beforeEach(async () => {
    page = await browser.newPage();
    loginPage = new LoginPage(page);
    await loginPage.navigate('https://og2.saigondeveloper.com/auth/signin');
    console.log('Navigated to login page');
  });

  test.afterEach(async () => {
    await page.close();
    console.log('Page closed');
  });

  const cases = [
    { username: 'admin', password: 'admin', expectedMessage: null },
    { username: '', password: 'your_password', expectedMessage: 'Invalid username or password' },
    { username: 'nhienkrajak', password: '', expectedMessage: 'Invalid username or password' },
    { username: 'wrong_username', password: 'your_password', expectedMessage: 'Invalid username or password' },
    { username: 'nhienkrajak', password: 'wrong_password', expectedMessage: 'Invalid username or password' },
    { username: '<script>alert(\'XSS\')</script>', password: 'your_password', expectedMessage: 'Invalid username or password' },
    { username: 'admin\' OR 1=1; --', password: 'password', expectedMessage: 'Invalid username or password' },
    { username: '', password: '', expectedMessage: 'Invalid username or password' },
    { username: '!@#$%^&*()', password: 'password', expectedMessage: 'Invalid username or password' },
    { username: ' nhienkrajak ', password: ' your_password ', expectedMessage: 'Invalid username or password' },
    { username: 'a'.repeat(500), password: 'a'.repeat(500), expectedMessage: 'Invalid username or password' },
    { username: 'Nhienkrajak', password: 'Your_Password', expectedMessage: 'Invalid username or password' }
  ];

  cases.forEach(({ username, password, expectedMessage }) => {
    test(`Login test with username: "${username}", password: "${password}"`, async () => {
      console.log(`Testing with username: "${username}", password: "${password}"`);
      await loginPage.login(username, password);
      console.log('Action');

      if (expectedMessage) {
        await loginPage.assertErrorMessage(expectedMessage);
        console.log('Login NG');
      } else {
        expect(page.url()).toBe('https://og2.saigondeveloper.com/auth/signin');
        console.log('Login OK');
      }
    });
  });
});
