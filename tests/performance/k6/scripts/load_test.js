import http from 'k6/http';
import { check, sleep } from 'k6';
import { Counter } from 'k6/metrics';

const host = 'https://og2.saigondeveloper.com';
const token = 'd51ab0aec09294d188fb894954534ddbafc67f2f';
const authorizationHeader = { headers: { 'Authorization': `Token ${token}` } };
const contentTypeHeader = { headers: { 'Content-Type': 'application/json' } };

const errorCount = new Counter('errors');

export const options = {
  vus: 10, // number of virtual users
  duration: '1m', // test duration
  thresholds: {
    errors: ['count<10'], // allow less than 10 errors
    http_req_duration: ['p(95)<500'], // 95% of requests should be below 500ms
  },
};

export default function () {
  // Token generation
  let res = http.post(`${host}/api/auth/token`, JSON.stringify({ username: 'admin', password: 'admin' }), contentTypeHeader);
//  check(res, {
//    'token generated': (r) => r.status === 200,
//  }) || errorCount.add(1);

  // Get user profile
  res = http.get(`${host}/api/auth/user`, authorizationHeader);
  check(res, {
    'profile retrieved': (r) => r.status === 200,
  }) || errorCount.add(1);

  // Get users
  res = http.get(`${host}/api/users`, authorizationHeader);
  check(res, {
    'users retrieved': (r) => r.status === 200,
  }) || errorCount.add(1);

  // Get user by ID
  res = http.get(`${host}/api/users/1`, authorizationHeader);
  check(res, {
    'user retrieved': (r) => r.status === 200,
  }) || errorCount.add(1);

  // Get dashboard
  res = http.get(`${host}/api/dashboard`, authorizationHeader);
  check(res, {
    'dashboard retrieved': (r) => r.status === 200,
  }) || errorCount.add(1);

  // Get daily table
  res = http.get(`${host}/api/dailytable/1`, authorizationHeader);
  check(res, {
    'daily table retrieved': (r) => r.status === 200,
  }) || errorCount.add(1);

  // Get setup daily table
  res = http.get(`${host}/api/setup-dailytable`, authorizationHeader);
  check(res, {
    'setup daily table retrieved': (r) => r.status === 200,
  }) || errorCount.add(1);

  // Past Data Comparison
  res = http.get(`${host}/api/d-comparison/1`, authorizationHeader);
  check(res, {
    'data comparison retrieved': (r) => r.status === 200,
  }) || errorCount.add(1);

  // Get d_spec_after
  res = http.get(`${host}/api/d-spec-after/1`, authorizationHeader);
  check(res, {
    'd_spec_after retrieved': (r) => r.status === 200,
  }) || errorCount.add(1);

  sleep(1); // sleep for 1 second between iterations
}
