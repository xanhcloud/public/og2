from locust import HttpUser, TaskSet, task, between

class UserBehavior(TaskSet):
    token = None

    def on_start(self):
        self.login()

    @task(1)
    def login(self):
        # Navigate to the login page to get any necessary cookies or tokens
        self.client.get("/auth/signin")

        # Correct the endpoint and ensure it's a POST request
        response = self.client.post("/api/auth/token", json={"username": "admin", "password": "admin"})

        if response.status_code == 200:
            self.token = response.json().get("token")
            print(f"Login successful, token: {self.token}")
        else:
            print(f"Failed to login: {response.status_code} {response.text}")

    @task(2)
    def get_profile(self):
        if self.token:
            self.client.get("/api/auth/user", headers={"Authorization": f"Token {self.token}"})
        else:
            print("No token found, cannot fetch profile.")

    @task(3)
    def get_users(self):
        if self.token:
            self.client.get("/api/users", headers={"Authorization": f"Token {self.token}"})
        else:
            print("No token found, cannot fetch profile.")

    @task(4)
    def get_user(self):
        if self.token:
            self.client.get("/api/users/1", headers={"Authorization": f"Token {self.token}"})
        else:
            print("No token found, cannot fetch profile.")

    @task(5)
    def get_dashboard(self):
        if self.token:
            self.client.get("/api/dashboard", headers={"Authorization": f"Token {self.token}"})
        else:
            print("No token found, cannot fetch profile.")

    @task(6)
    def get_daily_table(self):
        if self.token:
            self.client.get("/api/dailytable/1", headers={"Authorization": f"Token {self.token}"})
        else:
            print("No token found, cannot fetch profile.")

    @task(7)
    def get_setup_daily_table(self):
        if self.token:
            self.client.get("/api/setup-dailytable", headers={"Authorization": f"Token {self.token}"})
        else:
            print("No token found, cannot fetch profile.")

    @task(8)
    def past_data_comparison(self):
        if self.token:
            self.client.get("/api/d-comparison/1", headers={"Authorization": f"Token {self.token}"})
        else:
            print("No token found, cannot fetch profile.")

    @task(9)
    def get_d_spec_after(self):
        if self.token:
            self.client.get("/api/d-spec-after/1", headers={"Authorization": f"Token {self.token}"})
        else:
            print("No token found, cannot fetch profile.")

class WebsiteUser(HttpUser):
    tasks = [UserBehavior]
    wait_time = between(1, 5)
    host = "https://og2.saigondeveloper.com"

#if __name__ == "__main__":
#    import os
#    os.system("locust -f ./tests/prformance/locust/scripts/load_test.py --host=https://og2.saigondeveloper.com")
